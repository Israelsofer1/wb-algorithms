from pyspark.sql import SparkSession, SQLContext
from datetime import datetime
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, IntegerType, StringType
import re
import string
from pyspark.sql.utils import AnalysisException
from pyspark.sql.functions import broadcast
from Project.Common.logging_config import configure_logger
from ..constant import const

# ---------------------------------------------------------------------------

logger = configure_logger(__name__, "DEBUG")


def prepare_products(spark, path):
    """
    Read products to match and enrich them.
    :param spark: spark session
    :param path: path to products
    :return: products_df - enriched dataframe of products to match
    """

    logger.info('Prepare products started')

    products_df = spark.read.format("csv").option("header", "true").load(path + const.INPUT_FOLDER)
    products_df = products_df.withColumnRenamed("brand_id", "nn_brand_id")
    products_df = products_df.withColumnRenamed("serving_type_id", "nn_serving_type_id")
    products_df = products_df.withColumnRenamed("volume_id", "nn_volume")
    products_df = products_df.withColumnRenamed("country_id", "country")
    pos_bars_products_df = spark.sql("""
        select bp.product_id as id, bp.bar_id as bar_id_1, bp.pos_code as pos_code, bp.pos_product_volume as pos_product_volume
        from stg.pos_products_new_flat bp
        """)

    bars_df = spark.sql("""
    select b.id, b.state, bm.`value` as delivery 
    from stg.bars b 
    LEFT JOIN (SELECT bar_id,`value` FROM stg.bars_metadata WHERE `key` = 'pos_qc_approved') bm ON b.id = bm.bar_id 
    """)
    ka_df = spark.sql("""select bar_id, min(brewery_id) AS key_account_id from stg.breweries_bars bb JOIN
                        stg.breweries b ON bb.brewery_id = b.id WHERE b.brewery_type = 2 GROUP BY bb.bar_id""")
    pos_df = spark.sql("select bar_id as bar_id_2, pos_vendor from stg.pos_bars_config WHERE active")
    products_df = products_df.join(pos_bars_products_df, products_df['bar_product_id'] == pos_bars_products_df['id'])
    products_df = products_df.join(broadcast(bars_df), products_df['bar_id_1'] == bars_df['id'])
    products_df = products_df.join(broadcast(pos_df), products_df['bar_id_1'] == pos_df['bar_id_2'])
    products_df = products_df.join(broadcast(ka_df), products_df['bar_id_1'] == ka_df['bar_id'], how='left')
    products_df = products_df.drop('bar_id', 'bar_id_2', 'id')
    products_df = products_df.withColumnRenamed("bar_id_1", "bar_id")
    products_df = products_df.withColumn('temp', func.lower(func.col('title')))
    products_df = products_df.drop('title')
    products_df = products_df.withColumnRenamed("temp", "title")
    products_df = products_df.withColumn('temp_volume', func.col('pos_product_volume').cast(FloatType())).drop(
        func.col('pos_product_volume'))
    products_df = products_df.withColumnRenamed('temp_volume', 'pos_product_volume')
    products_df = products_df.fillna('0')
    products_df = products_df.fillna(0)

    logger.info('Prepare products ended')

    return products_df


def pull_rules_s3(spark, path):
    """ Pull rules from S3 and set the schema.
    :param spark: spark session
    :param path: path to rules
    :return: rules_df - rules dataframe
    """

    logger.info('Pull rules from s3 started')

    rules_df = spark.read.format("csv").option("header", "true").load(path)
    rules_df = rules_df.withColumn("rule_id", rules_df["rule_id"].cast(IntegerType()))
    rules_df = rules_df.withColumn("pos_vendor", rules_df["pos_vendor"].cast(IntegerType()))
    rules_df = rules_df.withColumn("key_account_id", rules_df["key_account_id"].cast(IntegerType()))
    rules_df = rules_df.withColumn("price_over", rules_df["price_over"].cast(FloatType()))
    rules_df = rules_df.withColumn("price_under", rules_df["price_under"].cast(FloatType()))
    rules_df = rules_df.withColumn("brand_id", rules_df["brand_id"].cast(IntegerType()))
    rules_df = rules_df.withColumn("serving_type_id", rules_df["serving_type_id"].cast(IntegerType()))
    rules_df = rules_df.withColumn("answer", rules_df["answer"].cast(FloatType()))
    rules_df = rules_df.withColumn("value", rules_df["value"].cast(FloatType()))
    rules_df = rules_df.withColumn("priority", rules_df["priority"].cast(IntegerType()))

    logger.info('Pull rules from s3 ended')

    return rules_df


def pull_unknown_rules_s3(spark, path):

    logger.info('Pull rules for unknown brands from s3 started')

    unk_rules_df = spark.read.format("csv").option("header", "true").load(path)
    unk_rules_df = unk_rules_df.withColumn("rule_id", unk_rules_df["rule_id"].cast(IntegerType()))
    unk_rules_df = unk_rules_df.withColumn("pos_vendor", unk_rules_df["pos_vendor"].cast(IntegerType()))
    unk_rules_df = unk_rules_df.withColumn("key_account_id", unk_rules_df["key_account_id"].cast(IntegerType()))
    unk_rules_df = unk_rules_df.withColumn("answer", unk_rules_df["answer"].cast(FloatType()))
    unk_rules_df = unk_rules_df.withColumn("priority", unk_rules_df["priority"].cast(IntegerType()))

    logger.info('Pull rules for unknown brands from s3 ended')

    return unk_rules_df


def pull_pos_bars_products(spark):
    """ Pull pos_bars_products (used by pos_code module and bar_price module).
    :param spark: spark session
    :return: pos_bars_products
    """

    logger.info('Pull pos bars products started')

    pos_bars_products_df = spark.sql("""SELECT
                            pbp.product_id as bar_product_id,
                            pbp.title as title_2,
                            pbp.price as price_2,
                            pbp.pos_code,
                            pbp.bar_id,
                            pbp.beer_brand_id as brand_id,
                            pbp.volume_per_unit * pbp.units as volume,
                            pbp.beer_serving_type_id as serving_type_id,
                            pbp.created_at as inserted_at
                            FROM stg.pos_products_new_flat pbp
                            join stg.bars b ON pbp.bar_id = b.id where pbp.beer_brand_id > 0
                            """)

    pos_bars_products_df = pos_bars_products_df.withColumn("bar_product_id",
                                                     pos_bars_products_df["bar_product_id"].cast(StringType()))

    logger.info('Pull pos bars products ended')

    return pos_bars_products_df


def pull_bar_pos_code(spark):
    """ Pull bars with pos_code <-> pos_product 1:1 relation.
    :param spark: spark session
    :return: bar_pos_code - bars that match 1:1 pos_code <-> pos_product relation
    """

    logger.info('Pull bar pos code started')

    bar_pos_code = spark.sql("""SELECT bar_id FROM stg.bars_pos_code""")

    logger.info('Pull bar pos code ended')

    return bar_pos_code


def pull_brands_names(spark):
    """ Pull and clean brands names for fuzzy matching.
    :param spark: spark session
    :return: brands_df - clean lower case brands list
    """
    brands_df = spark.sql(
        """select id as fuzzy_brand_id, name as matched_brand_name from stg.brands""")
    brands_df = brands_df.withColumn('matched_brand_name', udf_clean_titles(func.col('matched_brand_name')))
    return brands_df


# ---------------------------------------------------------------------------


def preprocess_bar_pos_code(spark):
    """ Preprocess for pos code module.
    :param spark: spark session
    :return: pos_code_bars_products - pos_bars_products of bars with 1:1 pos_code <-> pos_product relation
    """
    pos_bars_products_df = pull_pos_bars_products(spark)
    bar_pos_code = pull_bar_pos_code(spark)
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('brand_id', 'brand_id_2')
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('serving_type_id', 'serving_type_id_2')
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('volume', 'volume_2')
    # in the future add - pick the common one in case of different products
    pos_code_bars_products = pos_bars_products_df.join(broadcast(bar_pos_code), on='bar_id', how='inner').dropDuplicates(
        ['pos_code', 'bar_id'])
    return pos_code_bars_products


def clean_titles(title):
    """ Spark UDF - clean punctuation and set to lower case.
    :param title: strings series to clean
    :return: title - clean series
    """
    title = title.encode('utf-8')
    title = str(title).lower()
    title = title.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    return title


def has_column(df, col):
    """ Check if specific column exists in a dataframe.
    :param df: dataframe to check
    :param col: column name to check
    :return:
    """
    try:
        df[col]
        return True
    except AnalysisException:
        return False


def add_empty_columns(products, version, run_id, is_cross):
    products = products.withColumn("created_at", func.lit(datetime.now()))
    products = products.withColumn("super_brand_id", func.lit(0))
    products = products.withColumn("beer_type_id", func.lit(0))
    products = products.withColumn("brand_id_prob", func.when(func.col('brand_id') != 0, func.lit(1)).otherwise(func.lit(0)))
    products = products.withColumn("super_brand_id_prob", func.lit(0))
    products = products.withColumn("volume_id_prob", func.when(func.col('volume_id') != 0., func.lit(1)).otherwise(func.lit(0)))
    products = products.withColumn("volume_id_prob", func.when((func.col("country").isin(const.COUNTRIES_2_IGNORE_VOLUME_PREDICTION)) & (func.col("volume_id") == 0.001), func.lit(1)).otherwise(func.col("volume_id_prob")))
    products = products.withColumn("beer_type_id_prob", func.lit(0))
    products = products.withColumn("serving_type_id_prob", func.when(func.col('serving_type_id') != 0, func.lit(1)).otherwise(func.lit(0)))
    products = products.withColumn("model_type", func.lit('rule'))
    products = products.withColumn("run_id", func.lit(run_id))
    products = products.withColumn("version", func.lit(version))
    products = products.withColumn("is_cross_validation", func.lit(is_cross))
    products = products.withColumn("run_date", func.lit(datetime.now().date()))
    return products


udf_clean_titles = func.udf(clean_titles, StringType())


# ---------------------------------------------------------------------------


def add_matched_pos_code(products_df, pos_code_df):

    logger.info('Pos code started')

    pos_code_df = pos_code_df.withColumnRenamed('bar_product_id', 'bar_product_id_2')

    products_df = products_df.join(pos_code_df, on=['bar_id', 'pos_code'], how='left')
    joined_df = products_df.groupBy('bar_product_id').count()
    products_df = products_df.join(joined_df, on='bar_product_id')
    products_df = products_df.filter(
        (func.col('bar_product_id') != func.col('bar_product_id_2')) | (func.col('count') == 1))

    products_df = products_df.select('bar_id', 'bar_product_id', 'bar_id', 'title', 'price', 'brand_id_2',
                                     'serving_type_id_2', 'volume_2', 'nn_brand_id', 'nn_serving_type_id', 'nn_volume',
                                     'price', 'title', 'country', 'state', 'pos_vendor', 'key_account_id',
                                     'pos_product_volume', 'delivery')
    products_df = products_df.withColumnRenamed('brand_id_2', 'pos_code_brand_id')
    products_df = products_df.withColumnRenamed('serving_type_id_2', 'pos_code_serving_type_id')
    products_df = products_df.withColumnRenamed('volume_2', 'pos_code_volume')
    products_df = products_df.select('bar_id', 'bar_product_id', 'pos_code_brand_id', 'pos_code_serving_type_id',
                                     'pos_code_volume', 'nn_brand_id', 'nn_serving_type_id', 'nn_volume', 'price',
                                     'title', 'country', 'state', 'pos_vendor', 'key_account_id', 'pos_product_volume',
                                     'delivery')

    logger.info('Pos code ended')

    return products_df


def add_matched_price_volume(products_df, spark):

    logger.info('Price volume started')

    pos_bars_products_df = pull_pos_bars_products(spark)
    # diff_pct = 0.01
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('serving_type_id_2', 'serving_type_id')
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('volume', 'existing_volume')
    pos_bars_products_df = pos_bars_products_df.withColumnRenamed('bar_product_id', 'existing_bar_product_id')
    joined_df = products_df.join(pos_bars_products_df, on=((pos_bars_products_df.bar_id == products_df.bar_id) & (
                pos_bars_products_df.serving_type_id == products_df.rule_serving_type_id) & (
                                                                       pos_bars_products_df.serving_type_id == 2)))
    joined_df = joined_df.filter((func.col('existing_bar_product_id') != func.col('bar_product_id')) & (
                func.col('delivery') < func.col('inserted_at')))
    joined_df = joined_df.withColumn('dist', func.abs((func.col('price') - func.col('price_2'))))

    # joined_df = joined_df.filter(func.col('diff') < diff_pct)
    min_df = joined_df.groupBy(func.col('bar_product_id')).agg(
        func.min('dist').alias('dist'))
    joined_df = joined_df.join(min_df, on=(['bar_product_id', 'dist']))
    joined_df = joined_df.groupBy(func.col('bar_product_id')).agg(
        func.first(func.col('existing_volume')).alias('existing_volume'))
    joined_df = joined_df.select('bar_product_id', 'existing_volume')
    joined_df = joined_df.withColumnRenamed('existing_volume', 'bar_price_volume')
    products_df = products_df.join(joined_df, on='bar_product_id', how='left')

    logger.info('Price volume ended')

    return products_df


def check_rule(bc_rule, bar_product_id, country, state, title, price, brand_id, serving_type_id, volume_id, pos_vendor,
               key_account_id):

    logger.info('Check rule started, title: {:s}'.format(str(title)))

    i_rule_id = 0
    i_rule_type = 1
    i_country = 2
    i_state = 3
    i_pos_vendor = 4
    i_key_account_id = 5
    i_title = 6
    i_price_over = 7
    i_price_under = 8
    i_brand_id = 9
    i_serving_type_id = 10
    i_answer = 11
    i_operand = 12
    i_value = 13
    i_priority = 14
    for i in range(0, len(bc_rule)):

        if all([(bc_rule[i][i_country] is None or bc_rule[i][i_country] == country),
                (bc_rule[i][i_state] is None or bc_rule[i][i_state] == state),
                (bc_rule[i][i_pos_vendor] is None or bc_rule[i][i_pos_vendor] == pos_vendor),
                (bc_rule[i][i_key_account_id] is None or bc_rule[i][i_key_account_id] == key_account_id),
                (bc_rule[i][i_price_under] is None or bc_rule[i][i_price_under] > float(price)),
                (bc_rule[i][i_price_over] is None or bc_rule[i][i_price_over] < float(price)),
                (bc_rule[i][i_brand_id] is None or bc_rule[i][i_brand_id] == brand_id),
                (bc_rule[i][i_serving_type_id] is None or bc_rule[i][i_serving_type_id] == serving_type_id)]):
            if bc_rule[i][i_title] is not None:
                p = re.compile(bc_rule[i][i_title])
                m = p.search(str(title))
                if m:
                    if p.groups == 2:
                        if bc_rule[i][i_operand] == "*":
                            result = str(
                                str(round(float(m.group(1)) * float(m.group(2)) * bc_rule[i][i_answer], 3)) + ',' + str(
                                    bc_rule[i][i_rule_id]) + ',' + str(bc_rule[i][i_priority]))
                        else:
                            result = str(
                                str(round(float(str(m.group(1)) + str(m.group(2))) * bc_rule[i][i_answer], 3)) + ',' + str(
                                    bc_rule[i][i_rule_id]) + ',' + str(bc_rule[i][i_priority]))
                            logger.info('More than one capturing group, operand = concat, title: {:s}'.format(str(title)))
                        break
                    elif p.groups == 1:
                        result = str(str(round(float(m.group(1)) * bc_rule[i][i_answer], 3)) + ',' + str(
                            bc_rule[i][i_rule_id]) + ',' + str(bc_rule[i][i_priority]))
                        break
                    elif p.groups == 0:
                        result = str(str(round(bc_rule[i][i_answer] * bc_rule[i][i_value], 3)) + ',' + str(
                            bc_rule[i][i_rule_id]) + ',' + str(bc_rule[i][i_priority]))
                        break
                else:
                    result = str('0,' + '0,' + '0')
            else:
                result = str('0,' + '0,' + '0')
        else:
            result = str('0,' + '0,' + '0')
    return result


def match_products(rules, products_df, column_to_update, spark):

    logger.info('Rules started')

    bc = spark.sparkContext.broadcast(rules.collect())

    def pass_func(bar_product_id, country, state, title, price, brand_id, serving_type_id, volume_id, pos_vendor,
                  key_account_id):
        return check_rule(bc.value, bar_product_id, country, state, title, price, brand_id, serving_type_id, volume_id,
                          pos_vendor, key_account_id)

    check_rule_udf = func.udf(pass_func, StringType())

    cols = list(set(products_df.columns))
    cols.append('rule_' + column_to_update)
    cols.append('rule_id_' + column_to_update)
    cols.append('rule_priority_' + column_to_update)

    products_df = products_df.withColumn('temp', check_rule_udf(products_df['bar_product_id'],
                                                                products_df['country'],
                                                                products_df['state'],
                                                                products_df['title'],
                                                                products_df['price'],
                                                                products_df['nn_brand_id'],
                                                                products_df['nn_serving_type_id'],
                                                                products_df['nn_volume'],
                                                                products_df['pos_vendor'],
                                                                products_df['key_account_id']))
    temp_col = func.split(products_df['temp'], '\\,')

    products_df = products_df.withColumn('rule_' + column_to_update, temp_col.getItem(0).cast(FloatType()))\
                             .withColumn('rule_id_' + column_to_update, temp_col.getItem(1).cast(IntegerType()))\
                             .withColumn('rule_priority_' + column_to_update, temp_col.getItem(2).cast(IntegerType()))\
                             .select(cols)

    logger.info('Rules ended')

    return products_df


def check_unk_brands(bc_rule, country, state, title, pos_vendor, key_account_id):

    logger.info('Check unknown brands started, title: {:s}'.format(str(title)))

    i_rule_id = 0
    i_country = 1
    i_state = 2
    i_pos_vendor = 3
    i_key_account_id = 4
    i_title = 5
    i_answer = 6
    i_priority = 7

    res_brand_id = 0
    res_rule_id = 0
    res_priority_id = 0
    temp_brand_id = 0
    temp_rule_id = 0
    temp_priority_id = 0
    result = "{:d},{:d},{:d}".format(res_brand_id, res_rule_id, res_priority_id)
    title = re.sub("null", " ", re.sub('[' + string.punctuation + '′＞\-\-±§\\\★┗▶·]+',' ', str(title).lower())).strip()

    logger.info("Length of title without punctuations: {:d}".format(len(title)))

    for i in range(0, len(bc_rule)):
        if all([(bc_rule[i][i_country] is None or bc_rule[i][i_country] == country),
                (bc_rule[i][i_state] is None or bc_rule[i][i_state] == state),
                (bc_rule[i][i_pos_vendor] is None or bc_rule[i][i_pos_vendor] == pos_vendor),
                (bc_rule[i][i_key_account_id] is None or bc_rule[i][i_key_account_id] == key_account_id)]):
            if bc_rule[i][i_title] is not None:
                # print(bc_rule[i][i_title])
                p = re.compile(str(bc_rule[i][i_title]))
                m = p.search(str(title))
                if m:

                    logger.info("Matched regex {:s} in title {:s}".format(str(bc_rule[i][i_title]), str(title)))

                    if temp_brand_id == 0:
                        temp_brand_id = int(bc_rule[i][i_answer])
                        temp_rule_id = int(bc_rule[i][i_rule_id])
                        temp_priority_id = int(bc_rule[i][i_priority])
                    title = re.sub(p, ' ', str(title)).strip()

                    logger.info("Title after sub {:s}".format(str(title)))


    if temp_brand_id != 0 and len(title) == 0:

        logger.info("Length of title after removing unknown brand key words: {:d}".format(len(title)))

        res_brand_id = temp_brand_id
        res_rule_id = temp_rule_id
        res_priority_id = temp_priority_id
        result = "{:d},{:d},{:d}".format(res_brand_id, res_rule_id, res_priority_id)
    return result


def match_unknown_brands(unk_brand_rules, products_df, column_to_update, spark):

    logger.info('Matching of unknown brands started')

    bc = spark.sparkContext.broadcast(unk_brand_rules.collect())

    def pass_func(country, state, title, pos_vendor, key_account_id):
        return check_unk_brands(bc.value, country, state, title, pos_vendor, key_account_id)

    check_unk_brands_udf = func.udf(pass_func, StringType())
    cols = list(set(products_df.columns))
    if str('rule_' + column_to_update) not in cols:
        cols.append('rule_' + column_to_update)
        cols.append('rule_id_' + column_to_update)
        cols.append('rule_priority_' + column_to_update)
    products_df = products_df.withColumn('temp', check_unk_brands_udf(products_df['country'],
                                                                products_df['state'],
                                                                products_df['title'],
                                                                products_df['pos_vendor'],
                                                                products_df['key_account_id']))
    temp_col = func.split(products_df['temp'], '\\,')

    products_df = products_df.withColumn('rule_' + column_to_update, temp_col.getItem(0).cast(FloatType()))\
                             .withColumn('rule_id_' + column_to_update, temp_col.getItem(1).cast(IntegerType()))\
                             .withColumn('rule_priority_' + column_to_update, temp_col.getItem(2).cast(IntegerType()))\
                             .select(cols)

    logger.info('Matching of unknown brands ended')

    return products_df



def process_products_by_fuzzy_matching(products_df, brand_names_df):

    logger.info('Fuzzy matching started')

    products_df = products_df.withColumn('pr_title', udf_clean_titles(func.col('title')))
    products_df = products_df.fillna('', subset=['pr_title'])
    matched_df = products_df.join(brand_names_df,
                                  func.levenshtein(products_df['pr_title'], brand_names_df['matched_brand_name']) < 3)
    matched_df = matched_df.select(['bar_product_id', 'fuzzy_brand_id'])
    products_df = products_df.join(matched_df, on='bar_product_id', how='left')

    logger.info('Fuzzy matching ended')

    return products_df


def ensemble_rules(products_df):

    logger.info('Ensemble started')

    col_lst = ['brand_id', 'serving_type_id', 'volume_id']
    for column in col_lst:
        if not has_column(products_df, column):
            products_df = products_df.withColumn(column, func.lit(0))
    products_df = products_df.fillna('0.')
    products_df = products_df.fillna(0.)

    products_df = products_df.withColumn('brand_id', func.when(func.col('pos_code_brand_id') != 0.,
                                                               func.col('pos_code_brand_id').cast(StringType())).when(
        func.col('rule_brand_id') != 0., func.col('rule_brand_id').cast(StringType())).otherwise('0'))

    products_df = products_df.withColumn('serving_type_id', func.when(func.col('pos_code_serving_type_id') != 0.,
                                                                      func.col('pos_code_serving_type_id').cast(
                                                                          StringType())).when(
        func.col('rule_serving_type_id') != 0., func.col('rule_serving_type_id').cast(StringType())).otherwise('0'))

    products_df = products_df.withColumn('volume_id', func.when(func.col('pos_product_volume') != 0.,
                                                                func.col('pos_product_volume').cast(StringType())).when(
        func.col('pos_code_volume') != 0., func.col('pos_code_volume').cast(StringType())).when(
        func.col('rule_volume_id') != 0.,
        func.col('rule_volume_id').cast(StringType())).when(
        func.col('bar_price_volume') != 0., func.col('bar_price_volume').cast(StringType())).otherwise('0'))

    # fix Korea volume to 0.001
    products_df = products_df.withColumn("volume_id", func.when(
        (func.col("country").isin(const.COUNTRIES_2_IGNORE_VOLUME_PREDICTION)) & (func.col("volume_id").cast(FloatType()) == 0.),
        func.lit(0.001).cast(StringType())).otherwise('0'))

    logger.info('Ensemble ended')

    return products_df


# ---------------------------------------------------------------------------


def process_products(spark, products_path, rules_path, output_table, version, run_id, is_cross, flow, unknown_brands_path):

    logger.info('Process products started')

    # Pull Products and Rules
    products_df = prepare_products(spark, path=products_path)
    rules_df = pull_rules_s3(spark, path=rules_path)
    rules_df = rules_df.orderBy("priority", ascending=False)
    unknown_brands_rules = pull_unknown_rules_s3(spark=spark, path=unknown_brands_path)
    order_by_cols = ["priority", "ttl_len"]
    unknown_brands_rules = unknown_brands_rules.withColumn('ttl_len', func.length('title')).orderBy(*order_by_cols, ascending=False)
    brand_names_df = pull_brands_names(spark)

    # POS CODE
    pos_code_df = preprocess_bar_pos_code(spark)
    products_df = add_matched_pos_code(products_df, pos_code_df)

    # Fuzzy Brand
    # products_df = process_products_by_fuzzy_matching(products_df, brand_names_df)
    # products_df.collect()

    # Rules
    pipe = ['volume_id', 'serving_type_id', 'brand_id']

    for col_name in pipe:
        filtered_rules = rules_df[rules_df['rule_type'] == col_name]
        products_df = match_products(rules=filtered_rules, products_df=products_df, column_to_update=col_name,
                                     spark=spark)

    # Unknown brand rules
    products_df = match_unknown_brands(unk_brand_rules=unknown_brands_rules, products_df=products_df, column_to_update="brand_id", spark=spark)


    # Price Volume
    products_df = add_matched_price_volume(products_df, spark)

    # Ensemble
    products_df = ensemble_rules(products_df)
    log_df = add_empty_columns(products_df, version, run_id, is_cross)
    log_df = log_df.select(['bar_product_id', 'brand_id', 'serving_type_id', 'volume_id',
                                 'rule_id_volume_id', 'rule_priority_volume_id',
                                 'rule_id_serving_type_id', 'rule_priority_serving_type_id',
                                 'rule_id_brand_id', 'rule_priority_brand_id',
                                 'pos_code_brand_id', 'pos_code_volume', 'pos_code_serving_type_id',
                                 'pos_product_volume', 'bar_price_volume', 'run_date', 'run_id', 'created_at'])
    products_df = products_df.select(['country', 'bar_product_id', 'brand_id', 'serving_type_id', 'volume_id'])
    products_df = add_empty_columns(products_df, version, run_id, is_cross)
    products_df = products_df.drop('country')
    products_df.cache()
    log_df.cache()

    products_df = products_df.filter(
        (func.col('brand_id_prob') != 0.) | (func.col('serving_type_id_prob') != 0.) | (func.col('volume_id_prob') != 0.))
    log_df = log_df.filter(
        (func.col('brand_id_prob') != 0.) | (func.col('serving_type_id_prob') != 0.) | (func.col('volume_id_prob') != 0.))

    products_df = products_df.dropDuplicates()
    log_df = log_df.dropDuplicates()

    if flow == const.FLOW_PREDICT:

        products_df.repartition(1).registerTempTable("products")
        products_df.unpersist()
        spark.sql("""INSERT OVERWRITE TABLE """ + output_table + """ SELECT 
                    bar_product_id,
                    brand_id,
                    brand_id_prob,
                    super_brand_id,
                    super_brand_id_prob,
                    volume_id,
                    volume_id_prob,
                    serving_type_id,
                    serving_type_id_prob,
                    beer_type_id,
                    beer_type_id_prob,
                    version,
                    is_cross_validation,
                    created_at,
                    run_date,
                    run_id,
                    model_type
                    FROM products""")

        log_df.repartition(1).registerTempTable("log")
        log_df.unpersist()
        spark.sql("""INSERT OVERWRITE TABLE stg.beer_rule_base_log SELECT
                    bar_product_id,
                    brand_id,
                    serving_type_id,
                    volume_id,
                    rule_id_volume_id,
                    rule_priority_volume_id,
                    rule_id_serving_type_id,
                    rule_priority_serving_type_id,
                    rule_id_brand_id,
                    rule_priority_brand_id,
                    pos_code_brand_id,
                    pos_code_volume,
                    pos_code_serving_type_id,
                    pos_product_volume,
                    bar_price_volume,
                    created_at,
                    run_date,
                    run_id
                    FROM log""")

    elif flow == const.FLOW_TEST:

        products_df.repartition(1).write.format("csv").option("header", "true").mode("overwrite").save(products_path + '/' + const.OUTPUT_FOLDER + '/rule_results.csv/', compression="gzip")
        log_df.repartition(1).write.format("csv").option("header", "true").mode("overwrite").save(products_path + '/' + const.OUTPUT_FOLDER + '/rule_log.csv/', compression="gzip")

    elif flow == const.FLOW_TRAIN:
        # TODO: Train flow
        quit()
    return products_df


# ---------------------------------------------------------------------------


def rule_base(spark, bucket, model, version, flow, date, run_id, output_table, is_cross):

    logger.info('Rule base started')
    logger.info('Arguments received:')
    logger.info(f'Spark: {spark}')
    logger.info(f'Bucket: {bucket}')
    logger.info(f'Model: {model}')
    logger.info(f'Version: {version}')
    logger.info(f'Flow: {flow}')
    logger.info(f'Date session: {date}')
    logger.info(f'Run ID: {run_id}')
    logger.info(f'Output table: {output_table}')
    logger.info(f'Is cross: {is_cross}')

    if flow == const.FLOW_PREDICT:
        products_path = bucket + '/' + model + '/' + version + '/' + flow + '/' + date + '/' + run_id + '/'
    elif flow == const.FLOW_TEST:
        products_path = bucket + '/' + model + '/' + version + '/' + flow + '/'
    elif flow == const.FLOW_TRAIN:
        # TODO: Train flow
        quit()
    else:
        logger.error('flow %s is not recognized' % flow)
        print('flow %s is not recognized' % flow)
    rules_path = bucket + '/' + model + '/' + version + '/' + const.FLOW_TRAIN + '/metadata/rules.csv'
    unknown_brands_black_list_path = bucket + '/' + model + '/' + version + '/' + const.FLOW_TRAIN + '/metadata/unk_brands_blacklist.csv'
    return process_products(spark, products_path, rules_path, output_table, version, run_id, is_cross, flow, unknown_brands_path = unknown_brands_black_list_path)


# if __name__ == '__main__':
#
#     spark = SparkSession \
#         .builder \
#         .appName("rule_base - test") \
#         .enableHiveSupport() \
#         .getOrCreate()
#
#     bucket = "s3://dev-matching-steps"
#     model = "beer"
#     version = "0.1"
#     flow = "test"
#     date = "test_data"
#     run_id = "0"
#     rules = "s3://dev-matching-steps/beer/0.1/Train/metadata/rules.csv"
#     output_table = "pos_beer_suggestions"
#     is_cross = 0
#
#     rule_base(spark, bucket, model, version, flow, date, run_id, output_table, is_cross)
