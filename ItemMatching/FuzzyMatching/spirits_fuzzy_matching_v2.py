# -*- coding: utf-8 -*-
"""
Created on Sun Nov 04 2018

@author: MatanD
"""

##################################################################################
#1.importing packeges                                                            #
##################################################################################

import csv                                           # reading and writing csv file
import difflib                                       # module that provides classes and functions for comparing sequences
import operator                                      # assist library used to efficiency sort the sequences
import time
import pandas as pd
import numpy as np
import string

##################################################################################
#2.Opening csv file                                                              #
##################################################################################

def csv_reading_df(filename):
    data = pd.read_csv(filename,encoding = "utf-8")
    return(data)

##################################################################################
##3.matching func                                                                #
##################################################################################

def matcher(song_a,song_b):
    seq = difflib.SequenceMatcher(None,song_a,song_b)
    matching_rez = seq.ratio()*100
    return(matching_rez)

##################################################################################
##3.cleaning the data                                                            #
##################################################################################

def data_cleaner(df,column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation+"0123456789"), '')
    df[column_name]= df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return(df)

##################################################################################
##4.saving only top X the results in a form                                       #
##################################################################################

def strings_matcher(product_title,df_of_brands,id_column, name_column ,top_x_superbrands):

#having a string, this function comparing between string and df of other strings
#and picking the top_x from them.
    temp_df = df_of_brands
    temp_df['matching_res'] = temp_df[name_column].apply(lambda brand: matcher(brand,product_title))
    temp_df = temp_df.sort_values('matching_res',ascending=False)
    temp_df= temp_df.head(n=top_x_superbrands)
    temp_df = temp_df[[id_column,name_column]]
    df_to_tuples = [tuple(x) for x in temp_df.values]
    return(df_to_tuples)


##################################################################################
##6.type matcher                                                                 #
##################################################################################

def type_matcher(product_title,tuples_of_types):
    splitted_product_title = product_title.split()
    for word in splitted_product_title:
        for type in tuples_of_types:
            if word == type[1]:
                return(type)


##################################################################################
##7.func that compare between fuzzy result and algo result                       #
##################################################################################

def accuracy_check(name_by_algo ,list_of_fuzzy):
    for sugg in list_of_fuzzy:
        if name_by_algo in sugg[0]:
            return(1)
    return(0)

##################################################################################
##8.writing to csv file                                                          #
##################################################################################

def writing_to_csv(filename,df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return("Done")

##################################################################################
#9.init                                                                          #
##################################################################################
top_x_superbrands = 5
pd.options.mode.chained_assignment = None
start_time = time.time()

##################################################################################
#10.main                                                                         #
##################################################################################
df_of_brands = csv_reading_df("superbrands_v2_full.csv")
print(1)
df_of_products = csv_reading_df("v2.csv")
print(2)
brands_after_clean = data_cleaner(df_of_brands, 'title')
print(4)
products_after_clean = data_cleaner(df_of_products, 'title')
print(8)
products_after_clean['superbrand_fuzzy_suggestion'] = products_after_clean['title'].apply(lambda product_title: strings_matcher(product_title,brands_after_clean,'id','title',top_x_superbrands))
print(9)
#products_after_clean['type_fuzzy_suggestion'] = products_after_clean.apply(lambda row: type_matcher(row['title'],lst_of_tuples_with_types), axis=1)

##################################################################################
#11.end                                                                          #
##################################################################################
writing_to_csv('to_check_v3',products_after_clean)
print("this program executed in %s seconds " % (time.time() - start_time))


