import unittest
import ItemMatching.ItemSaver.post_process_nn as post_process_nn
from pyspark.sql import SQLContext, SparkSession
from pyspark import SparkContext
from pyspark.ml.linalg import Vectors

class category_endocder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .getOrCreate()
        self.path="/Users/orens/PycharmProjects/Data/sampled_data/"

    def test0_run_nn_post_process_predict_flow(self):
        model_name = 'beer'
        version = '0.32'
        flow = 'predict'
        date = '2019-07-22'
        run_id = '2019-07-22-12-14-18'

        result_df = post_process_nn.post_NN(spark=self.spark, path=self.path, model_name=model_name, version=version,flow=flow, date=date, run_id=run_id)
        result_df.show()