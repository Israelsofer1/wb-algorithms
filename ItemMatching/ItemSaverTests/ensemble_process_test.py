import unittest
import ItemMatching.ItemSaver.ensemble as ensemble
from pyspark.sql import SQLContext, SparkSession
from pyspark import SparkContext
from pyspark.ml.linalg import Vectors

class category_endocder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .getOrCreate()
        self.assertNotEqual(self.spark, None)
        self.path = '/Users/israelsofer/Data/ensemble_check'
        self.columns = ["bar_product_id","brand_id","brand_id_prob","super_brand_id","super_brand_id_prob",
                        "volume_id", "volume_id_prob", "serving_type_id","serving_type_id_prob", "beer_type_id",
                        "beer_type_id_prob", "version", "is_cross_validation","created_at", "run_date", "run_id","model_type"]

    def test0(self):
        row1 = (9970189, 2024, 1.0, 43, 1.0, 0.375, 0.460000008, 2, 0.540000021, 0, 0.0,
                0.1, 1, '2019-03-28-06-43-44', '28/03/2019', '2019-03-28-06-43-44',	'NN')
        row2 = (9970189, 2024, 0.93, 0, 0.0, 0.0, 0.0, 0, 0.0, 0, 0.0,
                0.1, 1, '2019-03-28-06-43-44', '28/03/2019', '2019-03-28-06-43-44', 'BOW')
        row3 = (9970189, 0, 0.0, 0, 0.0, 0.425, 1.0, 0, 0.0, 0, 0.0,
                0.1, 1, '2019-03-28-06-43-44', '28/03/2019', '2019-03-28-06-43-44', 'rule')

        dataFrame = self.spark.createDataFrame([row1, row2, row3], self.columns)
        result_df = ensemble.run_predict_ensemble(self.spark, dataFrame, 'beer', self.path, '0.1')
        self.assertEquals(result_df.count(), 1)
        print(result_df.show())

    def test1_run_ensemble_for_test_flow(self):
        model_name = 'beer'
        version = '0.1'
        flow = 'test'
        date = ''
        run_id = ''

        result_df = ensemble.run_ensemble(self.spark, self.path, model_name, version, flow, date, run_id)
        self.assertEquals(result_df.count(), 20000)
        print(result_df.show())