import unittest
import ItemMatching.ItemSaver.ensemble as ensemble
from pyspark.sql import SQLContext, SparkSession

class category_endocder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .getOrCreate()
        self.assertNotEqual(self.spark, None)
        self.path = '/Users/israelsofer/Data/output_pre_process/'

    def test0_max_from_two_first(self):
        result = ensemble.max_from_two(1, 0.9, 2, 0.85)
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 0.9)

    def test1_max_from_two_second(self):
        result = ensemble.max_from_two(1, 0.9, 2, 0.95)
        self.assertEquals(result[0], 2)
        self.assertEquals(result[1], 0.95)

    def test2_compare_two_equals(self):
        result = ensemble.compare_two(1, 0.9, 1, 0.95)
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 0.95)

    def test3_compare_two_not_equals(self):
        result = ensemble.compare_two(1, 0.9, 2, 0.95)
        self.assertEquals(result[0], 2)
        self.assertEquals(result[1], 0.95)

    def test4_compare_two_not_equals_with_brand_list(self):
        result = ensemble.compare_two(2, 0.9, 2, 0.95, [1,2,3])
        self.assertEquals(result[0], 2)
        self.assertEquals(result[1], 0.95)

    def test5_compare_two_not_equals_in_brand_list_spam_low(self):
        result = ensemble.compare_two(1, 0.9, 0, 0.7, [1,2,3])
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 0.9)

    def test6_compare_two_not_equals_in_brand_list_spam_high(self):
        result = ensemble.compare_two(1, 0.9, 0, 0.9, [1,2,3])
        self.assertEquals(result[0], 0)
        self.assertEquals(result[1], 0.9)

    def test7_compare_two_not_equals_not_in_brand_list(self):
        first_id = 5
        first_prob = 0.9

        exepect_id = 5
        exepect_prob = 0.9
        result = ensemble.compare_two(5, 0.9, 0, 0.9, [1,2,3])
        self.assertEquals(result[0], exepect_id)
        self.assertEquals(result[1], exepect_prob)

    def test8_choose_result_equals(self):
        result = ensemble.choose_result(1, 0.9, 1, 0.91, 1, 0.92, [])
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 0.92)

    def test9_choose_result_not_equals_rule_prob_1(self):
        result = ensemble.choose_result(8, 1, 5, 0.91, 1, 1.0, [])
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 1.0)

    def test10_choose_result_nn_equals_bow_over85(self):
        result = ensemble.choose_result(1, 0.89, 1, 0.81, 5, 0.8, [])
        self.assertEquals(result[0], 1)
        self.assertEquals(result[1], 0.89)

    def test11_choose_result_nn_equals_bow_under85(self):
        result = ensemble.choose_result(1, 0.8, 1, 0.81, 5, 0.9, [])
        self.assertEquals(result[0], 5)
        self.assertEquals(result[1], 0.9)