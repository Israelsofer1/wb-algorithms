import pandas as pd
import numpy as np
import string
import time
import operator


pd.options.mode.chained_assignment = None

def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)



tagged_products =  csv_reading_df('2000_tagged_products.csv') #all system products
words_in_union = csv_reading_df('words_in_union.csv')  #all words in union


count=0
lst_of_words=[]
for a in words_in_union.title:
    for b in tagged_products.title:
        if str(a) in str(b):
            lst_of_words.append(str(a))
            #print(a, ' in ', b)
            count = count+1
            break
print(count)
temp_df = pd.DataFrame(lst_of_words)
writing_to_csv('words_for_4280_products',temp_df)








