import pandas as pd
import numpy as np
import string
import time
import operator
from sklearn.feature_extraction.text import CountVectorizer


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)

most_common_food_products = csv_reading_df('filtered_most_common_words.csv')
train_with_super_type = csv_reading_df('train_with_super_type.csv')

clean_most_common_food_products = data_cleaner(most_common_food_products,'title')
clean_train_with_super_type = data_cleaner(train_with_super_type,'type_id')

def freq_count(title,dic,tuples):
    for item in lst_of_types:
        temp_dict[item] = 0
    for i in tuples:
        if title in str(i[0]):
            #print('title ' + str(title))
            #print(i[0])
            #print(i[1])
            dic[i[1]] += 1
    max_val = max(dic.items(), key=operator.itemgetter(1))[0]
    print(dic)
    return(max_val)


pd.options.mode.chained_assignment = None
temp_dict ={}

lst_of_types = clean_train_with_super_type.super_type_id.values

print(temp_dict)

subset_clean_train_with_super_type = clean_train_with_super_type[['type_id', 'super_type_id']]
tuples = [tuple(x) for x in subset_clean_train_with_super_type.values]
clean_most_common_food_products['common_tag'] = clean_most_common_food_products.title.apply(lambda x : freq_count(x,temp_dict,tuples))

writing_to_csv('filtered_union_after_types',clean_most_common_food_products)