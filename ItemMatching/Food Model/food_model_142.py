import pandas as pd
import numpy as np
import string
import time


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)


def data_cleaner_2(df, column_name):
    #df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df[column_name] = df[column_name].str.replace('[','')
    df[column_name] = df[column_name].str.replace(']','')
    df[column_name] = df[column_name].str.replace("'",'')
    df[column_name] = df[column_name].str.replace("'",'')
    df = df.dropna(subset=[column_name])
    return (df)


start_time = time.time()
train = csv_reading_df('train_set.csv')
train_after_clean = data_cleaner_2(train,'type_id')

#explode(train_after_clean.assign(var1=train_after_clean.type_id.str.split(',')), 'type_id')
#b = train_after_clean.type_id.str.split(',')
#print(train_after_clean.type_id[0:10])
#print(b[2][0])
#.dropna()[['index', 'value']].set_index('index').melt(id_vars='title')
train_after_explode = pd.DataFrame(train_after_clean.type_id.str.split(',').tolist(), index=train_after_clean.title).stack()
train_after_explode = train_after_explode.reset_index()[[0, 'title']]
train_after_explode.columns = ['type_id' ,'title']       # renaming var1
train_after_explode = train_after_explode[['title','type_id']]

writing_to_csv('train_after_explode', train_after_explode)
