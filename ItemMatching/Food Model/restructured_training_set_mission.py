import pandas as pd
import numpy as np
import string
import time
import operator
pd.options.mode.chained_assignment = None


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)

union_after_types = csv_reading_df('union_after_types.csv')
all_food_products = csv_reading_df('all_food_products.csv')
food_types_mapping = csv_reading_df('food_types_new.csv')

clean_union_after_types = data_cleaner(union_after_types,'title')
clean_all_food_products = data_cleaner(all_food_products,'title')
clean_food_types_mapping = data_cleaner(food_types_mapping,'course_id')

clean_union_after_types = clean_union_after_types[['title','common_tag']]
tuples_of_common_words = [tuple(x) for x in clean_union_after_types.values]
print(tuples_of_common_words)
def matching_by_word_apperance(title,tuples_of_common_words):
    temp_lst=[]
    lst_of_words_in_title = title.split()
    for common_word in tuples_of_common_words:
        for word in lst_of_words_in_title:
            if common_word[0] == word:
                if common_word[1] not in temp_lst:
                    temp_lst.append(common_word[1])
    return(temp_lst)


clean_food_types_mapping = clean_food_types_mapping[['super_type_id', 'course_id']]
tuples_of_type_and_course= [tuple(x) for x in clean_food_types_mapping.values]

def course_extractor(list_of_common_words,tuple_list):
    lst_of_courses =[]
    for i in list_of_common_words:
        for j in tuple_list:
            if i == j[0]:
                if j[1] not in lst_of_courses:
                    lst_of_courses.append(j[1])
    return(lst_of_courses)

clean_all_food_products['tag_by_common_words'] = clean_all_food_products.title.apply(lambda title : matching_by_word_apperance(title,tuples_of_common_words))
clean_all_food_products['course_id'] = clean_all_food_products.tag_by_common_words.apply(lambda lst: course_extractor(lst,tuples_of_type_and_course))

writing_to_csv('tagged_by_common_all_food_products',clean_all_food_products)