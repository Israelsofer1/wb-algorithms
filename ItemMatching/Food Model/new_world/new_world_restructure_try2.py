import pandas as pd
import numpy as np
import string
import time
import operator
from sklearn.feature_extraction.text import CountVectorizer
pd.options.mode.chained_assignment = None


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)


def lst_of_products_belongs(word,df):
    new_df = all_food_products[all_food_products.title.str.contains(word)]
    return(new_df.title.values)

all_food_products= csv_reading_df('all_food_products_batch1.csv')
all_food_products = data_cleaner(all_food_products,'title')
words_in_union = csv_reading_df('words_in_union.csv')
words_in_union = data_cleaner(words_in_union,'title')


words_in_union['matched_products'] = words_in_union.title.apply(lambda word : lst_of_products_belongs(word,all_food_products))

writing_to_csv('products_belong_to_words',words_in_union)
print(words_in_union.head())