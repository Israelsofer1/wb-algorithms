import pandas as pd
import numpy as np
import string
import time
import operator
from sklearn.feature_extraction.text import CountVectorizer
pd.options.mode.chained_assignment = None


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)


def check(word,df):
    lst= []
    for index,row in df.iterrows():
        if word in str(row['title']):
            lst.append(row['title'])
    return(lst)


all_food_products= csv_reading_df('all_food_products_batch1.csv')
all_food_products = data_cleaner(all_food_products,'title')
words_in_union = csv_reading_df('words_in_union.csv')
words_in_union = data_cleaner(words_in_union,'title')
vectorizer_a = CountVectorizer()
vectorizer_b = CountVectorizer()
food_products_vector = vectorizer_a.fit_transform(all_food_products.title)
union_words_vector = vectorizer_b.fit_transform(words_in_union.title)
union_names= vectorizer_b.get_feature_names()
product_names = vectorizer_a.get_feature_names()
print("num of products  " +str((food_products_vector.shape)) + " num of union  " + str(union_words_vector.shape) )
for i in range(union_words_vector.toarray().shape[0]):
    #print(i)
    for j in range(food_products_vector.toarray().shape[0]):
        #print(j)
        if food_products_vector.toarray()[i,j] == 1:
            print(union_names[i])
            print(all_food_products.title[j])


#print(union_words_vector.toarray()[0,0])

#writing_to_csv('words_and_their_products',words_in_union)

