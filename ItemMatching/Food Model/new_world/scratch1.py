import pandas as pd
import numpy as np
import string
import time
import operator
from sklearn.feature_extraction.text import CountVectorizer
pd.options.mode.chained_assignment = None


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)



all_food_products= csv_reading_df('all_food_products.csv')
all_food_products = data_cleaner(all_food_products,'title')
words_in_union = csv_reading_df('words_in_union.csv')
words_in_union = data_cleaner(words_in_union,'title')

new_df = pd.DataFrame([],columns=['title'])

def get_words_products(word):
    start_time = time.time()
    temp_df = all_food_products[all_food_products.title.str.contains(word)]
    lst_of_items = [word]*int(len(temp_df))
    sr = pd.Series(lst_of_items)
    temp_df = temp_df.assign(common_words=sr.values)
    print("the word is "+ str(word)+", number of products is "+str(len(temp_df)) + " and this program executed in %s seconds " % (time.time() - start_time))
    #print(temp_df.head())
    new_df.append(temp_df)

words_in_union.title.apply(lambda word: get_words_products(word))

writing_to_csv('words_with_matched_products', new_df)