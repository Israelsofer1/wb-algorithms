
class const(object):
    CONTINUOUS_FEATURE = 'cont'
    TEXT_FEATURE = 'txt'
    CATEGORY_FEATURE = 'single'
    #CATEGORY_FEATURE = 'multi'

    INPUT_FOLDER = 'input_data'
    PRE_PROCESS_FOLDER = 'pre_process'
    MAPPING_FOLDER = 'pre_process_mapping'
    MODEL_RESULTS_FOLDER = 'results'
    OUTPUT_FOLDER = 'output'

    FLOW_TRAIN = 'train'
    FLOW_TEST = 'test'
    FLOW_PREDICT = 'predict'
    MODEL_CATEGORY = 'category'
    MODEL_BEER = 'beer'

    MODEL_TYPE_NN = 'NN'
    MODEL_TYPE_BOW = 'BOW'
    MODEL_TYPE_RULE_BASE = 'rule'
    MODEL_TYPE_ENSEMBLE = 'ensemble'

    W2V_CATEGORY_MODEL_NAME ='category_model'
    W2V_BEER_MODEL_NAME ='beer_model'

    KOREAN_LANG = "korean"

    COUNTRIES_2_IGNORE_VOLUME_PREDICTION = ["Korea"]

    KINESIS_STREAM_SUFFIX = '-auto-matching-stream'


tables = {
    'category': 'pos_category_suggestions',
    'beer': 'pos_beer_suggestions',
    'beer_final': 'pos_beer_suggestions_final',
    'food': 'pos_food_suggestions',
    'spirit': 'pos_spirit_suggestions',
    'wine': 'pos_wine_suggestions'}

hive_base_paths = {
    'dev': 'wb-emr-hive-stg-dev',
    'stg': 'wb-emr-hive-staging',
    'perf': 'wb-emr-hive-perf',
    'prod': 'wb-wmr-hive-stg-prod'
}