import pymysql
import pandas as pd
pymysql.install_as_MySQLdb()
import string
import difflib
import time


def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name] = df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return (df)


def matcher(song_a, song_b):
    seq = difflib.SequenceMatcher(None, song_a, song_b)
    matching_rez = seq.ratio() * 100
    return (matching_rez)


def strings_matcher(product_title, df_of_brands, name_column, top_x_superbrands):
    # having a string, this function comparing between string and df of other strings
    # and picking the top_x from them.
    temp_df = df_of_brands
    temp_df['matching_res'] = temp_df[name_column].apply(lambda brand: matcher(brand, product_title))
    temp_df = temp_df.sort_values('matching_res', ascending=False)
    temp_df = temp_df.head(n=top_x_superbrands)
    temp_df.matching_res = temp_df.matching_res.astype(float).fillna(0.0)
    temp_df= temp_df.loc[temp_df['matching_res'] > 70]
    temp_df = temp_df[name_column]
    lst = list(temp_df.values)
    if lst != []:
        for i in lst:
            train_words_in_use.append(i)
    df_to_list = temp_df.values
    print(len(train_words_in_use))
    return (df_to_list)


def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)


def writing_to_csv(filename, df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return ("Done")


con = pymysql.connect(host='mysql01.prod.alcoholanalytics.com',
                            user='dm_matand',
                           password='LIvH0pc9tmy1gkm8',
                          db='weissserver',
                         charset='utf8mb4',
                        cursorclass=pymysql.cursors.DictCursor)

sql = "select * from pos_bars_products bp join bars b on bp.bar_id = b.id where category_id = 2 and b.country in ('Australia','United Kingdom' , 'United States' , 'Canada') group by bp.title  ; "

all_food_products = pd.read_sql(sql,con)
start_time = time.time()
#train_words = csv_reading_df('all_food_train_set.csv')
#english_products = csv_reading_df('small_food_products.csv')
train_words_in_use =[]
#train_words_after_clean = data_cleaner(train_words, 'title')
all_food_products_after_clean = data_cleaner(all_food_products, 'title')
all_food_products_after_clean = all_food_products_after_clean[['id','title','price']]
all_food_products_after_clean = all_food_products_after_clean[~all_food_products_after_clean['title'].str.contains("add|no|hold|sub|extra|tuesday")]

top_x_superbrands = 3

#english_products_after_clean['train_fuzzy_suggestion'] = english_products_after_clean['title'].apply(lambda product_title: strings_matcher(product_title,train_words_after_clean,'title',top_x_superbrands))

print(writing_to_csv('all_food_products_after_clean',all_food_products_after_clean))
print("this program executed in %s seconds " % (time.time() - start_time))
#print(train_words_in_use)
