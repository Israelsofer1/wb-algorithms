import pandas as pd
import string
import time


##################################################################################
#2.Opening csv file                                                              #
##################################################################################

def csv_reading_df(filename):
    data = pd.read_csv(filename,encoding = "utf-8")
    return(data)


##################################################################################
##3.cleaning the data                                                            #
##################################################################################

def data_cleaner(df,column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation+"0123456789"), '')
    df[column_name]= df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return(df)


##################################################################################
##8.writing to csv file                                                          #
##################################################################################

def writing_to_csv(filename,df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return("Done")


def check_if_common_type(product,df_of_common):
    product_splited = product.split()
    lst_of_types = []
    for word in product_splited:
        temp_df = df_of_common[df_of_common['title'] == word]
        if temp_df.empty:
            continue
        lst_of_types.append(temp_df['type_name'].values[0])
    if lst_of_types == []:
        return (None)
    return (lst_of_types)


def check_if_common_course(product,df_of_common):
    product_splited = product.split()
    lst_of_courses = []
    for word in product_splited:
        temp_df = df_of_common[df_of_common['title'] == word]
        #print ("word is " +str(word))
        #print("temp_df is " + str(temp_df.values))
        if temp_df.empty:
            continue
        lst_of_courses.append(temp_df['course_name'].values[0])
    if lst_of_courses == []:
        return (None)
    return (lst_of_courses)




df_of_products = csv_reading_df('by_revenue_food_to_match.csv')
df_of_products_after_clean = data_cleaner(df_of_products,'title')

#print(df_of_products.head())

df_of_common_words = csv_reading_df('by_revenue_50_most_common.csv')
df_of_common_words_after_clean = data_cleaner(df_of_common_words,'title')

#print(df_of_products_after_clean.head())
#print(df_of_common_words_after_clean.head())


df_of_products_after_clean['type_names'] = df_of_products_after_clean['title'].apply(lambda product: check_if_common_type(product,df_of_common_words_after_clean))
df_of_products_after_clean['courses_names'] = df_of_products_after_clean['title'].apply(lambda product: check_if_common_course(product,df_of_common_words_after_clean))

df_of_products_after_clean.dropna()

#print(df_of_products_after_clean.head())

writing_to_csv('by_revenue_products_after_common_matching',df_of_products_after_clean)

