import collections
import pandas as pd
import string
import time


##################################################################################
#2.Opening csv file                                                              #
##################################################################################

def csv_reading_df(filename):
    data = pd.read_csv(filename,encoding = "utf-8")
    return(data)


##################################################################################
##3.cleaning the data                                                            #
##################################################################################

def data_cleaner(df,column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation+"0123456789"), '')
    df[column_name]= df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return(df)


##################################################################################
##8.writing to csv file                                                          #
##################################################################################

def writing_to_csv(filename,df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return("Done")


list_of_words=[]
top_common = 50
df_of_products = csv_reading_df('by_revenue_most_common.csv')
df_of_products_after_clean = data_cleaner(df_of_products,'title')
df_of_products_after_clean['title'].apply(lambda product: list_of_words.append(product))
str_of_words = ' '.join(list_of_words)
#print (str_of_words)

word_counter = collections.Counter(str_of_words.split()).most_common(top_common)  #create list of tuples for word and num of appereance
df_after_count = pd.DataFrame(word_counter, columns=['title', 'num_of_appereance'])
writing_to_csv('by_revenue_products_after_count',df_after_count)



#this code input df of products, clean, split and count their appereance



