import logging as log
from py3.pos2 import pos_matcher as pm
from py3.auxilary3 import Helper as hp
import pandas as pd
import numpy as np


def predict(host, data_to_match):
    test_matcher = pm.POSMatcher(host=host, que=False)
    print(data_to_match.shape)
    test_matcher.set_data(data_to_match)
    return test_matcher.predict_data(model_id=1)

def classification_report_to_table(report):
    report = report.splitlines()
    res = []
    res.append(['']+report[0].split())
    for row in report[2:-2]:
        res.append(row.split())
    lr = report[-1].split()
    res.append([' '.join(lr[:3])]+lr[3:])
    return np.array(res)

if __name__ == '__main__':
    host='REPLICA'
    db_con = hp.DbExplorer(host)

    time_condition = "and date(bp.inserted_at) > '2018-10-01'"

    query = """SELECT bp.bar_id,
                    a.country as country_id,
                    bp.category_id,
                    bp.title,
                    bp.price,
                    COALESCE(units_in_pack,1) as units_in_pack,
                    a.is_bulk
                FROM pos_bars_products bp
                JOIN pos_products pp on pp.id = bp.product_id 
                JOIN pos_products_brands pb on pb.product_id = pp.id
                JOIN bars a ON a.id = bp.bar_id
                WHERE bp.category_id = 1
                and pp.id != 0 """ + time_condition
    data = pd.DataFrame(db_con.dbreader(query))
    print('The data size retrieved is ', data.shape)
    data = predict(host, data)
    print('The predicted data size is ', data.shape)