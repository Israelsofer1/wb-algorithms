from py3.pos2 import pos_matcher as pm
from py3.auxilary3 import Helper as hp
import pandas as pd
import numpy as np
from py3.pos2 import new_processing as npr, match2
from sklearn.model_selection import train_test_split
from ItemMatching.Reporting.run_matching import predict

class TrainValidateMatching(object):
    def __init__(self, base_df, host):
        result_split = self.split_train(base_df)
        self.host = host
        self.train_df = result_split[0]
        self.test_df = result_split[1]

    def split_train(self, data):
        X_train, y_train, X_test, y_test = train_test_split(data, data['brand_id'], test_size=0.2)
        return [X_train, X_test]

    def train(self):
        corp_name = 'default_corpus'
        name='3'
        mdl = npr.ModelSkeleton(model_name=name, corpus_name=corp_name, dir_load='../../py3/pos2')
        self.train_df.to_csv()
        match2.spirit_model(self.host, mdl, new_data=True, train_data=self.train_df, model_path='../../py3/pos2')

    def predict(self):
        predict(self.host, self.test_df)