import pandas as pd
import py3.auxilary3.Helper as hp
import py3.auxilary3.Translator as trans
from py3.pos2 import new_processing as npr
from py3.pos2.sym_spell import SymSpell, create_spellchecker
from py3.auxilary3.Helper import FileExplorer

#if __name__ == '__main__':
from ItemMatching.Reporting.TrainMatching import TrainValidateMatching

file_path = '/Users/israelsofer/Data/spirit_trainset.csv'
spirit_df = pd.read_csv(file_path, sep=',')
host = 'REPLICA'

validator = TrainValidateMatching(spirit_df, host)
validator.train()