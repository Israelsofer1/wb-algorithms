from pyspark.sql import SparkSession,SQLContext
from ItemMatching.VolumePrice.extract_volumes import run_extract_volumes
from ItemMatching.VolumePrice.volume_price_constants import Const as VolumeConst

if __name__ == '__main__':

    spark = SparkSession.builder.appName('TEST - VolumePrice - extract_volumes').enableHiveSupport().getOrCreate()

    bucket = 's3://dev-matching-steps'
    model = 'beer'
    version = '0.1'
    flow = 'train'
    run_extract_volumes(spark, bucket + '/' + model + '/' + version + '/' + flow + '/' + VolumeConst.MODELS_FOLDER + '/')
