import unittest
import ItemMatching.PreProcess.corpus_encoder as w2v
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from ItemMatching.PreProcess.w2v_gensim import Corpus
from gensim.models import KeyedVectors
import pandas as pd
from ..constant import const
import csv

class corpus_test(unittest.TestCase):


    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()
        self.df_input = self.spark.createDataFrame([
            (('PATAGONIA AMBER LAGER Bottle EXPORT',)),
            (('Beer RAHR & SONS BLND DRAFT',)),
            (('BIERE DRAFT KEITH ROUSSE 1/2 PICHET',)),
            (('GROSSE Domestic BIERE GR.MOLSON SONS EXPORT',)),
            (('Beer (Generic) Domestic FIRESTONE IPA',)),
            (('Beer Domestic Bottle CIDER Budweiser bottle',)),
            (('Cider GROSSE GRANNY MOLSON DRAFT',))], ["title"])

    def test0_gensim_beer_w2v(self):
        # file_path = '/Users/israelsofer/Data/word2vec/beer_input_data.csv'
        file_path = '/Users/adishemesh/Desktop/beer_w2v_train.csv'
        data_w2v = pd.read_csv(file_path, error_bad_lines=False)
        print(data_w2v.shape)
        data_w2v = data_w2v[data_w2v.category_id == 1]
        print(data_w2v.shape)
        w2v_corpus = Corpus(cname=const.W2V_BEER_MODEL_NAME, data=data_w2v['title'])
        mapping_w2v_df = w2v_corpus.model_mapping_df()
        self.assertGreater(mapping_w2v_df.shape[0], 0)
        print(w2v_corpus.model.wv.most_similar('bud', topn=10))
        print(w2v_corpus.model.wv.most_similar('margarita', topn=10))

    def test0_find_synonym(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        model = w2v_encoder.train(self.df_input, ['title'])

        synonyms = model.findSynonyms('bottle', 5)

        self.assertEquals(synonyms.toPandas().shape[0], 5)

    def test1_find_synonyms_big_file(self):
        w2v_encoder = w2v.corpus_encoder(self.spark)
        w2v_encoder.load_model('../', mmap='r')
        # test_df = self.spark.read.format("csv").option("header", "true").load('/Users/israelsofer/Data/output_4/' + 'beer_test.csv')
        # test_df = test_df.where(col("title").isNotNull())

        # print(test_df.toPandas().shape)

        # model = w2v_encoder.train(test_df.select('title'), ['title'])

        synonyms = model.findSynonyms('bud', 5)
        print(synonyms.toPandas()[:5])

        synonyms = model.findSynonyms('onz', 5)
        print(synonyms.toPandas()[:5])

        #synonyms = model.findSynonyms('ounce', 5)
        #print(synonyms.toPandas()[:5])
        #self.assertEquals(synonyms.toPandas().shape[0], 5)

    def test2_clustering(self):
        from nltk.cluster import KMeansClusterer
        import nltk

        w2v_encoder = w2v.corpus_encoder(self.spark)
        # w2v_encoder.load_model()
        test_df = self.spark.read.format("csv").option("header", "true").load(
            '/Users/israelsofer/Data/output_4/' + 'beer_test.csv')
        test_df = test_df.where(col("title").isNotNull())
        model = w2v_encoder.train(test_df.select('title'), ['title'])

        print(list(model.getVectors()))
        print(len(list(model.getVectors())))

        NUM_CLUSTERS = 3
        kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance, repeats=3)
        assigned_clusters = kclusterer.cluster(model.getVectors().toPandas()['vector'], assign_clusters=True)
        print(assigned_clusters)

        words = list(model.getVectors().toPandas()['word'])
        for i, word in enumerate(words):
            print(word + ":" + str(assigned_clusters[i]))

    def test3_clustering(self):
        from sklearn import cluster
        from sklearn import metrics
        NUM_CLUSTERS = 3
        kmeans = cluster.KMeans(n_clusters=NUM_CLUSTERS)
        kmeans.fit()

        labels = kmeans.labels_
        centroids = kmeans.cluster_centers_

        print("Cluster id labels for inputted data")
        print(labels)
        print("Centroids data")
        print(centroids)

        print(
            "Score (Opposite of the value of X on the K-means objective which is Sum of distances of samples to their closest cluster center):")
        print(kmeans.score(X))

        silhouette_score = metrics.silhouette_score(X, labels, metric='euclidean')

        print("Silhouette_score: ")
        print(silhouette_score)

    def test4_w2v_category_train(self):
        file_path = '/Users/israelsofer/Data/word2vec/input_data_titles.csv'
        data_w2v = pd.read_csv(file_path, error_bad_lines=False)
        print(data_w2v.shape)
        w2v_corpus = Corpus(cname=const.W2V_CATEGORY_MODEL_NAME, data=data_w2v['title'])
        mapping_w2v_df = w2v_corpus.model_mapping_df()
        self.assertGreater(mapping_w2v_df.shape[0], 0)
        print(w2v_corpus.model.wv.most_similar('bud', topn=10))
        print(w2v_corpus.model.wv.most_similar('margarita', topn=10))

    def test5_w2v_category_load(self):

        #w2v_corpus = Corpus(cname=const.W2V_CATEGORY_MODEL_NAME + '.model')
        model = KeyedVectors.load_word2vec_format('test_bin_format.bin')
        #mapping_w2v_df = w2v_corpus.model_mapping_df()

        #w2v_corpus.model.wv.save_word2vec_format('test_bin_format')

        #w2v_corpus.model.save('test_111' + '.model', sep_limit=500**3)
        print(model.wv.most_similar('bud', topn=10))
        print(model.wv.most_similar('margarita', topn=10))
        print(model.wv.most_similar('monaco', topn=10))



    def test6_w2v_category_x(self):
        import json

        w2v_corpus = Corpus(cname=const.W2V_CATEGORY_MODEL_NAME)
        word_vectors = w2v_corpus.model.wv

        words = list(word_vectors.vocab.keys())
        vectors = [word_vectors[word].tolist() for word in words]
        a = dict(zip(words, vectors))

        json = json.dumps(a)
        f = open("dict.json", "w")
        f.write(json)
        f.close()

    def test7_read(self):
        import json

        with open('dict.json') as json_file:
            data = json.load(json_file)
            print(len(data))

