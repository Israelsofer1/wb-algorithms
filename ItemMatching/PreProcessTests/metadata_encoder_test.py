import unittest
from ItemMatching.PreProcess.metadata_encoder import metadata_encoder
from pyspark.sql import SparkSession
import pandas as pd
from ItemMatching.PreProcess.matching_config_creator import full_model_dict
import os

def organize_data_pandas(input_df, model_name):
    input_df = input_df[input_df.price.notnull()]
    input_df['price'] = input_df.price.astype(float)
    input_df = input_df[input_df.title.notnull()]
    input_df = input_df[input_df.category_id.notnull()]

    for column in input_df.columns:
        if column in full_model_dict[model_name] and full_model_dict[model_name][column]['type'] == "single":
            input_df = input_df[input_df[column].notnull()]
    return input_df

class metadata_encoder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()

    def test1_test_set_encode_category_full(self):
        meta_encoder = metadata_encoder(spark_session=self.spark, base_path ="/Users/orens/PycharmProjects/Data/Korea/sampled_data/", model_name="category", version="0.1", flow="test", date="", run_id="", model_types=["NN"], language="Korean")
        encode_json_train = meta_encoder.encode_train_data(['brand_id'])
        encode_json_test = meta_encoder.encode_test_data()

    def test1_predict_set_category(self):
        meta_encoder = metadata_encoder(spark_session=self.spark, base_path ="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="category", version="0.32", flow="predict", date="", run_id="", model_types=["NN"])
        metadata = meta_encoder.encode_test_data()

    def test4_train_set_beer_full_nn_korean(self):
        # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
        meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/Korea/sampled_data/", model_name="beer", version="0.1", flow="train", date="", run_id="", model_types=["NN"], language="Korean")
        metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], limit_count=10)


    def test5_train_set_beer_full_bow_korean(self):
        # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
        meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/Korea/sampled_data/", model_name="beer", version="0.1", flow="train", date="", run_id="", model_types=["BOW"], language="Korean")
        metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], limit_count=10)


    def test6_test_set_beer_korean(self):
        meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/Korea/sampled_data/", model_name="beer", version="0.2", flow="predict", date="", run_id="", model_types=['NN', 'BOW'], language="Korean")
        metadata = meta_encoder.encode_test_data() #limit_count=100


    def test7_test_set_beer_korean(self):
        meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/Korea/sampled_data/", model_name="beer", version="0.2", flow="test", date="", run_id="", model_types=['BOW'], language="Korean")
        metadata = meta_encoder.encode_test_data(limit_count=100)

    #########################
    #The following tests are copied from the retrain branch and don't comply with this branch's version of preprocessing
    #########################

    # def test8_train_set_beer_incremental_nn(self):
    #     # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
    #     meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="beer", version="0.2", flow="train", date="", run_id="", model_types=["NN"])
    #     metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], limit_count=10, is_incremental_training=True)
    #
    #
    # def test9_train_set_category_incremental_nn(self):
    #     # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
    #     meta_encoder = metadata_encoder(self.spark, base_path ="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="category", version="0.2", flow="train", date="", run_id="", model_types=["NN"])
    #     metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], limit_count=10, is_incremental_training=True)
    #
    # def test10_train_set_beer_incremental_bow(self):
    #     # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
    #     meta_encoder = metadata_encoder(self.spark, base_path="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="beer", version="0.2", flow="train", date="", run_id="", model_types=["BOW"])
    #     metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], is_incremental_training=True) # , limit_count=10
    #
    # def test11_train_set_beer_incremental_bow_and_nn(self):
    #     # version, flow, date, run_id, model_types = [const.MODEL_TYPE_NN]):
    #     meta_encoder = metadata_encoder(self.spark, base_path="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="beer", version="0.21", flow="train", date="", run_id="", model_types=["BOW", "NN"])
    #     metadata = meta_encoder.encode_train_data(output_cols=['brand_id'], is_incremental_training=True) # , limit_count=10

    def test12_test_set_beer_korean(self):
        meta_encoder = metadata_encoder(self.spark, base_path="/Users/orens/PycharmProjects/Data/Korea/sampled_data/",
                                        model_name="beer", version="0.1", flow="test", date="", run_id="",
                                        model_types=['BOW', 'NN'], language="Korean")
        metadata = meta_encoder.encode_test_data()