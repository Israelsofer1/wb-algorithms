import unittest
import ItemMatching.PreProcess.corpus_encoder as w2v
import ItemMatching.PreProcess.w2v_gensim as w2v_gensim
from pyspark.sql import SparkSession
import os

class corpus_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()
        self.df_input = self.spark.createDataFrame([
            (('PATAGONIA AMBER LAGER Bottle EXPORT',)),
            (('Beer RAHR & SONS BLND DRAFT',)),
            (('BIERE DRAFT KEITH ROUSSE 1/2 PICHET',)),
            (('GROSSE Domestic BIERE GR.MOLSON SONS EXPORT',)),
            (('Beer (Generic) Domestic FIRESTONE IPA',)),
            (('Beer Domestic Bottle CIDER Budweiser bottle',)),
            (('Cider GROSSE GRANNY MOLSON DRAFT',))], ["title"])

        self.df_test = self.spark.createDataFrame([
            (('PATAGONIA AMBER LAGER',)),
            (('Beer RAHR',)),
            (('Stella Art draught',))], ["title"])

        self.unique_words = 28
        padding_len = 1
        self.unique_words = self.unique_words + padding_len
        self.unique_words_over_two = 12

    def test_00_clean_text(self):
        text = w2v.clean_data('The Boy, ate//     ice-cream.')
        self.assertEquals(text, 'the boy ate ice cream')

    def test_00_padding(self):
        out_string = w2v.padding('this is a test')
        self.assertEquals(len(str.split(out_string)), 20)

    def test_01_padding(self):
        out_string = w2v.padding("VERDANT                              SALAD")
        self.assertEquals(len(str.split(out_string)), 20)

    def test_0_pre_process(self):
        w2v_encoder = w2v.corpus_encoder(self.spark)
        df_output = w2v_encoder.transform_title_to_columns(self.df_input, ['title'])
        self.assertEquals(len(df_output.columns), 11)

    def test_1_encode_sparse_csv(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        model = w2v_encoder.train(self.df_input, ['title'])

        result = w2v_encoder.encode_vocab()

        self.assertNotEqual(model, None)
        self.assertEquals(result.count(), self.unique_words_over_two)

    def test_2_encode_dense_csv(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=False)
        model = w2v_encoder.train(self.df_input, ['title'])
        result = w2v_encoder.encode_dense(self.df_input, ['title'])
        self.assertEquals(len(result.columns), 2)

        value = result.select('word19').limit(1).toPandas()['word19'].iloc[0]
        self.assertEquals(0, int(value))

    def test_3_encode(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        model = w2v_encoder.train(self.df_input, ['title'])
        result = w2v_encoder.encode(self.df_input, 'title')
        self.assertEquals(len(result.columns), 2)

    def test_4_encode_average(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=False)
        model = w2v_encoder.train(self.df_input, ['title'])
        result = w2v_encoder.encode_average(self.df_input)
        column_count = 2
        row_count = self.df_input.count()
        self.assertEquals(column_count, len(result.columns))
        self.assertEquals(row_count, result.count())

    def test_5_encode_and_save(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        model = w2v_encoder.train(self.df_input, ['title'])
        path = '/Users/israelsofer/Data/output_pre_process/'
        w2v_encoder.save_model(path)
        self.assertTrue(os.path.isdir(path + 'word2vec_model'))
        result = w2v_encoder.encode(self.df_input, 'title')
        self.assertEquals(len(result.columns), 2)

    def test_5_load_and_encode(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        path = '/Users/israelsofer/Data/output_pre_process/'
        w2v_encoder.load_model(path)
        model = w2v_encoder.train(self.df_input, ['title'])
        result = w2v_encoder.encode(self.df_test, 'title')
        print('test data')
        print(result.show(10))
        self.assertEquals(len(result.columns), 2)


    def test_6_encode_and_save_bow(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        path = '/Users/israelsofer/Data/output_pre_process/'
        result = w2v_encoder.encode_bow(self.df_input, 'title')
        w2v_encoder.save_model(path)
        self.assertEquals(len(result.columns), 1)

    def test_7_load_and_encode_bow(self):
        w2v_encoder = w2v.corpus_encoder(self.spark, is_padding=True)
        path = '/Users/israelsofer/Data/output_pre_process/'
        w2v_encoder.load_model(path)
        result = w2v_encoder.encode_bow(self.df_test, 'title')
        self.assertEquals(len(result.columns), 1)

    def test_8_load_and_encode_wv(self):
        dict = w2v_gensim.model_mapping_dict(path="~/PycharmProjects/Data/Korea/w2v_models/W2V/", corpus_name="ko")
        print(type(dict))
