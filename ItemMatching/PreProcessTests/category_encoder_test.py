import unittest
import ItemMatching.PreProcess.category_encoder as encoder
from pyspark.sql import SparkSession
import os.path
from pyspark.ml.linalg import SparseVector, DenseVector


class category_endocder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .getOrCreate()
        self.assertNotEqual(self.spark, None)

    def test0_get_index_dense_vector(self):
        a = encoder.max_dense_index(DenseVector([1, 0, 0, 0]))
        print(a)


    def test0_data_to_index(self):
        df = self.spark.createDataFrame(
            [(0, "a"), (1, "b"), (2, "c"), (3, "a"), (4, "a"), (5, "c")],
            ["id", "category"])
        cat_encoder = encoder.category_encoder(self.spark)
        cat_encoder.data_to_index(df, ["category"])
        df_indexed = cat_encoder.transform_data_index(df)
        self.assertEqual(df_indexed.toPandas()['category_index'].max(), 2)
        self.assertEqual(len(cat_encoder.get_labels()['category_index']), 3)

    def test1_dense(self):
        df = self.spark.createDataFrame([
            (0.0, 1.0),
            (1.0, 0.0),
            (2.0, 1.0),
            (0.0, 2.0),
            (0.0, 1.0),
            (2.0, 0.0),
            (3.0, 3.0)
        ], ["example_1", "example_2"])

        cat_encoder = encoder.category_encoder(self.spark)
        encoded = cat_encoder.encode_average_vector(df, ["example_1", "example_2"])

        for column_name in df.columns:
            a = encoded.limit(1).select(column_name + '_sparse').collect()
            self.assertEquals(sum(eval(a[0][0])), 1)

    def test2_dense(self):
        df = self.spark.createDataFrame([
            (0.0, 1.0),
            (1.0, 0.0),
            (2.0, 1.0),
            (0.0, 2.0),
            (0.0, 1.0),
            (2.0, 0.0),
            (3.0, 3.0)
        ], ["example_1", "example_2"])

        cat_encoder = encoder.category_encoder(self.spark)
        cat_encoder.encode_dense(df, ["example_1"], ["example_2"])
        encoded = cat_encoder.one_hot_transform_dense(df, False)

        # Make sure all the vectors with one hot encoder has only one column assigned with one
        for column_name in df.columns:
            a = encoded.limit(1).select(column_name + '_dense').collect()
            self.assertEquals(len(a[0][0]), 3)
            self.assertEquals(sum(a[0][0]), 1)

    def test1_dense_with_random_values(self):
        df = self.spark.createDataFrame([
            (100.0, 11.0),
            (11.0, 100.0),
            (222.0, 11.0),
            (100.0, 222.0),
            (100.0, 11.0),
            (222.0, 100.0),
            (3000.0, 3000.0)
        ], ["example_1", "example_2"])

        cat_encoder = encoder.category_encoder(self.spark)
        encoded = cat_encoder.encode_average_vector(df, ["example_1", "example_2"])
        #print(encoded['example_1' + '_sparse'].show())

        for column_name in df.columns:
            a = encoded.limit(1).select(column_name + '_sparse').collect()
            print(a)
            self.assertEquals(sum(a[0][0]), 1)

    def test2_sparse_and_save(self):
        df = self.spark.createDataFrame([
            ('a', 'b'),
            ('b', 'a'),
            ('c', 'b'),
            ('a', 'c'),
            ('a', 'b'),
            ('b', 'a'),
            ('c', 'c')
        ], ["example_1", "example_2"])

        cat_encoder = encoder.category_encoder(self.spark, False)
        cat_encoder.encode(df, ["example_1"], ["example_2"], True)
        #encoded = cat_encoder.encode_dense(df, ["example_1", "example_2"])
        path = '/Users/israelsofer/Data/output_pre_process/'
        cat_encoder.save_pre_processing(path)

        self.assertTrue(os.path.isfile(path + 'label_mapping.txt'))
        self.assertTrue(os.path.isdir(path + 'string_pipeline'))
        self.assertTrue(os.path.isdir(path + 'one_hot_pipeline'))

    def test3_sparse_and_load(self):
        df = self.spark.createDataFrame([
            ('a', 'b'),
            ('d', 'a'),
            ('c', 'b'),
            ('a', 'e'),
            ('a', 'b'),
            ('b', 'a'),
            ('e', 'd')
        ], ["example_1", "example_2"])

        path = '/Users/israelsofer/Data/output_pre_process/'
        cat_encoder = encoder.category_encoder(self.spark, False)
        cat_encoder.load_pre_processing(path)
        encoded = cat_encoder.encode(df, ["example_1"], [], True)

        a = encoded.limit(2).select('example_1').collect()
        print(a)
        #self.assertEquals(sum(a[0][0]), 1)
