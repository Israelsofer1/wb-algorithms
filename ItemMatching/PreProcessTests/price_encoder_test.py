import unittest
import ItemMatching.PreProcess.price_encoder as encoder
from pyspark.sql import SQLContext, SparkSession
from pyspark import SparkContext
from pyspark.ml.linalg import Vectors

class category_endocder_test(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .getOrCreate()
        self.assertNotEqual(self.spark, None)
        self.path = '/Users/israelsofer/Data/output_pre_process/'
        self.dataFrame = self.spark.createDataFrame([('Hot Chicken', 2.5, 'Canada'),
                                                ('Templeton Rye', 15.62, 'Canada'),
                                                ('FIREBALL', 5.03, 'United Kingdom'),
                                                ('Benromach Sassicaia', 5.25, 'Canada'),
                                                ('Crown Lager Stubby', 6.0, 'Canada'),
                                                ('MIXED FRY', 2.95, 'United Kingdom'),
                                                ('tray bake cakes', 2.5, 'United Kingdom')], ["title", "price", 'country_id'])

        self.test_df = self.spark.createDataFrame([('Ice tea', 20, 'Canada'),
                                                    ('cookie', 7, 'United Kingdom')],
                                                    ["title", "price", 'country_id'])

    def test0_endcode_with_numbers(self):
        numeric_encoder = encoder.price_encoder(self.spark)
        encoded_df = numeric_encoder.encode_number(self.dataFrame, ['price'])

        for column_name in encoded_df.columns:
            if column_name.endswith('_scaled'):
                price_scaled = encoded_df.select(column_name).toPandas()

                # scaled items limited by zero and one
                self.assertEquals(sum(price_scaled[column_name] > 1), 0)
                self.assertEquals(sum(price_scaled[column_name] < 0), 0)
                self.assertEquals(sum(price_scaled[column_name] <= 1), price_scaled.shape[0])

                # There are two countries in the training set, so there are two zero items and two one items
                self.assertEquals(list(price_scaled[column_name]).count(0), 2)
                self.assertEquals(list(price_scaled[column_name]).count(1), 2)

    def test1_endcode_and_save_with_numbers(self):
        numeric_encoder = encoder.price_encoder(self.spark)
        encoded_df = numeric_encoder.encode_number(self.dataFrame, ['price'])
        numeric_encoder.save_scaler(self.path)


    def test2_load_and_encode_with_numbers(self):
        numeric_encoder = encoder.price_encoder(self.spark)
        numeric_encoder.load_scaler(self.path)
        encoded_df = numeric_encoder.encode_number(self.dataFrame, ['price'])

        for column_name in encoded_df.columns:
            if column_name.endswith('_scaled'):
                price_scaled = encoded_df.select(column_name).toPandas()

                # scaled items limited by zero and one
                self.assertEquals(sum(price_scaled[column_name] > 1), 0)
                self.assertEquals(sum(price_scaled[column_name] < 0), 0)
                self.assertEquals(sum(price_scaled[column_name] <= 1), price_scaled.shape[0])

                # There are two countries in the training set, so there are two zero items and two one items
                self.assertEquals(list(price_scaled[column_name]).count(0), 2)
                self.assertEquals(list(price_scaled[column_name]).count(1), 2)

    def test2_encode_with_dense_vector(self):
        numeric_encoder = encoder.price_encoder(self.spark)
        encoded_df = numeric_encoder.encode_dense_vector(self.dataFrame, ['price'])
        print(encoded_df.show())
        # TODO: move the dense vector creation into the encoder. The interface should be exactly like encode_numbers method
