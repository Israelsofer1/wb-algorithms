import sklearn
import xgboost as xgb
from sklearn import metrics
from sklearn.datasets import load_svmlight_file
import numpy as np
import joblib
import argparse
import os

class DbExplorer(object):
    def __init__(self,bucket ,model_name, model_version):
        self.bucket = bucket
        self.model_name = model_name
        self.model_version = model_version
        self.model = self.load_model()

    def load_model(self):
        #load model from s3
        pass

    def fit(DMatrix_train, DMatrix_valid, num_round, params, load_model=None, save_model=None):
        num_round = num_round
        dtrain = DMatrix_train

        if DMatrix_valid is not None:
            dvalid = DMatrix_valid
            watchlist = [(dtrain, 'train'), (dvalid, 'valid')]
            bst = xgb.train(params, dtrain, num_round, evals=watchlist,
                            verbose_eval=1, xgb_model=load_model,
                            maximize=True)
        else:
            watchlist = [(dtrain, 'train')]
            bst = xgb.train(params, dtrain, num_round, evals=watchlist,
                            verbose_eval=1, xgb_model=load_model)
        if save_model is not None:
            bst.save_model(save_model)

        return bst
    def predict(self):
        # run predict pipeline
        pass
    def split_data(self, ratio, dataset):
        #split dataset into 2
        pass
    def create_binary_set(self):
        #create dataset for binary classification (spam/ham)
        pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.
    parser.add_argument('--epochs', type=int, default=12)
    parser.add_argument('--batch_size', type=int, default=128)
    parser.add_argument('--num_classes', type=int, default=10)

    # input data and model directories
    parser.add_argument('--model_dir', type=str)
    parser.add_argument('--output_dir', type=str, default=os.environ.get('SM_MODEL_DIR'))
    parser.add_argument('--training', type=str, default=os.environ.get('SM_CHANNEL_TRAINING'))

    args, _ = parser.parse_known_args()