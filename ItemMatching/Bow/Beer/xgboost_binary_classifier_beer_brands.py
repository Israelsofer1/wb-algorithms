#!/usr/bin/env python
# coding: utf-8

# In[118]:


from sagemaker import get_execution_role

#IAM execution role that gives Amazon SageMaker access to resources in your AWS account.
#We can use the Amazon SageMaker Python SDK to get the role from our notebook environment. 
role = get_execution_role()
print(role)


# In[119]:


import pandas as pd
from sklearn.metrics import precision_score
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import preprocessing, ensemble,metrics, naive_bayes
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_curve
from sklearn.model_selection import cross_validate
import seaborn as sns
import scipy
from scipy import sparse
import sys
import string
import numpy as np
import matplotlib.pyplot as plt
import boto3
from time import gmtime, strftime                 
from sklearn.datasets import dump_svmlight_file   
import math


# In[120]:


bucket = 'wb-ds-models'
prefix = 'bow/beer/binary'


# In[121]:


s3 = boto3.resource('s3')

boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/train/train.libsvm').download_file('train.libsvm')
boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/validation/valid.libsvm').upload_file('valid.libsvm')
boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/test/test.libsvm').upload_file('test.libsvm')



# In[122]:


s3 = boto3.resource('s3')
sm = boto3.client('sagemaker')

job_name = 'bow-beer-binary-6-depth-600t-2-xgboost-' + strftime("%Y-%m-%d-%H-%M-%S", gmtime())
print("Training job", job_name)

from sagemaker.amazon.amazon_estimator import get_image_uri
container = get_image_uri(boto3.Session().region_name, 'xgboost')


# In[123]:


#train

create_training_params = {
    "RoleArn": role,
    "TrainingJobName": job_name,
    "AlgorithmSpecification": {
        "TrainingImage": container,
        "TrainingInputMode": "File"
    },
    "ResourceConfig": {
        "InstanceCount": 1,
        "InstanceType": "ml.m5.xlarge",
        "VolumeSizeInGB":1

    },
    "InputDataConfig": [
        {
            "ChannelName": "train",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": "s3://{}/{}/train".format(bucket, prefix),
                    "S3DataDistributionType": "FullyReplicated"
                }
            },
            "ContentType": "libsvm",
            "CompressionType": "None"
        },
        {
            "ChannelName": "validation",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": "s3://{}/{}/validation".format(bucket, prefix),
                    "S3DataDistributionType": "FullyReplicated"
                }
            },
            "ContentType": "libsvm",
            "CompressionType": "None"
        }
    ],
    "OutputDataConfig": {
        "S3OutputPath": "s3://{}/{}/bow/output".format(bucket, prefix)
    },
    "HyperParameters": {
        "max_depth":"6",
        "eta":"0.1",
        "eval_metric":"error",
        "subsample":"0.3",
        "objective":"binary:logistic",
        "num_round":"6000"
    },
    "StoppingCondition": {
        "MaxRuntimeInSeconds": 60 * 60 * 12
    }
}


# In[124]:


get_ipython().run_cell_magic('time', '', '#train\nsm.create_training_job(**create_training_params)\n\nstatus = sm.describe_training_job(TrainingJobName=job_name)[\'TrainingJobStatus\']\nprint(status)\n\ntry:\n    sm.get_waiter(\'training_job_completed_or_stopped\').wait(TrainingJobName=job_name)\nfinally:\n    status = sm.describe_training_job(TrainingJobName=job_name)[\'TrainingJobStatus\']\n    print("Training job ended with status: " + status)\n    if status == \'Failed\':\n        message = sm.describe_training_job(TrainingJobName=job_name)[\'FailureReason\']\n        print(\'Training failed with the following error: {}\'.format(message))\n        raise Exception(\'Training job failed\')')


# In[125]:


create_model_response = sm.create_model(
    ModelName=job_name,
    ExecutionRoleArn=role,
    PrimaryContainer={
        'Image': container,
        'ModelDataUrl': sm.describe_training_job(TrainingJobName=job_name)['ModelArtifacts']['S3ModelArtifacts']})

print(create_model_response['ModelArn'])


# In[126]:


xgboost_endpoint_config = 'bow-beer-binary-brandidallitems-xgboost-' + strftime("%Y-%m-%d-%H-%M-%S", gmtime())
print(xgboost_endpoint_config)
create_endpoint_config_response = sm.create_endpoint_config(
    EndpointConfigName=xgboost_endpoint_config,
    ProductionVariants=[{
        'InstanceType': 'ml.m4.xlarge',
        'InitialInstanceCount': 1,
        'ModelName': job_name,
        'VariantName': 'AllTraffic'}])

print("Endpoint Config Arn: " + create_endpoint_config_response['EndpointConfigArn'])


# In[127]:


get_ipython().run_cell_magic('time', '', '\nxgboost_endpoint = \'bow-beer-binary-brandidallitems-xgboost-endpoint-\' + strftime("%Y%m%d%H%M", gmtime())\nprint(xgboost_endpoint)\ncreate_endpoint_response = sm.create_endpoint(\n    EndpointName=xgboost_endpoint,\n    EndpointConfigName=xgboost_endpoint_config)\nprint(create_endpoint_response[\'EndpointArn\'])\n\nresp = sm.describe_endpoint(EndpointName=xgboost_endpoint)\nstatus = resp[\'EndpointStatus\']\nprint("Status: " + status)\n\ntry:\n    sm.get_waiter(\'endpoint_in_service\').wait(EndpointName=xgboost_endpoint)\nfinally:\n    resp = sm.describe_endpoint(EndpointName=xgboost_endpoint)\n    status = resp[\'EndpointStatus\']\n    print("Arn: " + resp[\'EndpointArn\'])\n    print("Status: " + status)\n\n    if status != \'InService\':\n        message = sm.describe_endpoint(EndpointName=xgboost_endpoint)[\'FailureReason\']\n        print(\'Endpoint creation failed with the following error: {}\'.format(message))\n        raise Exception(\'Endpoint creation did not succeed\')')


# In[128]:


runtime = boto3.client('runtime.sagemaker')


# In[129]:


print(xgboost_endpoint)


# In[130]:


def do_predict(data, endpoint_name, content_type, treshold):
    payload = '\n'.join(data)
    response = runtime.invoke_endpoint(EndpointName=endpoint_name, 
                                   ContentType=content_type, 
                                   Body=payload)
    result = response['Body'].read()
    result = result.decode("utf-8")
    result = result.split(',')
    preds = [float((num)) for num in result]
    preds = [0 if num<treshold else 1 for num in preds]

    return preds

def batch_predict(data, batch_size, endpoint_name, content_type, treshold):
    items = len(data)
    arrs = []
    
    for offset in range(0, items, batch_size):
        if offset+batch_size < items:
            results = do_predict(data[offset:(offset+batch_size)], endpoint_name, content_type, treshold = treshold)
            arrs.extend(results)
        else:
            arrs.extend(do_predict(data[offset:items], endpoint_name, content_type,treshold))
        sys.stdout.write('.')
    return(arrs)


# In[131]:


get_ipython().run_cell_magic('time', '', "import json\n\nwith open('test.libsvm', 'r') as f:\n    payload = f.read().strip()\n\nlabels = [int(line.split(' ')[0]) for line in payload.split('\\n')]\ntest_data = [line for line in payload.split('\\n')]\n\ntres = [0.5,0.6,0.7,0.8,0.9]\nfor i in tres:\n    preds = batch_predict(test_data, 100, xgboost_endpoint, 'text/x-libsvm',i)\n    #print(X_test)\n    print ('\\nerror rate=%f' % ( sum(1 for i in range(len(preds)) if preds[i]!=labels[i]) /float(len(preds))))\n    print(pd.crosstab(index=np.array(labels), columns=np.array(preds)))\n    prec, rec, fbeta_test, support = metrics.precision_recall_fscore_support(labels, preds, average = None)\n    print(prec)\n    print(rec)\n    print(fbeta_test)\n    print(support)")


# In[72]:





# In[73]:


0.5
error rate=0.240802
col_0      0      1
row_0              
0      16566   2374
1      11953  28604
CPU times: user 2 s, sys: 93 ms, total: 2.09 s
Wall time: 19.3 s
    
[0.58087591 0.92336497]
[0.87465681 0.70527899]
[0.69811838 0.79972042]
[18940 40557]

0.6
error rate=0.244954
col_0      0      1
row_0              
0      17072   1868
1      12706  27851
CPU times: user 1.94 s, sys: 93.3 ms, total: 2.03 s
Wall time: 18.9 s

[0.57330915 0.93714459]
[0.90137276 0.68671253]
[0.70084979 0.79261768]
[18940 40557]

0.7
error rate=0.248836
col_0      0      1
row_0              
0      17455   1485
1      13320  27237
CPU times: user 2.07 s, sys: 58.9 ms, total: 2.13 s
Wall time: 19.6 s
    
[0.56718115 0.94829747]
[0.92159451 0.67157334]
[0.70220255 0.78629888]
[18940 40557]


0.8
error rate=0.263492
col_0      0      1
row_0              
0      17768   1172
1      14505  26052

[0.55055309 0.95694975]
[0.93812038 0.6423552 ]
[0.69388632 0.768711  ]
[18940 40557]

0.9

error rate=0.296200
label      0      1
pred              
0      18119    821
1      16802  23755

[0.51885685 0.96659342]
[0.95665259 0.58571886]
[0.67280593 0.72943055]
[18940 40557]


# In[ ]:




