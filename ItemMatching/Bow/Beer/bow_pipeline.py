import sklearn
import xgboost as xgb
from sklearn import metrics
from sklearn.datasets import load_svmlight_file
from sklearn.datasets import make_classification
import numpy as np
import joblib
#import bow_binary
#import bow_multiclass
import argparse
import os
import ast
import pandas as pd
from pyspark.mllib.linalg import SparseVector
from scipy import sparse
import os
import codecs

class bow_pipeline(object):
    def __init__(self,bucket ,model_name, model_version):
        self.bucket = bucket
        self.model_name = model_name
        self.model_version = model_version
        #self.model = self.load_model()

    def load_model(self):
        #load model from s3
        pass
    def train_pipeline(self):
        # run train pipeline
        pass
    def predict_pipeline(self):
        # run predict pipeline
        pass
    def split_data(self, ratio, dataset):
        #split dataset into 2
        pass
    def create_binary_set(self):
        #create dataset for binary classification (spam/ham)
        pass
    def create_multiclass_set(self):
        #create dataset for multiclass classification
        pass
    def fit_model(self, config):
        # fit the model
        pass
    def predict(self, model):
        #predict
        pass

    def process_json(self):

        labeled = False
        test_df = pd.read_json('preprocess.json', lines=True)

        size = test_df['title'][0]['size']
        country_size = len(ast.literal_eval(test_df['country_id'][0])) + 1
        price_size = 1

        if labeled:
            x_shape = size + country_size + price_size + 1
            y_shape = len(test_df.index)
            y = np.zeros((y_shape, 1), dtype=float)
        else:
            x_shape = size + country_size + price_size

        X = np.zeros((x_shape, x_shape), dtype=float)

        for idx,row in test_df.iterrows():
            count_vect = list(zip(row['title']['indices'], row['title']['values']))
            for word in count_vect:
                X[idx-1,word[0]] = word[1]
            X[idx - 1, word[0]]
        try:
             country = str(size+[i for i,x in enumerate(ast.literal_eval(row['country_id'])) if x == 1.0][0]+2)+":1.0"
        except:
             country = str(size+len(ast.literal_eval(row['country_id']))+1)+":1.0"


    def load_data(self, path):
        ending = 'gz'
        data = pd.DataFrame()

        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(ending):
                    partial_df = pd.read_json(path + file, compression='gzip')
                    data = data.append(partial_df, ignore_index=True)

            data['sparse'] = data.apply(
                lambda row: sparse.csr_matrix((row['title']['values'], ([0] * len(row['title']['indices']),
                                                                        row['title']['indices'])),
                                              shape=(1, row['title']['size'])), axis=1)

        lst = [x[1] for x in data['sparse'].iteritems()]
        sparse_matrix = sparse.vstack(lst)
        return xgb.DMatrix(sparse_matrix)

if __name__ == '__main__':


    path = 'C:/Users/user/Desktop/git/analytics/ItemMatching/Bow/Beer/prep/'
    bow = bow_pipeline(bucket='test',model_version='test',model_name='test')
    bow.load_data(path = path)



    # test_df = pd.read_json('preprocess.json', lines=True)
    # libsvm = ""
    # for index, row in test_df.iterrows():
    #     size = row['title']['size']
    #     libsvm_row = str(row['brand_id'][0])+" "
    #     word_count = list(zip(row['title']['indices'],row['title']['values']))
    #     word_count2 = str([str(x)+":"+str(y)+" " for x, y in word_count])
    #     word_count2 = ''.join(c for c in word_count2 if not c in ["'","[","]","\\",","])
    #     price = str(size+1)+":"+str(row['price'][0])+" "
    #     try:
    #         country = str(size+[i for i,x in enumerate(ast.literal_eval(row['country_id'])) if x == 1.0][0]+2)+":1.0"
    #     except:
    #         country = str(size+len(ast.literal_eval(row['country_id']))+1)+":1.0"
    #     libsvm_row = libsvm_row + word_count2 + price + country
    #     libsvm = libsvm + libsvm_row + "\n"
    #
    # print(libsvm)
    # text_file = open("testtt.libsvm", "w")
    # text_file.write(libsvm)
    # text_file.close()
    #
    test = xgb.DMatrix("test2")
    test2 = xgb.DMatrix("train.libsvm")


    print('test')

    #X,y = make_classification()
    #clf = xgb.XGBClassifier()



