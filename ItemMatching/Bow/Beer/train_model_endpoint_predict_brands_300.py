#!/usr/bin/env python
# coding: utf-8

# In[24]:


from sagemaker import get_execution_role

#IAM execution role that gives Amazon SageMaker access to resources in your AWS account.
#We can use the Amazon SageMaker Python SDK to get the role from our notebook environment. 
role = get_execution_role()
print(role)

import pandas as pd
from sklearn.metrics import precision_score
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import preprocessing, ensemble,metrics, naive_bayes
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_curve
from sklearn.model_selection import cross_validate
import seaborn as sns
import scipy
from scipy import sparse
import sys
import string
import numpy as np
import matplotlib.pyplot as plt
import boto3
from time import gmtime, strftime                 
from sklearn.datasets import dump_svmlight_file   
import math

bucket = 'wb-ds-models'
prefix = 'bow/beer'

# Input
# label_to_learn - 'brand_id', 'serving_type_id', 'volume'
label = 'brand_id'


# In[25]:


s3 = boto3.resource('s3')
sm = boto3.client('sagemaker')

job_name = 'bow-beer-brandid-6-depth-600t-2-xgboost-' + strftime("%Y-%m-%d-%H-%M-%S", gmtime())
print("Training job", job_name)

from sagemaker.amazon.amazon_estimator import get_image_uri
container = get_image_uri(boto3.Session().region_name, 'xgboost')


# In[26]:


#train

create_training_params = {
    "RoleArn": role,
    "TrainingJobName": job_name,
    "AlgorithmSpecification": {
        "TrainingImage": container,
        "TrainingInputMode": "File"
    },
    "ResourceConfig": {
        "InstanceCount": 1,
        "InstanceType": "ml.m5.xlarge",
        "VolumeSizeInGB":1

    },
    "InputDataConfig": [
        {
            "ChannelName": "train",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": "s3://{}/{}/train".format(bucket, prefix),
                    "S3DataDistributionType": "FullyReplicated"
                }
            },
            "ContentType": "libsvm",
            "CompressionType": "None"
        },
        {
            "ChannelName": "validation",
            "DataSource": {
                "S3DataSource": {
                    "S3DataType": "S3Prefix",
                    "S3Uri": "s3://{}/{}/validation".format(bucket, prefix),
                    "S3DataDistributionType": "FullyReplicated"
                }
            },
            "ContentType": "libsvm",
            "CompressionType": "None"
        }
    ],
    "OutputDataConfig": {
        "S3OutputPath": "s3://{}/{}/bow/output".format(bucket, prefix)
    },
    "HyperParameters": {
        "max_depth":"6",
        "eta":"0.1",
        "eval_metric":"merror",
        "subsample":"0.3",
        "objective":"multi:softmax",
        "num_class":"301",
        "num_round":"600"
    },
    "StoppingCondition": {
        "MaxRuntimeInSeconds": 60 * 60 * 12
    }
}


# In[27]:


get_ipython().run_cell_magic('time', '', '#train\nsm.create_training_job(**create_training_params)\n\nstatus = sm.describe_training_job(TrainingJobName=job_name)[\'TrainingJobStatus\']\nprint(status)\n\ntry:\n    sm.get_waiter(\'training_job_completed_or_stopped\').wait(TrainingJobName=job_name)\nfinally:\n    status = sm.describe_training_job(TrainingJobName=job_name)[\'TrainingJobStatus\']\n    print("Training job ended with status: " + status)\n    if status == \'Failed\':\n        message = sm.describe_training_job(TrainingJobName=job_name)[\'FailureReason\']\n        print(\'Training failed with the following error: {}\'.format(message))\n        raise Exception(\'Training job failed\')')


# In[28]:


job_name = 'bow-beer-brandid-6-depth-600t-xgboost-2019-01-02-15-23-18'
create_model_response = sm.create_model(
    ModelName=job_name,
    ExecutionRoleArn=role,
    PrimaryContainer={
        'Image': container,
        'ModelDataUrl': sm.describe_training_job(TrainingJobName=job_name)['ModelArtifacts']['S3ModelArtifacts']})

print(create_model_response['ModelArn'])


# In[29]:


xgboost_endpoint_config = 'bow-beer-brandidallitems-xgboost-' + strftime("%Y-%m-%d-%H-%M-%S", gmtime())
print(xgboost_endpoint_config)
create_endpoint_config_response = sm.create_endpoint_config(
    EndpointConfigName=xgboost_endpoint_config,
    ProductionVariants=[{
        'InstanceType': 'ml.m4.xlarge',
        'InitialInstanceCount': 1,
        'ModelName': job_name,
        'VariantName': 'AllTraffic'}])

print("Endpoint Config Arn: " + create_endpoint_config_response['EndpointConfigArn'])


# In[30]:


get_ipython().run_cell_magic('time', '', '\nxgboost_endpoint = \'bow-beer-brandidallitems-xgboost-endpoint-\' + strftime("%Y%m%d%H%M", gmtime())\nprint(xgboost_endpoint)\ncreate_endpoint_response = sm.create_endpoint(\n    EndpointName=xgboost_endpoint,\n    EndpointConfigName=xgboost_endpoint_config)\nprint(create_endpoint_response[\'EndpointArn\'])\n\nresp = sm.describe_endpoint(EndpointName=xgboost_endpoint)\nstatus = resp[\'EndpointStatus\']\nprint("Status: " + status)\n\ntry:\n    sm.get_waiter(\'endpoint_in_service\').wait(EndpointName=xgboost_endpoint)\nfinally:\n    resp = sm.describe_endpoint(EndpointName=xgboost_endpoint)\n    status = resp[\'EndpointStatus\']\n    print("Arn: " + resp[\'EndpointArn\'])\n    print("Status: " + status)\n\n    if status != \'InService\':\n        message = sm.describe_endpoint(EndpointName=xgboost_endpoint)[\'FailureReason\']\n        print(\'Endpoint creation failed with the following error: {}\'.format(message))\n        raise Exception(\'Endpoint creation did not succeed\')')


# In[31]:


runtime = boto3.client('runtime.sagemaker')


# In[32]:


print(xgboost_endpoint)


# In[35]:


def do_predict(data, endpoint_name, content_type):
    payload = '\n'.join(data)
    response = runtime.invoke_endpoint(EndpointName=endpoint_name, 
                                   ContentType=content_type, 
                                   Body=payload)
    result = response['Body'].read()
    print(result)
    result = result.decode("utf-8")
    result = result.split(',')
    
    preds = [float((num)) for num in result]
    preds = [round(num) for num in preds]
    return preds

def batch_predict(data, batch_size, endpoint_name, content_type):
    items = len(data)
    arrs = []
    
    for offset in range(0, items, batch_size):
        if offset+batch_size < items:
            results = do_predict(data[offset:(offset+batch_size)], endpoint_name, content_type)
            break
            arrs.extend(results)
        else:
            arrs.extend(do_predict(data[offset:items], endpoint_name, content_type))
        sys.stdout.write('.')
    return(arrs)


# In[36]:


get_ipython().run_cell_magic('time', '', "import json\n\nwith open('test.libsvm', 'r') as f:\n    payload = f.read().strip()\n\nlabels = [int(line.split(' ')[0]) for line in payload.split('\\n')]\ntest_data = [line for line in payload.split('\\n')]\npreds = batch_predict(test_data, 1, xgboost_endpoint, 'text/x-libsvm')\n#print(X_test)\nprint ('\\nerror rate=%f' % ( sum(1 for i in range(len(preds)) if preds[i]!=labels[i]) /float(len(preds))))\nprint(pd.crosstab(index=np.array(labels), columns=np.array(preds)))")


# In[ ]:




