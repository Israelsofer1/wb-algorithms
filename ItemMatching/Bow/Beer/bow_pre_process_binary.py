#!/usr/bin/env python
# coding: utf-8

# In[126]:


from sagemaker import get_execution_role

#IAM execution role that gives Amazon SageMaker access to resources in your AWS account.
#We can use the Amazon SageMaker Python SDK to get the role from our notebook environment. 
role = get_execution_role()
print(role)


# In[127]:


import pandas as pd
from sklearn.metrics import precision_score
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import preprocessing, ensemble,metrics, naive_bayes
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_curve
from sklearn.model_selection import cross_validate
import seaborn as sns
import scipy
from scipy import sparse
import sys
import string
import numpy as np
import matplotlib.pyplot as plt
import boto3
from time import gmtime, strftime                 
from sklearn.datasets import dump_svmlight_file   
import math


# In[128]:


bucket = 'wb-ds-models'
read_prefix = 'bow/beer/binary'
read_prefix_dataset = 'bow/beer'
prefix = 'bow/beer/binary'


# In[129]:


# Input
# label_to_learn - 'brand_id', 'serving_type_id', 'volume'
label = 'brand_id'

# train only n top classes / -1 to disable
n_top_classes = 300
# feature engineering method tfidf / count
feat_eng_method = 'count'
max_features = None
sample = None
extra_features = ['country_id_Australia','country_id_United Kingdom','country_id_United States','country_id_Argentina','country_id_Canada','country_id_Brazil']
# extra_features = ['price_norm','country_Argentina','country_Korea','country_Australia','country_Canada','country_United Kingdom','country_United States']
# extra_features = None


# In[130]:


#read the data
raw_data_filename = 'train_set_cleaned_bow.csv'

s3 = boto3.resource('s3')
s3.Bucket(bucket).download_file(read_prefix_dataset + '/' + raw_data_filename, 'raw_data.csv')

data = pd.read_csv('./raw_data.csv')
data.count()


# In[131]:


# keep only n top classes, other turn to unknown
df = data
agg_brands = data.groupby(label).count()
agg_brands = agg_brands['title'].sort_values(ascending = False)
all_samples = agg_brands.sum()
if sample:
   df = data.sample(n = sample)
top_classes_list = agg_brands.nlargest(n_top_classes)
def reassignlabels(orig_label, top_classes_list):
    try: 
        if top_classes_list[orig_label]:
            #return orig_label
            return 1
    except:
        return 0
        # return -1

df[label] = df[label].apply(lambda x: reassignlabels(x, top_classes_list))

# make unknown class approx 5% of the data (1720 samples)

df_known = df[df[label] != 0]
df_unknown = df[df[label] == 0]
# df_unknown_sample = df_unknown.sample(n=1720)
df = df_known
df = df.append(df_unknown)


# In[134]:


import re
print(len(df_known))
print(len(df_unknown))

# df_unknown['title'] = df_unknown['title'].str.replace('[{}]'.format(string.punctuation), '')
# df_unknown['title'] = df_unknown['title'].str.lower()
# df_known['title'] = df_known['title'].str.replace('[{}]'.format(string.punctuation), '')
# df_known['title'] = df_known['title'].str.lower()

stopwords = ['long','500','sm','14oz','ipa','pint','ale','big',
'daddy','draft','pale','pitcher','half','can','lager','beer','stout','pt','glass','btl',
'6pk','jug','16oz','20oz','oz','10oz','22oz','8oz','330','20','dft','bottle','gls','tap',
'glass','draught','sample','60oz','small','500ml','32oz','pichet','large','pinte','pitch',
'34oz','lrg','lg','750','lt','3pk','mug','24X330ml','6X330ml','4p','cider','pilsner','red','black','hh','blonde','porter'
'amber','brown'  ,'craft','the' ,'light' ,'dark' ,'blue' ,'pils' ,'wheat','gold','pnt' ,'330ml' ,'taster' ,'4pk','two','xl',
'330ml','lime','50','drft','12oz','18oz','375ml','24x330ml','hop','porter','old','summer', 'sch', 'st', 'amber', 'white', 'pear',
 'apple', 'of', 'sgl', 'ctn', 'session', 'sam', 'dog', 'ginger', 'golden', 'happy', 'island', 'city', 'jack', 'fill', 'dry',
             'point', 'winter', 'little', 'north', 'shandy', 'schooner', 'pot', 'moon', 'keg', 'brew', 'stone', 'night', 'slv',
             'gl', 'new', '4z', 'seasonal', 'mill', 'kolsch', 'green', '10', 'jg', 'bt', 'fat', 'rock', 'de', '375', 'tall',
             'original', 'mg', 'on', 'radler', 'up', 'house', 'young', 'founders', 'bitter', 'adams', 'free', 'cask', 'seas',
             'rt', 'and', 'brewing', 'head', 'carlton', 'la', 'abby', 'honey', 'balter', 'growler', 'sg', 'coast', 'brooklyn',
             'hard', 'standard', 'life', 'ct', 'xxxx', 'stein', 'squire', 'flight', 'wild', 'grapefruit', 'n2', 'vanilla', '18',
             'four', 'asahi', 'blood', 'henrys', 'magners', 'star', 'orchard', 'pack', 'reg', 'rekorderlig', 'pines', '16',
             'bay', '6p', 'goat', 'belgium', 'stb', 'brick', 'xpa', 'mid', 'strongbow', 'fut', 'coop', 'bell', 'stub', 'wood',
             'cans', '12', 'nitro', 'case', 'fruit', 'third', 'strawberry', 'moose', 'heineken', 'sun', 'dr', 'one', 'premium',
             'sleeve', 'cigar', 'steam', 'quilmes', 'lite', 'west', 'five', 'sierra', 'rebel', 'deep', 'pirate', 'rousse',
             'super', 'fullers', 'clear', 'high', 'weisse', 'lagunitas', 'pit', 'allagash', 'strong', 'leinys', 'berry',
             'irish', 'crush', 'muskoka', 'guest', 'nevada', 'smiths', 'cloud', 'special', 'no', 'full', 'mountain', 'pure',
             'blanche', 'john', 'harpoon', 'belgian', 'iron', 'goose', 'batch', 'sweet', 'velvet', 'hopped', 'great',
             'single', 'sleeman', 'hoppy', 'devils', 'hills', 'ommegang', 'haze', 'james', 'top', 'prem', 'helles',
             'truck', 'middy', 'blond', 'staff', 'east', '6x330ml', 'coopers', 'jacks', 'boulevard', 'angry',
             'sweetwater', 'street', 'bright', 'cidre', 'crisp', 'heavy', 'lazy', 'all', 'castle', 'sp', 'fresh',
             'root', 'verre', 'ok', 'season', 'abita','in', 'orange', 'crowler', 'anchor', 'to', 'ounce', 'imperial',
             'smith', '2pt']

stopwords = []

# df_unknown['title'] = df_unknown['title'].str.replace('[{}]'.format(removed), '')
# df_known['title'] = df_known['title'].str.replace('[{}]'.format(removed), '')


def clean_title(title, stopwords):
    title = str(title).replace('[{}]'.format(string.punctuation), '')
    title = title.lower()
    if stopwords:
        resultwords  = [word for word in re.split("\W+",title) if word not in stopwords]
        result = ' '.join(resultwords)
        return result
    else:
        return title

df['title'] = df['title'].apply(lambda x: clean_title(x, False))
print(df.isna().sum())
df = df.dropna()

# def get_top_n_words(corpus, n=None):
#     vec = CountVectorizer().fit(corpus)
#     bag_of_words = vec.transform(corpus)
#     sum_words = bag_of_words.sum(axis=0) 
#     words_freq = [(word, sum_words[0, idx]) for word, idx in     vec.vocabulary_.items()]
#     words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
#     return words_freq[:n]

# top_words1 = get_top_n_words(df_known['title'], n = 1000)
# top_words2 = get_top_n_words(df_unknown['title'], n = 1000)

# # #print(top_words1)
# # #print(top_words2)

# similar_words = []
# for word, count in top_words2:
#     if word in [word for word,count in top_words1]:
#         similar_words.append((word,count))
# len(similar_words)
# print(similar_words)


# In[135]:



def get_top_n_words(corpus, n=None):
    vec = CountVectorizer().fit(corpus)
    bag_of_words = vec.transform(corpus)
    sum_words = bag_of_words.sum(axis=0) 
    words_freq = [(word, sum_words[0, idx]) for word, idx in     vec.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    return words_freq[:n]


df_known = df[df['brand_id'] == 1]
df_unknown = df[df['brand_id'] == 0]


top_words1 = get_top_n_words(df_known['title'], n = 1000)
top_words2 = get_top_n_words(df_unknown['title'], n = 1000)

# #print(top_words1)
# #print(top_words2)

similar_words = []
for word, count in top_words2:
    if word in [word for word,count in top_words1]:
        similar_words.append((word,count))
len(similar_words)
# print(similar_words)
over100 = [x for x,y in similar_words if y>100]
print(over100)

stopwords = ('long','500','sm','14oz','ipa','pint','ale','big',
'daddy','draft','pale','pitcher','half','can','lager','beer','stout','pt','glass','btl',
'6pk','jug','16oz','20oz','oz','10oz','22oz','8oz','330','20','dft','bottle','gls','tap',
'glass','draught','sample','60oz','small','500ml','32oz','pichet','large','pinte','pitch',
'34oz','lrg','lg','750','lt','3pk','mug','24X330ml','6X330ml','4p','cider','pilsner','red','black','hh','blonde','porter'
'amber','brown'  ,'craft','the' ,'light' ,'dark' ,'blue' ,'pils' ,'wheat','gold','pnt' ,'330ml' ,'taster' ,'4pk','two','xl',
'330ml','lime','50','drft','amber','12oz','18oz','375ml','24x330ml','hop','porter','old')


# In[136]:


#preprocess
# Dataset Preparation
df = df[['title','country_id','price',label]]
# df['price_norm'] = df.groupby('country_id')['price'].apply(lambda x: (x-min(x))/(max(x)-min(x)))

df = df.drop(['price'], axis=1)

df = pd.get_dummies(df, columns = ['country_id'])
X = df.drop([label], axis=1)
y = df[label]
encoder = preprocessing.LabelEncoder()
y = encoder.fit_transform(y)
X[label] = y.tolist()

all_samples = X
all_samples.head(100)


# In[137]:


# Smart sampleing
# remove superbrands with less than 3 samples, split the rest equality
train = []
validation = []
test = []
all_samples = all_samples.sample(frac=1).reset_index(drop=True)

#tell israel what i need to get (label as column)
def split_samples(all_samples, ratio,label):
    
    list1 = pd.DataFrame(columns=all_samples.columns.values)
    list2 = pd.DataFrame(columns=all_samples.columns.values)
    super_count = all_samples[label].value_counts().rename_axis('unique_values').reset_index(name='counts')
    for index, row in super_count.iterrows():
        label_samples = all_samples.loc[all_samples[label] == np.int64(row['unique_values'])]
        split1 = len(label_samples)*ratio
        split2 = len(label_samples)*(1-ratio)
        list1 = list1.append(label_samples.head(math.ceil(split1)))
        list2 = list2.append(label_samples.tail(math.floor(split2)))
        
    return list1, list2
    
# filtered_df = all_samples.groupby(label).filter(lambda x: len(x) > 2)

train, validation = split_samples(all_samples = all_samples, ratio = 0.6,label = label)
validation, test = split_samples(validation, ratio = 0.5,label = label)

train = train.sample(frac=1).reset_index(drop=True)
validation = validation.sample(frac=1).reset_index(drop=True)
test = test.sample(frac=1).reset_index(drop=True)

X_train = train.drop([label], axis=1)
y_train = train[label]
X_valid = validation.drop([label], axis=1)
y_valid = validation[label]
X_test = test.drop([label], axis=1)
y_test = test[label]

# sanity check of the  matrix and label
print('x_train' + str(X_train.shape))
print('y_train' + str(y_train.shape))
print('x_valid' + str(X_valid.shape))
print('y_valid' + str(y_valid.shape))
print('x_test' + str(X_test.shape))
print('y_test' + str(y_test.shape))


# In[138]:


# create a count vectorizer object




if feat_eng_method == 'count':
    print('test')
    count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}',max_features=max_features)
    all_samples['title']
    count_vect.fit(all_samples['title'])
    # transform the training and validation data using count vectorizer object
    xtrain_count =  count_vect.transform(X_train['title'])
    xvalid_count =  count_vect.transform(X_valid['title'])
    xtest_count =  count_vect.transform(X_test['title'])

    if extra_features:
        xtrain_count = scipy.sparse.hstack((xtrain_count,X_train[extra_features].values.astype(float)),format='csr')
        xvalid_count = scipy.sparse.hstack((xvalid_count,X_valid[extra_features].values.astype(float)),format='csr')
        xtest_count = scipy.sparse.hstack((xtest_count,X_test[extra_features].values.astype(float)),format='csr')

        
# create a word level tf-idf
if feat_eng_method == 'tfidf':
    
    tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=max_features)
    tfidf_vect.fit(all_samples['title'])
    xtrain_tfidf =  tfidf_vect.transform(X_train['title'])
    xvalid_tfidf =  tfidf_vect.transform(X_valid['title'])
    xtest_tfidf =  tfidf_vect.transform(X_test['title'])
    if extra_features:
        xtrain_tfidf = scipy.sparse.hstack((xtrain_tfidf,X_train[extra_features].values.astype(float)),format='csr')
        xvalid_tfidf = scipy.sparse.hstack((xvalid_tfidf,X_valid[extra_features].values.astype(float)),format='csr')
        xtest_tfidf = scipy.sparse.hstack((xtest_tfidf,X_test[extra_features].values.astype(float)),format='csr')


# In[139]:


# sanity chec 2 of the matrix and label
if feat_eng_method == 'count':
    print('x_train' + str(xtrain_count.shape))
    print('y_train' + str(y_train.shape))
    print('x_valid' + str(xvalid_count.shape))
    print('y_valid' + str(y_valid.shape))
    print('x_test' + str(xtest_count.shape))
    print('y_test' + str(y_test.shape))
if feat_eng_method == 'tfidf':
    print('x_train' + str(xtrain_tfidf.shape))
    print('y_train' + str(y_train.shape))
    print('x_valid' + str(xvalid_tfidf.shape))
    print('y_valid' + str(y_valid.shape))
    print('x_test' + str(xtest_tfidf.shape))
    print('y_test' + str(y_test.shape))
    print(len(y_train.value_counts()))
    print(len(y_valid.value_counts()))
    print(len(y_test.value_counts()))


# In[140]:


# save as libsvm files

if feat_eng_method == 'count':
    dump_svmlight_file(X=xtrain_count, y=y_train, f='train.libsvm')
    dump_svmlight_file(X=xvalid_count, y=y_valid, f='valid.libsvm')
    dump_svmlight_file(X=xtest_count, y=y_test, f='test.libsvm')
if feat_eng_method == 'tfidf':
    dump_svmlight_file(X=xtrain_tfidf, y=y_train, f='train.libsvm')
    dump_svmlight_file(X=xvalid_tfidf, y=y_valid, f='valid.libsvm')
    dump_svmlight_file(X=xtest_tfidf, y=y_test, f='test.libsvm')
    
    
boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/train/train.libsvm').upload_file('train.libsvm')
boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/validation/valid.libsvm').upload_file('valid.libsvm')
boto3.Session().resource('s3').Bucket(bucket).Object(prefix + '/test/test.libsvm').upload_file('test.libsvm')


# In[89]:


#save encoder
#TODO


# In[44]:


#test diffrent treshold for brands
import matplotlib.pyplot as plt
plt.figure(figsize=(20,10))
agg_brands = data.groupby(label).count()
agg_brands = agg_brands['title'].sort_values(ascending = False)
all_samples = agg_brands.sum()

lst = []
for i in range(0,2001,100):
   if agg_brands.nlargest(i) is not None:
       lst.append((i,(agg_brands.nlargest(i).sum() / all_samples))) 

plt.plot(*zip(*lst))
top_100 = agg_brands.nlargest(100)
top_200 = agg_brands.nlargest(200)
top_300 = agg_brands.nlargest(300)
top_400 = agg_brands.nlargest(400)
top_500 = agg_brands.nlargest(500)
top_600 = agg_brands.nlargest(600)
print("top 100 - " + str(top_100.sum() / all_samples))
print("top 200 - " + str(top_200.sum() / all_samples))
print("top 300 - " + str(top_300.sum() / all_samples))
print("top 400 - " + str(top_400.sum() / all_samples))
print("top 500 - " + str(top_500.sum() / all_samples))
print("top 600 - " + str(top_600.sum() / all_samples))
print(top_300.mean())
# from io import StringIO

#csv_buffer = StringIO()
# top_300.to_csv(csv_buffer)
# s3_resource = boto3.resource('s3')
# s3_resource.Object(bucket, 'top_300.csv').put(Body=csv_buffer.getvalue())


# In[ ]:




