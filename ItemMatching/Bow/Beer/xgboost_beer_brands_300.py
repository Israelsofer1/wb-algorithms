import sklearn
import xgboost as xgb
from sklearn import metrics
from sklearn.datasets import load_svmlight_file
import numpy as np
import joblib
import pickle
import sagemaker.sklearn as sklearn

def fit(DMatrix_train,DMatrix_valid ,num_round,params,load_model=None, save_model=None):
    num_round = num_round
    dtrain = DMatrix_train

    if DMatrix_valid is not None:
        dvalid = DMatrix_valid
        watchlist = [(dtrain, 'train'), (dvalid, 'valid')]
        bst = xgb.train(params, dtrain, num_round, evals=watchlist,
                         verbose_eval=1, xgb_model=load_model,
                        maximize=True)
    else:
        watchlist = [(dtrain, 'train')]
        bst = xgb.train(params, dtrain, num_round, evals=watchlist,
                        verbose_eval=1, xgb_model=load_model)
    if save_model is not None:
        bst.save_model(save_model)

    return bst

def predict(DMatrix_test, model,params):

    bst = xgb.Booster(model_file=model, params=params)
    predictions = bst.predict(DMatrix_test)
    labels = predictions.argmax(axis=1)
    probs = np.amax(predictions,axis=1)
    predictions = np.array((labels, probs)).T
    return predictions


def grade_model(labels, predictions, average, threshold = 0.5):

    joined_predictions = np.hstack((np.reshape(labels,(-1,1)), predictions))

    if threshold:
        filtered_joined_predictions = joined_predictions[joined_predictions[:,2] >= threshold]
    else:
        filtered_joined_predictions = joined_predictions

    prec, rec, fbeta_test, support = metrics.precision_recall_fscore_support(filtered_joined_predictions[:,0], filtered_joined_predictions[:, 1],
                                                                             average=average)
    print(prec)
    print(rec)
    print(fbeta_test)
    print(support)

if __name__ == '__main__':
    # load - train, validation % test sets
    dtrain = xgb.DMatrix('train.libsvm')
    dvalid = xgb.DMatrix('valid.libsvm')
    dtest = xgb.DMatrix('test.libsvm')
    testsvm = load_svmlight_file('test.libsvm')
    print('loaded')
    test_labels = testsvm[1]
    DM_test_feats = xgb.DMatrix(testsvm[0])

    # specify parameters via map
    params = {'max_depth': 6, 'eta': 0.05, 'objective': 'binary:logistic', 'verbosity':1, 'silent':1,
              'eval_metric': 'mlogloss','n_estimators':1000,'subsample':0.3}
    num_round = 600
    #model = fit(params=params, DMatrix_train=dtrain,DMatrix_valid=dvalid, num_round=num_round,save_model=True)
    predictions = predict(DM_test_feats, model = 'bow_300_27_1',params = params)
    grade_model(labels=test_labels, predictions=predictions, average='weighted',threshold=0.5)
    print('done')