import sklearn
from sklearn import metrics
from sklearn.datasets import load_svmlight_file
import numpy as np
import argparse
import os
import json
import pandas as pd
import boto3
import time

from subprocess import call
from sagemaker_containers.beta.framework import (
    content_types, encoders, env, modules, transformer, worker)
from scipy import sparse
from sklearn.model_selection import train_test_split

call("pip install xgboost".split(" "))
import xgboost as xgb
import math
from io import BytesIO, TextIOWrapper


class bow_multiclass(object):
    def __init__(self, bucket, model_name, model_version, binary):
        self.bucket = bucket
        self.model_name = model_name
        self.model_version = str(model_version)
        self.binary = binary
        self.model = None

    def filter_brands(self, data):
        # filter brand that are not in the 300 top brands/spam or ham
        if not self.binary:
            is_top300 = data['brand_id'] != 0
            return data[is_top300]
        else:
            data.loc[data['brand_id'] > 0, 'brand_id'] = 1
            return data

    def split_samples(self, all_samples, ratio, label):
        # split samples into 2 sets based on ratio, make sure that all labels
        # will be in both with the same ratio of the original data set
        list1 = pd.DataFrame(columns=all_samples.columns.values)
        list2 = pd.DataFrame(columns=all_samples.columns.values)
        super_count = all_samples[label].value_counts().rename_axis('unique_values').reset_index(name='counts')
        for index, row in super_count.iterrows():
            label_samples = all_samples.loc[all_samples[label] == np.int64(row['unique_values'])]
            split1 = len(label_samples) * ratio
            split2 = len(label_samples) * (1 - ratio)
            list1 = list1.append(label_samples.head(math.ceil(split1)))
            list2 = list2.append(label_samples.tail(math.floor(split2)))

        return list1, list2

    def load_data(self, train_dir):
        # load model from s3
        ending = 'gz'
        data = pd.DataFrame()
        for root, dirs, files in os.walk(train_dir):
            for file in files:
                if file.endswith(ending):
                    partial_df = pd.read_json(train_dir + "/" + file, compression='gzip', lines=True)
                    data = data.append(partial_df, ignore_index=True)

        data = self.filter_brands(data)
        if self.binary == 0:
            data['brand_id'] = data['brand_id'] - 1
            data['sparse'] = data.apply(
                lambda row: sparse.csr_matrix((row['data_vector']['values'][0:len(row['data_vector']['values']) - 1],
                                               ([0] * (len(row['data_vector']['indices']) - 1),
                                                row['data_vector']['indices'][
                                                0:len(row['data_vector']['indices']) - 1])),
                                              shape=(1, row['data_vector']['size'] - 1)), axis=1)
        else:
            data['sparse'] = data.apply(lambda row: sparse.csr_matrix((row['data_vector']['values'],
                                                                             ([0] * len(row['data_vector']['indices']),
                                                                              row['data_vector']['indices'])),
                                                                            shape=(1, row['data_vector']['size'])),
                                              axis=1)

        train, validation = self.split_samples(all_samples=data, ratio=0.8, label='brand_id')

        tr_lst = [x[1] for x in train['sparse'].iteritems()]
        X_train = sparse.vstack(tr_lst)
        y_train = train['brand_id'].values

        va_lst = [x[1] for x in validation['sparse'].iteritems()]
        X_val = sparse.vstack(va_lst)
        y_val = validation['brand_id'].values

        y_train = y_train.astype(int)
        y_val = y_val.astype(int)

        self.training = xgb.DMatrix(X_train, label=y_train)
        self.validation = xgb.DMatrix(X_val, label=y_val)

    def train(self, DMatrix_train, DMatrix_valid, num_round, hyperparameters, load_model=None, save_model=None):
        num_round = num_round
        dtrain = DMatrix_train

        if DMatrix_valid is not None:
            dvalid = DMatrix_valid
            watchlist = [(dtrain, 'train'), (dvalid, 'valid')]
            bst = xgb.train(hyperparameters, dtrain, num_round, evals=watchlist,
                            verbose_eval=1, xgb_model=load_model,
                            maximize=True)
        else:
            watchlist = [(dtrain, 'train')]
            bst = xgb.train(hyperparameters, dtrain, num_round, evals=watchlist,
                            verbose_eval=1, xgb_model=load_model)
        self.model = bst

        return bst

    def save_model(self, output_dir):
        self.model.save_model(output_dir + '/multi_class.model')


# functions for sagemaker scriptmode

def model_fn(model_dir):
    print('model fn :)')
    model = xgb.Booster(model_file=model_dir + '/multi_class.model')

    return model


def default_input_fn(input_data, content_type):
    """Takes request data and de-serializes the data into an object for prediction.
        When an InvokeEndpoint operation is made against an Endpoint running SageMaker model server,
        the model server receives two pieces of information:
            - The request Content-Type, for example "application/json"
            - The request data, which is at most 5 MB (5 * 1024 * 1024 bytes) in size.
        The input_fn is responsible to take the request data and pre-process it before prediction.
    Args:
        input_data (obj): the request data.
        content_type (str): the request Content-Type.
    Returns:
        (obj): data ready for prediction.
    """
    pass
    # np_array = encoders.decode(input_data, content_type)
    # return np_array.astype(np.float32) if content_type in content_types.UTF8_TYPES else np_array


def input_fn(input_body, content_type):
    print('input fn :)')
    my_json = input_body.decode('utf8').replace("'", '"')
    pd_data = pd.read_json(my_json, lines=True)
    # pd_data = pd_data.head(30000)
    if content_type == 'binary':
        pd_data['sparse'] = pd_data.apply(
            lambda row: sparse.csr_matrix((row['data_vector']['values'][0:len(row['data_vector']['values']) - 1],
                                           ([0] * (len(row['data_vector']['indices']) - 1),
                                            row['data_vector']['indices'][0:len(row['data_vector']['indices']) - 1])),
                                          shape=(1, row['data_vector']['size'] - 1)), axis=1)
    else:
        pd_data['sparse'] = pd_data.apply(
            lambda row: sparse.csr_matrix((row['data_vector']['values'], ([0] * len(row['data_vector']['indices']),
                                                                          row['data_vector']['indices'])),
                                          shape=(1, row['data_vector']['size'])), axis=1)
    lst = [x[1] for x in pd_data['sparse'].iteritems()]
    X = sparse.vstack(lst)
    return X


def default_predict_fn(input_data, model):
    """A default predict_fn for PyTorch. Calls a model on data deserialized in input_fn.
    Args:
        input_data: input data (Numpy array) for prediction deserialized by input_fn
        model: SciKit-Learn model loaded in memory by model_fn
    Returns: a prediction
    """
    output = model.predict(input_data)

    return output


def predict_fn(data, model):
    print('predict fn :)')
    start = time.time()
    data = xgb.DMatrix(data)
    end = time.time()
    print(end - start)
    predictions = model.predict(data)
    if predictions.ndim == 2:
        labels = predictions.argmax(axis=1)
        probs = np.amax(predictions, axis=1)
        predictions = np.array((labels, probs)).T
    end2 = time.time()
    print(end2 - end)
    return predictions


def output_fn(prediction, accept):
    print('output fn :)')
    a = worker.Response(encoders.encode(prediction, accept), accept, mimetype=accept)
    return a


if __name__ == '__main__':
    model_name = 'beer'
    model_ver = 0.1
    bucket = 'wb_ds_models'
    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.

    # input data, model directories, hyperparameters and number of rounds
    parser.add_argument('--model_dir', type=str)
    parser.add_argument('--output_dir', type=str, default=os.environ.get('SM_MODEL_DIR'))
    parser.add_argument('--training', type=str, default=os.environ.get('SM_CHANNEL_TRAINING'))
    parser.add_argument('--max_depth', type=int, default=os.environ.get('SM_HP_MAX_DEPTH'))
    parser.add_argument('--eta', type=float, default=os.environ.get('SM_HP_ETA'))
    parser.add_argument('--objective', type=str, default=os.environ.get('SM_HP_OBJECTIVE'))
    parser.add_argument('--num_class', type=int, default=os.environ.get('SM_HP_NUM_CLASS'))
    parser.add_argument('--verbosity', type=int, default=os.environ.get('SM_HP_VERBOSITY'))
    parser.add_argument('--silent', type=int, default=os.environ.get('SM_HP_SILENT'))
    parser.add_argument('--eval_metric', type=str, default=os.environ.get('SM_HP_EVAL_METRIC'))
    parser.add_argument('--n_estimators', type=int, default=os.environ.get('SM_HP_N_ESTIMATORS'))
    parser.add_argument('--subsample', type=float, default=os.environ.get('SM_HP_SUBSAMPLE'))
    parser.add_argument('--num_round', type=int, default=os.environ.get('SM_HP_NUM_ROUND'))
    parser.add_argument('--binary', type=int, default=os.environ.get('SM_HP_BINARY'))
    parser.add_argument('--model_name', type=str, default=os.environ.get('SM_HP_MODEL_NAME'))
    parser.add_argument('--model_ver', type=float, default=os.environ.get('SM_HP_MODEL_VER'))
    parser.add_argument('--bucket', type=str, default=os.environ.get('SM_HP_BUCKET'))

    args, _ = parser.parse_known_args()

    num_round = args.num_round
    if args.binary == 0:
        hyperparameters = {'max_depth': args.max_depth, 'eta': args.eta, 'objective': args.objective,
                           'num_class': args.num_class, 'verbosity': args.verbosity, 'silent': args.silent,
                           'eval_metric': args.eval_metric, 'n_estimators': args.n_estimators,
                           'subsample': args.subsample}
    else:
        hyperparameters = {'max_depth': args.max_depth, 'eta': args.eta, 'objective': args.objective,
                           'verbosity': args.verbosity, 'silent': args.silent, 'eval_metric': args.eval_metric,
                           'n_estimators': args.n_estimators, 'subsample': args.subsample}
    multiclass = bow_multiclass(bucket=args.bucket, model_version=args.model_ver, model_name=args.model_name, binary=args.binary)
    multiclass.load_data(args.training)
    model = multiclass.train(DMatrix_train=multiclass.training, DMatrix_valid=multiclass.validation,
                             hyperparameters=hyperparameters, num_round=num_round)

    multiclass.save_model(args.output_dir)

    ## pycharm run

    # local check
#     num_round = 1

#     hyperparameters = {'max_depth': 6, 'eta': 0.1, 'objective': 'binary:logistic', 'verbosity': 1,
#                        'silent': 1,
#                        'eval_metric': 'error', 'n_estimators': 1000, 'subsample': 0.3}
#     model_name = 'beer'
#     model_ver = 0.1
#     bucket = 'wb_ds_models'
#     parser = argparse.ArgumentParser()
#     multiclass = bow_multiclass(bucket=model_name, model_version=model_ver, model_name=model_name, binary=1)
#     multiclass.load_data(train_dir="train")
#     model = multiclass.train(DMatrix_train=multiclass.training, DMatrix_valid=multiclass.validation,
#                               hyperparameters=hyperparameters, num_round=num_round)

