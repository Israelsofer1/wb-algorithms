from spell_check import SymSpell


def spell_model_name(model_name):
    """
        Creates a standard name for all spell checkers
    :param model_name: Name the model the checker is made for
    :return: The name under which the checker is saved
    """
    return 'spell_%s.p' % model_name


def create_spellchecker(data, model_name, min_word_count):
    """

    :param data: List of strings
    :param model_name: Name the model the checker is for
    :return: True if success
    """
    # print("Do it locally! Not in Spark")
    spell = SymSpell(count_threshold=min_word_count)
    corpus = spell.prepare_corpus(data)
    spell.learn(corpus)
    spell.save(model_name)
    return spell


def global_spell_check(min_words):
    from Common.DAL.Connectors.SQL import DbExplorer
    with DbExplorer('PROD') as db:
        data = db.dbreader("""
        select title from pos_bars_products bp where category_id is not null"""
                           )
    data = [unicode(v['title']) for v in data]
    create_spellchecker(data, 'spell_checker', min_words)


def beer_spell_check(min_words):
    from Common.DAL.Connectors.SQL import DbExplorer
    with DbExplorer('PROD') as db:
        data = db.dbreader("""
        select title from pos_bars_products bp join bars a on a.id = bp.bar_id 
        where category_id = 1 and a.country IN('Canada', 'Australia', 'Argentina', 'Brazil')"""
                           )
    data = [unicode(v['title']) for v in data]
    create_spellchecker(data, 'spell_checker', min_words)


if __name__ == '__main__':
    global_spell_check(10)