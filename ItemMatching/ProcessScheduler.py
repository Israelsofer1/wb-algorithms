import boto3
import datetime
import json
import base64

# Choosing the resource from boto3 module
sqs = boto3.resource('sqs')
lambda_client = boto3.client('lambda')
job_name = "matching_job" + datetime.now()
sm = boto3.client('sagemaker', region_name='eu-west-1')
model_name = "meal-flow-classifier5-model"


def run_lambda(name, payload):
    print("start running lambda")
    response = lambda_client.invoke(FunctionName=name,
                                    InvocationType='RequestResponse',
                                    LogType='Tail',
                                    Payload=json.dumps(payload).encode('utf-8'))
    print("Returned Payload: " + response['Payload'].read().decode())
    log_result = base64.b64decode(response["LogResult"]).decode('utf-8')
    print("Returned logs:" + log_result)
    print("Lambda running end")
    return response


def get_payload_for_lambda(new_file):
    step_name = new_file.split('/')[0]
    payload = {
        "jobs_group_name": "matching_job",
        "config": [
            {
                "Name": "matching_job",
                "Scheduled": "Daily",
                "PyScript": "matching_job.py",
                "ZipFiles": ["s3://dev-stas/zip/jobs.zip"],
                "ParametersMap": {
                    "StepName": step_name
                }
            }
        ]
    }
    return payload


def run_sage_mamker_step():

    response = sm.create_transform_job(
        TransformJobName=job_name,
        ModelName=model_name,
        MaxConcurrentTransforms=10,
        MaxPayloadInMB=10,
        BatchStrategy='MultiRecord',

        TransformInput={
            'DataSource': {
                'S3DataSource': {
                    'S3DataType': 'S3Prefix',
                    'S3Uri': 's3://wb-ds-models/analytics-sagemaker-dev/meal-flow/test.csv'
                }
            },
            'ContentType': 'text/csv',
            'CompressionType': 'None',
            'SplitType': 'Line'
        },
        TransformOutput={
            'S3OutputPath': 's3://dev-matching-flow/sage',

        },
        TransformResources={
            'InstanceType': 'ml.m4.xlarge',
            'InstanceCount': 1
        }
    )

    print(response)
    response = sm.describe_transform_job(
        TransformJobName=job_name
    )
    return response


if __name__ == '__main__':

    # Get the queue named test
    # Define queue per model?

    queue = sqs.get_queue_by_name(QueueName='dev-matching-flow')

    while True:
        messages = queue.receive_messages(WaitTimeSeconds=5, MaxNumberOfMessages=10)
        for message in messages:
            print("Message received: {0}".format(message.body))
            try:
                new_file = json.loads(message.body)["Records"][0]["s3"]["object"]["key"]
                print(new_file)
                # some logic according to the file name and step peyload createion
                step_name = new_file.split('/')[0]
                if step_name == 'matching':
                    response = run_sage_mamker_step()
                    status = response['TransformJobStatus']
                    print("Status: " + status)
                else:
                    payload = get_payload_for_lambda(new_file)
                    print(payload)
                    # my test should be replaced by real lambda according to env
                    # etl-stg-provision-scheduled-jobs-run-emr-cluster or etl-dev-provision-scheduled-jobs-run-emr-cluster
                    # or etl-prod-provision-scheduled-jobs-run-emr-cluster
                    run_lambda('mytest', payload)

                message.delete()
            except Exception as e:
                print(e)
                message.change_visibility(0)  #  return message back to queue


