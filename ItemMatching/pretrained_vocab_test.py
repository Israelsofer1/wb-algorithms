from mxnet.contrib import text
from ast import literal_eval
import os
import pandas as pd
import numpy as np


def find_file(root_path, ending):
    # if root_path == '':
    #     return os.path.join(root_path, file_name)
    for root, dirs, files in os.walk(root_path):
        for file in files:
            if file.endswith(ending):
                return os.path.join(root, file)


def to_arrays(dframe):
    out = {}
    for k in dframe:
        if k[-7:] == '_string':
            out[k[:-7]] = np.array(list(dframe[k].apply(lambda x: np.array(literal_eval(x))).values))
        else:
            out[k] = dframe[k].values
    return out


def load_data(base_path):
    data = pd.read_csv(find_file(os.path.join(base_path, 'title.gzip'), ending=".csv.gz"),
                       compression='gzip')
    data = to_arrays(data)
    return {data['word'][i]: data['vector'][i] for i in range(data['word'].shape[0])}


def main():
    load_data(base_path='v0')


if __name__ == '__main__':
    main()
