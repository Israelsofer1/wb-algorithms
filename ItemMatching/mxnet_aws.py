# coding=utf-8
import logging

from subprocess import call

import io
import gzip
import re
import ast
import numpy as np
import os
import json
import struct
import warnings
import boto3
import math

call("pip install keras".split(" "))
call("pip install tensorflow".split(" "))
from keras import backend as K
from keras import optimizers, regularizers, losses, activations, utils
from keras.backend.common import epsilon
from keras.models import Model, Sequential, load_model, model_from_json
from keras.layers import Input, Dense, Dropout, LSTM, Activation, Embedding
from keras.callbacks import EarlyStopping, ModelCheckpoint, Callback
from keras.layers.merge import Concatenate, Add
from keras.callbacks import EarlyStopping, ModelCheckpoint

call("pip install pandas".split(" "))
import pandas

import pandas as pd
from ast import literal_eval
import tensorflow as tf


# Define the network according to the meta data
def model_cat_generic(meta, hyperparameters, custom_loss=None):
    # Set different activations and loss, depending on the field type
    # activations = {'single': 'softmax', 'multi': 'sigmoid'}
    # loss_funcs = {'single': 'categorical_crossentropy', 'multi': 'binary_crossentropy'}

    # Treating text input

    # TODO: change the number 10 to config read - like meta['title'].size

    txt_input = Input(shape=(5, 75), name='title')
    # embd = Embedding(output_dim=200, input_dim=len(meta.features['title'].i2v),
    #                 input_length=(meta.features['title'].text_max_len,))(txt_input)

    x = LSTM(200, activation='relu', dropout=0.5, recurrent_dropout=0.5, bias_initializer='glorot_uniform',
             name='LSTM')(txt_input)

    # Add the non-textual input for every other independent feature
    non_text_input = []
    for f, v in meta.items():
        if v['direction'] == 'input' and v['type'] != 'txt':
            non_text_input.append(Input(shape=(v['max'],), name=f))

    # Combine input data
    if non_text_input:
        x = Concatenate()([x] + non_text_input)

    # a layer instance is callable on a tensor, and returns a tensor
    x = Dense(400, activation='relu', name='shared_dense')(x)
    x = Dropout(0.5, name='shared_drop')(x)

    # For every dependent feature we have a separate output layer, which we concatenate for the optimizer
    # Basic - Either Final output or parent fields
    depend_no_hierarchy = [f for f, v in meta.items() if v['direction'] == 'output' and v['parent'] is None]

    basic_xs = {n: Dense(int(0.5 * meta[n]['max']), activation='relu', name="%s_dense" % (n))(x)
                for n in depend_no_hierarchy}

    basic_predictions = {f: Dense(meta[f]['max'], activation='softmax', name=str(f))(fx)
                         for f, fx in basic_xs.items()}

    # Child - Fields dependent on the parent
    depend_with_hierarchy = [f for f, v in meta.items() if v['direction'] == 'output' and v['parent'] is not None]

    child_xs = {n: Dense(int(0.5 * meta[n]['max']), activation='relu', name="%s_dense" % (n)
                         )(Concatenate()([x] + [basic_predictions[meta[n]['parent']]]))
                for n in depend_with_hierarchy}

    child_predictions = {f: Dense(meta[f]['max'],
                                  activation='softmax',
                                  name=str(f))(fx)
                         for f, fx in child_xs.items()}
    basic_predictions.update(child_predictions)

    all_predictions = [pred for f, pred in basic_predictions.items()]
    basic_xs.update(child_xs)

    # This defines the model itself and its optimizers
    model = Model(inputs=[txt_input] + non_text_input, outputs=all_predictions)
    op = optimizers.Adam(lr=hyperparameters['learning_rate'], epsilon=(10 * hyperparameters['learning_rate']),
                         amsgrad=True)
    if custom_loss is None:
        custom_loss = {str(f): 'categorical_crossentropy' for f in basic_xs}

    model.compile(optimizer=op
                  , metrics=['accuracy']
                  , loss=custom_loss
                  )
    return model


def find_file(root_path, ending):
    for root, dirs, files in os.walk(root_path):
        for file in files:
            if file.endswith(ending):
                return os.path.join(root, file)


def take_index(data, compare_array, val):
    """
    Take from data only the values in the indexes where they correspond to the values in compare_array
    """
    return {
        k: np.take(v, np.where(compare_array == val), axis=0, out=None, mode='raise')[0] if not isinstance(v, list)
        else np.array([np.take(l, np.where(compare_array == val), axis=0, out=None, mode='raise')[0] for l in v])
        for k, v in data.items()
    }


def load_data(base_path, model_name, version, fold=None, train_prop=0.85, limit=None, limit_output=None):
    model_path = os.path.join(base_path, str(model_name), str(version))  # , str(version)

    # Load meta data from the config file
    with open(os.path.join(base_path, 'matching_config.json'), "r") as file:
        meta = json.load(file)[model_name]

    # Load the file that saves the mapping of the data
    with open(os.path.join(model_path, 'Train', 'pre_process_mapping', 'label_mapping_NN.json'), "r") as f:
        map = json.load(f)

        # Limit the number of outputs to a closed list, regardless of the data
        if limit_output is not None:
            meta = {k: v for k, v in meta.items() if not (k not in limit_output and meta[k]['direction'] == 'output')}

        # Attach size to each parameter in the config file
        for k in meta:
            if meta[k]['type'] in ('single', 'multi'):
                meta[k]['max'] = len(map[k + '_index'])
            else:
                meta[k]['max'] = 1

    # Read the w2v
    path = find_file(os.path.join(model_path, 'Train', 'pre_process', 'pre_process_w2v.gzip'), ending=".json.gz")
    print('The data file loaded from:', path)
    vectors = pd.read_json(path, lines=True, compression='gzip')
    vectors = vectors.rename(columns={"title_vector": "title"})
    # Read the labels
    path = find_file(os.path.join(model_path, 'Train', 'pre_process', 'pre_process_NN.gzip'), ending=".json.gz")
    print('The data file loaded from:', path)
    labels = pd.read_json(path, lines=True, compression='gzip')

    # Filter out errors
    vectors = vectors[vectors['title'].apply(lambda x: len(x)) == 10]
    #     vectors = vectors[vectors["bar_product_id"].str.contains('[A-Za-z]', regex=True) == False]
    #     labels = labels[labels["bar_product_id"].str.contains('[A-Za-z]', regex=True) == False]
    vectors['bar_product_id'] = vectors['bar_product_id'].astype('int64')
    labels['bar_product_id'] = labels['bar_product_id'].astype('int64')

    #     print(vectors.head(5))
    #     print(labels.head(5))

    # Join them
    labels = labels.set_index('bar_product_id')
    vectors = vectors.set_index('bar_product_id')
    data = pd.merge(labels, vectors, left_index=True, right_index=True)
    data = data.reset_index()
    # data = labels.join(vectors, on='bar_product_id', how='inner', lsuffix='_vec', rsuffix='_dat').dropna(how='all')

    #     print(data)

    data = data[:limit]
    data = data[[k for k in meta]]

    print('Processing data')

    # Processing the input
    for column in meta:
        if column == 'title':
            # literal_eval
            data[column] = data[column].apply(lambda title: np.array(title))
        elif column == 'country_id':
            data[column] = [ast.literal_eval(t) for t in data[column].values]
            meta[column]['max'] = len(data[column][0])
        elif meta[column]['type'] == 'single':
            from keras.utils import to_categorical
            data[column] = data[column].apply(
                lambda col_value: to_categorical(col_value[0], num_classes=meta[column]['max']))
            meta[column]['max'] = len(data[column][0])
        else:
            data[column] = data[column].apply(lambda col_value: col_value[0])
    # data = data.T.to_dict().values()
    # for k, v in data.items():
    #     data[k] = np.array(v)

    print('Creating dict')

    # Joining it all together
    data = {k: np.array([np.array(r) for r in data[k]]) for k in data.columns}
    # data.update({'title': np.array(titles)})

    if limit is not None:
        data = {k: v[:limit] for k, v in data.items()}

    print('Splitting data')

    # Split into train and eval data with 1 of 2 methods
    if fold is not None:
        # Split into train and validation using folds
        fold_indeces = np.where(data['fold'] == fold)
        dtrain = {k: np.take(data[k], fold_indeces, axis=0, out=None, mode='raise')[0] for k, v in data.items()}
        other_indeces = np.where(data['fold'] != fold)
        deval = {k: np.take(data[k], other_indeces, axis=0, out=None, mode='raise')[0] for k, v in data.items()}
    else:
        # Train test split using indexes
        size = data[list(data.keys())[0]].shape[0]
        idx = np.random.choice([1, 0], size=size, p=[train_prop, 1 - train_prop])
        dtrain = take_index(data, idx, val=1)
        deval = take_index(data, idx, val=0)

    # Output tuples (train, validation) data for dependent and independent features
    indep = [{k: v for k, v in dct.items() if meta[k]['direction'] == 'input'} for dct in (dtrain, deval)]
    dep = [{k: v for k, v in dct.items() if meta[k]['direction'] == 'output'} for dct in (dtrain, deval)]
    # dep = [{'%s_label' % k: v for k, v in dct.items() if meta[k]['direction'] == 'output'} for dct in (dtrain, deval)]

    print('Finished reading data')

    x_train = indep[0]
    x_test = indep[1]
    y_train = dep[0]
    y_test = dep[1]
    return x_train, y_train, x_test, y_test, meta


# def load_model(path, mname):
#     sym, arg_params, aux_params = mx.model.load_checkpoint(prefix=os.path.join(path, mname), epoch=0)
#     return sym, arg_params, aux_params


def train(model, check_path, x_train, y_train, x_test, y_test, hyperparameters):
    early = EarlyStopping(monitor='loss', min_delta=0.0001, patience=hyperparameters['stop_rounds'])
    check = ModelCheckpoint(filepath=check_path,
                            period=1, verbose=1, monitor='loss', save_best_only=True)
    model.fit(x=x_train,
              y=y_train,
              batch_size=hyperparameters['batch'],
              epochs=hyperparameters['epochs'],
              verbose=2,
              validation_data=(x_test, y_test))

    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    return model


# # FIX FIX FIX
# def get_train_context(num_cpus, num_gpus):
#     if num_gpus > 0:
#         return [mx.gpu(i) for i in range(num_gpus)]
#     return mx.cpu()


# ---------------------------------------------------------------------------- #
# Request handlers for Infer models                                           #
# ---------------------------------------------------------------------------- #


def predict_fn(input_data, model):
    res = model.predict(input_data)
    print(res)
    return res


def model_fn(model_dir):
    model = load_model("%s/model.h5" % model_dir)
    #     # Load Keras model
    #     # Runs 1 time when starting service
    #     json_file = open('%s/model.json' % model_dir, 'r')
    #     model = json_file.read()
    return model


def save_model(model, output_dir):
    model.save("%s/model.h5" % output_dir)


#     model_json = model.to_json()
#     with open("%s/model.json" % output_dir, "w") as json_file:
#         json_file.write(model_json)


def input_fn(input_data, content_type):
    #     input_data = '[' + str(input_data).replace("}\n{", "}\n,{") + ']'  # .replace("'", '"')
    #     print(input_data)
    parsed = json.loads(input_data)

    if not isinstance(parsed, dict):
        print('Solving double-string issue')
        parsed = ast.literal_eval(parsed)

    if not isinstance(parsed[0], np.ndarray):
        print('recasting to NDarray')
        # In case the input is a double string
        for k, v in parsed.items():
            parsed[k] = np.array(v)
    return parsed
    # model.bind(for_training=False, data_shapes=[(k, v.shape) for k, v in parsed.items()])
    # return mx.io.NDArrayIter(data=parsed)


if __name__ == '__main__':

    hyperparameters = {"learning_rate": 0.001, 'batch': 10000,
                     'mname': 'beer', 'version': '0.1', 'embedding': True,
                     'epochs': 1, 'early_stop_rounds': 1}

    x_train, y_train, x_test, y_test, meta = load_data('', 'beer', '0.1')
    model = model_cat_generic(meta, hyperparameters)

    col = 'title'
    for f in (x_train, x_test):
        print(f[col][0])

    train(model, '', x_train, y_train, x_test, y_test, hyperparameters)

    save_model(model, '')

    # data = pd.read_json('pos_trainset2.json.gz', lines=True, compression='gzip')
    #
    # train(channel_input_dirs={'data': 'model_versioning'}, hosts=['PROD'], current_host='PROD',
    #       # hyperparameters={"learning_rate": 0.001, 'batch': 1, 'mname': 'beer',
    #       #                  'version': '0.1', 'limit': 50, 'limit_labels': ['brand_id']},
    #       hyperparameters={"learning_rate": 0.01, 'batch': 100, 'limit_labels': ['brand_id'],
    #                        'mname': 'beer', 'version': '0.1'},
    #       num_cpus=3, num_gpus=0)

    # ctx = mx.cpu()
    # sym, arg_params, aux_params = mx.model.load_checkpoint('model', 0)
    # mod = mx.mod.Module(symbol=sym, context=ctx, data_names=['title', 'country_id', 'price'],
    #                     label_names=['superbrand_id_label',	'brand_id_label', 'volume_id_label' 'serving_type_id_label', 'type_id_label'])
    #
    # data = load_data(base_path='model_versioning', mname='beer', version='0.1')
    # data['price'] = data['price'].reshape((-1, 1))
    # mod.bind(for_training=False, data_shapes=[(k, v.shape) for k, v in data.items()])
    # mod.set_params(arg_params, aux_params, allow_missing=True)
