from pyspark.sql import SparkSession
from ..constant import const
from pyspark.sql.types import FloatType
from pyspark.sql.functions import concat, lit, udf, greatest, first, col
from math import sqrt, erf
from datetime import datetime
from volume_price_constants import Const as VolumeConst
from volume_price_constants import Schemas as VolumeSchemas


def unpack_parameters(df):
    for i in range(len(VolumeConst.PARAMETERS)):
        df = df.withColumn(VolumeConst.PARAMETERS[i], col('parameters')[i])
        df = df.withColumn(VolumeConst.PARAMETERS[i] + 'delta', col('parameters_delta')[i])
    df = df.drop('parameters', 'parameters_delta')
    return df


def read(spark, path):
    df = spark.read.format("csv").option("header", "true").option('inferSchema', 'true').load(path + '/' + const.INPUT_FOLDER)
    df = df.withColumnRenamed('country_id', 'country')
    df = df.withColumn('temp', df['brand_id'].cast('int')).drop('brand_id')
    df = df.withColumnRenamed('temp', 'brand_id')
    df = df.drop('volume_id')
    bars_df = spark.sql("""select id as bar_product_id, bar_id from stg.pos_bars_products""")  #TODO: check bars use here vs enrich
    return df.join(bars_df, on='bar_product_id')


def get_model(spark, path):
    df = spark.read.json(path).select('beer_brand_id', 'beer_serving_type_id', 'level_type', 'level_value', 'parameters',
                                 'parameters_delta', 'width', 'beer_volume')
    df = df.withColumn('temp', df['beer_volume'].cast('float')).drop('beer_volume')
    df = df.withColumnRenamed('temp', 'volume_id')
    df = df.withColumnRenamed('beer_serving_type_id', 'serving_type_id')
    df = df.withColumnRenamed('beer_brand_id', 'brand_id')  # TODO: replace the renames when writing the files
    return df


def normal_cdf(mu, sigma, x, w):
    try:
        return (0.5 * (1. + erf(((x + w) - mu)/(sigma * sqrt(2.))))) - (0.5 * (1. + erf(((x - w) - mu)/(sigma * sqrt(2.)))))
    except TypeError:
        return -1.


def enrich_products(spark, products):
    bars = spark.sql("""select id as bar_id, """ + ', '.join(VolumeConst.LEVELS) + """ from stg.bars b join stg.pos_bars_config bc on b.id = bc.bar_id""")
    return products.join(bars, on='bar_id')


def get_available_volumes(spark, products, model):
    sc = spark.sparkContext
    volumes = spark.createDataFrame(sc.emptyRDD(), VolumeSchemas.fit_volumes)
    for level in VolumeConst.LEVELS:
        level = level.split('.')[1]
        temp_volumes = products.withColumn('key',
                                       concat(col('brand_id'), lit(','), col('serving_type_id'), lit(','), lit(level),
                                              lit(','), col(level))).select('bar_product_id', 'key')
        temp_model = model.withColumn('key',
                                      concat(col('brand_id'), lit(','), col('serving_type_id'), lit(','), col('level_type'),
                                             lit(','),
                                             col('level_value'))).select('key', 'volume_id')
        volumes = volumes.union(temp_volumes.join(temp_model, on='key', how='left').drop('key'))
    volumes = volumes.groupBy('bar_product_id', 'volume_id').agg(first('bar_product_id').alias('bar_product_id_temp'), first('volume_id').alias('volume_id_temp')).drop('bar_product_id_temp', 'volume_id_temp')
    products = volumes.join(products, on='bar_product_id', how='left').drop('key')
    products = products.dropna(subset='volume_id')
    return products


def get_results(products, model, level):
    products = products.join(model, on='key', how='left')
    for x in ['mu', 'sigma', 'price', 'width']:  # TODO: find more elegant solution
        products = products.withColumn('temp' + x, products[x].cast('float')).drop(x)
        products = products.withColumnRenamed('temp' + x, x)
    normal_cdf_udf = udf(normal_cdf, FloatType())
    products = products.withColumn(str(level + '_prob'), normal_cdf_udf(col('mu'), col('sigma'), col('price'), col('width')))
    return products


def ensemble(products):
    columns = [x.split('.')[1] + '_prob' for x in VolumeConst.LEVELS]
    products = products.withColumn('volume_id_prob', greatest(*columns))
    products = products.withColumn('keep', col('volume_id_prob') == greatest(*columns)).drop(*columns)
    return products.filter(col('keep') == True)


def format_results(df, version, is_cross_validation, run_id):
    df = df.withColumn('beer_type_id_prob', lit(0.))
    df = df.withColumn('super_brand_id_prob', lit(0.))
    df = df.withColumn('version', lit(version))
    df = df.withColumn('is_cross_validation', lit(is_cross_validation))
    df = df.withColumn('run_date', lit(datetime.now().date()))
    df = df.withColumn('run_id', lit(run_id))
    df = df.withColumn('model_type', lit('volume_price'))
    df = df.withColumn('created_at', lit(datetime.now()))
    return df


def write_results(spark, df, flow, output_table, path):
    if flow == const.FLOW_PREDICT:
        df.repartition(1).registerTempTable('results')
        return spark.sql("""insert overwrite table stg.""" + output_table + """ select bar_product_id, brand_id,
                            brand_id_prob, super_brand_id, super_brand_id_prob, volume_id, volume_id_prob,
                            serving_type_id, serving_type_id_prob, beer_type_id, beer_type_id_prob, version,
                            is_cross_validation, created_at, run_date, run_id, model_type from results""")
    elif flow == const.FLOW_TEST:
        df.drop('bar_id', 'title', 'pos_vendor', 'price', 'train', 'category_id', 'country', 'country_id', 'state_id', 'pos_category_name', 'unique_id', 'city').repartition(1).write.csv(path=path + '/' + const.OUTPUT_FOLDER + '/volume_price_results.csv/', compression="gzip",
                                    mode='overwrite', header=True)


def run_predict(spark, flow_path, model_path, bucket, model, version, flow, date, run_id, output_table, is_cross):
    print("%s - volume price - %s - make predictions - started" % (datetime.now(), flow))
    print("%s - volume price - %s - make predictions - get products" % (datetime.now(), flow))
    products = read(spark, flow_path)
    print("%s - volume price - %s - make predictions - enrich products" % (datetime.now(), flow))
    products = enrich_products(spark, products)
    print("%s - volume price - %s - make predictions - get model" % (datetime.now(), flow))
    model = get_model(spark, model_path)
    print("%s - volume price - %s - make predictions - unpack parameters" % (datetime.now(), flow))
    model = unpack_parameters(model)
    print("%s - volume price - %s - make predictions - get available volumes" % (datetime.now(), flow))
    products = get_available_volumes(spark, products, model)
    for level in VolumeConst.LEVELS:
        level = level.split('.')[1]
        print("%s - volume price - %s - make predictions - build keys level: %s" % (datetime.now(), flow, level))
        products = products.withColumn('key',
                                       concat(col('brand_id'), lit(','), col('serving_type_id'), lit(','),
                                              col('volume_id'), lit(','), lit(level), lit(','), col(level)))
        temp_model = model.withColumn('key', concat(col('brand_id'), lit(','), col('serving_type_id'), lit(','),
                                                    col('volume_id'), lit(','), col('level_type'), lit(','),
                                                    col('level_value')))\
                                    .drop('brand_id', 'serving_type_id', 'level_type', 'level_value', 'volume_id')

        print("%s - volume price - %s - make predictions - get results level: %s" % (datetime.now(), flow, level))
        products = get_results(products, temp_model, level).drop('mu', 'mudelta', 'sigma', 'sigmadelta', 'amplitude', 'amplitudedelta', 'width') #TODO: list comprehension based on constants
        products = products.fillna(0., subset=['volume_id', level + '_prob']).drop('key')
    # TODO: consider removing filter and add if 0 - pass

    products = products.filter((col('volume_id') != 0.) & ((col('country_id_prob') != 0.) | (col('state_id_prob') != 0.) | (col('city_prob') != 0.) | (col('pos_vendor_prob') != 0.)))
    # products = products.filter(' | '.join(["col(" + x + ') != 0.' for x in VolumeConst.LEVELS]))
    print("%s - volume price - %s - make predictions - ensemble levels" % (datetime.now(), flow))
    results = ensemble(products)
    results = results.filter((col('volume_id_prob') != 0.))
    print("%s - volume price - %s - make predictions - format & write results" % (datetime.now(), flow))
    results = format_results(results, version, is_cross, run_id)
    write_results(spark, results, flow, output_table, flow_path)
    print("%s - volume price - %s - make predictions - finished" % (datetime.now(), flow))
    return 'predict_SUCCESS'


if __name__ == "__main__":



    spark = SparkSession \
        .builder \
        .appName("VolumePrice - predict") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()

    model_path = 'output/results/flat'
    products_path = 'products_predict_enriched.csv'
    products_path = 'predict_products_enriched_test.csv'
    bucket = 'dev-matching_steps'
    model = 'VolumePrice'
    version = 0.1
    flow = 'predict'
    date = '2019-04-01'
    run_id = '432'
    output_table = 'pos_beer_suggestions'
    is_cross = 0

    run_predict(spark, products_path, model_path, bucket, model, version, flow, date, run_id, output_table, is_cross)
