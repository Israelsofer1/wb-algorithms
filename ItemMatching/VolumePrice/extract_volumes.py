from volume_price_constants import Const as VolumeConst
from volume_price_constants import Schemas as VolumeSchemas
from pyspark.sql import SparkSession
from pyspark.sql.functions import abs, col, lit, lag, least
from pyspark.sql.window import Window
from datetime import datetime


def get_products(spark, table, level, test=False):
    if test:
        return spark.read.format('csv').option('header', 'true').option('inferSchema', 'true').load('products.csv')
    else:
        if level == 'pos_vendor':
            is_bulk = '1'
        else:
            is_bulk = '0'
        # TODO:  find more elegant solution to the filter (implement in dataframe - see todo below)
        return spark.sql("""select """ + level + """, brand_id, volume, serving_type_id, frequency, 
                                    rank() over(partition by brand_id, serving_type_id, """ + level + """ order by frequency desc) 
                                            as frequency_rank,
                                    rank() over(partition by brand_id, serving_type_id, """ + level + """ order by volume asc) 
                                            as volume_rank, 
                                    rank() over(partition by brand_id, serving_type_id, """ + level + """ order by brand_type_vol_sales desc)
                                            as sales_rank, 
                                    brand_type_vol_sales,
                                    sum(brand_type_vol_sales) over(partition by brand_id, serving_type_id, """ + level + """ order by brand_type_vol_sales desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) 
                                            as cume_brand_type_sales,
                                    sum(brand_type_vol_sales) over(partition by brand_id, serving_type_id, """ + level + """) 
                                            as brand_type_sales
                            from	(select pb.brand_id, pb.volume, pp.serving_type_id, """ + table + """.""" + level +""",
                                            count(*) as frequency, 
                                            sum(product_sales) as brand_type_vol_sales
                                    from stg.pos_bars_products bp 
                                    join (select oi.bar_product_id, 
                                                SUM(pos_l1.sales_before_tax) as product_sales 
                                          from stg.pos_l1 join stg.pos_orders_items oi on pos_l1.order_item_id = oi.id 
                                          where pos_l1.nd > date_sub(now(), """ + VolumeConst.SALES_PERIOD + """) 
                                          group by bar_product_id) sales on bp.id = sales.bar_product_id 
                                    join stg.pos_products_brands pb ON bp.product_id = pb.product_id 
                                    join stg.pos_products pp on bp.product_id = pp.id 
                                    join stg.bars b on bp.bar_id = b.id
                                    join stg.pos_bars_config bc on b.id = bc.bar_id 
                                    where bp.category_id =1 AND bp.product_id != 0
                                            and bp.id in (select bp.id 
                                                        from stg.pos_bars_products bp 
                                                        join stg.pos_products_brands pb on bp.product_id = pb.product_id
                                                        group by bp.id  HAVING COUNT(DISTINCT pb.id) = 1)
                                            and b.is_bulk = """ + is_bulk + """
                                            and """ + table + """.""" + level + """ != 0
                                            and """ + table + """.""" + level + """ is not null
                                    group by pb.brand_id, pb.volume, pp.serving_type_id, """ + table + """.""" + level + """) a""")

# TODO: change query to get one raw dataframe and aggregate in enrich_data + move
# TODO: consider taking (adding) the sales qty for each brand+size instead of pos_bar_product qty


def clean_enrich_data(df, level):

    # should contain agg calculations that are currently in get products query

    df = df.withColumn('cume_sales_percent', col('cume_brand_type_sales')/col('brand_type_sales'))
    w = Window().partitionBy(['brand_id', 'serving_type_id', level]).orderBy(col('cume_sales_percent'))
    df = df.select("*", lag('cume_sales_percent').over(w).alias('previous_cume_sales_percent'))\
                    .fillna(0., 'previous_cume_sales_percent')
    volumes = df.filter(((least(df['cume_sales_percent'], df['previous_cume_sales_percent'])) < VolumeConst.CUME_SALES_THRESHOLD)
                      & (df['brand_type_vol_sales'] > VolumeConst.SALES_THRESHOLD))
    return volumes


def check_adjacent_bins(volumes, level, n_bins_rank_to_check):  # TODO: make process iterative?
    for n in range(1, n_bins_rank_to_check+1):
        for i in [VolumeConst.RIGHT, VolumeConst.LEFT]:
            suffix = '_' + str(i*n)
            col_list = [c + suffix for c in volumes.columns]
            volumes_temp = volumes.toDF(*col_list)                   # TODO select required columns only
            volumes = volumes.join(volumes_temp, on=[col('brand_id') == col('brand_id' + suffix),
                                                     col('serving_type_id') == col('serving_type_id' + suffix),
                                                     (col('volume_rank') + i*n) == col('volume_rank' + suffix),
                                                     (col(level)) == col(level + suffix)], how='left')
            volumes = volumes.filter((col('frequency') > col('frequency' + suffix))
                                 & (abs(col('volume') - col('volume' + suffix)) < VolumeConst.VOLUME_DISTANCE_THRESHOLD)
                                 | (col('frequency' + suffix).isNull()))
            volumes = volumes.drop(*col_list)
        volumes = volumes.select(['brand_id', 'serving_type_id', 'volume', level]).\
            withColumn('level_type', lit(level)).withColumnRenamed(level, 'level_value')
    # TODO: constraint proportion in order to diff between adjacent and additional size?
    return volumes


def write_results(df, path, test=False):
    if test:
        df.repartition(1).write.csv(path='output/', mode='overwrite', header=True)
    else:
        df.repartition(1).write.csv(path=path, mode='overwrite', header=True)
    return


def run_extract_volumes(spark, path, flow, test=False):
    print("%s - volume price - %s - extract volumes - started" % (datetime.now(), flow))
    if test:
        levels = ['b.country', 'b.state', 'b.city', 'bc.pos_vendor']        #TODO:  Remove all test usage
    else:
        levels = VolumeConst.LEVELS
    sc = spark.sparkContext
    results = spark.createDataFrame(sc.emptyRDD(), VolumeSchemas.volume_results)
    for level in levels:
        table = level.split('.')[0]
        level = level.split('.')[1]
        print("%s - volume price - %s - extract volumes - get products for level: %s" % (datetime.now(), flow, level))
        products = get_products(spark, table=table, level=level, test=test)
        products = products.filter((col('volume') != 0.) & (col('frequency') > VolumeConst.MIN_FREQUENCY))  # TODO: move to query? or to clean_enrich
        print("%s - volume price - %s - extract volumes - clean & enrich level: %s" % (datetime.now(), flow, level))
        products = clean_enrich_data(products, level=level)
        print("%s - volume price - %s - extract volumes - check adjacent bins level: %s" % (datetime.now(), flow, level))
        products = check_adjacent_bins(products, level, VolumeConst.N_BINS_RANK_TO_CHECK)
        results = results.union(products)
    products.unpersist()
    print("%s - volume price - %s - extract volumes - write results" % (datetime.now(), flow))
    write_results(results, path + VolumeConst.THIS_MODEL_FOLDER + '/' + VolumeConst.SIZES_FOLDER + '/', test=test)
    print("%s - volume price - %s - extract volumes - finished" % (datetime.now(), flow))
    return 'extract_volumes_SUCCESS'


if __name__ == '__main__':

    spark = SparkSession \
        .builder \
        .appName("VolumePrice - extract_volumes") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()

    results_path = ''
    run_extract_volumes(spark, results_path, test=True)
