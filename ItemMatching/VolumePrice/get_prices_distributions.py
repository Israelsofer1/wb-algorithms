
def get_matching(spark, catalog, local=False):
    if local:
        matching_df = spark.read.format("csv").option("header", "true").load("l1.csv")
        matching_df = matching_df.withColumn('sale_price', matching_df.sale_price.cast("float"))
    else:
        matching_df = spark.sql("""SELECT sales_before_tax/item_qty as sale_price, \
                            beer_brand_id, \
                            beer_serving_type_id, \
                            beer_volume/item_qty as beer_volume
                            FROM stg.pos_l1 l1 JOIN stg.bars b ON l1.bar_id = b.id \
                            AND nd > '2019-01-01'""")
    return matching_df.join(catalog, on=['beer_brand_id', 'beer_serving_type_id', 'beer_volume'])
