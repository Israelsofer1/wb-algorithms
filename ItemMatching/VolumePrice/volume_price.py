from ..constant import const
from volume_price_constants import Const as VolumeConst
from pyspark.sql import SparkSession
from datetime import datetime
from extract_volumes import run_extract_volumes
from calculate_price_distributions import run_calculate_price_distributions
from predict import run_predict


def volume_price(spark, bucket, model, version, flow, date, run_id, output_table, is_cross):
    if flow == const.FLOW_PREDICT:
        flow_path = bucket + '/' + model + '/' + version + '/' + flow + '/' + date + '/' + run_id + '/'
        model_path = bucket + '/' + model + '/' + version + '/' + const.FLOW_TRAIN + '/' + VolumeConst.MODELS_FOLDER + '/' + VolumeConst.THIS_MODEL_FOLDER + '/' + VolumeConst.DISTRIBUTIONS_FOLDER +'/'

        print("%s - volume price - %s - started" % (datetime.now(), const.FLOW_PREDICT))
        print("flow_path: %s" % flow_path)
        print("model_path: %s" % model_path)

        run_predict(spark, flow_path, model_path, bucket, model, version, flow, date, run_id, output_table, is_cross)

    elif flow == const.FLOW_TEST:
        flow_path = bucket + '/' + model + '/' + version + '/' + flow + '/'
        model_path = bucket + '/' + model + '/' + version + '/' + const.FLOW_TRAIN + '/' + VolumeConst.MODELS_FOLDER + '/' + VolumeConst.THIS_MODEL_FOLDER + '/' + VolumeConst.DISTRIBUTIONS_FOLDER +'/'

        print("%s - volume price - %s - started" % (datetime.now(), const.FLOW_TEST))
        print("flow_path: %s" % flow_path)
        print("model_path: %s" % model_path)

        run_predict(spark, flow_path, model_path, bucket, model, version, flow, date, run_id, output_table, is_cross)

    elif flow == const.FLOW_TRAIN:
        results_path = bucket + '/' + model + '/' + version + '/' + flow + '/' + VolumeConst.MODELS_FOLDER + '/'

        print("%s - volume price - %s - started" % (datetime.now(), const.FLOW_TRAIN))
        print("results_path: %s" % results_path)

        run_extract_volumes(spark, results_path, flow)
        run_calculate_price_distributions(spark, results_path, flow)

    else:
        print('flow %s is not recognized' % flow)
        quit()
    return 'SUCCESS'


if __name__ == '__main__':

    spark = SparkSession \
        .builder \
        .appName("VolumePrice") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()
