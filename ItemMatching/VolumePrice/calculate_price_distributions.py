from volume_price_constants import Const as VolumeConst
from volume_price_constants import Schemas as VolumeSchemas
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, expr, percent_rank, count, lit, min as sql_min, max as sql_max, round, concat, first, collect_list, udf, array, log
from pyspark.sql.types import StructType, IntegerType
from scipy.optimize import curve_fit, OptimizeWarning
from scipy.special import gammaincc
from math import pi
from numpy import exp, sqrt, diag, subtract, power, histogram as np_histo  #, histogram_bin_edges
import warnings
from datetime import datetime


def write_results(df, path, test=False):
    if test:
        df.repartition(1).write.json(path='output/results/flat', mode='overwrite')
    else:
        df.repartition(1).write.json(path=path, mode='overwrite')
    return


def iqr_filter(df, col_name):
    grouped_df = df.groupBy(['key']).agg(
        sql_max('sale_price').alias('max'),
        sql_min('sale_price').alias('min'),
        expr('percentile_approx(%s, 0.5)' % col_name).alias('med'),
        expr('percentile_approx(%s, 0.25)' % col_name).alias('low'),
        expr('percentile_approx(%s, 0.75)' % col_name).alias('high')
    )
    df = df.join(grouped_df, on=['key'])
    df = df.withColumn('IQR', col('high') - col('low'))
    df = df.withColumn('upbound', col('med') + 3 * col('IQR'))
    df = df.withColumn('lbound', col('med') - 3 * col('IQR'))
    df = df.filter((col(col_name) > col('lbound')) & (col(col_name) < col('upbound')))
    grouped_df = df.groupBy(['key']).agg(count(lit(1)).alias('prices_count'))
    df = df.join(grouped_df, on=['key'])
    return df


def flatten_struct(schema, prefix=""):
    result = []
    for elem in schema:
        if isinstance(elem.dataType, StructType):
            result += flatten_struct(elem.dataType, prefix + elem.name + ".")
        else:
            result.append(col(prefix + elem.name).alias(elem.name))
    return result


def get_prices(spark, volumes, test=False):
    if test:
        matching_df = spark.read.format("csv").option("header", "true").option('inferSchema', 'true').load("l1.csv")
        matching_df = matching_df.withColumn('sale_price', matching_df.sale_price.cast("float"))
    else:
        matching_df = spark.sql("""select sales_before_tax/item_qty as sale_price, \
                            beer_brand_id as beer_brand_id_l1, \
                            beer_serving_type_id as beer_serving_type_id_l1, \
                            beer_volume/item_qty as beer_volume_l1,
                            country_id,
                            state_id,
                            city,
                            pos_vendor
                            from stg.pos_l1 l1 join stg.bars b on l1.bar_id = b.id \
                            join stg.pos_bars_config bc on b.id = bc.bar_id
                            where sales_before_tax > 0 
                            and category_id = 1
                            and nd > date_sub(now(), """ + VolumeConst.SALES_PERIOD + """)""")
    cond = [volumes['beer_brand_id'] == matching_df['beer_brand_id_l1'],
            volumes['beer_serving_type_id'] == matching_df['beer_serving_type_id_l1'],
            volumes['beer_volume'] == matching_df['beer_volume_l1'],
            ((concat(col('level_type'), col('level_value')) == concat(lit('country_id'), col('country_id'))) | (
                concat(col('level_type'), col('level_value')) == concat(lit('state_id'), col('state_id'))) | (
                concat(col('level_type'), col('level_value')) == concat(lit('city'), col('city'))) | (
                concat(col('level_type'), col('level_value')) == concat(lit('pos_vendor'), col('pos_vendor'))))]
    return matching_df.join(volumes, on=cond).drop('beer_brand_id_l1', 'beer_serving_type_id_l1', 'beer_volume_l1')  # ['beer_brand_id', 'beer_serving_type_id', 'beer_volume'])
    # TODO: find a more elegant solution


def chi2_fit(x, y, a0, y_error):
    def norm(z, *w):
        return (w[2]/(sqrt(2*pi)*w[1]))*exp(-(subtract(z, w[0])**2)/(2*(w[1]**2)))
    # def log_normal_function(x, *w):
    #     return w[2] / (x * sqrt(2 * pi) * w[1]) * exp(-(subtract(log(x), w[0]) ** 2) / (2 * (w[1] ** 2)))
    with warnings.catch_warnings():
        warnings.simplefilter("error", OptimizeWarning)
        try:
            (a, c) = curve_fit(f=norm, xdata=x, ydata=y, sigma=y_error, p0=a0)
            aerr = sqrt(diag(c))
            chi2 = sum(subtract(y, norm(x, *a))**2/(power(y_error, 2)))
            dof = len(y) - len(a0)
            prob = gammaincc(dof/2., chi2/2.)
            return a.tolist(), aerr.tolist(), chi2.item(), prob.item(), dof
        except (OptimizeWarning, RuntimeError):
            print("Error - optimization failure")
            pass


def normal_dist(x, *a):
    return (a[2]/(sqrt(2*pi)*a[1]))*exp(-(subtract(x, a[0])**2)/(2*(a[1]**2)))


# def log_normal_function(x, *a):
#     return a[2] / (x * sqrt(2 * pi) * a[1]) * exp(-(subtract(log(x), a[0]) ** 2) / (2 * (a[1] ** 2)))


def get_volumes(spark, path, test=False):
    if test:
        return spark.read.format("csv").option("header", "true").option('inferSchema', 'true').load("products.csv")
    else:
        return spark.read.format("csv").option("header", "true").option('inferSchema', 'true').load(path)


def my_histogram(prices_list, n):  # TODO: remove and drop n calc
    # counts, temp_bins = np_histo(prices_list, bins=histogram_bin_edges(prices_list, bins='auto'))
    counts, temp_bins = np_histo(prices_list, bins=n)
    temp_bins = [float(x) for x in temp_bins]
    bins = []
    for i in range(len(temp_bins)-1):
        bins.append((temp_bins[i+1] + temp_bins[i])/2.)
    counts = [float(x) for x in counts]
    max_count = max(counts)
    zero_indices = [i for i, count in enumerate(counts) if count == 0]
    for i in sorted(zero_indices, reverse=True):
        del bins[i]
        del counts[i]
    num_bins = len(bins)
    return bins, counts, max_count, num_bins


def calculate_error(column, max_val):
    # Some error model
    # error = []
    # for i in column:
    #     if i == 0:
    #         error.append(0.75 * max_val)
    #     elif i == max_val:
    #         error.append(sqrt(max_val)/2)
    #     else:
    #         error.append(sqrt(i))
    # error = [float(x) for x in error]

    # "Reverse" Poisson
    error = []
    a = sqrt(max_val)
    for i in column:
        if i == max_val:
            error.append(sqrt(max_val)/2.)  # TODO: add in_val and take sqrt of it
        else:
            error.append(a - sqrt(i))
    error = [float(x) for x in error]
    return error


def clean_agg_titles(df, columns_to_keep):
    for column in df.columns:
        if column in columns_to_keep:  # TODO: was used for key column - check if redundant
            continue
        start_index = column.find('(')
        end_index = column.find(')')
        if (start_index and end_index):
            df = df.withColumnRenamed(column, column[start_index + 1:end_index])
        return df


def run_calculate_price_distributions(spark, path, flow, test=False):
    print("%s - volume price - %s - calculate price distributions - started" % (datetime.now(), flow))
    # Define UDFs
    error_udf = udf(calculate_error, VolumeSchemas.error)
    histogram_udf = udf(my_histogram, VolumeSchemas.histogram)
    chi2_udf = udf(chi2_fit, VolumeSchemas.chi_2)
    print("%s - volume price - %s - calculate price distributions - get volumes" % (datetime.now(), flow))
    volumes = get_volumes(spark, path + VolumeConst.THIS_MODEL_FOLDER + '/' + VolumeConst.SIZES_FOLDER + '/', test=test)
    volumes = volumes.withColumn('key', concat(col('beer_brand_id'), lit(','), col('beer_serving_type_id'), lit(','),
                                               col('beer_volume'), lit(','), col('level_type'), lit(','),
                                               col('level_value')))
    print("%s - volume price - %s - calculate price distributions - get prices" % (datetime.now(), flow))
    prices = get_prices(spark, volumes, test=test)
    print("%s - volume price - %s - calculate price distributions - cleanse data" % (datetime.now(), flow))
    prices = iqr_filter(prices, 'sale_price')
    prices = prices.filter(col('prices_count') > VolumeConst.SAMPLE_SIZE_TH)
    # Check sample size vs # of keys
    # a = []
    # for i in range(100):
    #     a.append((i, prices.filter(col('prices_count') > i).groupBy('key').agg(lit(1)).count()))
    # print(a)

    prices = prices.withColumn('width', 2 * col('IQR') / ((col('prices_count')) ** (1 / 3.)))  # Freedman-Diaconis rule)
    prices = prices.withColumn('n', round((col('max') - col('min')) / col('width')).cast(IntegerType()))
    prices = prices.filter((col('n') > VolumeConst.NUM_BINS_LOW) & (col('n') < VolumeConst.NUM_BINS_HIGH))
    # prices = prices.filter((col('n') > VolumeConst.NUM_BINS_LOW))
    # Check # of bins vs # of keys
    # a = []
    # for i in range(100):
    #     a.append((i, prices.filter(col('n') > i).groupBy('key').agg(lit(1)).count()))
    # print(a)

    print("%s - volume price - %s - calculate price distributions - collect prices and ggroup" % (datetime.now(), flow))
    prices_to_list = prices.groupBy('key').agg(collect_list(col('sale_price')).alias('prices'))
    exprs = {x: "first" for x in prices.columns if x not in ('sale_price', 'key')}
    prices = prices.groupBy('key').agg(exprs)
    prices = spark.createDataFrame(prices.rdd, schema=VolumeSchemas.clean_agg_titles)
    prices = prices_to_list.join(prices, on='key')

    # prices.repartition(1).registerTempTable('prices')
    prices.unpersist()
    spark.catalog.clearCache()
    # prices = spark.sql("""select * from prices""")
    print("%s - volume price - %s - calculate price distributions - build histograms" % (datetime.now(), flow))
    prices = prices.withColumn('histogram', histogram_udf(col('prices'), col('n')))
    # prices = prices.withColumn('zero_indices', prices.histogram.__getitem__('bin'))

    prices = prices.filter(prices.histogram.__getitem__('num_bins') > 9)

    prices = prices.withColumn('bins', prices.histogram.__getitem__('bin'))
    prices = prices.withColumn('counts', prices.histogram.__getitem__('count'))
    prices = prices.withColumn('max_counts',  prices.histogram.__getitem__('max_count'))
    prices = prices.drop('histogram')
    print("%s - volume price - %s - calculate price distributions - calculate fit initial parameters" % (datetime.now(), flow))
    prices = prices.withColumn('error', error_udf(col('counts'), col('max_counts')))
    prices = prices.withColumn('a0', array(col('med'), subtract(col('high'), col('med')) / 2.698, col('max_counts')))   # TODO: assuming normal distribution. Incorperate into dist type parameters.
    print("%s - volume price - %s - calculate price distributions - fit distribution" % (datetime.now(), flow))
    prices = prices.withColumn('results', chi2_udf(col('bins'), col('counts'), col('a0'), col('error')))
    prices = prices.dropna()
    print("%s - volume price - %s - calculate price distributions - flatten results" % (datetime.now(), flow))
    prices = prices.select(flatten_struct(prices.schema))
    print("%s - volume price - %s - calculate price distributions - write results" % (datetime.now(), flow))
    write_results(prices, path + VolumeConst.THIS_MODEL_FOLDER + '/' + VolumeConst.DISTRIBUTIONS_FOLDER + '/', test)
    print("%s - volume price - %s - calculate price distributions - finished" % (datetime.now(), flow))
    return 'calculate_price_distributions_SUCCESS'


if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("VolumePrice - calculate_price_distributions") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()

    path = 'output/'
    run_calculate_price_distributions(spark, path, True)

# TODO: constraint to train products (DM verified)
