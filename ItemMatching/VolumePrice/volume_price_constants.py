from pyspark.sql.types import StructType, StructField, FloatType, IntegerType, ArrayType, StringType


class Const(object):
    # Constant Thresholds
    # all exclusive

    CUME_SALES_THRESHOLD = 0.95         # Cumulative sales % threshold (i.e. products that "covers" x % of sales in desc
                                        # order of sales contribution) - upper bound (%)
    SALES_PERIOD = '60'                 # Sales period to check (days)
    SALES_THRESHOLD = 1.                # Product (brand + type + volume) sales threshold - lower bound (local currency)
    BRAND_SALES_THRESHOLD = '?'         #
    VOLUME_DISTANCE_THRESHOLD = 0.01
    MIN_FREQUENCY = 10
    N_BINS_RANK_TO_CHECK = 1            # The number of adjacent bin ranks to check for too close volumes. (refers to both sides, i.e n=2 => check 2 ranks to the left and 2 ranks to the right of the bin)
    RIGHT = 1
    LEFT = -1
    LEVELS = ['b.country_id', 'b.state_id', 'b.city', 'bc.pos_vendor']
    RESULTS_SET = ['mu', 'sigma', 'mu', 'sigma', 'chi2', 'prob', 'dof']
    SAMPLE_SIZE_TH = 100                 # Sample size threshold
    NUM_BINS_LOW = 5                    # Number of bins (data points) threshold
    NUM_BINS_HIGH = 150
    PARAMETERS = ['mu', 'sigma', 'amplitude']
    MODELS_FOLDER = 'model'
    THIS_MODEL_FOLDER = 'volume_price'
    SIZES_FOLDER = 'sizes'
    DISTRIBUTIONS_FOLDER = 'distributions'


class Schemas(object):
    # UDF schemas
    histogram = (StructType([StructField("bin", ArrayType(FloatType()), True),
                             StructField("count", ArrayType(FloatType()), True),
                             StructField('max_count', FloatType(), True),
                             StructField('num_bins', IntegerType(), True)]))

    chi_2 = StructType([StructField('parameters', ArrayType(FloatType()), True),
                        StructField('parameters_delta', ArrayType(FloatType()), True),
                        StructField('chi2', FloatType(), True),
                        StructField('prob', FloatType(), True),
                        StructField('dof', IntegerType(), True)])

    error = ArrayType(FloatType())

    clean_agg_titles = StructType([StructField('key', StringType(), True),
                                   StructField('beer_serving_type_id', IntegerType(), True),
                                   StructField('IQR', FloatType(), True),
                                   StructField('pos_vendor', IntegerType(), True),
                                   StructField('city', StringType(), True),
                                   StructField('n', IntegerType(), True),
                                   StructField('beer_volume', FloatType(), True),
                                   StructField('min', FloatType(), True),
                                   StructField('lbound', FloatType(), True),
                                   StructField('low', FloatType(), True),
                                   StructField('max', FloatType(), True),
                                   StructField('level_type', StringType(), True),
                                   StructField('prices_count', IntegerType(), True),
                                   StructField('level_value', IntegerType(), True),
                                   StructField('upbound', FloatType(), True),
                                   StructField('beer_brand_id', IntegerType(), True),
                                   StructField('country_id', IntegerType(), True),
                                   StructField('width', FloatType(), True),
                                   StructField('high', FloatType(), True),
                                   StructField('med', FloatType(), True),
                                   StructField('state_id', IntegerType(), True)])

    volume_results = StructType([StructField('beer_brand_id', IntegerType(), False),
                                 StructField('beer_serving_type_id', IntegerType(), False),
                                 StructField('beer_volume', FloatType(), False),
                                 StructField('level_value', StringType(), False),
                                 StructField('level_type', StringType(), False)])

    fit_volumes = StructType([StructField('bar_product_id', IntegerType(), True),
                              StructField('volume_id', FloatType(), True)])
