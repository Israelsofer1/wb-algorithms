%spark.pyspark

mysql_url = "jdbc:mysql://mysql04.dev.alcoholanalytics.com:3306/weissserver?zeroDateTimeBehavior=convertToNull"
mysql_user = "app_pdi_prod"
mysql_password = "U8q47PUdRdalvG0k"

tables = {
    'beer': 'pos_beer_suggestions',
    'food': 'pos_food_suggestions',
    'spirit': 'pos_spirit_suggestions',
    'nab': 'pos_nab_suggestions',
    'wine': 'pos_wine_suggestions'
}
fields = {'beer': ['brand_id', 'super_brand_id', 'serving_type_id', 'volume_id']}
id_field = 'bar_product_id'


def get_res(run_id, tbl):
    out = {}
    for model_id in ['nn', 'rule']:
        out[model_id] = spark.read \
            .format("jdbc") \
            .option("url", mysql_url) \
            .option("dbtable",
                    "SELECT * FROM {t} WHERE model_id = {m} and run_id = {r}".format(t=tbl, m=model_id, r=run_id)) \
            .option("user", mysql_user) \
            .option("password", mysql_password) \
            .option("driver", "com.mysql.jdbc.Driver") \
            .load()
    return out


def override(high_df, low_df, mname):
    min_thresh = 0.9
    columns = fields[mname]
    for col in columns:
        print("Set column %s for hdf" % col)
        high_df = high_df.withColumn('%s_new' % col, high_df['%s_new' % col])
        high_df = high_df.withColumn('%s_prob_new' % col, high_df['%s_prob_new' % col])
        print("Filter hdf by probability")
        high_join = high_df.filter(high_df['%s_prob' % col] > min_thresh)
        print("Join hdf and ldf")
        low_df.join(high_join.select([id_field, '%s_new' % col, '%s_prob_new' % col]),
                    high_df.bar_product_id == low_df.bar_product_id,
                    how = 'left')
        print("Set column %s for ldf" % col)
        low_df = low_df.withColumn(col, low_df['%s_new' % col])
        low_df = low_df.withColumn('%s_prob' % col, low_df['%s_prob_new' % col])
    return low_df


def main(run_id, model_name):
    print("Get the data for each model")
    dfs = get_res(run_id, tbl=table[model_name])
    print("Join the base (main) dataframe with all the others using different rules")
    base_df = dfs['nn']
    priority = ['rule']
    for mkey in priority:
        base_df = override(high_df=dfs[mkey], low_df=base_df, mname=model_name)
    print("Get rid of unneeded columns")
    accepted_fields = fields[model_name] + ['%s_prob' % f for f in fields[model_name]] + [id_field]
    for col in base_df.columns:
        if col not in accepted_fields:
            base_df = base_df.drop(col).collect()
    print("Save the base table")
    output_table = '%s_suggestions' % model_nameproperties
    prop = dict()
    prop['user'] = mysql_user
    prop['password'] = mysql_password
    prop['driver'] = 'com.mysql.jdbc.Driver'
    base_df.write.jdbc(mysql_url, output_table, mode='overwrite', properties=prop)


brands_df.show(10)