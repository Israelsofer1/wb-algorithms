import pyspark.sql.functions as func
from pyspark.sql.types import FloatType, IntegerType, StringType, MapType, ArrayType, StructType
import numpy as np


def get_mapping_category(broadcast, results_df, column):
    def get_mapping_index(original_value):
        return str(broadcast.value[int(original_value)])

    get_mapping_index_udf = func.udf(get_mapping_index, StringType())
    results_df = results_df.withColumn(column, get_mapping_index_udf(func.col(column)))
    return results_df


def get_highest_category(broadcast, results_df, column):

    def get_highest_index(list_values):
        list_values = [float(value) for value in list_values]
        index = np.argmax(list_values)
        return int(broadcast.value[index])

    get_highest_index_udf = func.udf(get_highest_index, IntegerType())

    results_df = results_df.withColumn(column, get_highest_index_udf(func.col(column)))
    return results_df


def get_highest_value(list_values):
    list_values = [float(value) for value in list_values]
    return max(list_values)


get_highest_value_udf = func.udf(lambda x: get_highest_value(x), FloatType())
