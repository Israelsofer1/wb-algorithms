from ..constant import const
import os
from .data_to_merge import get_mapping, get_input_data_by_path, get_results_by_path_beer
import pandas as pd
import pyspark.sql.functions as func
from pyspark.sql.types import IntegerType
from .get_highest import get_mapping_category
from ..PreProcess.matching_config_creator import beer_field_bow

def bow_process(spark, path, model, version, flow, date, run_id):
    base_folder = os.path.join(path, model, version)
    if flow == const.FLOW_PREDICT:
        folder = os.path.join(base_folder, flow, date, run_id)
    else:
        if flow != const.FLOW_TEST:
            print('error! the flow', flow, 'is not supported')
        folder = os.path.join(base_folder, flow)

    input_path = os.path.join(folder, const.PRE_PROCESS_FOLDER, 'pre_process_BOW.gzip')
    original_df = get_input_data_by_path(spark, input_path)

    mapping = get_mapping(spark, path, model, version, const.MODEL_TYPE_BOW)

    # output of the spam or ham (top 300)
    binary_path = os.path.join(folder, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_BOW.lower(), 'binary')
    binary_results_df = get_results_by_path_beer(spark, binary_path, [beer_field_bow])

    multi_path = os.path.join(folder, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_BOW.lower(), 'multi')
    multi_results_df = get_results_by_path_beer(spark, multi_path, [beer_field_bow])

    # Get the lass real index from the mapping
    column = beer_field_bow
    broadcast = spark.sparkContext.broadcast(mapping[column])
    mapping_count = {key: len(value) for (key, value) in mapping.items()}
    print('The mapping count for feature', column, 'is', mapping_count[column])

    multi_results_df = multi_results_df
    multi_results_df = multi_results_df.withColumn(column, multi_results_df[column].cast(IntegerType()))
    multi_results_df = multi_results_df.withColumn(column, func.col(column) + func.lit(1))
    multi_results_df = get_mapping_category(broadcast, multi_results_df, column)

    binary_results_count = binary_results_df.count()
    multi_results_count = multi_results_df.count()
    original_count = original_df.count()

    if binary_results_count != multi_results_count:
        raise Exception("Binary BOW model results count and Multi BOW model results count are not equal")
    if binary_results_count != original_count:
        if binary_results_count > original_count:
            raise Exception("Results contain more rows that the original data")
        else:
            original_df = original_df.limit(binary_results_count)

    binary_result_pd = binary_results_df.toPandas()
    binary_result_pd = binary_result_pd.rename(columns={'value': 'spam_ot_ham_prob'})
    multi_result_pd = multi_results_df.toPandas()

    original_pd = original_df.toPandas()

    print(original_pd.shape, binary_result_pd.shape, multi_result_pd.shape)

    join_df = pd.concat([original_pd, binary_result_pd, multi_result_pd], axis=1)
    print(join_df.head())

    output_df = spark.createDataFrame(join_df)
    return output_df

def aggregate_results(results_df):
    binary_prob = 'spam_ot_ham_prob'
    results_df = results_df.withColumn(beer_field_bow, func.when(func.col(binary_prob) > 0.5, func.col(beer_field_bow)).otherwise(0))
    results_df = results_df.withColumn(beer_field_bow + '_prob',
                                      func.when(func.col(binary_prob) > 0.5, func.col(binary_prob)).
                                      otherwise(1 - func.col(binary_prob)))
    results_df = results_df.drop('spam_ot_ham_prob')
    results_df.show()
    return results_df

# def bow_process(spark, path, model, version, flow, date, run_id):
#     base_folder = os.path.join(path, model, version)
#     if flow == const.FLOW_TEST:
#         folder = os.path.join(base_folder, flow)
#     elif flow == const.FLOW_PREDICT:
#         folder = os.path.join(base_folder, flow, date, run_id)
#     else:
#         print('error! the flow', flow, 'is not supported')
#         folder='path'
#
#     # csv with the origin products
#     orig_products_df = spark.read.format("csv").option("header", "true").load(
#         os.path.join(folder, const.PRE_PROCESS_FOLDER, 'pre_process_BOW.gzip'))
#
#     # read mapping
#     mapping = get_mapping(spark, path, model, version, const.MODEL_TYPE_BOW)
#     label_mapping_lst = mapping['brand_id']
#     label_mapping_lst = [int(i) for i in label_mapping_lst]
#
#     # output of the spam or ham (top 300)
#     binary_path = os.path.join(folder, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_BOW,  'binary')
#     print('search file in', binary_path)
#     full_path = 's3://dev-matching-steps/beer/0.1/test/results/BOW/binary/part-00000-c18cd15d-2d16-43d0-bf4b-26a710698930-c000.json.gz.out'
#         #(binary_path, 'json.gz.out')
#     print('read output file of binary class bow model from', full_path)
#     output_binary = spark.read.text(full_path)
#     output_binary_lst = ast.literal_eval(output_binary.collect()[0][0])
#     bin_rdd = spark.sparkContext.parallelize(output_binary_lst)
#     row = Row("binary_prob")
#     binary_df = bin_rdd.map(row).zipWithIndex().toDF(['binary_prob', 'rowNum'])
#
#     # brand_id output
#     multi_path = os.path.join(folder, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_BOW, 'multi')
#     print('search file in', multi_path)
#     full_path = find_file(multi_path, 'json.gz.out')
#     print('read output file of multi class bow model from', full_path)
#     output_multiclass = spark.read.text(full_path)
#     output_multiclass_lst = ast.literal_eval(output_multiclass.collect()[0][0])
#     multi_rdd = spark.sparkContext.parallelize(output_multiclass_lst)
#     row_multi = Row("multi_prob")
#     multi_df = multi_rdd.map(row_multi).zipWithIndex().toDF(['multi_prob', 'rowNum'])
#
#     # join the predictions to the orig products df
#     # since there are no common column between these two data frames add row_index so that it can be joined
#     binary_df = binary_df.withColumn("binary_prob", binary_df["binary_prob"]["binary_prob"])
#     multi_df = multi_df.withColumn("multi_prob", multi_df["multi_prob"]["multi_prob"])
#     try:
#         # Add row number to original pre process products
#         orig_products_df = orig_products_df.withColumn('rowNum', func.row_number().over(Window.orderBy(func.lit(1))))
#         orig_products_df = orig_products_df.withColumn('rowNum', orig_products_df['rowNum'] - 1)
#
#         orig_products_df = orig_products_df.join(binary_df['rowNum', "binary_prob"], on=["rowNum"])
#         orig_products_df = orig_products_df.join(multi_df['rowNum', "multi_prob"], on=["rowNum"])
#
#         orig_products_df = orig_products_df.withColumn("predicted_brand_id", orig_products_df['multi_prob'][0])
#         orig_products_df = orig_products_df.withColumn("multi_prob", orig_products_df['multi_prob'][1])
#
#         colmap = func.create_map([func.lit(x) for x in chain(*mapping['branf_id'].items())])
#         orig_products_df = orig_products_df.withColumn("predicted_brand_id",
#                                                        colmap.getItem(orig_products_df["predicted_brand_id"]))
#         orig_products_df = orig_products_df.withColumn("brand_id_prob",
#                                                        orig_products_df["binary_prob"] * orig_products_df["multi_prob"])
#     except IndexError as error:
#         print("%s - 'label mapping containes %s' + ' and its not match the label from the s&h'   - started" % (
#         datetime.now(), len(label_mapping_lst)))
#
#     orig_products_df.orderBy("bar_product_id", ascending=False)
#     orig_products_df = orig_products_df.drop('brand_id','volume_id','serving_type_id','super_brand_id','beer_type_id')
#     orig_products_df = orig_products_df.withColumnRenamed("predicted_brand_id", "brand_id")
#     print('bow process sucess %s'  % (datetime.now()))
#     return orig_products_df



