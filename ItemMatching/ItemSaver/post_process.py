from pyspark.sql import SparkSession
from .post_process_nn import post_NN
from .save_data import save_data , upload_to_beer_table, save_data_to_csv, add_fields
from .post_process_bow import bow_process, aggregate_results
import pyspark.sql.functions as func
from ..constant import const
from ..PreProcess.matching_config_creator import beer_field_order
import os


def run_all(spark, base_path, model, version, flow, date, run_id):
    output_path = os.path.join(base_path, model, version, flow, const.OUTPUT_FOLDER)

    # post_process_nn
    NN_df = post_NN(spark, base_path, model, version, flow, date, run_id)
    NN_df = NN_df.withColumn('model_type', func.lit(const.MODEL_TYPE_NN))

    columns_order = list(sorted(NN_df.columns))
    NN_df = NN_df.select(columns_order)

    save_data_to_csv(full_df=NN_df, path=output_path, file_name='nn_results.csv', output_columns=NN_df.columns)

    # pos_process_bow
    bow_df = bow_process(spark, base_path, model, version, flow, date, run_id)
    save_data_to_csv(full_df=bow_df, path=output_path, file_name='bow_results.csv', output_columns=bow_df.columns)

    # Update the bag of words with the missing data
    bow_df = aggregate_results(results_df=bow_df)
    bow_df = add_fields(bow_df, beer_field_order)
    bow_df = bow_df.withColumn('model_type', func.lit(const.MODEL_TYPE_BOW))
    columns_order = list(sorted(bow_df.columns))
    bow_df = bow_df.select(columns_order)

    output_df = NN_df.union(bow_df)

    # Save the data
    if flow == const.FLOW_PREDICT:
        output_df = save_data(output_df, model, version, date, run_id)
        upload_to_beer_table(spark, output_df, model)
    else:
        if flow != const.FLOW_TEST:
            print('Error with flow name! use test flow folder')
        save_data_to_csv(full_df=output_df, path=output_path, file_name='final_results.csv', output_columns=output_df.columns)




