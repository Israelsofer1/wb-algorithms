import pyspark.sql.functions as func
from datetime import datetime
from ..constant import tables, const, hive_base_paths
from .db_access_config import db_access
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType, DateType, TimestampType, BooleanType
from Project.Common.KinesisSaver.kinesis_saver import KinesisSaver
import boto3
import os

prob_suffix = '_prob'

fields = {
    'category' :['bar_product_id','category_id'],
    'beer': ['bar_product_id','brand_id', 'super_brand_id', 'serving_type_id', 'volume_id','beer_type_id'],
    'metadata' : ['model_type', 'version', 'run_date', 'run_id', 'created_at', 'is_cross_validation']}


def add_fields(df, lst_of_columns):
    prob_suffix = '_prob'

    for column_name in lst_of_columns:
        if column_name not in df.schema.names:
            print('Add column', column_name, 'that is not predicted from the model')
            df = df.withColumn(column_name, func.lit(0))
            df = df.withColumn(column_name + prob_suffix, func.lit(0))
    return df


def save_data(result_df, model_name, version, date, run_id, is_cross_validation=True):
    result_df = result_df.withColumn('beer_type_id', func.lit(0))
    result_df = result_df.withColumn('beer_type_id_prob', func.lit(0))

    result_df = result_df.withColumn('version',func.lit(version))
    result_df = result_df.withColumn('run_date',func.lit(date))
    result_df = result_df.withColumn('run_id',func.lit(run_id))
    result_df = result_df.withColumn('created_at',func.lit(datetime.now()))
    result_df = result_df.withColumn('is_cross_validation',func.lit(is_cross_validation))

    return result_df

def save_data_to_csv(full_df, path, file_name, output_columns):
    print('save the data into', path + '/' + file_name)
    full_df = full_df.select(output_columns)
    full_df.coalesce(1).write.mode('overwrite').option("header","true").csv(path + '/' + file_name, compression="gzip")

def upload_to_beer_table(spark,df, model_name):
    df.repartition(1).registerTempTable("products")
    table_name =tables[model_name]
    print('save data to table', table_name, 'with columns', df.columns)
    print('insert into ' + str(table_name))
    if model_name.endswith('final'):
        query =  """INSERT OVERWRITE TABLE stg.{tn}
            SELECT
            bar_product_id,
            brand_id ,
            brand_id_prob,
            super_brand_id,
            super_brand_id_prob,
            volume_id,
            volume_id_prob,
            serving_type_id,
            serving_type_id_prob,
            beer_type_id,
            beer_type_id_prob,
            version,
            is_cross_validation,
            created_at,
            run_date,
            run_id
            FROM products""".format(tn = table_name)
    else:
        query = """INSERT OVERWRITE TABLE stg.{tn}
                    SELECT
                    bar_product_id,
                    brand_id ,
                    brand_id_prob,
                    super_brand_id,
                    super_brand_id_prob,
                    volume_id,
                    volume_id_prob,
                    serving_type_id,
                    serving_type_id_prob,
                    beer_type_id,
                    beer_type_id_prob,
                    version,
                    is_cross_validation,
                    created_at,
                    run_date,
                    run_id,
                    model_type
                    FROM products""".format(tn=table_name)

    print('num of rows to insert', df.count())
    result = spark.sql(query)
    print(str(table_name) + ' table has been updated with result', result)
    return result


def upload_to_category_table(spark,df, model_name):
    df.repartition(1).registerTempTable("products")
    table_name =tables[model_name]
    print('insert into ' + str(table_name))
    query =  """INSERT OVERWRITE TABLE stg.{tn}
            SELECT
            bar_product_id,
            category_id ,
            category_id_prob,
            version,
            is_cross_validation,
            created_at,
            run_date,
            run_id
            FROM products""".format(tn=table_name)
    result = spark.sql(query)
    print(str(table_name) + ' table has been updated')
    return result


def map_columns_to_sql_table(output_df, model_name):
    if model_name == 'beer':
        # map columns
        output_df = output_df.withColumn('type_id', func.lit(0))
        output_df = output_df.withColumn('type_id_prob', func.lit(0.0))
        output_df = output_df.withColumn('volume', func.col('volume_id'))
        output_df = output_df.drop('volume_id')
        output_df = output_df.withColumn('volume_prob', func.col('volume_id_prob'))
        output_df = output_df.drop('volume_id_prob')
        output_df = output_df.withColumn('superbrand_id', func.col('super_brand_id'))
        output_df = output_df.drop('super_brand_id')
        output_df = output_df.withColumn('superbrand_id_prob', func.col('super_brand_id_prob'))
        output_df = output_df.drop('super_brand_id_prob')
    elif model_name == 'category':
        output_df = output_df.withColumn('created_at', func.lit(datetime.now()))
        output_df = output_df.withColumn('learned', func.lit(0))
        output_df = output_df.withColumn('duplicate_match', func.lit(0))
    else:
        print('Error! model name =' + model_name + 'does not exists')
    output_df = output_df.withColumn('pos_auto_matching_id', func.lit(0))
    output_df = output_df.withColumn('pos_auto_matching_status', func.lit(0))
    output_df = output_df.withColumn('pos_auto_matching_modified_at', func.lit(0))
    return output_df


def save_to_sql(output_df, model_name, environment):
    output_table = 'pos_%s_suggestions' % model_name

    print('insert into ' + str(output_table) + ' in db ' + db_access.mysql_url[environment])

    output_df.write \
        .format("jdbc") \
        .option("url", db_access.mysql_url[environment]) \
        .option("dbtable", output_table) \
        .option("user", db_access.mysql_master_user) \
        .option("password", db_access.mysql_master_password) \
        .option("driver", "com.mysql.jdbc.Driver") \
        .mode("append") \
        .save()


def upload_to_kinesis(spark, model_name, stream_name, run_id, environment):
    saver = KinesisSaver(spark, model_name, stream_name, environment=environment)
    saver.load_transform(run_id)
    return saver.batch_send_to_kinesis()


def delete_suggestions_by_run_id(spark, model_name, date, run_id, environment='dev'):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(hive_base_paths[model_name])
    spark.sql('ALTER TABLE stg.' + tables[model_name] + ' DROP IF EXISTS PARTITION(run_date = ' + date + ', run_id = ' + run_id + ')')
    prefix = os.path.join(tables[model_name], date, run_id)
    for key in bucket.objects.filter(Prefix=prefix + '/'):
        print(key)

    if model_name == const.MODEL_BEER:
        spark.sql('ALTER TABLE stg.' + tables[model_name] + '_final DROP IF EXISTS PARTITION(run_date = ' + date + ', run_id = ' + run_id + ')')
        prefix = os.path.join(tables[model_name]+'_final', date, run_id)
        for key in bucket.objects.filter(Prefix=prefix + '/'):
            key.delete()

# def upload_to_category_sql(spark, df, model_name):
#     df.repartition(1).registerTempTable("products")
#     table_name =tables[model_name]
#     print('insert into ' + str(table_name))
#
#     df.write \
#         .format("jdbc") \
#         .option("url", mysql_url) \
#         .option("dbtable", table_name) \
#         .option("user", mysql_master_user) \
#         .option("password", mysql_master_password) \
#         .option("driver", "com.mysql.jdbc.Driver") \
#         .mode("append") \
#         .save()
