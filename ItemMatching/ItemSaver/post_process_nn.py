import pandas as pd
from .data_to_merge import get_mapping, get_results_by_path_beer, get_input_data_by_path
from ..constant import const
from .get_highest import get_mapping_category
from ..PreProcess.matching_config_creator import beer_field_order
import os


def get_original_value_for_column(spark, results_df, map_by_column_dict):
    mapping_count = {key: len(value) for (key, value) in map_by_column_dict.items()}
    for column in map_by_column_dict.keys():
        mapping = map_by_column_dict[column]
        broadcast = spark.sparkContext.broadcast(mapping)

        print('The mapping count for feature', column, 'is', mapping_count[column])
        results_df = get_mapping_category(broadcast, results_df, column)
    return results_df


def join_tables(spark, results_df, original_df):
    # TODO: the spark way should run better, however its very hard to do union by columns
    # Join them with a fake id
    # original_df = original_df.withColumn('rowNum', func.row_number().over(Window.orderBy(func.lit(1))))
    # original_df = original_df.withColumn('rowNum', original_df['rowNum'])
    # result_df = result_df.withColumn('rowNum', func.row_number().over(Window.orderBy(func.lit(1))))
    # result_df = result_df.withColumn('rowNum', result_df['rowNum'])
    # result_df = result_df.join(original_df, "rowNum", "inner").drop("rowNum")

    # For now convert to pandas + union + convert back to spark
    result_pd = results_df.toPandas()
    original_pd = original_df.toPandas()
    join_df = pd.concat([result_pd, original_pd], axis=1)
    output_df = spark.createDataFrame(join_df)
    return output_df


def post_NN(spark, path, model_name, version, flow, date, run_id):
    mapping = get_mapping(spark=spark, path=path, model_name=model_name, version=version, model_type=const.MODEL_TYPE_NN)
    if flow == const.FLOW_PREDICT:
        results_path = os.path.join(path, model_name, version, flow,
                                    date, run_id, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_NN.lower())
        pre_process_path = os.path.join(path, model_name, version, flow,
                                        date, run_id, const.PRE_PROCESS_FOLDER, 'pre_process_NN.gzip')
    else:
        if flow != const.FLOW_TEST:
            print('Error with flow name! use test flow folder')
        results_path = os.path.join(path, model_name, version, flow, const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_NN.lower())
        pre_process_path = os.path.join(path, model_name, version, flow, const.PRE_PROCESS_FOLDER, 'pre_process_NN.gzip')

    result_df = get_results_by_path_beer(spark, results_path, beer_field_order)
    result_df = get_original_value_for_column(spark, result_df, mapping)

    original_df = get_input_data_by_path(spark, pre_process_path)
    original_df_count = original_df.count()
    result_df_count = result_df.count()
    if original_df_count != result_df_count:
        if result_df_count > original_df_count:
            raise Exception("Results contain more rows that the original data")
        else:
            original_df = original_df.limit(result_df_count)
    joined_df = join_tables(spark=spark, results_df=result_df, original_df=original_df)
    return joined_df
