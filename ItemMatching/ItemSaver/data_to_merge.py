from datetime import datetime
import json
import os
from pyspark.sql import SparkSession, Row
import ast
import pyspark.sql.functions as func
from ..constant import const

def create_mapping(df, key):
    # Assuming the original column ends with an _index
    df = df.withColumn(key, func.explode("%s_index" % key))
    mlst = df.select(key).collect()
    mlst = [float(r[key]) for r in mlst]
    # The final mapping looks like: {index: matched_id}
    mapping = {i: v for i, v in enumerate(mlst)}
    return mapping


def get_mapping(spark, path, model_name, version, model_type):
    # Read the mapping
    file = 'label_mapping_' + model_type + '.json'
    mapping_path = os.path.join(path, model_name, version, const.FLOW_TRAIN, const.MAPPING_FOLDER, file)
    print('read label mapping from:', mapping_path)
    mapping = spark.read.json(mapping_path)
    mapping_df = mapping.toPandas()
    mapping_df.columns = mapping_df.columns.str.replace('_index', '')
    print('The mapping columns for model', model_name, 'are', mapping_df.columns)

    # Read the config file
    config_path = os.path.join(path, 'matching_config.json')
    print('read config data from:', config_path, 'for model', model_name)
    config = json.loads(spark.read.json(config_path, multiLine=True).toJSON().collect()[0])

    map_by_column_dict = {}
    for column in mapping_df.columns:
        if config[model_name][column]['direction'] == 'output':
            map_by_column_dict[column] = mapping_df[column][0]
    if len(map_by_column_dict) == 0:
        print('Error! there is no output columns for model', model_name)
    else:
        print('The output columns are:', map_by_column_dict.keys())
    return map_by_column_dict


def get_results_by_path(spark, data_path, col_names):
    print('read results data from:', data_path, 'with columns', col_names)
    data = spark.read.text(data_path)
    data_arr = [[str(v) for v in col] for col in ast.literal_eval(data.collect()[0][0])]

    cols = Row(*col_names)
    rdd1 = spark.sparkContext.parallelize(data_arr)
    rdd2 = rdd1.map(lambda x: cols(x))
    results_df = rdd2.toDF(col_names)
    print('the input data size is:', results_df.count())
    return results_df


def create_row(raw_data, column_count, row_index):
    row = ()
    for column_index in range(0, column_count):
        add_to_row = tuple(raw_data[column_index][row_index])
        row = row + add_to_row
    return row

def get_results_by_path_beer(spark, data_path, col_names):
    # Be careful - the column order is important!
    table_columns = []
    for column in col_names:
        table_columns.append(column)
        table_columns.append(column + '_prob')

    print('read results data from:', data_path, 'with columns', col_names)
    data = spark.read.text(data_path)

    raw_data = ast.literal_eval(data.collect()[0][0])

    from pyspark.sql.types import DoubleType

    if isinstance(raw_data, list) and len(raw_data) != 0:
        if isinstance(raw_data[0], float):
            row_count = len(raw_data)
            data_arr = [raw_data[row_index] for row_index in range(0, row_count)]
            results_df = spark.createDataFrame(data_arr, DoubleType())

        elif isinstance(raw_data[0], list) and isinstance(raw_data[0][0], list):
            row_count = len(raw_data[0])
            column_count = len(raw_data)
            print('The results data shape is (', row_count, column_count, ')')
            data_arr = [create_row(raw_data, column_count, row_index) for row_index in range(0, row_count)]
            results_df = spark.createDataFrame(data_arr, table_columns)

        elif isinstance(raw_data[0], list) and (isinstance(raw_data[0][0], int) or isinstance(raw_data[0][0], float)):
            row_count = len(raw_data)
            data_arr = [tuple(raw_data[row_index]) for row_index in range(0, row_count)]
            results_df = spark.createDataFrame(data_arr, table_columns)

        else:
            print('Error, the output from the model was not in the correct format for beer items')
            print('The format is:' + type(raw_data[0]))
            results_df = spark.createDataFrame([("99", "99")], ["col1", "col2"])
    else:
        print('Error, the output from the model was not in the correct format for beer items')
        print('The format is:' + type(raw_data))
        results_df = spark.createDataFrame([("99", "99")], ["col1", "col2"])

    return results_df

def get_input_data_by_path(spark, pre_process_path):
    print('read input pre process data from:', pre_process_path)
    original_df = spark.read.json(pre_process_path)
    print('the input data size is:', original_df.count())
    original_df = original_df.select(['bar_product_id'])
    return original_df

    # print('read results data from:', data_path, 'with columns', beer_field_order)
    # data = spark.read.text(data_path)
    #
    # raw_data = ast.literal_eval(data.collect()[0][0])
    # row_count = len(raw_data[0])
    # column_count = len(raw_data)
    # print('The results data shape is (', row_count, column_count, ')')
    #
    # if column_count != len(beer_field_order):
    #     print('Error! the return data from sage-maker have', row_count, 'but the config defined', len(beer_field_order))
    #
    # data_arr = [create_row(raw_data, column_count, row_index) for row_index in range(0, row_count)]
    #
    # # Be careful - the column order is important!
    # table_columns = []
    # for column in beer_field_order:
    #     table_columns.append(column)
    #     table_columns.append(column + '_prob')
    #
    # results_df = spark.createDataFrame(
    #     data_arr,
    #     table_columns)
    #
    # return results_df


# def get_mapping()
# # Get the label mapping
# file = 'label_mapping_' + model_type + '.json'
#
# mapping_path = os.path.join(path, model_name, version, const.FLOW_TRAIN, const.MAPPING_FOLDER, file)
# print('read label mapping from:', mapping_path)
# mapping = spark.read.json(mapping_path)
#
# # Load config to get the column list
# config_path = os.path.join(path, 'matching_config.json')
# print('read config data from:', config_path, 'for model', model_name)
# config = json.loads(spark.read.json(config_path, multiLine=True).toJSON().collect()[0])
#
# # There is "_index" at the end of each field so we remove last 6 chars
# mapping = {k[:-6]: create_mapping(mapping, k[:-6]) for k in mapping.columns if
#            config[model_name][k[:-6]]['direction'] == 'output'}
# def get_results(spark, path, mapping, model_name, version, flow, date, run_id, model_type):
#     # Read the output data
#     if flow == const.FLOW_TEST:
#         data_path = os.path.join(path, model_name, version, flow,
#                                  const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_NN.lower())
#     elif flow == const.FLOW_PREDICT:
#         data_path = os.path.join(path, model_name, version, flow, date, run_id,
#                                  const.MODEL_RESULTS_FOLDER, const.MODEL_TYPE_NN.lower())
#     else:
#         print('error')
#
#     print('read data from:', data_path, 'for model', model_name)
#     data = spark.read.text(data_path)
#     data = [[str(v) for v in col] for col in ast.literal_eval(data.collect()[0][0])]
#
#     col_names = list(mapping.keys())
#     cols = Row(*col_names)
#     results_df = spark.sparkContext.parallelize([cols(*r) for r in zip(*data)]).toDF()
#     return results_df
#
# def get_pre_process_data(spark, path, model_name, version, flow, date, run_id):
#     if flow == 'test' or flow == 'Match':
#         data_path = os.path.join(path, model_name, version, flow, const.PRE_PROCESS_FOLDER, 'pre_process_NN.gzip')
#     elif flow == 'predict':
#         data_path = os.path.join(path, model_name, version, flow, date, run_id, const.PRE_PROCESS_FOLDER, 'pre_process_NN.gzip')
#
#     # Get the original ID's
#     print('read pre_process data from:', data_path, 'for model', model_name)
#     original_df = spark.read.json(data_path)
#     original_df = original_df.select(['bar_product_id'])
#     return original_df
