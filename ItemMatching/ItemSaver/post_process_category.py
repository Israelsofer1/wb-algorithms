from pyspark.sql import SparkSession, Row
import os
from pyspark.sql.window import Window
import json
import ast
from .save_data import save_data_to_csv, save_data, upload_to_category_table,save_to_sql, map_columns_to_sql_table, upload_to_kinesis, delete_suggestions_by_run_id
from ..constant import const
from .get_highest import get_highest_value_udf, get_highest_category
import pandas as pd
from .data_to_merge import get_mapping, get_results_by_path, get_input_data_by_path


def find_file(root_path, ending):
    for root, dirs, files in os.walk(root_path):
        for file in files:
            if file.endswith(ending):
                return os.path.join(root, file)


def post_category(spark, col_names, map_by_column_dict, results_df, original_df):
    print('get highest probability for each feature row')
    for column in col_names:
        mapping = map_by_column_dict[column]
        broadcast = spark.sparkContext.broadcast(mapping)

        print('The mapping for feature', column, 'is', mapping)
        results_df = results_df.withColumn('%s_prob' % column, get_highest_value_udf(results_df[column]))
        results_df = get_highest_category(broadcast, results_df, column)

    # Join them with a fake id
    #original_df = original_df.withColumn('rowNum', func.row_number().over(Window.orderBy(func.lit(1))))
    #original_df = original_df.withColumn('rowNum', original_df['rowNum'])
    #results_df = results_df.withColumn('rowNum', func.row_number().over(Window.orderBy(func.lit(1))))
    #results_df = results_df.withColumn('rowNum', results_df['rowNum'])
    #results_df = results_df.join(original_df, "rowNum", "inner").drop("rowNum")

    result_pd = results_df.toPandas()
    original_pd = original_df.toPandas()
    join_df = pd.concat([result_pd, original_pd], axis=1)
    output_df = spark.createDataFrame(join_df)
    return output_df

def run_all(spark, path, model_name, version, flow, date, run_id):
    mapping = get_mapping(spark, path, model_name, version, const.MODEL_TYPE_NN)

    # Read and map the data
    if flow == const.FLOW_PREDICT:
        pre_process_path = os.path.join(path, model_name, version, flow, date, run_id, const.PRE_PROCESS_FOLDER,
                                        'pre_process_NN.gzip')
        data_path = os.path.join(path, model_name, version, flow, date, run_id, const.MODEL_RESULTS_FOLDER)
    else:
        if flow != const.FLOW_TEST:
            print('Error with flow name! use test flow folder')
        pre_process_path = os.path.join(path, model_name, version, flow, const.PRE_PROCESS_FOLDER,
                                        'pre_process_NN.gzip')
        data_path = os.path.join(path, model_name, version, flow, const.MODEL_RESULTS_FOLDER)

    # Load the data - input + results
    col_names = list(mapping.keys())
    original_df = get_input_data_by_path(spark, pre_process_path)
    results_df = get_results_by_path(spark, data_path, col_names)

    # Union the tables together with the highest prob
    output_df = post_category(spark, col_names, mapping, results_df, original_df)
    print('expected columns are' + str(output_df.columns))
    # Save the data
    if flow == const.FLOW_PREDICT:
        print('flow is FLOW_PREDICT')
        environment = path.split('//')[1].split('-')[0]
        output_df = save_data(output_df, model_name, version, date, run_id)
        print('data saved to hive, columns after data saved are' + str(output_df.columns))
        upload_to_category_table(spark, output_df, model_name)
        try:
            stream_name = environment + const.KINESIS_STREAM_SUFFIX
            print('send category matching suggestions to kinesis through stream', stream_name)
            debug_var = upload_to_kinesis(spark, model_name, stream_name, run_id, environment)
            print(debug_var)
        except Exception:
            # delete_suggestions_by_run_id(spark, model_name, date, run_id)
            print(Exception)
        #     # TODO: log traceback
        #     exit()
    else:
        if flow != const.FLOW_TEST:
            print('Error with flow name! use test flow folder')
        path = os.path.join(path, model_name, version, flow, const.OUTPUT_FOLDER)
        save_data_to_csv(full_df=output_df, path=path, file_name='final_results.csv', output_columns=output_df.columns)
