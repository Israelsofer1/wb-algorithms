from ..constant import tables, const
import pyspark.sql.functions as func
from .save_data import save_data, upload_to_beer_table, map_columns_to_sql_table, save_to_sql, save_data_to_csv, add_fields, upload_to_kinesis
from .data_to_merge import get_mapping
from pyspark.sql.types import StringType, DoubleType, IntegerType, StructType, StructField
from ..PreProcess.matching_config_creator import beer_field_order
import os

fields = {'beer': ['brand_id', 'super_brand_id', 'serving_type_id', 'volume_id']}
id_field = 'bar_product_id'


def get_results_from_table(spark, table_name, date, run_id):
    q = "SELECT * FROM stg.{table} WHERE run_date='{run_date}' AND run_id = '{id}'".format(table=table_name, run_date=date, id=run_id)
    print(q)
    return spark.sql(q)


def rename_columns(df, columns, suffix):
    return_columns = []
    for column in columns:
        if column != 'bar_product_id':
            return_columns.append(column + '_prob' + '_' + suffix)
            return_columns.append(column + '_' + suffix)
            df = df.withColumn(column + '_' + suffix, func.col(column))
            df = df.withColumn(column + '_prob' + '_' + suffix, func.col(column + '_prob'))
        else:
            return_columns.append(column)
    df = df.select(return_columns)
    return df


def transform_table(results_df, columns):
    fields = ['brand_id', 'super_brand_id', 'serving_type_id', 'volume_id']
    id_field = 'bar_product_id'

    all_columns = fields[:]
    all_columns.append(id_field)

    # Convert to integer the id field
    results_df = results_df.withColumn(id_field, results_df[id_field].cast(StringType()))

    for column_name in fields:
        col_id_type = IntegerType()
        if column_name == 'volume_id':
            col_id_type = DoubleType()
        results_df = results_df.withColumn(column_name, results_df[column_name].cast(col_id_type))
        results_df = results_df.withColumn(column_name + '_prob', results_df[column_name + '_prob'].cast(DoubleType()))

    nn_results_df = results_df.filter(results_df['model_type'] == 'NN')
    nn_results_df = rename_columns(nn_results_df, all_columns, 'NN')
    nn_results_df.show()

    bow_results_df = results_df.filter(results_df['model_type'] == 'BOW')
    bow_results_df = rename_columns(bow_results_df, all_columns, 'BOW')
    bow_results_df.show()

    rule_results_df = results_df.filter(results_df['model_type'] == 'rule')
    rule_results_df = rename_columns(rule_results_df, all_columns, 'rule')
    rule_results_df.show()

    merged_df = nn_results_df.join(bow_results_df, on=id_field)
    merged_df = merged_df.join(rule_results_df, on=id_field, how='left')

    for column in merged_df.columns:
        merged_df = merged_df.withColumn(column, func.when(func.col(column).isNull(), 0).otherwise(func.col(column)))

    return merged_df

def max_from_two(first_id, first_prob, second_id, second_prob):
    if first_prob > second_prob:
        return first_id, first_prob
    else:
        return second_id, second_prob

def compare_two(first_id, first_prob, second_id, second_prob, brand_list=[]):
    if first_id == second_id:
        return first_id, max(first_prob, second_prob)
    elif len(brand_list) > 0:
        spam_or_ham_threshold = 0.85
        if first_id in brand_list and second_id == 0:
            print('conflict')
            if second_prob > spam_or_ham_threshold:
                return second_id, second_prob
            else:
                return first_id, first_prob
        elif first_id not in brand_list and second_id == 0:
            return first_id, first_prob
        else:
            return max_from_two(first_id, first_prob, second_id, second_prob)
    else:
        return max_from_two(first_id, first_prob, second_id, second_prob)


def choose_result(nn_id, nn_prob, bow_id, bow_prob, rule_id, rule_prob, brand_list):
    if nn_id == bow_id and bow_id == rule_id:
        return nn_id, max(nn_prob, bow_prob, rule_prob)
    elif rule_prob == 1.0 and rule_id > 0:
        return rule_id, rule_prob
    else:
        nn_bow_max_id, nn_bow_max_prob = compare_two(nn_id, nn_prob, bow_id, bow_prob, brand_list)  # type: (int, double)
        nn_rule_max_id, nn_rule_max_prob = compare_two(nn_id, nn_prob, rule_id, rule_prob)
        bow_rule_max_id, bow_rule_max_prob = compare_two(bow_id, bow_prob, rule_id, rule_prob)

        if nn_bow_max_prob > 0.85 and rule_prob < 0.85:
            return nn_bow_max_id, nn_bow_max_prob
        elif nn_rule_max_prob > 0.85 and bow_id < 0.85:
            return nn_rule_max_id, nn_rule_max_prob
        elif bow_rule_max_prob > 0.85 and nn_id < 0.85:
            return bow_rule_max_id, bow_rule_max_prob
        else:
            first_max_id, first_max_prob = max_from_two(nn_bow_max_id, nn_bow_max_prob, nn_rule_max_id, nn_rule_max_prob)
            second_max_id, second_max_prob = max_from_two(nn_bow_max_id, nn_bow_max_prob, bow_rule_max_id, bow_rule_max_prob)
            return max_from_two(first_max_id, first_max_prob, second_max_id, second_max_prob)

def choose_results_for_all(broadcast, input_df, column_name):
    def choose_result_udf(nn_id, nn_prob, bow_id, bow_prob, rule_id, rule_prob):
        return choose_result(nn_id, nn_prob, bow_id, bow_prob, rule_id, rule_prob, broadcast.value)

    return_id_type = IntegerType()
    if column_name == 'volume_id':
        return_id_type = DoubleType()

    result_type = StructType([
        StructField("id", return_id_type, False),
        StructField("prob", DoubleType(), False)
    ])

    choose_result_udf = func.udf(choose_result_udf, result_type)

    input_df = input_df.withColumn(column_name,
                               choose_result_udf(func.col(column_name + '_NN'),
                                                 func.col(column_name + '_prob' + '_NN'),
                                                 func.col(column_name + '_BOW'),
                                                 func.col(column_name + '_prob' + '_BOW'),
                                                 func.col(column_name + '_rule'),
                                                 func.col(column_name + '_prob' + '_rule')))
    return input_df

def column_name_to_array(column_name):
    return [column_name + '_' + const.MODEL_TYPE_NN,
            column_name + '_prob' + '_' + const.MODEL_TYPE_NN,
            column_name + '_' + const.MODEL_TYPE_BOW,
            column_name + '_prob' + '_' + const.MODEL_TYPE_BOW,
            column_name + '_' + const.MODEL_TYPE_RULE_BASE,
            column_name + '_prob' + '_' + const.MODEL_TYPE_RULE_BASE]

def load_data_from_csv(spark, base_path, model_name, version, flow):
    output_path = os.path.join(base_path, model_name, version, flow, const.OUTPUT_FOLDER)
    columns = ['bar_product_id']
    columns.extend(multiple_columns(beer_field_order))

    # Load NN + BOW results
    file_path = os.path.join(output_path, 'final_results.csv')
    final_results_df = spark.read.format("csv").option("header", "true").load(file_path)
    columns.append('model_type')
    final_results_df = final_results_df.select(columns)
    print('load data from', file_path, 'for ensemble with columns', final_results_df.columns)

    # Load rule base results
    file_path = os.path.join(output_path, 'rule_results.csv')
    rule_base_results_df = spark.read.format("csv").option("header", "true").load(file_path)
    rule_base_results_df = rule_base_results_df.select(columns)
    rule_base_results_df = rule_base_results_df.withColumn('model_type', func.lit(const.MODEL_TYPE_RULE_BASE))

    print('load data from', file_path, 'for ensemble with columns', rule_base_results_df.columns)
    results_df = rule_base_results_df.union(final_results_df)
    return results_df

def run_ensemble(spark, base_path, model_name, version, flow, date, run_id):
    if flow == 'predict':
        results_df = get_results_from_table(spark, table_name=tables[model_name], date=date, run_id=run_id)
    elif flow == 'test':
        results_df = load_data_from_csv(spark, base_path, model_name, version, flow)
    else:
        print('error, the glow name', flow, 'not matched to test or predict')
        results_df = spark.createDataFrame([])

    environment = base_path.split('//')[1].split('-')[0]
    output_df = run_predict_ensemble(spark, results_df, model_name, base_path, version)
    save_ensemble_data(spark, environment, output_df, base_path, model_name, version, flow, date, run_id)
    return output_df

def multiple_columns(col_names):
    # Be careful - the column order is important!
    table_columns = []
    for column in col_names:
        table_columns.append(column)
        table_columns.append(column + '_prob')
    return table_columns

def run_predict_ensemble(spark, results_df, model_name, base_path, version):
    print("Get the data for the model")
    merged_df = transform_table(results_df, fields[model_name])
    print('data count after split transform table', merged_df.count())

    model_mapping = get_mapping(spark, base_path, model_name, version, 'BOW')
    brand_list = model_mapping['brand_id']
    broadcast = spark.sparkContext.broadcast(brand_list)
    for column_name in fields[model_name]:
        if column_name != 'bar_product_id':
            print('choose best output (id, prob) for:', column_name)
            merged_df = choose_results_for_all(broadcast, merged_df, column_name)

    all_columns = fields[model_name][:]
    all_columns.append(id_field)
    merged_df = merged_df.select(all_columns)

    print('data count after choose result', merged_df.count())
    output_df = merged_df.select(all_columns)
    for column_name in output_df.columns:
        if column_name in fields[model_name]:
            print('split column', column_name, 'from struct to prob + id')
            output_df = output_df.withColumn(column_name + '_prob', func.col(column_name + ".prob"))
            output_df = output_df.withColumn(column_name, func.col(column_name + ".id"))

    print('data count after split struct', output_df.count())

    return output_df


def save_ensemble_data(spark, environment, output_df, bucket, model_name, version, flow, date, run_id):
    if flow == const.FLOW_PREDICT:
        output_hive_df = save_data(output_df, model_name, version, date, run_id, is_cross_validation=True)
        print('save beer table to hive in environment', environment)
        upload_to_beer_table(spark, output_hive_df, 'beer_final')
        stream_name = environment + const.KINESIS_STREAM_SUFFIX
        print('send beer matching suggestions to kinesis through stream', stream_name)
        upload_to_kinesis(spark, model_name, stream_name, run_id, environment)
        # TODO: add rollback

    elif flow == const.FLOW_TEST:
        output_path = os.path.join(bucket, model_name, version, flow, const.OUTPUT_FOLDER)
        print('save data with flow', flow, 'to folder', output_path)
        save_data_to_csv(output_df, output_path, 'ensemble_results.csv', output_df.columns)
    else:
        print('error in the flow name, cannot save the results')


