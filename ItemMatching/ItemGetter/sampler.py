from pyspark.sql import SparkSession
import pyspark.sql.functions as func
from pyspark.sql.types import FloatType, StringType, MapType, ArrayType
from datetime import datetime, timedelta
from ast import literal_eval
from ..constant import const

#dstart = datetime.strftime((datetime.now() - timedelta(days=4)).date(), '%Y-%m-%d')
#dend = datetime.strftime((datetime.now() - timedelta(days=1)).date(), '%Y-%m-%d')

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL split_orders_df from stg to Redshift") \
    .enableHiveSupport() \
    .getOrCreate()


def get_data_file(file_path, model):
    # file_adrs = 's3://wb-ds-models/data/%s.csv' % fname
    df = spark.read.csv(file_path, header=True)
    df = df.withColumn('category_id', func.col('category_id').astype('int'))
    # if model == const.MODEL_BEER:
    #     df = df.withColumn('type_id', eval_udf(df['type_id']))
    #     df = df.withColumn('type_id', func.explode(df['type_id']))
    return df


#ToDo: Divide the function to beer and category and for each of them split the logic of countries and language to seperate functions
#ToDo: Define a regular expression map to support languages
def get_data_sql(spark, mname, limit=None, countries=None, language=None):
    korea_english_query = ""
    if mname == 'category':
        q = """
            SELECT  bp.product_id as bar_product_id, COALESCE(sp.title, bp.title) as title, bp.price, bp.category_id, a.country as country_id, bp.pos_category_name
            FROM	stg.pos_products_new_flat bp
                    LEFT JOIN 
                    (select product_id, first(tags) as tags from stg.pos_final_products group by product_id) pfp on bp.product_id = pfp.product_id
                    LEFT JOIN 
                    stg.pos_spelling sp on sp.id = bp.product_id
                    JOIN
                    stg.bars a ON a.id = bp.bar_id
                    JOIN
                    (SELECT bar_id, `value` as delivery_date FROM stg.bars_metadata WHERE `key` = 'pos_qc_approved') bm on a.id = bm.bar_id
            WHERE pfp.tags.tag_groups[0].tag_sets[0].category_id is not null 
            AND pfp.tags.tag_groups[0].tag_sets[0].category_id != 0 AND a.status BETWEEN 10 AND 19
            AND bp.created_at < bm.delivery_date
            AND bp.title != ''
        """
        if language is not None and str(language).lower() == const.KOREAN_LANG:
            q += get_korean_clause()
        if countries is None or countries == [] or len(countries) == 0:
            korea_english_query = q
            q += """ AND a.country IN ('Canada' , 'Australia', 'United States', 'United Kingdom', 'Argentina', 'Brazil', 'France') """
        else:
            country_clause = """ AND a.country IN """
            country_clause += "('" + "','".join(countries) + "')"
            q += country_clause
    elif mname == 'beer':
        q = """
            SELECT  bp.product_id as bar_product_id, bp.title, bp.price, bp.category_id, a.country as country_id, bp.pos_category_name,
                    bp.beer_brand_id as brand_id, coalesce(sb.super_brand_id, 0) as super_brand_id, cast(cast(bp.volume_per_unit as double)*cast(bp.units as double) as double) as volume_id, 
                    null as beer_type_id, bp.beer_serving_type_id as serving_type_id
            FROM	stg.pos_products_new_flat bp
                    LEFT JOIN 
                    (select product_id, first(tags) as tags from stg.pos_final_products group by product_id) pfp on bp.product_id = pfp.product_id
                    LEFT JOIN
                    stg.bars a ON a.id = bp.bar_id
                    LEFT JOIN
                    stg.pos_beer_brands_super_brands sb ON sb.brand_id = bp.beer_brand_id
                    JOIN
                    (SELECT bar_id, `value` as delivery_date FROM stg.bars_metadata WHERE `key` = 'pos_qc_approved') bm on a.id = bm.bar_id
            WHERE   pfp.tags.tag_groups[0].tag_sets[0].category_id = 1 and (bp.is_guessed = 0 or bp.is_guessed = False) and (bp.do_not_learn = 0 or bp.do_not_learn = False) 
                    and cast(bp.beer_brand_id_prob as double) > 1 and cast(bp.beer_serving_type_id_prob as double) >1 and cast(bp.volume_per_unit_prob as double) >1
                    and a.status between 10 and 19
                    -- and (source = 0 or source = 2 or (source = 1 AND bm.delivery_date is not null and bp.created_at < bm.delivery_date))
                    AND bp.product_id != 0
                    AND bp.title != ''
                    """
        if language is not None and str(language).lower() == const.KOREAN_LANG:
            q += get_korean_clause()
        if countries is None or countries == [] or len(countries) == 0:
            korea_english_query = q
            q += """ and a.country in ('Canada', 'Australia','United States','United Kingdom','Argentina', 'Brazil', 'France') """
        else:
            country_clause = """ and a.country in """
            country_clause += "('" + "','".join(countries) + "')"
            q += country_clause
    else:
        raise Exception('Wrong model name, available: 0, 1')

    if limit is not None:
        lim_str = " LIMIT %s " % limit
    else:
        lim_str = ""
    q += lim_str
    print("query for sampling is: " + q)
    korea_english_df = None
    if len(korea_english_query) > 0:
        korea_english_df = get_korea_english_data_sql(spark, korea_english_query)
    general_df = spark.sql(q)
    if korea_english_df is not None and not korea_english_df.rdd.isEmpty():
        general_df = general_df.union(korea_english_df)
    return general_df


def get_korea_english_data_sql(spark, query):
    query += " AND a.country IN ('Korea') AND bp.title not rlike '[ㄱ-ㅎ가-힣]' AND bp.pos_category_name not rlike '[ㄱ-ㅎ가-힣]'"
    print("Korean query for sampling is: \n" + query)
    return spark.sql(query)

def get_korean_clause():
    clause = "AND CONCAT(bp.title, ' ', bp.pos_category_name) rlike '[ㄱ-ㅎ가-힣]' "
    return clause

def eval_err_handle(x):
    try:
        return literal_eval(x)
    except:
        return []

def remove_statistical_matching(input_df):
    print('remove items with more then one brand for beer')
    df_product_with_two_brands = input_df.groupby('bar_product_id').agg(
        func.count(func.col('brand_id')).alias("count_brand_id"))
    df_grouped_common = df_product_with_two_brands.filter(func.col('count_brand_id') < 2)
    input_df = input_df.join(df_grouped_common, on='bar_product_id')
    columns = input_df.columns
    columns.remove('count_brand_id')
    return input_df.select(columns)

def remove_rare_brands(input_df, brand_min = 2):
    print('remove rare brands for beer, brand appear with minimum of', brand_min + 1)
    df_grouped = input_df.groupby('brand_id').count()
    df_grouped_common = df_grouped.filter(func.col('count') > brand_min)
    input_df = input_df.join(df_grouped_common, on='brand_id')
    columns = input_df.columns
    columns.remove('count')
    return input_df.select(columns)

eval_udf = func.udf(lambda x: eval_err_handle(x), ArrayType(StringType()))

concat_udf = func.udf(lambda cols: ".".join([str(x) if x is not None else "*" for x in cols]), StringType())

dict_udf = func.udf(lambda maps: {key: f[key] for f in maps for key in f}, MapType(StringType(), FloatType()))


def smart_sample(df, path, dist_cols, model, version):
    print("Creating dist key")
    key_name = "unique_id"
    df = df.withColumn(key_name, concat_udf(func.array(dist_cols)))

    # print("Getting key proportions, not allowing small datasets to be split")
    # prop = 0.8
    # min_count = 4
    # count_name = "count"
    # dist_counts = df.groupBy(key_name).count()
    # dist_counts = dist_counts.toPandas().set_index(key_name).T.to_dict()

    # fractions = {k: float(max(prop * v[count_name], min(min_count, v[count_name]))) / v[count_name] for k, v in
    #            dist_counts.items()}

    # print("Sampling according to the proportions")
    # seed = 8
    # df.sampleBy(col=key_name, fractions=fractions, seed=seed)
    # df = df.drop(key_name)

    if model == const.MODEL_CATEGORY:
        proportions = {1: 0.4, 2: 0.1, 3: 0.25, 4: 0.5, 5: 0.5, 6: 0.5}
        print('Smart sampling for ', model, 'with the proportion:', proportions)
        df = df.sampleBy(col='category_id', fractions=proportions, seed=8)
        print(df.count())

    print('Split and mark for test/train, join them later')
    split_name = 'train'
    splits = df.randomSplit([0.2, 0.8])

    splits[0] = splits[0].withColumn(split_name, func.lit(0))
    splits[1] = splits[1].withColumn(split_name, func.lit(1))

    print("Upload the sample")
    splits[1].repartition(1).write.mode('overwrite').option('header', 'true').csv(
        path + str(model) + '/' + str(version) + '/' + const.FLOW_TRAIN + '/' + const.INPUT_FOLDER, compression='gzip')

    splits[0].repartition(1).write.mode('overwrite').option('header', 'true').csv(
        path + str(model) + '/' + str(version) + '/' + const.FLOW_TEST + '/' + const.INPUT_FOLDER, compression='gzip')
    return splits

dist_cols_archive = {'category': ['category_id'],
                     'beer': ['category_id', 'brand_id', 'serving_type_id', 'country_id']}


# No flow and date - because its only train + test flow and each training session is a new version
def pos_matching_sampler(spark, path, model_name, version, file_path=None, countries="", language=""):
    # Oren: By default countries and language can be either None or empty string so I check both
    if isinstance(countries,str) and len(countries) > 0:
        countries = list(countries.split(","))
    elif isinstance(countries,str) and len(countries) == 0:
        countries = None
    if file_path:
        new_products_df = get_data_file(file_path, model_name)
    else:
        new_products_df = get_data_sql(spark, model_name, countries=countries, language=language)
    if model_name == const.MODEL_BEER:
        new_products_df = remove_rare_brands(new_products_df)
        new_products_df = remove_statistical_matching(new_products_df)
    train_test_df = smart_sample(df=new_products_df, path=path, dist_cols=dist_cols_archive[model_name], model=model_name, version=version)
    return train_test_df
