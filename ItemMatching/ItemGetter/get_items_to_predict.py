import os
from ..constant import const, tables
import pyspark.sql.functions as func
import ast


def get_country_id_4_country(spark, countries):
    q = """select id
        from stg.countries
        where name in """
    q+= "('" + "','".join(countries) + "')"
    query_res = spark.sql(q)
    query_res_lst = query_res.select("id").collect()
    country_ids = [str(row["id"]) for row in query_res_lst]
    return country_ids


def get_category_data(spark, country_ids="", lang=""):
    korea_english_query = ""
    q = """SELECT pbp.product_id as bar_product_id, pbp.title, pbp.price, b.country as country_id, pbp.pos_category_name, pbp.category_id as category_id, pbp.created_at as inserted_at 
            FROM stg.pos_products_new_flat pbp
            LEFT JOIN 
            (select product_id, first(tags) as tags from stg.pos_final_products group by product_id) pfp on pbp.product_id = pfp.product_id
            JOIN stg.bars b ON pbp.bar_id = b.id
            LEFT JOIN 
            stg.pos_category_suggestions sug on pbp.product_id = sug.bar_product_id 
            WHERE (pfp.tags.tag_groups[0].tag_sets[0].category_id = 0 OR pfp.tags.tag_groups[0].tag_sets[0].category_id IS NULL)
            AND pbp.title != ''
            AND sug.bar_product_id is null"""
    if country_ids == "":
        korea_english_query = q
        q += """ AND b.country_id IN (24,186,187,194,195,200,60) """
    else:
        country_clause = """ AND b.country_id IN """
        country_clause += "(" + ",".join(country_ids) + ")"
        q += country_clause
    if lang is not None and lang != "" and str(lang).lower() == const.KOREAN_LANG:
        q += get_korean_clause()
    q+= """ ORDER BY pbp.created_at desc"""
    query_res = spark.sql(q)
    korea_english_df = None
    if len(korea_english_query) > 0:
        korea_english_df = get_korea_english_data_sql(spark, korea_english_query)
    if korea_english_df is not None and not korea_english_df.rdd.isEmpty():
        query_res = query_res.union(korea_english_df)
        query_res = query_res.orderBy(func.desc("inserted_at"))
    return (query_res)

def get_beer_data(spark, country_ids="", lang=""):
    korea_english_query = ""
    q = """SELECT  pbp.product_id as bar_product_id, pbp.title, pbp.price, pbp.category_id, b.country as country_id, pbp.pos_category_name, pbp.beer_brand_id as brand_id, 
                    coalesce(sb.super_brand_id, 0) as super_brand_id, cast(cast(pbp.volume_per_unit as double)*cast(pbp.units as double) as double) as volume_id, pbp.beer_serving_type_id as serving_type_id, pbp.created_at as inserted_at
        FROM	stg.pos_products_new_flat pbp
                inner join 
                (select product_id, first(tags) as tags from stg.pos_final_products group by product_id) pfp on pbp.product_id = pfp.product_id
				left JOIN
                stg.pos_spelling ps on ps.id = pbp.product_id
                JOIN
                stg.bars b ON b.id = pbp.bar_id
                LEFT JOIN
                stg.pos_beer_brands_super_brands sb ON sb.brand_id = pbp.beer_brand_id
                LEFT JOIN 
                stg.pos_beer_suggestions_final sug on pbp.product_id = sug.bar_product_id 
        WHERE   pfp.tags.tag_groups[0].tag_sets[0].category_id = 1 and (pbp.is_guessed = 0 or pbp.is_guessed = False) and (pbp.do_not_learn = 0 or pbp.do_not_learn = False)
        and pbp.beer_brand_id is null and pbp.volume_per_unit is null and pbp.beer_serving_type_id is null
        AND sug.bar_product_id is null 
        AND pbp.title != '' """
    if lang is not None and lang != "" and str(lang).lower() == const.KOREAN_LANG:
        q += get_korean_clause()
    if country_ids == "":
        korea_english_query = q
        q += """ AND b.country_id IN (24,186,187,194,195,200,60) """
    else:
        country_clause = """ AND b.country_id IN """
        country_clause += "(" + ",".join(country_ids) + ")"
        q += country_clause
    q+= """ORDER BY pbp.created_at desc
            """
    query_res = spark.sql(q)
    korea_english_df = None
    if len(korea_english_query) > 0:
        korea_english_df = get_korea_english_data_sql(spark, korea_english_query)
    if korea_english_df is not None and not korea_english_df.rdd.isEmpty():
        query_res = query_res.union(korea_english_df)
        query_res = query_res.orderBy(func.desc("inserted_at"))
    return (query_res)


def get_korea_english_data_sql(spark, query):
    query += """ AND b.country_id IN (198) AND pbp.title not rlike '[ㄱ-ㅎ가-힣]' AND pbp.pos_category_name not rlike '[ㄱ-ㅎ가-힣]' 
                 ORDER BY pbp.created_at desc
    """
    return spark.sql(query)


def get_korean_clause():
    clause = "AND CONCAT(pbp.title, ' ', pbp.pos_category_name) rlike '[ㄱ-ㅎ가-힣]' "
    return clause


def items_to_predict(spark, path, model, version, flow, date, run_id, countries="", language="", limit=-1):
    print('type of countries:', type(countries))
    print('countries:', countries)
    if isinstance(countries,str) and len(countries) > 0:
        countries = list(countries.split(","))
    country_ids = ""
    if countries is not None and countries != "" and countries != [] and len(countries) != 0:
        country_ids = get_country_id_4_country(spark=spark, countries=countries)
    if model == const.MODEL_BEER:
        df_input = get_beer_data(spark, country_ids=country_ids, lang=language)
    elif model == const.MODEL_CATEGORY:
        df_input = get_category_data(spark, country_ids=country_ids, lang=language)
    else:
        print('Error! mdoel name does not exist, using category model as default')
        df_input = get_category_data(spark, country_ids=country_ids, lang=language)

    if limit != -1:
        df_input = df_input.limit(limit)
    path = os.path.join(path, model, version, flow, date, run_id, const.INPUT_FOLDER)
    # Since we are using CSV, replace comma from the data
    df_input = df_input.withColumn('title', func.regexp_replace('title', ',', ' '))
    df_input = df_input.withColumn('title', func.regexp_replace('title', '\\"', ' '))
    #check countries petch - to delete
    df_input = df_input.withColumn('country_id',func.when(df_input.country_id == 'USA', func.lit('United States')).otherwise(df_input.country_id))
    print('Load data from hive DB to', path)
    df_input.coalesce(1).write.mode('overwrite').option('header', 'true').csv(path, compression="gzip")
    return df_input
