import pandas as pd
import numpy as np
import json


class WordChecker(object):
    def __init__(self):
        # Read the required data
        with open('word_sim_counts.json', 'rb') as outf:
            check_dict = json.load(outf)
        self.tref = check_dict['reference']
        self.count = np.array(check_dict['counts'])

    def closest_words(self, token, nlargest=6):
        """
        returns nlargest - 1 similar words
        :return:
        """
        # Find the N largest counts
        tvector = self.count[self.tref.index(token)]
        largest_indexes = np.argpartition(tvector, -nlargest)[-nlargest:]
        # Sort them
        indx_count = zip(largest_indexes, tvector[largest_indexes])
        indx_count.sort(key=lambda x: x[1])
        # print and output
        sorted_indexes = zip(*indx_count)[0]
        sorted_tokens = [self.tref[i] for i in sorted_indexes if self.tref[i] != token]
        print(sorted_tokens)
        return sorted_tokens

    def words_sim(self, token1, token2, nlargest=16):
        """
        Find the number of overlapping words appearing in
        :return:
        """
        t1_sim = self.closest_words(token1, nlargest)
        t2_sim = self.closest_words(token2, nlargest)
        overlap = list(set(t1_sim) & set(t2_sim))
        print(overlap)
        return float(len(overlap)) / min([(nlargest - 1), len(t1_sim), len(t2_sim)])


def word_update(tokens, count_mtrx, reference):
    for t in tokens:
        t_idx = reference.index(t)
        for t2 in tokens:
            t2_idx = reference.index(t2)
            count_mtrx[t_idx, t2_idx] += 1
            count_mtrx[t2_idx, t_idx] += 1


def construct_sim(set_name):
    data = pd.read_csv(set_name)
    tokens = data['title'][:50000].str.lower().str.split().dropna()
    # Create a corpus
    tref = set()
    tokens.apply(tref.update)
    tref = list(tref)
    # Create a nXn matrix and update it, refer to the corpus for indexes
    count_mtrx = np.zeros((len(tref), len(tref)))
    tokens.apply(lambda x: word_update(x, count_mtrx, tref))
    # Save the count as a JSON
    res = {
        'reference': tref,
        'counts': count_mtrx.tolist()
    }
    with open('word_sim_counts.json', 'wb') as outf:
        json.dump(res, outf)


def str_cleaner(title):
    pass


if __name__ == '__main__':
    # construct_sim('pos_trainset.csv')
    wc = WordChecker()
    wc.words_sim('bud', 'coors')