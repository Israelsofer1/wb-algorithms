from pyspark.sql import  Window,SparkSession
from pyspark.sql import SQLContext, Row
from pyspark.sql.functions import dayofweek, dayofyear
from pyspark.sql.functions import concat, col, lit, count, regexp_replace, split, explode, col, udf ,max , rank ,hash, row_number
from pyspark.sql.functions import to_date, date_format, from_json
from pyspark.sql.functions import UserDefinedFunction
from pyspark.sql.types import StringType, DateType, DoubleType, IntegerType, StructField, StructType, BooleanType, ArrayType
from pyspark.sql.types import *
from datetime import date, datetime, timedelta
import traceback
import argparse
import json
import os
import boto3

# from Common.logging_config import configure_logger
from Project.Common.logging_config import configure_logger

logger = configure_logger(__name__, "DEBUG")



class SuggestiosMigrator(object):

    def __init__(self, spark, tbl_name, hive_s3_bucket):
        self.spark = spark
        self.tbl_name = tbl_name
        self.hive_s3_bucket = hive_s3_bucket
        self.current_s3_location = "s3://" + hive_s3_bucket
        self.partitions = ["run_date","run_id"]
        if self.tbl_name == "pos_beer_suggestions":
            self.partitions.append("model_type")
        self.df_partitions = "run_id"

    def copyTblContent(self, src_folder, dest_folder):
        src_folder += "/"
        bucket_name = self.hive_s3_bucket
        session = boto3.Session()
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket_name)
        for object in bucket.objects.filter(Prefix=src_folder):
            srcKey = object.key
            if not srcKey.endswith('/'):
                fileName = "/".join(srcKey.split('/')[1:])
                destFileKey = dest_folder + '/' + fileName
                copySource = bucket_name + '/' + srcKey
                s3.Object(bucket_name, destFileKey).copy_from(CopySource=copySource)

    def delS3FolderContent(self, folder_name):
        bucket_name = self.hive_s3_bucket
        session = boto3.Session()
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket_name)
        for object in bucket.objects.filter(Prefix=folder_name):
            srcKey = object.key
            if not srcKey.endswith('/'):
                s3.Object(bucket_name, srcKey).delete()


    def get_data_2_migrate(self):
        self.spark.sql("REFRESH TABLE stg." + self.tbl_name)
        self.spark.sql("MSCK REPAIR TABLE stg." + self.tbl_name)
        self.initial_df_2_migrate = self.spark.sql("""
        SELECT DISTINCT pmp.productid as product_id, sug.*
        FROM stg.""" + self.tbl_name + """ sug
        INNER JOIN stg.pos_matching_products pmp on cast(sug.bar_product_id as integer) = pmp.barproductid
        """)
        return

    def drop_unnecesary_columns(self):
        original_columns = self.initial_df_2_migrate.columns
        original_columns.remove("bar_product_id")
        self.initial_df_2_migrate = self.initial_df_2_migrate.select(*original_columns).withColumnRenamed("product_id", "bar_product_id")
        return

    def write_new_tbl(self):
        backup_folder_name = self.tbl_name + "_BU_" + datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        self.copyTblContent(src_folder=self.tbl_name, dest_folder=backup_folder_name)
        self.temp_tbl_name = self.tbl_name + "_temp"
        self.temp_s3_location = os.path.join(self.current_s3_location, self.temp_tbl_name)
        column_names_df = self.spark.sql("describe stg.{}".format(self.tbl_name))
        column_names_array = [row["col_name"] for row in column_names_df.select("col_name").collect()]
        first_idx_to_ignore = next(i for i, obj in enumerate(column_names_array) if obj.startswith("# Partition Infor"))
        self.columns_list_2_save = column_names_array[:first_idx_to_ignore]
        self.spark.sql("CREATE DATABASE IF NOT EXISTS stg")
        self.spark.sql("DROP TABLE IF EXISTS stg." + self.temp_tbl_name)
        self.spark.sql("CREATE TABLE IF NOT EXISTS stg." + self.temp_tbl_name + " like stg." + self.tbl_name + " LOCATION '" + self.temp_s3_location + "' ")
        registered_tbl_name = "df2migrate_" + self.temp_tbl_name
        logger.info("Initial data count in table {} is {}".format(self.tbl_name, self.initial_df_2_migrate.count()))
        self.initial_df_2_migrate.repartition(self.df_partitions).registerTempTable(registered_tbl_name)
        query_for_temp_tbl = """INSERT OVERWRITE TABLE stg.""" + self.temp_tbl_name + """
                        PARTITION (""" + ",".join(self.partitions) + """)
                        select """ + ",".join(self.columns_list_2_save) + """
                        from """ + registered_tbl_name
        self.spark.sql(query_for_temp_tbl)
        self.spark.sql("MSCK REPAIR TABLE stg." + self.temp_tbl_name)
        # self.delS3FolderContent(folder_name=self.tbl_name)
        query_from_temp_tbl_to_sug = """INSERT OVERWRITE TABLE stg.""" + self.tbl_name + """
                                    PARTITION (""" + ",".join(self.partitions) + """)
                                    select * from stg.""" + self.temp_tbl_name
        self.spark.sql(query_from_temp_tbl_to_sug)
        self.spark.sql("REFRESH TABLE stg." + self.tbl_name)
        self.spark.sql("MSCK REPAIR TABLE stg." + self.tbl_name)
        count_items_after_migrate = int(self.spark.sql("SELECT COUNT(*) as count_items from stg." + self.tbl_name).select("count_items").collect()[0]["count_items"])
        logger.info("Data count in table {} after migration is {}".format(self.tbl_name, count_items_after_migrate))
        self.delS3FolderContent(folder_name=self.temp_tbl_name)
        self.spark.sql("DROP TABLE IF EXISTS stg." + self.temp_tbl_name)

    def read_process_write_drop(self):
        self.get_data_2_migrate()
        self.drop_unnecesary_columns()
        self.write_new_tbl()
