import unittest
from ItemMatching.ItemGetter import get_items_to_predict
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
import pandas as pd
from ItemMatching.constant import const
import csv
import flatten_pos_prod_new

class Test_flatten_jobs(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()

    def test_flatten_pos_prod_new(self):
        input_df_pos_prod = self.spark.read.json("/Users/orens/PycharmProjects/Data/sampled_data/flattening_jobs/pos_prod_new.json")
        input_df_flat = self.spark.read.json("/Users/orens/PycharmProjects/Data/sampled_data/flattening_jobs/pos_prod_flat.json")
        # input_df.show()
        tbl_flatter = flatten_pos_prod_new.Flat_Tbl_products_new(self.spark, -1)
        tbl_flatter.df2flatten = input_df_pos_prod
        tbl_flatter.df_pos_prod_new_flat = input_df_flat
        tbl_flatter.get_new_and_modified_prod_ids()
        tbl_flatter.flatten_df()
        tbl_flatter.handle_casting_errors()
        tbl_flatter.prepare_items_2_write()

if __name__ == '__main__':
    unittest.main()
