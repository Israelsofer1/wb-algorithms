from pyspark.sql import  Window,SparkSession
from pyspark.sql import SQLContext, Row
from pyspark.sql.functions import dayofweek, dayofyear
from pyspark.sql.functions import concat, col, lit, count, regexp_replace, split, explode, col, udf ,max , rank ,hash, row_number
from pyspark.sql.functions import to_date, date_format, from_json
from pyspark.sql.functions import UserDefinedFunction
from pyspark.sql.types import StringType, DateType, DoubleType, IntegerType, StructField, StructType, BooleanType, ArrayType
from pyspark.sql.types import *
from datetime import date, datetime, timedelta
import traceback
import argparse
import json
import os
import sys
sys.path.insert(1, os.path.join(sys.path[0], '...'))

# from Common.logging_config import configure_logger
from Project.Common.logging_config import configure_logger

logger = configure_logger(__name__, "DEBUG")


#Tbl: pos_products_new_flat
#Col: product_id : String
#Col: bar_id : Integer
#Col: title : String
#Col: price : Float
#Col: created_at : DateTime
#Col: modified_at : DateTime
#Col: pos_code : String
#Col: pos_product_volume : String
#Col: pos_category_name : String
#Col: _do_not_learn : Boolean
#Col: _is_guessed : Boolean
#Col: category_id : Integer
#Col: category_id_prob : Float
#Col: beer_brand_id : Integer
#Col: beer_brand_id_prob : Float
#Col: beer_serving_type_id : Integer
#Col: beer_serving_type_id_prob : Float
#Col: units : Float
#Col: units_prob : Float
#Col: volume_per_unit : Float
#Col: volume_per_unit_prob : Float
#Col: food_tags : Array<Struct: food_tag : String, food_tag_prob : Float>
#Col: food_types : Array<Struct: food_type : String, food_type_prob : Float>
#Col: food_super_types : Array<Struct: food_super_type : String, food_super_type_prob : Float>
#Col: food_course : String
#Col: food_course_prob : Float
#Col: food_cooking_method : String
#Col: food_cooking_method_prob : Float
#Col: food_taste : String
#Col: food_taste_prob : Float
#Col: food_nutrition : String
#Col: food_nutrition_prob : Float
#Col: food_shareable : Boolean
#Col: food_shareable_prob : Float


#Constants:
TBL_POS_PRODUCTS = "stg.pos_products"
ALIAS_TBL_POS_PRODUCTS = "ppn"
TBL_POS_PRODUCTS_FLAT = "stg.pos_products_new_flat"
CONST_FLATTENED_DF = "flattened_df"
CONST_ID = "id"
CONST_PROD_ID = "product_id"
CONST_NEW_AND_MODIFIED_ID = "new_modified_ids"
CONST_MODIFIED_AT = "modified_at"
CONST_EXISTING_MODIFIED_AT = "existing_modified_at"
CONST_NEW_MODIFIED_AT = "new_modified_at"
CONST_IS_ERR_ROW = "is_error_row"
CONST_IS_NEW = "is_new"
CONST_PROD_ROW_ID = "product_row_id"
CONST_PARTITION_KEY = "partition_key"
CONST_TAGS = "tags"
CONST_TEMP_JSON_TAGS = "temp_json_tags"
CONST_TAG_GROUPS = "tag_groups"
CONST_TAG_SETS = "tag_sets"
CONST_POS_CAT_NAME_INPUT = "pos_category"
CONST_POS_CAT_NAME_OUTPUT = "pos_category_name"
CONST_POS_CODE_INPUT = "pos_code"
CONST_POS_CODE_OUTPUT = "pos_code"
CONST_POS_PROD_VOLUME_INPUT = "pos_product_volume"
CONST_POS_PROD_VOLUME_OUTPUT = "pos_product_volume"
CONST_CATEGORY_ID_INPUT = "category_id"
CONST_CATEGORY_ID_OUTPUT = "category_id"
CONST_BRAND_ID_INPUT = "beer_brand_id"
CONST_BRAND_ID_OUTPUT = "beer_brand_id"
CONST_SERVING_TYPE_ID_INPUT = "beer_serving_type_id"
CONST_SERVING_TYPE_ID_OUTPUT = "beer_serving_type_id"
CONST_UNITS_INPUT = "units"
CONST_UNITS_OUTPUT = "units"
CONST_VOLUME_PER_UNIT_INPUT = "volume_per_unit"
CONST_VOLUME_PER_UNIT_OUTPUT = "volume_per_unit"
CONST_FOOD_COURSE_INPUT = "food_course"
CONST_FOOD_COURSE_OUTPUT = "food_course"
CONST_FOOD_COOKING_METHOD_INPUT = "food_cooking_method"
CONST_FOOD_COOKING_METHOD_OUTPUT = "food_cooking_method"
CONST_FOOD_TASTE_INPUT = "food_taste"
CONST_FOOD_TASTE_OUTPUT = "food_taste"
CONST_FOOD_NUTRITION_INPUT = "food_nutrition"
CONST_FOOD_NUTRITION_OUTPUT = "food_nutrition"
CONST_FOOD_SHAREABLE_INPUT = "food_shareable"
CONST_FOOD_SHAREABLE_OUTPUT = "food_shareable"
CONST_DO_NOT_LEARN_INPUT = "_do_not_learn"
CONST_DO_NOT_LEARN_OUTPUT = "do_not_learn"
CONST_IS_GUESSED_INPUT = "_is_guessed"
CONST_IS_GUESSED_OUTPUT = "is_guessed"
CONST_FOOD_TAGS_INPUT = "food_tags"
CONST_FOOD_TAGS_OUTPUT = "food_tags"
CONST_INNER_FOOD_TAG_INPUT = "food_tag"
CONST_INNER_FOOD_TAG_OUTPUT = "food_tag"
CONST_FOOD_TYPES_INPUT = "food_types"
CONST_FOOD_TYPES_OUTPUT = "food_types"
CONST_INNER_FOOD_TYPE_INPUT = "food_type"
CONST_INNER_FOOD_TYPE_OUTPUT = "food_type"
CONST_FOOD_SUPER_TYPES_INPUT = "food_super_types"
CONST_FOOD_SUPER_TYPES_OUTPUT = "food_super_types"
CONST_INNER_FOOD_SUPER_TYPE_INPUT = "food_super_type"
CONST_INNER_FOOD_SUPER_TYPE_OUTPUT = "food_super_type"
CONST_WORD_COL_NAME = "col_name"
CONST_WORD_VALUE = "value"
CONST_WORD_PROB = "prob"
CONST_STR_SHORT = "str"
CONST_INT_SHORT = "int"
CONST_FLOAT_SHORT = "float"
CONST_BOOL_SHORT = "bool"

class Flat_Tbl_products_new(object):
    def __init__(self, _spark, _limit):
        self.spark = _spark
        self.limit = _limit
        self.df2flatten = None
        self.df_pos_prod_new_flat = None
        self.flattened_df = None
        self.errors_flattened_df = None
        self.df_new_and_modified_ids = None
        return


    def get_flat_tbl_schema(self):
        column_names_df = self.spark.sql("describe {}".format(TBL_POS_PRODUCTS_FLAT))
        column_names_array = [row[CONST_WORD_COL_NAME] for row in column_names_df.select(CONST_WORD_COL_NAME).collect()]
        first_idx_to_ignore = next(i for i, obj in enumerate(column_names_array) if obj.startswith("# Partition Infor"))
        columns_list_2_save = column_names_array[:first_idx_to_ignore]
        return columns_list_2_save


    def get_items_2_flatten(self):
        self.spark.sql("REFRESH TABLE " + TBL_POS_PRODUCTS)
        self.spark.sql("MSCK REPAIR TABLE " + TBL_POS_PRODUCTS)
        query = """ 
        SELECT *
        FROM """ + TBL_POS_PRODUCTS + " " + ALIAS_TBL_POS_PRODUCTS

        if self.limit > 0:
            query += " limit {:d} ".format(self.limit)
        self.df2flatten = self.spark.sql(query)
        return


    def get_items_from_pos_prod_new_flat(self):
        self.spark.sql("REFRESH TABLE " + TBL_POS_PRODUCTS_FLAT)
        self.spark.sql("MSCK REPAIR TABLE " + TBL_POS_PRODUCTS_FLAT)
        self.df_pos_prod_new_flat = self.spark.sql(""" 
                SELECT *
                FROM """ + TBL_POS_PRODUCTS_FLAT
                )
        return


    def get_new_and_modified_prod_ids(self):
        df_existing_ids = self.df_pos_prod_new_flat.select(col(CONST_PROD_ID), col(CONST_MODIFIED_AT).alias(CONST_EXISTING_MODIFIED_AT))
        self.df_new_and_modified_ids = self.df2flatten.select(col(CONST_ID), col(CONST_MODIFIED_AT).alias(CONST_NEW_MODIFIED_AT))
        self.df_new_and_modified_ids = self.df_new_and_modified_ids\
            .join(df_existing_ids, self.df_new_and_modified_ids[CONST_ID] == df_existing_ids[CONST_PROD_ID], how="left")\
            .where(CONST_PROD_ID + " is null or " + CONST_EXISTING_MODIFIED_AT + " != " + CONST_NEW_MODIFIED_AT)\
            .select(col(CONST_ID).alias(CONST_NEW_AND_MODIFIED_ID))
        return


    def get_tags_schema(self):
        ans = StructType(
            [
                StructField(CONST_POS_CODE_OUTPUT, StringType(), True),
                StructField(CONST_POS_PROD_VOLUME_OUTPUT, StringType(), True),
                StructField(CONST_POS_CAT_NAME_OUTPUT, StringType(), True),
                StructField(CONST_DO_NOT_LEARN_OUTPUT, BooleanType(), True),
                StructField(CONST_IS_GUESSED_OUTPUT, BooleanType(), True),
                StructField(CONST_CATEGORY_ID_OUTPUT, IntegerType(), True),
                StructField(CONST_CATEGORY_ID_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_BRAND_ID_OUTPUT, IntegerType(), True),
                StructField(CONST_BRAND_ID_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_SERVING_TYPE_ID_OUTPUT, IntegerType(), True),
                StructField(CONST_SERVING_TYPE_ID_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_UNITS_OUTPUT, DoubleType(), True),
                StructField(CONST_UNITS_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_VOLUME_PER_UNIT_OUTPUT, DoubleType(), True),
                StructField(CONST_VOLUME_PER_UNIT_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_FOOD_TAGS_OUTPUT, ArrayType(StructType([
                    StructField(CONST_INNER_FOOD_TAG_OUTPUT, StringType(), True),
                    StructField(CONST_INNER_FOOD_TAG_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True)
                ])), True),
                StructField(CONST_FOOD_TYPES_OUTPUT, ArrayType(StructType([
                    StructField(CONST_INNER_FOOD_TYPE_OUTPUT, StringType(), True),
                    StructField(CONST_INNER_FOOD_TYPE_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True)
                ])), True),
                StructField(CONST_FOOD_SUPER_TYPES_OUTPUT, ArrayType(StructType([
                    StructField(CONST_INNER_FOOD_SUPER_TYPE_OUTPUT, StringType(), True),
                    StructField(CONST_INNER_FOOD_SUPER_TYPE_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True)
                ])), True),
                StructField(CONST_FOOD_COURSE_OUTPUT, StringType(), True),
                StructField(CONST_FOOD_COURSE_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_FOOD_COOKING_METHOD_OUTPUT, StringType(), True),
                StructField(CONST_FOOD_COOKING_METHOD_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_FOOD_TASTE_OUTPUT, StringType(), True),
                StructField(CONST_FOOD_TASTE_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_FOOD_NUTRITION_OUTPUT, StringType(), True),
                StructField(CONST_FOOD_NUTRITION_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_FOOD_SHAREABLE_OUTPUT, BooleanType(), True),
                StructField(CONST_FOOD_SHAREABLE_OUTPUT + "_" + CONST_WORD_PROB, DoubleType(), True),
                StructField(CONST_IS_ERR_ROW, BooleanType(), True)
            ]
        )
        return ans

    def flatten_df(self):
        tags_schema = self.get_tags_schema()
        tags_flattening_udf = udf(flatten_items,tags_schema)
        original_columns = self.df2flatten.columns
        cols2keep = original_columns
        cols2keep.remove(CONST_TAGS)
        cols2keep.append(CONST_TEMP_JSON_TAGS + ".*")
        cols2keep = [col(item) for item in cols2keep]
        df_only_new_and_modified_ids = self.df2flatten.join(self.df_new_and_modified_ids, self.df2flatten[CONST_ID] == self.df_new_and_modified_ids[CONST_NEW_AND_MODIFIED_ID], how="inner").drop(CONST_NEW_AND_MODIFIED_ID)
        self.flattened_df = df_only_new_and_modified_ids
        # self.flattened_df.persist()
        self.flattened_df = self.flattened_df.withColumn(CONST_TEMP_JSON_TAGS, tags_flattening_udf(self.flattened_df[CONST_TAGS]))
        logger.info('list of columns in {} after parsing json: {:s}'.format(CONST_FLATTENED_DF, str(self.flattened_df.columns)))
        self.flattened_df = self.flattened_df.select(cols2keep)
        self.errors_flattened_df = self.flattened_df.where(CONST_IS_ERR_ROW + " = True")
        self.flattened_df = self.flattened_df.where(CONST_IS_ERR_ROW + " = False")
        self.flattened_df.show()
        return


    def handle_casting_errors(self):
        if (self.errors_flattened_df is not None) and (self.errors_flattened_df.count() > 0):
            logger.error("Number of records in {} with malformed structure :{:d}".format(TBL_POS_PRODUCTS , self.errors_flattened_df.count()))
            self.errors_flattened_df = self.errors_flattened_df.limit(20)
            self.errors_flattened_df.persist()
            logger.error("Error examples: \n")
            array_errors_flattened_df = self.errors_flattened_df.rdd.collect()
            for row in array_errors_flattened_df:
                logger.error(str(row))
        return


    def prepare_items_2_write(self):
        original_columns = self.df_pos_prod_new_flat.columns
        self.flattened_df = self.flattened_df.withColumn(CONST_IS_NEW,lit(1))
        self.df_pos_prod_new_flat = self.df_pos_prod_new_flat.withColumn(CONST_IS_NEW,lit(0))
        self.flattened_df = self.flattened_df.withColumnRenamed(CONST_ID, CONST_PROD_ID).select(*self.df_pos_prod_new_flat.columns)
        df_unioned_existing_new_and_modified_prods = self.flattened_df.union(self.df_pos_prod_new_flat)
        df_unioned_existing_new_and_modified_prods = df_unioned_existing_new_and_modified_prods.withColumn(CONST_PROD_ROW_ID, row_number().over(Window.partitionBy(CONST_PROD_ID).orderBy(col(CONST_IS_NEW).desc())))
        self.flattened_df = df_unioned_existing_new_and_modified_prods.where(CONST_PROD_ROW_ID + " = 1").select(*original_columns)
        return


    def write_flat_df(self):
        self.flattened_df.repartition(CONST_PARTITION_KEY).createOrReplaceTempView(CONST_FLATTENED_DF)
        self.spark.sql("SELECT * FROM " + CONST_FLATTENED_DF).show()
        col_list_2_insert = self.get_flat_tbl_schema()
        self.spark.sql("""
        INSERT OVERWRITE TABLE """ + TBL_POS_PRODUCTS_FLAT + """ PARTITION (""" + CONST_PARTITION_KEY + """) SELECT """ + ",".join(col_list_2_insert) +
        """ from """ + CONST_FLATTENED_DF
        )
        return


    def read_preprocess_flatten_posprocess_write(self):
        self.get_items_2_flatten()
        self.get_items_from_pos_prod_new_flat()
        self.get_new_and_modified_prod_ids()
        self.flatten_df()
        self.handle_casting_errors()
        self.prepare_items_2_write()
        self.write_flat_df()
        return


def get_types_of_tags_in_tag_sets():
    dict_tags_with_prob_get_only_val = {
        CONST_POS_CAT_NAME_INPUT: [CONST_POS_CAT_NAME_OUTPUT, CONST_STR_SHORT],
        CONST_POS_CODE_INPUT: [CONST_POS_CODE_OUTPUT, CONST_STR_SHORT],
        CONST_POS_PROD_VOLUME_INPUT: [CONST_POS_PROD_VOLUME_OUTPUT, CONST_STR_SHORT]
    }
    dict_tags_with_prob_get_value_and_prob = {
        CONST_CATEGORY_ID_INPUT: [CONST_CATEGORY_ID_OUTPUT, CONST_INT_SHORT],
        CONST_BRAND_ID_INPUT: [CONST_BRAND_ID_OUTPUT, CONST_INT_SHORT],
        CONST_SERVING_TYPE_ID_INPUT: [CONST_SERVING_TYPE_ID_OUTPUT, CONST_INT_SHORT],
        CONST_UNITS_INPUT: [CONST_UNITS_OUTPUT, CONST_FLOAT_SHORT],
        CONST_VOLUME_PER_UNIT_INPUT: [CONST_VOLUME_PER_UNIT_OUTPUT, CONST_FLOAT_SHORT],
        CONST_FOOD_COURSE_INPUT: [CONST_FOOD_COURSE_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_COOKING_METHOD_INPUT: [CONST_FOOD_COOKING_METHOD_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_TASTE_INPUT: [CONST_FOOD_TASTE_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_NUTRITION_INPUT: [CONST_FOOD_NUTRITION_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_SHAREABLE_INPUT: [CONST_FOOD_SHAREABLE_OUTPUT, CONST_BOOL_SHORT]
    }
    dict_tags_without_prob = {
        CONST_DO_NOT_LEARN_INPUT: [CONST_DO_NOT_LEARN_OUTPUT, CONST_BOOL_SHORT],
        CONST_IS_GUESSED_INPUT: [CONST_IS_GUESSED_OUTPUT, CONST_BOOL_SHORT]
    }
    dict_tags_arrays_with_prob_get_value_and_prob = {
        CONST_FOOD_TAGS_INPUT: [CONST_INNER_FOOD_TAG_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_TYPES_INPUT: [CONST_INNER_FOOD_TYPE_OUTPUT, CONST_STR_SHORT],
        CONST_FOOD_SUPER_TYPES_INPUT: [CONST_INNER_FOOD_SUPER_TYPE_OUTPUT, CONST_STR_SHORT]
    }
    return dict_tags_with_prob_get_only_val, dict_tags_with_prob_get_value_and_prob, dict_tags_without_prob, dict_tags_arrays_with_prob_get_value_and_prob


def flatten_items(_tag_groups):
    dict_tags_with_prob_get_only_val, dict_tags_with_prob_get_value_and_prob,\
    dict_tags_without_prob, dict_tags_arrays_with_prob_get_value_and_prob = get_types_of_tags_in_tag_sets()
    # logger.info("tag_groups before flattening: {:s}".format(str(_tag_groups)))
    check_tag_with_probab = lambda tag_set, key: ((key in tag_set) and (tag_set[key] is not None) and
                                                 (CONST_WORD_VALUE in tag_set[key]) and
                                                 (tag_set[key][CONST_WORD_VALUE] is not None))

    check_tag_without_probab = lambda tag_set, key: ((key in tag_set) and (tag_set[key] is not None))

    check_value_in_array_tag = lambda tag: ((tag is not None) and (CONST_WORD_VALUE in tag) and (tag[CONST_WORD_VALUE] is not None))

    check_probab_in_array_tag = lambda tag: ((tag is not None) and (CONST_WORD_PROB in tag) and (tag[CONST_WORD_PROB] is not None))

    get_probab_for_tag = lambda tag_set, key: float(tag_set[key][CONST_WORD_PROB]) if (key in tag_set) and (tag_set[key] is not None) \
                                                and (CONST_WORD_PROB in tag_set[key]) and (tag_set[key][CONST_WORD_PROB] is not None) \
                                                else None

    tag_groups = json.loads(_tag_groups)
    ans = {}
    tag_sets = None
    try:
        tag_sets = tag_groups[CONST_TAG_GROUPS][0][CONST_TAG_SETS][0]
        if tag_sets is None:
            raise Exception(CONST_TAG_SETS + " is of NoneType")

        for key_iter, value_iter in dict_tags_with_prob_get_only_val.items():
            if check_tag_with_probab(tag_set=tag_sets, key=key_iter):
                ans[value_iter[0]] = convert_tag_answer_to_proper_variable_type(output_value=tag_sets[key_iter][CONST_WORD_VALUE], output_type=value_iter[1])
            else:
                ans[value_iter[0]] = None

        for key_iter, value_iter in dict_tags_with_prob_get_value_and_prob.items():
            if check_tag_with_probab(tag_set=tag_sets, key=key_iter):
                ans[value_iter[0]] = convert_tag_answer_to_proper_variable_type(output_value=tag_sets[key_iter][CONST_WORD_VALUE],output_type=value_iter[1])
            else:
                ans[value_iter[0]] = None
            ans[value_iter[0] + "_" + CONST_WORD_PROB] = get_probab_for_tag(tag_set=tag_sets,key=key_iter)

        for key_iter, value_iter in dict_tags_without_prob.items():
            if check_tag_without_probab(tag_set=tag_sets, key=key_iter):
                ans[value_iter[0]] = convert_tag_answer_to_proper_variable_type(output_value=tag_sets[key_iter],output_type=value_iter[1])
            else:
                ans[value_iter[0]] = None

        for key_iter, value_iter in dict_tags_arrays_with_prob_get_value_and_prob.items():
            if check_tag_without_probab(tag_set=tag_sets, key=key_iter):
                tags_arr = []
                for tag in tag_sets[key_iter]:
                    tag_ans = { }
                    tag_ans[value_iter[0]] = convert_tag_answer_to_proper_variable_type(
                        output_value=tag[CONST_WORD_VALUE],output_type=value_iter[1]) if check_value_in_array_tag(tag=tag) else None
                    tag_ans[value_iter[0] + "_" + CONST_WORD_PROB] =  convert_tag_answer_to_proper_variable_type(
                        output_value=tag[CONST_WORD_PROB],output_type=CONST_FLOAT_SHORT) if check_probab_in_array_tag(tag=tag) else None
                    tags_arr.append(tag_ans)
                ans[key_iter] = tags_arr
            else:
                ans[key_iter] = [{value_iter[0]: None, value_iter[0] + "_" + CONST_WORD_PROB: None}]
        ans[CONST_IS_ERR_ROW] = False
    except Exception as err:
        logger.error('Exception: {:s}, '
                     ' before flattening: {:s}'.format(str(err),str(_tag_groups)))
        err_stack = traceback.format_exc()
        logger.error("Full error traceback: {:s}".format(str(err_stack)))

        ans = handle_json_parsing_exception(dict_tags_with_prob_get_only_val, dict_tags_with_prob_get_value_and_prob, dict_tags_without_prob, dict_tags_arrays_with_prob_get_value_and_prob, CONST_IS_ERR_ROW)
    # logger.info(CONST_TAG_GROUPS + " after flattening: {:s}".format(str(ans)))
    return ans


def handle_json_parsing_exception(dict_tags_with_prob_get_only_val, dict_tags_with_prob_get_value_and_prob, dict_tags_without_prob, dict_tags_arrays_with_prob_get_value_and_prob, err_key):
    ans = {}
    for k, v in dict_tags_with_prob_get_only_val.items():
        ans[v[0]] = None
    for k, v in dict_tags_with_prob_get_value_and_prob.items():
        ans[v[0]] = None
        ans[v[0] + "_" + CONST_WORD_PROB] = None
    for k, v in dict_tags_without_prob.items():
        ans[v[0]] = None
    for k, v in dict_tags_arrays_with_prob_get_value_and_prob.items():
        ans[k] = [{v[0]: None, v[0] + "_" + CONST_WORD_PROB: None}]
    ans[err_key] = True
    return ans


def convert_tag_answer_to_proper_variable_type(output_value, output_type):
    converted_value = None
    if output_type == CONST_STR_SHORT:
        converted_value = str(output_value)
    elif output_type == CONST_INT_SHORT:
        converted_value = int(output_value)
    elif output_type == CONST_FLOAT_SHORT:
        converted_value = float(output_value)
    elif output_type == CONST_BOOL_SHORT:
        converted_value = bool(output_value)
    else:
        logger.error("{} is not a familiar variable type, returning null value".format(output_type))
        converted_value = None
    return converted_value