import unittest
from pyspark.sql import SparkSession
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, IntegerType, StringType
import ItemMatching.RuleBase.beer_rule_base as beer_rule_base
import pandas as pd
import os
import datetime



class RuesTests(unittest.TestCase):
    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()

    def test_rules(self):
        # rules_df = self.spark.read.format("csv").option("header", "true").option('inferSchema', 'true').option("header", "true").option("mode", "DROPMALFORMED").load("/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/rules.csv")
        rules_df = beer_rule_base.pull_rules_s3(spark=self.spark, path="/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/rules.csv")
        rules_df = rules_df.orderBy("priority", ascending=False)
        rules_df.show()
        products_df = self.spark.read.format("csv").option("header", "true").option('inferSchema', 'true').option("header", "true").option("mode", "DROPMALFORMED").load("/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/Korean_Unknown.csv")
        products_df.show()
        pipe = ['volume_id', 'serving_type_id', 'brand_id']
        for col_name in pipe:
            filtered_rules = rules_df[rules_df['rule_type'] == col_name]
            products_df = beer_rule_base.match_products(rules=filtered_rules, products_df=products_df, column_to_update=col_name,
                                         spark=self.spark)
            products_df.show()
        products_df.repartition(1).write.csv(path="/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/Korean_Unknown_rules_ans_" + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".csv", mode='overwrite', header=True)

    def test_unk_brands_rules(self):
        unk_brands_rules_df = beer_rule_base.pull_unknown_rules_s3(spark=self.spark, path="/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/unk_brands_blacklist.csv")
        order_by_cols = ["priority", "ttl_len"]
        unk_brands_rules_df = unk_brands_rules_df.withColumn('ttl_len', func.length('title')).orderBy(
            *order_by_cols, ascending=False)
        unk_brands_rules_df.show()
        products_df = self.spark.read.format("csv").option("header", "true").option('inferSchema', 'true').option(
            "header", "true").option("mode", "DROPMALFORMED").load(
            "/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/Korean_Unknown.csv")
        products_df = products_df.withColumn('title',
                                           func.concat(func.when(func.col("title").isNotNull(), func.col("title")).otherwise(func.lit(" ")),
                                           func.lit(" "),
                                           func.when(func.col("pos_category_name").isNotNull(), func.col("pos_category_name")).otherwise(func.lit(" "))))
        products_df.show()
        products_df = beer_rule_base.match_unknown_brands(unk_brand_rules=unk_brands_rules_df,products_df=products_df, column_to_update="brand_id",spark=self.spark)
        products_df.show()
        products_df.repartition(1).write.csv(
            path="/Users/orens/PycharmProjects/Data/Korean/rule_base_definition/Korean_Unknown_rules_ans_" + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".csv",
            mode='overwrite', header=True)
