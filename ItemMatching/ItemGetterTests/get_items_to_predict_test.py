import unittest
from ItemMatching.ItemGetter import get_items_to_predict
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
import pandas as pd
from ItemMatching.constant import const
import csv

class sampler_test(unittest.TestCase):


    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()


    def test_items_to_predict(self):
        _df = get_items_to_predict.items_to_predict(spark=self.spark, path="s3://dev-matching-steps/", model="category", version=0.1, flow="predict", date="", run_id="", countries="Korea", language="Korean", limit=-1)

