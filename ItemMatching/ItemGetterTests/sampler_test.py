import unittest
from ItemMatching.ItemGetter import sampler
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
import pandas as pd
from ItemMatching.constant import const
import csv

class sampler_test(unittest.TestCase):


    def setUp(self):
        self.spark = SparkSession \
            .builder \
            .appName("Python Spark SQL example") \
            .enableHiveSupport() \
            .getOrCreate()
        self.new_products_df = self.spark.read.option("header", "true").option("mode", "DROPMALFORMED").csv("new_products_df.csv")
        self.current_ver_train_df =  self.spark.read.option("header", "true").option("mode", "DROPMALFORMED").csv("/Users/orens/PycharmProjects/Data/sampled_data/beer/0.2/train/input_data/*/*.csv.gz")
        # self.current_ver_train_df = self.spark.read.option("header", "true").option("mode", "DROPMALFORMED").csv("current_ver_train_df.csv")
        self.current_ver_test_df = self.spark.read.option("header", "true").option("mode", "DROPMALFORMED").csv("/Users/orens/PycharmProjects/Data/sampled_data/beer/0.2/test/input_data/*/*.csv.gz")
        # self.current_ver_test_df = self.spark.read.option("header", "true").option("mode", "DROPMALFORMED").csv("current_ver_test_df.csv")

    def test_handle_data_before_preprocess_inc_training(self):
        # spark, path, model_name, version, partial_retrain = False, new_version = None, new_products_df = None, current_ver_train_df = None, current_ver_test_df = None, brand_min = None
        # self.new_products_df.show()
        # self.current_ver_train_df.show()
        # self.current_ver_test_df.show()
        sampler.pos_matching_sampler(spark=self.spark, path="/Users/orens/PycharmProjects/Data/sampled_data/", model_name="category", version="0.1", countries="", language="")



