from pyspark.ml.feature import OneHotEncoderEstimator, OneHotEncoderModel, StringIndexer
from pyspark.ml.linalg import VectorUDT
from pyspark.sql.functions import udf, col
from pyspark.sql.types import StringType, IntegerType
from pyspark.ml import Pipeline, PipelineModel
from ..constant import const
import boto3
import json

# TODO: Add another key for the last item as category null - so we encode all categories with spark
# TODO: Move to one pipeline for index and one hot encoding


def array_to_string(my_list):
    return '[' + ','.join([str(elem) for elem in my_list]) + ']'


def dense_to_sparse(vector):
    return vector.toArray()


def max_dense_index(vector):
    import numpy as np
    sparse_arr = vector.toArray()
    if np.sum(sparse_arr) == 0:
        print('dense vector does not create an index for the last entry', sparse_arr)
        return len(sparse_arr)
    else:
        return int(np.argmax(sparse_arr))

class category_encoder(object):
    INDEX_SUFFIX = '_index'
    DENSE_SUFFIX = '_dense'
    SPARSE_SUFFIX = '_sparse'

    # The ctor get the spark session (one for all classes) and init basic data
    def __init__(self, spark_session, is_w2v):
        self.spark = spark_session
        self.index_model = None
        self.one_hot_encoder = None
        self.fields = None
        self.category_mapping = {}
        self.file_suffix = '_' + const.MODEL_TYPE_NN if is_w2v else '_' + const.MODEL_TYPE_BOW


    def get_labels(self):
        if self.index_model is None:
            return None
        else:
            return {c.name: c.metadata["ml_attr"]["vals"] for c in self.fields if c.name.endswith(self.INDEX_SUFFIX)}

    # Encode data for input columns
    # in case of a training set use the output columns as well
    def encode(self, to_encode_df, input_columns, output_columns, is_one_hot):
        all_columns = list(input_columns)
        all_columns.extend(output_columns)
        print(all_columns)

        # First step - fit data to index model
        if self.index_model is None:
            full_index_model = self.data_to_index(to_encode_df, all_columns)
            self.index_model = self.data_to_index(to_encode_df, input_columns)
        else:
            full_index_model = self.index_model

        encode_df = self.transform_data_index(full_index_model, to_encode_df)

        # Add index suffix for input and output columns
        input_columns_index = self.get_output_columns(encode_df, self.INDEX_SUFFIX, input_columns)
        output_columns_index = self.get_output_columns(encode_df, self.INDEX_SUFFIX, output_columns)

        # Second step - fit data to one hot encoder, over the test data use the loaded encoder
        if self.one_hot_encoder is None:
            self.encode_dense(encode_df, input_columns_index, output_columns_index, self.INDEX_SUFFIX)

        # Third step - transform the data to dense vector
        is_encode_output = (len(output_columns) != 0)
        encode_dense_df = self.one_hot_transform_dense(encode_df, is_encode_output)

        if is_one_hot:
            encode_dense_df = self.one_hot_transform_output(encode_dense_df, input_columns)

        # Move the transform data back to the original column name
        # For input column use full sparse array, for output columns take max index
        for column in all_columns:
            if column in input_columns:
                suffix = self.SPARSE_SUFFIX if is_one_hot else self.DENSE_SUFFIX
                encode_dense_df = encode_dense_df.withColumn(column, encode_dense_df[column + suffix])
                encode_dense_df = encode_dense_df.drop(column + suffix)
            else:
                max_dense = udf(max_dense_index, IntegerType())
                encode_dense_df = encode_dense_df.withColumn(column, max_dense(encode_dense_df[column + self.DENSE_SUFFIX]))

            encode_dense_df = encode_dense_df.drop(column + self.DENSE_SUFFIX)
            encode_dense_df = encode_dense_df.drop(column + self.INDEX_SUFFIX)
        return encode_dense_df

    # Fit string data to index model for each category column
    def data_to_index(self, to_encode_df, all_columns):
        indexers = [StringIndexer(inputCol=column, outputCol=column + self.INDEX_SUFFIX).fit(to_encode_df) for column in all_columns]

        pipeline_index = Pipeline(stages=indexers)
        return pipeline_index.fit(to_encode_df)

    # Transform data to index
    def transform_data_index(self, model, to_encode_df):
        df_return = model.transform(to_encode_df)
        self.fields = df_return.schema.fields
        return df_return

    # Load the pre process - for index model and one hot encoder model
    def load_pre_processing(self, path):
        self.index_model = PipelineModel.load(path + 'string_pipeline' + self.file_suffix)
        if self.index_model is None:
            raise Exception('The string pipeline is not exist')

        self.one_hot_encoder = [None, None]
        self.one_hot_encoder[0] = OneHotEncoderModel.load(path + 'one_hot_pipeline' + self.file_suffix)

        if self.one_hot_encoder[0] is None:
            raise Exception('THe one hot encoding pipeline is not exist')

    # Save the model
    def save_pre_processing(self, path):
        if self.index_model is None or self.get_labels() is None or self.one_hot_encoder is None:
            raise Exception('In order to save pre process data all the pre process steps need to be done')
        else:
            print('save pre process one hot to:', path)
            self.index_model.write().overwrite().save(path + 'string_pipeline' + self.file_suffix)
            self.one_hot_encoder[0].write().overwrite().save(path + 'one_hot_pipeline' + self.file_suffix)

            # Support in saving for S3 or local directory
            if 's3' in path:
                client = boto3.client('s3')
                bucket = path.split('/')[2:3][0]
                path_no_bucket = '/'.join(path.split('/')[3:]) + 'label_mapping' + self.file_suffix + '.json'
                print('write file', path_no_bucket, ' from bucket ', bucket)
                client.put_object(Body=json.dumps(self.get_labels()), Bucket=bucket, Key=path_no_bucket)
            else:
                with open(path + 'label_mapping' + self.file_suffix + '.txt', 'w') as file:
                    file.write(str(self.get_labels()))
                file.close()

    def get_mapping(self):
        return self.category_mapping

    # <editor-fold desc="Manage the column names suffix: no suffix, _dense, _sparse">

    def create_output_columns(self, input_columns, output_sign):
        output_columns = []
        for column_name in input_columns:
            output_columns.append(column_name + output_sign)
        return output_columns

    def replace_suffix_output_columns(self, input_columns, input_sign, output_sign):
        return [column.replace(input_sign, output_sign) for column in input_columns]

        output_columns = []
        for column_name in input_columns:
            output_columns.append(column_name + output_sign)
        return output_columns

    def get_output_columns(self, input_df, column_sign, column_prefixes):
        output_columns = []
        for column_name in input_df.columns:
            if column_name.endswith(column_sign) and column_name.startswith(tuple(column_prefixes)):
                output_columns.append(column_name)
        return output_columns

    # </editor-fold>

    # Encoding using average vector
    def encode_average_vector(self, to_encode_df, input_columns):
        output_columns = self.create_output_columns(input_columns, '_dense')
        encoder = OneHotEncoderEstimator(inputCols=input_columns, outputCols=output_columns, dropLast=False)
        model = encoder.fit(to_encode_df)
        encoded_dense = model.transform(to_encode_df)

        encoded_sparse = encoded_dense
        for out_column in output_columns:
            to_sparse = udf(dense_to_sparse, VectorUDT())
            to_string = udf(array_to_string, StringType())
            encoded_sparse = encoded_sparse.withColumn(out_column.replace(self.DENSE_SUFFIX, self.SPARSE_SUFFIX), to_sparse(col(out_column)))
            encoded_sparse = encoded_sparse.withColumn(out_column.replace(self.DENSE_SUFFIX, self.SPARSE_SUFFIX), to_string(col(out_column)))

        return encoded_sparse

    # Encoding using dense vector and set the model variables of the class
    def encode_dense(self, to_encode_df, input_columns, output_columns, input_sign=None):
        if input_sign is None:
            result_input_columns = self.create_output_columns(input_columns, self.DENSE_SUFFIX)
            result_output_columns = self.create_output_columns(output_columns, self.DENSE_SUFFIX)
        else:
            result_input_columns = self.replace_suffix_output_columns(
                input_columns, input_sign=input_sign, output_sign=self.DENSE_SUFFIX)
            result_output_columns = self.replace_suffix_output_columns(
                output_columns, input_sign=input_sign, output_sign=self.DENSE_SUFFIX)

        encoder_input = OneHotEncoderEstimator(inputCols=input_columns, outputCols=result_input_columns, dropLast=False)
        encoder_output = OneHotEncoderEstimator(inputCols=output_columns, outputCols=result_output_columns, dropLast=False)

        # There is a list of two models in the pipeline
        self.one_hot_encoder = [None, None]
        self.one_hot_encoder[0] = encoder_input.fit(to_encode_df)
        self.one_hot_encoder[1] = encoder_output.fit(to_encode_df)

    # transform the data to dense vector
    def one_hot_transform_dense(self, to_encode_df, is_transform_output):
        encoded_dense = self.one_hot_encoder[0].transform(to_encode_df)
        if is_transform_output:
            encoded_dense = self.one_hot_encoder[1].transform(encoded_dense)
        return encoded_dense

    # Transform the dense vector to sparse
    def one_hot_transform_output(self, encoded_dense, out_columns):
        output_columns = self.create_output_columns(out_columns, self.DENSE_SUFFIX)

        encoded_sparse = encoded_dense
        for out_column in output_columns:
            to_string = udf(array_to_string, StringType())
            to_sparse = udf(dense_to_sparse, VectorUDT())
            encoded_sparse = encoded_sparse.withColumn(out_column.replace(self.DENSE_SUFFIX, self.SPARSE_SUFFIX), to_sparse(col(out_column)))
            encoded_sparse = encoded_sparse.withColumn(out_column.replace(self.DENSE_SUFFIX, self.SPARSE_SUFFIX), to_string(col(out_column)))
        return encoded_sparse


    # Not in use!!!!!
    # Transform the dense encoding to sparse
    # def one_hot_transform_sparse(self, to_encode_df, is_transform_output, all_columns):
    #     output_columns = self.create_output_columns(all_columns, '_dense')
    #
    #     encoded_dense = self.one_hot_transform_dense(to_encode_df, is_transform_output)
    #     encoded_sparse = encoded_dense
    #     for out_column in output_columns:
    #         to_sparse = udf(dense_to_sparse, VectorUDT())
    #         to_string = udf(array_to_string, StringType())
    #         encoded_sparse = encoded_sparse.withColumn(out_column.replace('_dense', '_sparse'),
    #                                                    to_sparse(col(out_column)))
    #         encoded_sparse = encoded_sparse.withColumn(out_column.replace('_dense', '_sparse'), to_string(col(out_column)))
    #
    #     return encoded_sparse