from pyspark.ml.feature import StandardScaler, MinMaxScaler, MaxAbsScaler
import pyspark.sql.functions as fn


# TODO: Add remove outliers function for price.
# TODO: We need to use built-in encoder - not complete
class price_encoder(object):
    def __init__(self, spark_session):
        self.spark = spark_session
        self.scaled_by_country = None

    # Wrap the encoding with generic method - we can switch between encoders (MinMax, Standard, inside implementation)
    def encode(self, input_df, columns_to_scale):
        return self.encode_number(input_df, columns_to_scale)

    # Encode with min max scaler implemented with our code. handle train and test options
    def encode_number(self, input_df, columns_to_scale):
        encoded_df = input_df
        for column_name in columns_to_scale:
            if self.scaled_by_country is None:
                grouped_df = encoded_df.groupBy('country_id').agg(
                    fn.max(column_name).alias('max'),
                    fn.min(column_name).alias('min'),
                    fn.expr('percentile_approx(%s, 0.5)' % column_name).alias('med'),
                    fn.expr('percentile_approx(%s, 0.25)' % column_name).alias('low'),
                    fn.expr('percentile_approx(%s, 0.75)' % column_name).alias('high')
                )
                self.scaled_by_country = grouped_df
            else:
                grouped_df = self.scaled_by_country

            encoded_df = encoded_df.join(grouped_df, 'country_id')

            # REMOVE OUTLIERS
            encoded_df = encoded_df.withColumn('IQF', fn.col('high') - fn.col('low'))
            encoded_df = encoded_df.withColumn('upbound', fn.col('med') + 3 * fn.col('IQF'))
            encoded_df = encoded_df.withColumn('lbound', fn.col('med') - 3 * fn.col('IQF'))
            encoded_df.filter((fn.col(column_name) < fn.col('upbound'))).filter((fn.col(column_name) > fn.col('lbound')))

            # SCALE
            encoded_df = encoded_df.withColumn(column_name,
                                               (fn.col(column_name) - fn.col('min')) / (fn.col('max') - fn.col('min')))
        for cname in ['max', 'min', 'med', 'low', 'high', 'IQF', 'upbound', 'lbound']:
            encoded_df = encoded_df.drop(cname)
        return encoded_df

    # Load the scaler (CSV file: min max per country)
    def load_scaler(self, path):
        self.scaled_by_country = self.spark.read.option('header', 'true').csv(path + 'scaler.csv')

    # Save the scaler (CSV file: min max per country)
    def save_scaler(self, path):
        if self.scaled_by_country is not None:
            self.scaled_by_country.repartition(1).write.mode('overwrite').option('header', 'true').csv(path + 'scaler.csv')

    # 1. transfer the columns to one dense vector
    # 2. run it through the min max scalar
    # 3. transfer the dense vector back to the columns as scaled
    def encode_dense_vector(self, input_df, columns_to_scale):
        min_max_scaler = MinMaxScaler(inputCol="price", outputCol="min_max_scaled_price")

        min_max_model = min_max_scaler.fit(input_df)
        scaled_data = min_max_model.transform(input_df)
        return scaled_data
