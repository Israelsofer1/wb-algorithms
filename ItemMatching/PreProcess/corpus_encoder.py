import pyspark.sql.functions as fn
from pyspark.ml.feature import CountVectorizer, CountVectorizerModel
from pyspark.sql.types import StringType, ArrayType
import re
from ..constant import const
from konlpy.tag import Okt


# def clean_data(title):
# #     if title is not None:
# #         #title = title.decode('ascii', 'ignore')
# #         #title = str(title)
# #         ' '.join(re.split('(\d+)', re.sub(' +', ' ', re.sub('[^a-zA-Z0-9 ]+', ' ', (title.lower())))))
# #
# #         #title = re.sub(r'[,.;:#@?!%&*$/-]', ' ', title)
# #         title = re.sub(' +', ' ', title)
# #         title = title.lower()
# #         title = title.strip()
# #         return title
# #     else:
# #         return ''

def clean_data(text, lang=None):
    if text is not None:
        if str(lang).lower() == const.KOREAN_LANG:
            cleaned_text = ' '.join(re.split('(\d+)', re.sub('[\[\]!@#$%^&*(),.?′`\'":{}|<>＞＆\_\-+\-/]+',' ', re.sub(' +', ' ', (text.lower()))))).strip()  # Korean script
            print("Cleaned text is: {:s}".format(cleaned_text))
            return cleaned_text
        return ' '.join(re.split('(\d+)', re.sub(' +', ' ', re.sub('[^a-zA-Z0-9 ]+', ' ', (text.lower()))))).strip()
    return ""


def tokenize_koren_text(text):
    # try:
    #     text = str(text).encode('euc_kr').decode('cp949')
    # except:
    #     print("Unable to encode {:s} to Korean".format(str(text)))
    #     text = str(text)
    okt = Okt()
    return okt.morphs(text)

class corpus_encoder(object):
    def __init__(self, spark, min_words=2, is_padding=True, lang=None):
        # Minimum occurrence for word hyper parameter
        self.max_len = 10
        self.min_occur = min_words
        self.spark = spark
        self.is_padding = is_padding
        self.word2vec_model = None
        self.bow_model = None
        self.lang = lang

    def encode_bow(self, data_df, column):
        token_data_df = self.__split_tokens__(data_df, [column])
        if self.bow_model is None:
            self.bow_model = self.build_bow_model(token_data_df, column, 2)

        transform_df = self.bow_transform(token_data_df, column)
        transform_df = transform_df.drop(column)
        transform_df = transform_df.withColumn(column, fn.col('result'))
        transform_df = transform_df.drop('result')
        return transform_df

    def build_bow_model(self, data_df, column, min_occuracnes):
        # fit a CountVectorizerModel from the corpus
        count_words = CountVectorizer(inputCol=column, outputCol='result', minDF=min_occuracnes)
        return count_words.fit(data_df)

    def bow_transform(self, data_df, column):
        # Make sure that the data frame contains title column
        if column in data_df.columns:
            return self.bow_model.transform(data_df)
        else:
            raise Exception('no column name title')

    def save_model(self, path):
        if self.bow_model is None:
            print('BOW model is not initialize')
        else:
            self.bow_model.write().overwrite().save(path + const.MODEL_TYPE_BOW + '_model')

        # if self.word2vec_model is None:
        #    print('Word to vec model is not initialize')
        # else:
        #    self.word2vec_model.write().overwrite().save(path + 'word2vec_model')

    def load_model(self, path):
        self.bow_model = CountVectorizerModel.load(path + const.MODEL_TYPE_BOW + '_model')
        if self.bow_model is None:
            raise Exception('BOW model is not exists in path: ' + path)

        # self.word2vec_model = Word2VecModel.load(path + 'word2vec_model')
        # if self.word2vec_model is None:
        #    raise Exception('Word2vec model is not exists in path: ' + path)



    def __split_tokens__(self, data_df, cols):
        clean_data_udf = fn.udf(clean_data, StringType())
        tokenize_koren_text_udf = fn.udf(tokenize_koren_text, ArrayType(StringType()))
        if str(self.lang).lower() == const.KOREAN_LANG:
            data_df = data_df.withColumn('title', clean_data_udf(fn.col(cols[0]), fn.lit(self.lang))).withColumn('title', tokenize_koren_text_udf(fn.col(cols[0])))
        else:
            data_df = data_df.withColumn('title', clean_data_udf(fn.col(cols[0])))
            data_df = data_df.withColumn('title', fn.split(fn.col(cols[0]), " ").alias('title'))
        # TODO: tokenize the words better
        # data = [Helper.tokenize_combo(d['title'], padding=self.max_len) for d in data if d['title'] is not None]
        print("Tokenized dataframe: size({:1.0f})".format(data_df.count()))
        data_df.show()
        return data_df


    #def encode(self, data_df, column_name):
    #    encode_df = self.encode_dense(data_df, [column_name])
    #    return encode_df

    # def __split_tokens_with_padding__(self, data_df, cols):
    #     clean_data_udf = udf(clean_data, StringType())
    #     data_df = data_df.withColumn('title', clean_data_udf(data_df['title']))
    #
    #     padding_udf = udf(padding, StringType())
    #     data_df = data_df.withColumn('title', padding_udf(data_df['title']))
    #
    #     return self.__split_tokens__(data_df, cols)

    # def spark_word_2_vec(self, data_df):
    #     # Learn a mapping from words to Vectors.
    #     word2Vec = Word2Vec(vectorSize=self.max_len, minCount=0, inputCol="title", outputCol="result")
    #     model = word2Vec.fit(data_df)
    #     return model
    #
    # def encode_average(self, data_df):
    #     if self.is_padding:
    #         print('be careful! you are using padding, but you want to create average vector')
    #
    #     data_df = data_df.withColumn('title_orig', data_df['title'])
    #     data_df = self.__split_tokens__(data_df, ['title'])
    #     encode_df = self.word2vec_model.transform(data_df)
    #     array_to_string_udf = udf(array_to_string, StringType())
    #     encode_df = encode_df.withColumn('vector_string', array_to_string_udf(encode_df["result"]))
    #     encode_df = encode_df.drop('result')
    #     encode_df = encode_df.drop('title')
    #
    #     encode_df = encode_df.select('title_orig', 'vector_string')
    #     return encode_df
    #
    # def encode_dense(self, data_df, columns):
    #     if not self.is_padding:
    #         print('be careful! you are not using padding, but you want to create vector for each word')
    #
    #     # Save backup for title before splitting to vector
    #     data_df = data_df.withColumn('title_original', col('title'))
    #     data_df = self.transform_title_to_columns(data_df, columns)
    #     word2vec_dict = self.get_vectors_dict()
    #
    #     transform_df = data_df
    #     transform_df = transform_df.drop('title')
    #     transform_df = transform_df.withColumn('title', col('title_original'))
    #     transform_df = transform_df.drop('title_original')
    #
    #     word_column_list = [word for word in transform_df.columns if word.startswith('word')]
    #     transform_df = transform_df.\
    #         withColumn("title_vector", translate_array(word2vec_dict)(struct([transform_df[x] for x in word_column_list])))
    #     for word_column in word_column_list:
    #         transform_df = transform_df.drop(word_column)
    #
    #     transform_df = transform_df.drop('title')
    #     transform_df = transform_df.withColumn('title', col('title_vector'))
    #     transform_df = transform_df.drop('title_vector')
    #
    #     return transform_df
    #
    # def encode_vocab(self):
    #     return self.word2vec_model.getVectors()
    #
    # def get_vectors_dict(self):
    #     vectors_ = self.word2vec_model.getVectors()
    #
    #     #array_to_string_udf = udf(array_to_string, StringType())
    #     #vectors_ = vectors_.withColumn('vector_string', array_to_string_udf(vectors_["vector"]))
    #
    #     vectors_df = vectors_.toPandas()
    #     vectors_dict = dict(zip(vectors_df.word, vectors_df.vector))
    #
    #     if not self.is_padding:
    #         vectors_dict[None] = 0
    #
    #     return vectors_dict
    #
    # def transform_title_to_columns(self, data_df, cols):
    #     if self.is_padding:
    #         clean_data_udf = udf(clean_data, StringType())
    #         data_df = data_df.withColumn('title', clean_data_udf(data_df['title']))
    #
    #         padding_udf = udf(padding, StringType())
    #         data_df = data_df.withColumn('title', padding_udf(data_df['title']))
    #
    #     for i in range(0, self.max_len):
    #         data_df = reduce(
    #             lambda pos_df, column_name: pos_df.withColumn('word' + str(i),
    #                                                           split(col(column_name), " ")[i].alias(column_name)),
    #             cols,
    #             data_df
    #         )
    #     return data_df
    # def train(self, data_df, columns):
    #     if self.word2vec_model is None:
    #         # TODO: combine multiple columns into one column, e.g., pos_categor_name, modifier
    #
    #         # The W2V embedding is trained on our words
    #         if self.is_padding:
    #             token_data_df = self.__split_tokens_with_padding__(data_df, columns)
    #         else:
    #             token_data_df = self.__split_tokens__(data_df, columns)
    #
    #         bow_model = self.build_bow_model(token_data_df, columns[0], 2)
    #         token_data_df = token_data_df.withColumn('title', unknown_mapping(bow_model.vocabulary)('title'))
    #         self.word2vec_model = self.spark_word_2_vec(token_data_df)
    #
    #     return self.word2vec_model

# def array_to_string(my_list):
#     return '[' + ','.join([str(elem) for elem in my_list]) + ']'
#
# def translate(mapping):
#     def translate_(col):
#         if col in mapping:
#             map_array = mapping[col]
#         else:
#             map_array = mapping[unknown_token]
#             # print('word ' + col + ' is not exists in the word2vec model')
#         return map_array
#
#     return udf(translate_, StringType())
#
# def translate_array(mapping):
#     def translate_and_append(row):
#         map_title_of_arrays = []
#         for col in row:
#             if col in mapping:
#                 map_array = mapping[col]
#             else:
#                 map_array = mapping[unknown_token]
#             map_title_of_arrays.append(map_array)
#         return '[' + ','.join([str(elem) for elem in map_title_of_arrays]) + ']'
#
#     return udf(translate_and_append, StringType())
#
# def unknown_mapping(mapping):
#     def map_data(col):
#         map_array = [word_key if word_key in mapping else unknown_token for word_key in col]
#         return map_array
#
#     return udf(map_data, ArrayType(StringType()))
#
# unknown_token = "<!unknown>"
# padding_token = "<!pad> "
#
# def padding(col_string):
#     split_title = re.split(r'\s{1,}', col_string.strip())
#     title_with_padding = ''
#     for index in range(0, 20):
#         if index < len(split_title):
#             title_with_padding = title_with_padding + split_title[index].strip() + ' '
#         else:
#             title_with_padding = title_with_padding + padding_token
#
#     title_with_padding = title_with_padding.strip()
#     return title_with_padding

