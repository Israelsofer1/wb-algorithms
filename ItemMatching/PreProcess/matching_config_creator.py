import json

FULL_FEATURE_NN_CSV_NAME ='pre_process_NN.gzip'
FULL_FEATURE_W2V_CSV_NAME ='pre_process_w2v.gzip'
FULL_FEATURE_BOW_CSV_NAME ='pre_process_BOW.gzip'
TEXT_FEATURE_CSV_NAME = 'title.gzip'

input_features = {"price": {'type': 'cont', 'direction': 'input', 'file': FULL_FEATURE_NN_CSV_NAME},
                  "country_id": {'type': 'single', 'direction': 'input', 'file': FULL_FEATURE_NN_CSV_NAME},
                  "title": {'type': 'txt', 'direction': 'input', 'file': TEXT_FEATURE_CSV_NAME}}

category_dict = input_features.copy()
category_dict["category_id"] = {'type': 'single', 'direction': 'output', 'parent': None, 'file': FULL_FEATURE_NN_CSV_NAME}

beer_dict = input_features.copy()
beer_dict["super_brand_id"] = {'type': 'single', 'direction': 'output', 'parent': None, 'file': FULL_FEATURE_NN_CSV_NAME}
beer_dict["brand_id"] = {'type': 'single', 'direction': 'output', 'parent': "super_brand_id", 'file': FULL_FEATURE_NN_CSV_NAME}
beer_dict["serving_type_id"] = {'type': 'single', 'direction': 'output', 'parent': None, 'file': FULL_FEATURE_NN_CSV_NAME}
#beer_dict["beer_type_id"] = {'type': 'single', 'direction': 'output', 'parent': None, 'file': FULL_FEATURE_CSV_NAME}
beer_dict["volume_id"] = {'type': 'single', 'direction': 'output', 'parent': None, 'file': FULL_FEATURE_NN_CSV_NAME}

beer_field_order = ['super_brand_id', 'serving_type_id', 'volume_id', 'brand_id']
beer_field_bow = 'brand_id'

full_model_dict = {"category": category_dict, 'beer': beer_dict, 'beer_field_order':beer_field_order}

def create_json():
    with open('matching_config.json', 'w') as fp:
        json.dump(full_model_dict, fp, indent=4)


create_json()