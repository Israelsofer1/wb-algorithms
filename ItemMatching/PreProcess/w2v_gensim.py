import pyspark.sql.functions as func
from pyspark.sql.types import FloatType, StringType, ArrayType, IntegerType
from pyspark.sql import SQLContext
from ..constant import const
import ast

# --------------------------------------------------------------------------------------


from gensim.models import Word2Vec, KeyedVectors, FastText
from collections import Counter
import pandas as pd
import re
import numpy as np
from konlpy.tag import Okt


unknown_token = "<UNKNWN>"
pad_token = "<PAD>"

class Corpus(object):
    def __init__(self, cname='default_corpus', data=None, min_words=2, model_type="W2V", is_train=False):
        # Minimum occurrence for word hyper parameter
        self.name = cname
        self.model_type = model_type
        self.max_len = 10
        self.window_len = 3
        self.min_occur = min_words
        self.unknown_token = unknown_token
        self.pad_token = pad_token
        self.data = data
        self.i2v = None
        self.v2i = None
        self.model = None
        if is_train:
            self.model_train()


    def model_train(self, data=None):
        if data is None:
            data = self.data
        if data is not None:
            if self.model_type == "W2V":
                self.model = self.w2v_train(data)
            else:
                self.model = self.fasttext_train(data)
        else:
            self.model = Word2Vec.load('%s' % self.name)
        self.i2v = {i: v for i, v in enumerate(self.model.wv.index2word)}
        self.v2i = {v: i for i, v in self.i2v.items()}


    def model_save(self, model_name, path_to_save):
        self.model.save(path_to_save + model_name)
        return

    def w2v_train(self, data):
        data = self.__corpus_training_extract__(data)
        # The W2V embedding is trained on our words
        model = Word2Vec(data, size=100, min_count=self.min_occur, window=self.window_len, sg=1, iter=100)
        model.wv.save_word2vec_format(self.name + '.bin')
        # model.save(self.name)
        return model

    def fasttext_train(self, data):
        data = self.__corpus_training_extract__(data)
        # print(len(data))
        # hundredth_of_data_len = int(len(data)/100)   [:hundredth_of_data_len]
        # print(hundredth_of_data_len)
        model = FastText(data, size=100, min_count=self.min_occur, window=self.window_len, sg=1, iter=100)
        return model

    def vectorize(self, text):
        tokens = self.__tokenize_combo__(text, padding=self.max_len)
        return str([self.model[t].tolist() if t in self.model else self.model[self.unknown_token].tolist()
                    for t in tokens])

    def model_mapping_df(self):
        words = list(self.model.wv.vocab.keys())
        vectors = [self.model[word].tolist() for word in words]

        vord2vec_df = pd.DataFrame(
            {'word': words,
             'vector': vectors,
             })
        return vord2vec_df

    def model_mapping_dict(self):
        corp = Corpus('default_w2v')
        wordVocab = [k for (k, v) in corp.model.wv.vocab.iteritems()]
        vec = [corp.model[word].tolist() for word in wordVocab]
        return dict(zip(wordVocab, vec))

    def __corpus_training_extract__(self, data):
        # print(type(data))
        print("Started preprocessing")
        print("Started unicode conversion")
        # unicode2str_udf = func.udf(unicode2str, ArrayType(StringType()))
        # data = data.withColumn('title', unicode2str_udf(func.col('title')))
        # data = data.apply(unicode2str)
        print("Ended unicode conversion")
        print("Started tokenization and lemmatization") # and stopwords removal
        data = [self.__tokenize_combo__(d, padding=self.max_len) for d in data if isinstance(d, str)]
        # tokenize_udf = func.udf(self.tokenize, ArrayType(StringType()))
        # data = data.withColumn('title', tokenize_udf(func.col('title')))
        print("Ended tokenization, lemmatization")  # and stopwords removal
        # We stream the titles into the corpus trainer
        # We count the words to exclude words that appear less than min_occur and don't insert_many it in the dictionary
        print("Started removing words with less than minimal frequency")
        # data = data.select(["title"]).toPandas().apply(self.unicode2str).values.tolist()
        count = Counter(x for xs in data for x in xs)
        tokens = [[t if count[t] >= self.min_occur else self.unknown_token for t in r] for r in data]
        print("Ended removing words with less than minimal frequency")
        print("Completed preprocessing")
        return tokens

    def __tokenize_combo__(self, text, padding=0):
        token = []
        if text is not None and text != '':
            token = clean_text(text).split()
            # Pad the sentences to a uniform length
            if padding > 0 and len(token) > 0:
                token += [self.pad_token for _ in range(padding - len(token))]
                token = token[:padding]
        return token


def clean_text(text, lang=None):
    if str(lang).lower() == const.KOREAN_LANG:
        cleaned_text = ' '.join(re.split('(\d+)', re.sub('[\[\]!@#$%^&*(),.?′`\'":{}|<>＞＆\_\-+\-/]+',' ', re.sub(' +', ' ', (text.lower()))))).strip()  # Korean script
        # print("Cleaned text is: {:s}".format(cleaned_text))
        return cleaned_text
    return ' '.join(re.split('(\d+)', re.sub(' +', ' ', re.sub('[^a-zA-Z0-9 ]+', ' ', (text.lower()))))).strip()



# --------------------------------------------------------------------------------------


def df_w2v(df, column, model_name='default_w2v'):
    corp = Corpus(model_name)

    def w2v(text):
        return corp.vectorize(text)
    vec_udf = func.udf(w2v, StringType())

    df = df.withColumn(column, vec_udf(df[column]))
    return df


def train(model_name):
    from Common.DAL.Connectors.SQL import DbExplorer
    import pandas as pd
    with DbExplorer('PROD') as db:
        data = pd.DataFrame(db.dbreader(q="select bp.title from pos_products_new_flat bp where bp.category_id > 0"))
        data = data['title'].values
    Corpus(model_name, data)

#######################

def tokenize_with_zero(text, lang):
    return tokenize(text, lang, padding=0)

def tokenize(text, lang, padding=10):
    token = []
    print("Type of text is: {:s}".format(str(type(text))))
    print("Text is: {:s}, length of text is: {:d}".format(text, len(text)))
    # if str(lang).lower() == const.KOREAN_LANG:
    #     try:
    #         text = str(text).encode('euc_kr').decode('cp949')
    #     except:
    #         print("Unable to encode {:s} to Korean".format(str(text)))
    #         text = str(text)
    if text is not None and len(text) > 0:
        print("Is korean? {:s}".format(str(str(lang).lower() == const.KOREAN_LANG)))
        if str(lang).lower() == const.KOREAN_LANG:
            print("Korean Tokenization")
            cleaned_text = clean_text(text, lang=lang)
            print("Cleaned text is: {:s}".format(cleaned_text))
            token = tokenize_koren_text(cleaned_text) #.split() not needed in korean
        else:
            print("Non-Korean Tokenization")
            token = clean_text(text).split()
        # Pad the sentences to a uniform length
        if padding > 0 and len(token) > 0:
            token += [pad_token for _ in range(padding - len(token))]
            token = token[:padding]
    print("Text is: {:s}, Tokenized line is:".format(text))
    print(token)
    return token

def tokenize_koren_text(text):
    okt = Okt()
    return okt.morphs(text)

def str_to_array(title):
    title_string = ast.literal_eval(title)
    #print('test', title_string)
    return title_string

def count_array(vector):
    return len(vector)

def array_to_string(my_list):
    return '[' + ','.join([str(elem) for elem in my_list]) + ']'

def model_mapping_dict(path, corpus_name):
    print("w2v model path:" + path + corpus_name)
    model = Word2Vec.load(path + corpus_name)
    words = list(model.vocab.keys())
    vectors = [model[word].tolist() for word in words]
    return dict(zip(words, vectors))

def model_mapping_dict_from_bin(path, corpus_name):
    model = KeyedVectors.load_word2vec_format(path + corpus_name + '.bin')
    words = list(model.vocab.keys())
    vectors = [model[word].tolist() for word in words]
    return dict(zip(words, vectors))

def model_mapping_df(path, corpus_name):
    model = Word2Vec.load(path + corpus_name)

    #model = Word2Vec.load('/Users/israelsofer/S3 Backup/%s' % corpus_name)
    words = list(model.wv.vocab.keys())
    vectors = [model[word].tolist() for word in words]

    vord2vec_df = pd.DataFrame(
        {'word': words,
         'vector': vectors,
         })
    return vord2vec_df

def model_word2index_df(path, corpus_name):
    model = Word2Vec.load(path + corpus_name)

    words = list(model.wv.vocab.keys())
    vectors = [model.wv.vocab[word].index for word in words]

    vord2vec_df = pd.DataFrame(
        {'word': words,
         'vector': vectors,
         })
    return vord2vec_df

def model_word2index_dict(path, corpus_name):
    model = Word2Vec.load(path + corpus_name)

    words = list(model.wv.vocab.keys())
    vectors = [model.wv.vocab[word].index for word in words]

    return dict(zip(words, vectors))

def process_words(input_df):
    if 'bar_product_id' in input_df.columns:
        vector_columns = ['bar_product_id', 'title', 'brand_id']
    else:
        vector_columns = input_df.columns#['title', 'brand_id']
        print('Error! there is no bar_product_id field, no merge can be made with the structured data')

    input_df = input_df.select(vector_columns)
    input_df = input_df.filter(input_df.title.isNotNull())

    tokenize_udf = func.udf(tokenize, ArrayType(StringType()))
    input_df = input_df.withColumn('title', tokenize_udf(func.col('title')))
    input_df = input_df.select('bar_product_id', 'title')

    return input_df

def title_to_vector(text, mapping):
    arr = []
    for t in text:
        if t in mapping:
            arr.append(list(mapping[t]))
        else:
            arr.append(list(mapping[unknown_token]))
    return arr


def title_to_average_vector(text, mapping):
    print(text)

    vectors = np.array([mapping[t] for t in text if t in mapping])
    if len(vectors) == 0:
        mean_vector = mapping[unknown_token]
    else:
        mean_vector = vectors.mean(axis=0)

    return str(mean_vector)


def title_to_index(text, mapping):
    arr = []
    for t in text:
        if t in mapping:
            arr.append(mapping[t])
        else:
            arr.append(mapping[unknown_token])
    return arr

def process_all_titles(broadcast, input_df, encode_type, lang=None):
    print("Dataframe before tokenization: size({:1.0f})".format(input_df.count()))
    input_df.show()
    def title_to_index_udf(name):
        return title_to_index(name, broadcast.value)

    def title_to_vector_udf(name):
        return title_to_vector(name, broadcast.value)

    def udf_title_to_average_vector(name):
        return title_to_average_vector(name, broadcast.value)
    lang = lang if lang is not None else "null"
    if encode_type == 'average':
        tokenize_udf = func.udf(tokenize_with_zero, ArrayType(StringType()))
    else:
        tokenize_udf = func.udf(tokenize, ArrayType(StringType()))
    # input_df = input_df.toPandas()
    # input_df["title"] = input_df["title"].apply(lambda ttl: tokenize(ttl, lang))
    input_df = input_df.withColumn('title', tokenize_udf(func.col('title'), func.lit(lang)))
    print("Tokenized dataframe: size({:1.0f})".format(input_df.count()))
    input_df.show()
    if type(list(broadcast.value.values())[0]) == int:
        udfTitleProcess = func.udf(title_to_index_udf, ArrayType(IntegerType()))
    elif type(list(broadcast.value.values())[0]) == list and encode_type == 'average':
        udfTitleProcess = func.udf(udf_title_to_average_vector, StringType())
    elif type(list(broadcast.value.values())[0]) == list and encode_type == 'concat':
        udfTitleProcess = func.udf(title_to_vector_udf, ArrayType(ArrayType(FloatType())))

    input_df = input_df.withColumn('title', udfTitleProcess(func.col('title')))
    df_not_mapped_titles = input_df.filter(func.size('title') == 0)
    print("Dataframe of titles that were not mapped to word vectors: size({:1.0f})".format(df_not_mapped_titles.count()))
    df_not_mapped_titles.show()
    input_df = input_df.filter(func.size('title') > 0)
    if input_df.rdd.isEmpty():
        raise Exception("All titles were not mapped to vectors! Dataframe is empty")
    return input_df

def count_array(vector):
    return len(vector)

def process_udf_style(spark, path, input_df, model_name, lang=None):
    if model_name == const.MODEL_CATEGORY:
        corpus_w2v = const.W2V_CATEGORY_MODEL_NAME
    elif model_name == const.MODEL_BEER:
        corpus_w2v = const.W2V_BEER_MODEL_NAME
    else:
        corpus_w2v = const.W2V_CATEGORY_MODEL_NAME
        print('Error! the model name does not exist, use default model name', corpus_w2v)

    print('load w2v model from:', corpus_w2v, 'for model', model_name)
    #word2vec_dict = model_mapping_dict(path, corpus_w2v)
    word2vec_dict = model_mapping_dict_from_bin(path, corpus_w2v)
    input_df = input_df.filter(func.trim(input_df.title).isNotNull())

    broadcast = spark.sparkContext.broadcast(word2vec_dict)
    w2v_df = process_all_titles(broadcast, input_df, 'concat', lang)

    if 'bar_product_id' in w2v_df.columns:
        w2v_df = w2v_df.select('bar_product_id', 'title')
    else:
        w2v_df = w2v_df.select('title')

    return w2v_df



# Process input data frame as a join with the mapper
def process_join(spark, input_df, path):
    sqlContext = SQLContext(spark)

    # w2v options: default_w2v (word = vector 100), test_corpus(word = vector 25, w2v_75 = vector 25)
    mapping_df = model_word2index_df(path, const.W2V_CATEGORY_MODEL_NAME)

    if 'bar_product_id' in input_df.columns:
        vector_columns = ['bar_product_id', 'title', 'brand_id']
    else:
        vector_columns = input_df.columns#['title', 'brand_id']
        print('Error! there is no bar_product_id field, no merge can be made with the structured data')


    w2v_df = sqlContext.createDataFrame(mapping_df)
    input_df = input_df.select(vector_columns)
    input_df = input_df.filter(input_df.title.isNotNull())

    tokenize_udf = func.udf(tokenize, ArrayType(StringType()))
    input_df = input_df.withColumn('title', tokenize_udf(func.col('title')))

    input_df = input_df.withColumn('title_word', func.explode('title'))

    #print('check padding')
    #print(input_df.filter(input_df.title_word == unknown_token).show(10))

    #join_outer_df = input_df.join(w2v_df, input_df.title_word == w2v_df.word, how='left')
    #unknown_words_df = join_outer_df.filter(join_outer_df.word.isNull())

    # unknown_item = str(mapping_df[mapping_df.word == unknown_token].iloc[0]['vector'])
    # #print(unknown_item)
    # unknown_words_df = unknown_words_df.withColumn('vector', lit(unknown_item))
    #
    # str_to_arr_udf = udf(str_to_array, ArrayType(StringType()))
    # #unknown_words_df = unknown_words_df.withColumn('vector', str_to_arr_udf(col('vector')))
    #
    # join_df = input_df.join(w2v_df, input_df.title_word == w2v_df.word)
    #
    # vector_columns.append('vector')
    # join_df = join_df.select(vector_columns)
    # unknown_words_df = unknown_words_df.select(vector_columns)
    # join_df = join_df.union(unknown_words_df)
    #
    # vector_columns.remove('vector')

    join_df = input_df.join(w2v_df, input_df.title_word == w2v_df.word)
    join_df = join_df.groupBy(vector_columns).agg(func.collect_list("word").alias('title_vector'))

    join_df = join_df.withColumn('title', func.col('title_vector'))
    if 'bar_product_id' in join_df.columns:
        join_df = join_df.select('bar_product_id', 'title')
    else:
        join_df = join_df.select('title')
    return join_df
