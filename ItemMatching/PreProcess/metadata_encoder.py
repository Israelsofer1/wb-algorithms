import os
from pyspark.sql.types import DoubleType, StringType
import pyspark.sql.functions as func
from ..constant import const
from .price_encoder import price_encoder
from .category_encoder import category_encoder
from .corpus_encoder import corpus_encoder
from .matching_config_creator import full_model_dict, FULL_FEATURE_BOW_CSV_NAME, FULL_FEATURE_NN_CSV_NAME, FULL_FEATURE_W2V_CSV_NAME
from .w2v_gensim import process_udf_style
from pyspark.ml.feature import VectorAssembler

def count_array(vector):
    return len(vector)

# Base function that should appears in Common. Because of relative import we ignore it
def merge_two_dicts(dict_one, dict_two):
    dict_combine = dict_one.copy()  # start with x's keys and values
    dict_combine.update(dict_two)  # modifies z with y's keys and values & returns None
    return dict_combine

def array_to_string(my_list):
    return '[' + ','.join([str(elem) for elem in my_list]) + ']'

# This class wrap all encoders and save the entire output into a single place
class metadata_encoder(object):
    def __init__(self, spark_session, base_path, model_name, version, flow, date, run_id, model_types=[const.MODEL_TYPE_NN], language=""):
        self.local_address = os.path.join(base_path, model_name, version) + '/'
        if flow == const.FLOW_PREDICT:
            self.test_folder = os.path.join(flow, date, run_id, const.INPUT_FOLDER)
            self.test_pre_process_folder = os.path.join(flow, date, run_id, const.PRE_PROCESS_FOLDER) + '/'
        elif flow == const.FLOW_TEST:
            self.test_folder = os.path.join(flow, const.INPUT_FOLDER)
            self.test_pre_process_folder = os.path.join(flow, const.PRE_PROCESS_FOLDER) + '/'

        self.spark = spark_session
        self.columns = self.read_columns_from_config(model_name)
        self.input_columns = self.read_columns_from_config(model_name, direction='input')
        self.model_types = model_types
        self.model_name = model_name
        self.flow = flow
        self.lang = language
        if language is None or language == "" or len(language) == 0:
            self.lang = None
        # Static folders
        self.train_folder = os.path.join(const.FLOW_TRAIN, const.INPUT_FOLDER) + '/'
        self.train_pre_process_folder = os.path.join(const.FLOW_TRAIN, const.PRE_PROCESS_FOLDER) + '/'
        self.train_mapping_folder = os.path.join(const.FLOW_TRAIN, const.MAPPING_FOLDER) + '/'

    # <editor-fold desc="This functions read the columns and build column list/dict for input/output columns">

    def read_columns_from_config(self, model_name, direction=None):
        return self.create_columns(full_model_dict[model_name], direction)

    def create_columns(self, features, direction=None):
        columns = {const.TEXT_FEATURE: [], const.CONTINUOUS_FEATURE: [], const.CATEGORY_FEATURE: [], 'multi': []}
        for feature in features:
            if direction is None or features[feature]['direction'] == direction:
                columns[features[feature]['type']].append(feature)
        return columns

    def get_flat_input_columns(self, field_type=None):
        flat_columns = []
        for level in self.input_columns:
            if field_type is None or level == field_type:
                flat_columns.extend(self.input_columns[level])
        return flat_columns

    def get_all_columns_flat(self, columns):
        output_columns = columns[const.TEXT_FEATURE]
        output_columns.extend(columns[const.CONTINUOUS_FEATURE])
        output_columns.extend(columns[const.CATEGORY_FEATURE])
        return output_columns

    # </editor-fold>

    # Preprocess stuff: Cast types and remove null values
    def organize_data(self, input_df, flow):
        print('The model is: ', self.model_name)
        # if self.model_name == const.MODEL_CATEGORY:
        if "pos_category_name" in input_df.columns:
            print('combine title with pos category name')
            #print(input_df.show())

            # TODO: If the pos category is only a number ignore it!
            input_df = input_df.withColumn('title',
                                           func.concat(func.when(func.col("title").isNotNull(), func.col("title")).otherwise(func.lit("  ")),
                                           func.lit(' '),
                                           func.when(func.col("pos_category_name").isNotNull(), func.col("pos_category_name")).otherwise(func.lit(""))))
            input_df = input_df.drop('pos_category_name')
            #print(input_df.show())

        print('Convert all fields to the correct types and clear rows with null values')

        for column in input_df.columns:
            # Specifically, handle volume id as double and all other category columns as string
            if column == 'volume_id':
                print('change column: ', column, 'type to double')
                input_df = input_df.withColumn(column, input_df[column].cast(DoubleType()))
            elif column in full_model_dict[self.model_name]:
                if full_model_dict[self.model_name][column]['type'] == "cont":
                    input_df = input_df.withColumn(column, input_df[column].cast(DoubleType()))
                    print('change column: ', column, 'type to double')
                elif full_model_dict[self.model_name][column]['type'] == "single":
                    input_df = input_df.withColumn(column, input_df[column].cast(StringType()))
                    print('change column: ', column, 'type to string')
                elif full_model_dict[self.model_name][column]['type'] == "txt":
                    input_df = input_df.withColumn(column, input_df[column].cast(StringType()))
                    print('change column: ', column, 'type to string')

            if flow == const.FLOW_PREDICT:
                if column in full_model_dict[self.model_name] and full_model_dict[self.model_name][column]['direction'] == 'input':
                    input_df = input_df.filter(input_df[column].isNotNull())
                    print('clean the null values from', column, 'for flow', flow)
            else:
                if column in full_model_dict[self.model_name]:
                    input_df = input_df.filter(input_df[column].isNotNull())
                    print('clean the null values from', column, 'for flow', flow)

        print('Finish converting the fields')
        return input_df

    def __encode_data__(self, folder, save_folder, output_cols=[], is_to_load=False, limit_count=None):
        input_df = self.spark.read.format("csv").option("header", "true").load(self.local_address + folder)
        if limit_count is not None:
            input_df = input_df.limit(limit_count)

        print('The data frame loaded correctly with columns: ', input_df.columns)
        print('Start encoding bow and NN')

        encode_nn = None
        input_df = self.organize_data(input_df, self.flow)
        if const.MODEL_TYPE_NN in self.model_types:
            print('Model w2v starts because of model type status', const.MODEL_TYPE_NN)
            encode_nn = self.encode_data_w2v_from_df(input_df, is_to_load, save_folder)

        encode_bow = None
        if const.MODEL_TYPE_BOW in self.model_types:
            print('Model BOW starts because of model type status', const.MODEL_TYPE_BOW)
            encode_bow = self.encode_data_bow_from_df(input_df, output_cols, is_to_load, save_folder)
        return [encode_nn, encode_bow]

    def encode_train_data(self, output_cols=[], limit_count=None):
        return self.__encode_data__(self.train_folder, self.train_pre_process_folder, output_cols, False, limit_count)

    def encode_test_data(self, limit_count=None):
        self.__encode_data__(self.test_folder, self.test_pre_process_folder, [], True, limit_count)

    # This function get top items for label
    # This is build for the BOW, that is limited by 300 types for category
    def get_top_labels(self, df, label, count_top=300):
        agg_brands = df.groupBy(label).agg(func.count(label).alias("label_count"))
        pandas_brands = agg_brands.toPandas()
        top_classes_list = list(pandas_brands.nlargest(count_top, 'label_count')[label])

        df = df.withColumn(label, func.when(func.col(label).isin(top_classes_list), func.col(label)).otherwise(func.lit(0)))
        return df

    # Pre process continuous (price) field
    def continous_encoder(self, matching_df, is_to_load):
        encoder = price_encoder(self.spark)
        print_addition = ''
        model_path = self.local_address + self.train_mapping_folder

        if is_to_load:
            print_addition = ' with pre trained encoder'
            encoder.load_scaler(path=model_path)

        encode_data = encoder.encode(matching_df, self.columns[const.CONTINUOUS_FEATURE])

        if not is_to_load:
            # Save pre process scalars fields to S3
            print_addition = ' and save the model to: ' + model_path
            encoder.save_scaler(path=model_path)

        print('Finish to encode scalar model', print_addition)
        return encode_data

    def onehot_encoder(self, matching_df, input_columns, output_columns, is_w2v, is_to_load):
        # Pre process category (category/brand/serving_type, brand) field
        encoder = category_encoder(self.spark, is_w2v)
        model_path = self.local_address + self.train_mapping_folder

        if is_to_load:
            encoder.load_pre_processing(path=model_path)
            print('Finish to load the category model (for input columns only)', input_columns)

        encode_data = encoder.encode(matching_df, input_columns, output_columns, is_w2v)
        print('Finish encode category fields')

        if not is_to_load:
            # Save pre process category fields to S3
            save_path = self.local_address + self.train_mapping_folder

            encoder.save_pre_processing(path=save_path)
            print('Finish to save the category model (for input columns)',  input_columns)
            print('and label mapping (for output columns)', output_columns)

        return encode_data

    def w2v_encoder(self, matching_df):
        vector_df = process_udf_style(self.spark, self.local_address, matching_df, self.model_name, self.lang)
        return vector_df

    def bow_encoder(self, matching_df, is_to_load):
        print('Start encode string as BOW')
        encoder = corpus_encoder(self.spark, is_padding=True, lang=self.lang)
        model_path = self.local_address + self.train_mapping_folder

        if is_to_load:
            encoder.load_model(path=model_path)
            print('Finish to load the corpus model', 'title')

        encode_data = encoder.encode_bow(matching_df, 'title')

        if not is_to_load:
            # Save pre process category fields to S3
            encoder.save_model(path=model_path)
            print('Finish to save the corpus BOW model (for input columns)',  'title')

        return encode_data

    def encode_data_bow_from_df(self, matching_df, output_cols, is_to_load, save_folder):
        for column in output_cols:
            matching_df = self.get_top_labels(matching_df, column)

        encode_data = self.continous_encoder(matching_df, is_to_load)

        # Set input and output category columns
        input_category_columns = self.get_flat_input_columns(field_type=const.CATEGORY_FEATURE)
        if is_to_load:
            output_category_columns = []
        else:
            output_category_columns = output_cols

        encode_data = self.onehot_encoder(encode_data, input_category_columns, output_category_columns, False, is_to_load)

        encode_data_bow = self.bow_encoder(encode_data, is_to_load)

        decode_columns = self.get_flat_input_columns()
        print(decode_columns)

        for column in decode_columns:
            print(column)
            print(encode_data_bow.filter(encode_data_bow[column].isNull()).show())
            encode_data_bow = encode_data_bow.filter(encode_data_bow[column].isNotNull())

        print(encode_data_bow.show())
        assembler = VectorAssembler(inputCols=decode_columns, outputCol='data_vector')
        encode_data_bow = assembler.transform(encode_data_bow)

        output_cols.append('data_vector')

        if 'bar_product_id' in encode_data_bow.columns:
            output_cols.append('bar_product_id')
        else:
            print('The unique column bar_product_id is not exist in ', encode_data_bow.columns)
        self.save_data_to_json(encode_data_bow, path=self.local_address + save_folder,
                                file_name=FULL_FEATURE_BOW_CSV_NAME, output_columns=output_cols)
        print('finish save BOW pre process data')

        return encode_data_bow


    def encode_data_w2v_from_df(self, matching_df, is_to_load, save_folder):
        # Only save the decoder on the train
        encode_data = self.continous_encoder(matching_df, is_to_load)

        # Set input and output category columns
        input_category_columns = self.get_flat_input_columns(field_type=const.CATEGORY_FEATURE)
        if is_to_load:
            output_category_columns = []
        else:
            output_category_columns = list(set(self.columns[const.CATEGORY_FEATURE]) - set(input_category_columns))
        encode_data = self.onehot_encoder(encode_data, input_category_columns, output_category_columns, True, is_to_load)

        encode_data_w2v = self.w2v_encoder(matching_df)
        output_columns = self.get_all_columns_flat(self.columns)
        if ('bar_product_id' in encode_data.columns):
            output_columns.append('bar_product_id')

        is_join_tables = is_to_load

        self.save_data_to_json_w2v(encode_data, encode_data_w2v, path=self.local_address + save_folder,
                                is_join_tables=is_join_tables, file_name=FULL_FEATURE_NN_CSV_NAME, output_columns=output_columns)

        print('finish save word2vec pre process data')
        return [encode_data, encode_data_w2v]
        #encode_data_w2v.explain()

    def save_data_to_json(self, full_df, path, file_name, output_columns):
        full_df = full_df.select(output_columns)
        full_df.coalesce(1).write.mode('overwrite').json(path + file_name, compression='Gzip')

    def save_data_to_json_w2v(self, full_df, w2v_df, is_join_tables, path, file_name, output_columns):
        output_columns.remove('title')
        print(output_columns)
        full_df = full_df.select(output_columns)

        # Transform columns to arrays - continuous data and category output data
        for column in full_df.columns:
            if column in self.columns[const.CONTINUOUS_FEATURE]:
                full_df = full_df.withColumn(column, func.array(column))
            elif column in self.columns[const.CATEGORY_FEATURE] and column not in self.input_columns[const.CATEGORY_FEATURE]:
                full_df = full_df.withColumn(column, func.array(column))

        if is_join_tables:
            full_df = full_df.join(w2v_df, on='bar_product_id')
            print('starting write as json')
            full_df.coalesce(1).write.mode('overwrite').json(path + file_name, compression='Gzip')
        else:
            print('Save continuous and category data, the column names are:')
            print(output_columns)

            full_df = full_df.select(output_columns)
            full_df.coalesce(1).write.mode('overwrite').json(path + file_name, compression='Gzip')

            print('Save text data as vectors, the column names are:')
            print(w2v_df.columns)
            w2v_df.coalesce(1).write.mode('overwrite').json(path + FULL_FEATURE_W2V_CSV_NAME, compression='Gzip')


    # <editor-fold desc="Save functions - Not in use!!!">
    # def save_data_to_hdf5(self, code_data, path):
    #     hf = h5py.File(path + 'pre_process.hdf5', 'w')
    #     for key in code_data.keys():
    #         if key != 'title':
    #             hf.create_dataset(key, data=code_data[key], compression_opts=9, compression='gzip')
    #     hf.close()
    #     # write with built in pandas function
    #     code_data['title'].to_hdf(path + 'pre_process.hdf5', key='word2vec', mode='w')
    #     hf.close()
    #
    # def save_by_column(self, full_df, path):
    #     print('original columns = ', full_df.columns)
    #     output_columns = self.get_all_columns_flat()
    #     print('relevant columns = ', output_columns)
    #     full_df = full_df.select(output_columns)
    #     full_data_df = full_df.toPandas()
    #
    #     results_dict = {}
    #     for column in full_data_df.columns:
    #         if column == 'country_id':
    #             full_data_df[column] = full_data_df[column].apply(lambda col: eval(col))
    #         elif column != 'title':
    #             full_data_df[column] = full_data_df[column].apply(lambda col: [col])
    #         print(full_data_df[column].values[0])
    #         results_dict[column] = list(full_data_df[column].values)
    #
    #     if 's3' in path:
    #         bucket_s3 = path.split('/')[2:3]
    #         path_no_bucket = path.split('/')[3:]
    #         self.save_json_to_s3(bucket=bucket_s3, path=path_no_bucket, dict_to_save=results_dict)
    #     else:
    #         import gzip
    #         with gzip.open(path + 'pre_process_data.json.gz', 'wb') as f:
    #             f.write(json.dumps(results_dict).encode('utf-8'))
    #
    # def save_json_to_s3(self, bucket, path, dict_to_save):
    #     # A GzipFile must wrap a real file or a file-like object. We do not want to
    #     # write to disk, so we use a BytesIO as a buffer.
    #     gz_body = BytesIO()
    #     gz = GzipFile(None, 'wb', 9, gz_body)
    #     gz.write(dict_to_save.encode('utf-8'))  # convert unicode strings to bytes!
    #     gz.close()
    #     # GzipFile has written the compressed bytes into our gz_body
    #     import boto3
    #     client = boto3.client('s3')
    #
    #     client.put_object(
    #         Bucket=bucket,
    #         Key=path,  # Note: NO .gz extension!
    #         ContentType='text/plain',  # the original type
    #         ContentEncoding='gzip',  # MUST have or browsers will error
    #         Body=gz_body.getvalue()
    #     )
    # </editor-fold>



