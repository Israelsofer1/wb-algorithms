import unittest
from Common.DAL.DataLoader.Mapper import Map as map

TEST_HOST = 'DEV'

class MapperTest(unittest.TestCase):

    def test0_select(self):
        tst_cols = ['country_id', 'title_simple', 'price']
        res_sel = 'SELCT DESTINCT a.country as country_id, bp.title as title_simple, bp.price as price'
        self.assertEqual(tst_cols, res_sel)

    def test1_table_check(self):
        tst_cols = ['country_id', 'title_simple', 'price']
        res_sel = ['a', 'bp']
        self.assertEqual(tst_cols, res_sel)


if __name__ == '__main__':
    unittest.main()