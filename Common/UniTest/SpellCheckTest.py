import unittest
from Common.ExternalAPI import SpellChecker as sc

SAMPLE_STRINGS = ['hi', 'hi', 'hi', 'hu', 'hu', 'ho']
NAME = 'test'

class SpellTest(unittest.TestCase):

    def test0_create(self):
        sc.create_spellchecker(SAMPLE_STRINGS, model_name=NAME, min_word_count=2)

    def test1_spelling(self):
        words = ['hi', 'ho', 'ha', 'hu', 'hhhhhhh']
        expected = ['hi', 'hi', 'hi', 'hu', 'hhhhhhh']
        schecker = sc.read_spellchecker(NAME)
        res = [schecker.standard_check(w) for w in words]
        self.assertEqual(res, expected)

    def test2_split(self):
        words = 'hihu'
        expected = 'hi hu'
        schecker = sc.read_spellchecker(NAME)
        res = schecker.standard_check(words)
        self.assertEqual(res, expected)


if __name__ == '__main__':
    unittest.main()