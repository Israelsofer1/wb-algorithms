import unittest
from Common.DAL.Connectors.Files import FileExplorer

class MongoTest(unittest.TestCase):

    def test0_csv(self):
        fe = FileExplorer()
        fname = 'testcsv'
        test_lod = [{'var_str': 'a', 'var_int': 1, 'var_float': 0.5, 'var_key': 2}]
        fe.csv_writer(dlst=test_lod, name=fname)
        out = fe.csv_reader(name=fname)
        self.assertEqual(out, test_lod)

    def test1_json(self):
        fe = FileExplorer()
        fname = 'testjson'
        test_json = {'var_str': 'a', 'var_int': 1, 'var_float': 0.5, 'var_key': 2}
        fe.jsonwrite(obj=test_json, name=fname)
        out = fe.jsonread(name=fname)
        self.assertEqual(out, test_json)

    def test2_pkl(self):
        fe = FileExplorer()
        fname = 'testpkl'
        test_pkl = unittest.TestCase()
        fe.pklwriter(obj=test_pkl, name=fname)
        out = fe.pklreader(name=fname)
        self.assertEqual(out, test_pkl)


if __name__ == '__main__':
    unittest.main()