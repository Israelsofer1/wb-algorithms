import unittest
import logging as log
from Common.DAL.Connectors.SQL import DbExplorer as dbe

TEST_HOST = 'DEV'

class SQLTest(unittest.TestCase):

    def test0_alter(self):
        con = dbe(host=TEST_HOST)
        con.dbalter("DROP TABLE IF EXISTS ds_test")
        q = """
        CREATE TABLE ds_test (
        var_str varchar(25),
        var_int int,
        var_float float,
        var_key int,
        PRIMARY KEY (var_key)
        );
        """
        con.dbalter(q)

    def test1_read(self):
        con = dbe(host=TEST_HOST)
        q = "SELECT *   FROM    pos_bars_products   WHERE   price = 1   LIMIT 1"
        out = con.dbreader(q=q)
        self.assertEqual(out[0]['price'], 1)

    def test2_write_delete(self):
        # Test insert_many
        con = dbe(host=TEST_HOST)
        test_lod = [{'var_str': 'a', 'var_int': 1, 'var_float': 0.5, 'var_key': 2}]
        con.insert(lst=test_lod, table='ds_test')
        out = con.dbreader(q="SELECT * FROM ds_test")
        self.assertEqual(out, test_lod)
        # Test remove row
        q = "DELETE FROM ds_test WHERE var_key > 0"
        con.dbalter(q)
        out = con.dbreader(q="SELECT * FROM ds_test")
        self.assertEqual(len(out), 0)


if __name__ == '__main__':
    log.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=log.INFO,
                    filename='sql.log')
    unittest.main()