import Common.ExternalAPI.Translator as ggl
from pyspark.sql.types import *
from pyspark.sql import SparkSession, Row
import unittest

TEST_HOST = 'DEV'

class GoogleTranslateServiceTest(unittest.TestCase):

    def test0_send(self):
        spark = SparkSession \
                    .builder \
                    .appName("Python_Spark_SQL_Google_Translate_test") \
                    .enableHiveSupport() \
                    .getOrCreate()
        x = ggl.Translator()
        test_list = ["BIERES", "80", "BULLES", "(뼈)간장티꾸닭", " 콜라(대)추가", "(참)클래식", "VASO + AMBER LAGER PINTA", "GRIFFON ROUSSE", "CREAM ALE", "STELLA ARTOIS"]
        test_list = [Row(x) for x in test_list]
        test_df = spark.createDataFrame(test_list, ["title"])
        test_df.show()
        df = x.translate_df(test_df)
        df.show()
        x.write_translated_df_2_hive(df)

    def test1_send(self):
        translator = ggl.GoogleTranslate()
        test_list = ["BIERES", "80", "BULLES", "(뼈)간장티꾸닭", " 콜라(대)추가", "(참)클래식", "VASO + AMBER LAGER PINTA", "GRIFFON ROUSSE", "CREAM ALE", "STELLA ARTOIS"]
        for text in test_list:
            translation = translator.trans_text(string_2_trans=text, source_lang=None)
            translated_text = translation["translations"][0]["translatedText"]
            print(translated_text)


    def test2_send_korean(self):
        spark = SparkSession \
                    .builder \
                    .appName("Python_Spark_SQL_Google_Translate_test") \
                    .enableHiveSupport() \
                    .getOrCreate()
        x = ggl.Translator(spark=spark, src_lang="ko")
        test_list = ["(뼈)간장티꾸닭", " 콜라(대)추가", "(참)클래식", "가브리살", "가자미", "가정식백반", "가죽", "가지", "간", "가라아게", "감튀"]
        test_list = [Row(x) for x in test_list]
        test_df = spark.createDataFrame(test_list, ["title"])
        test_df.show()
        df = x.translate_df(test_df, src_lang=x.src_lang)
        df.show()


    def test3_try_translating_non_string_types(self):
        spark = SparkSession \
            .builder \
            .appName("Python_Spark_SQL_Google_Translate_test") \
            .enableHiveSupport() \
            .getOrCreate()
        x = ggl.Translator(spark=spark, src_lang="ko")
        # test_list = ["(뼈)간장티꾸닭", " 콜라(대)추가", "(참)클래식"]
        # test_list = u'(뼈)간장티꾸닭'
        test_list = [5, None]
        ggl.translate_text(text=test_list, src_lang=x.src_lang, translator=x.translator)


    def test4_limit_cost(self):
        spark = SparkSession \
            .builder \
            .appName("Python_Spark_SQL_Google_Translate_test") \
            .enableHiveSupport() \
            .getOrCreate()
        x = ggl.Translator(spark=spark, src_lang="ko", usd_limit=0.002)
        test_list = ["(뼈)간장티꾸닭", " 콜라(대)추가", "(참)클래식", "가브리살", "가자미", "가정식백반", "가죽", "가지", "간", "가라아게", "감튀", "가리비", "간장게장정식", "갈매기살", "감자튀김", "계란말이", "고르곤졸라", "골뱅이무침", "김치찌개", "돼지목삼겹살", "뚝배기불고기", "반건조오징어", "123반건조오징어123", "234반건조오징어234", "098반건조오징어998", "알리오올리오", "깍두기채끝등심"]
        test_list = [Row(x) for x in test_list]
        test_df = spark.createDataFrame(test_list, ["title"])
        test_df.show()
        df = x.limit_cost(test_df)
        df.show()
