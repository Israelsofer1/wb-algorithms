import unittest
import Common.ExternalAPI.SQS as sqs
from Common.DAL.Connectors.Files import  FileExplorer

MESSAGE = {'foo': 'bar'}
QUE_NAME = 'dev_pos_automatching.fifo'

class SQSTest(unittest.TestCase):

    def test0_exists(self):
        que = sqs.SQSWrap(QUE_NAME)
        urls = que.show_all_queues_urls()
        self.assertNotEqual(len(urls), 0)

    def test0_write(self):
        que = sqs.SQSWrap(QUE_NAME)
        que.dict_to_queue(dictionary=MESSAGE, message_group_id='test')

    def test1_read(self):
        que = sqs.SQSWrap(QUE_NAME)
        msgs = que.receive_messages(num_messages=1)
        self.assertEqual(len(msgs), 1)


if __name__ == '__main__':
    unittest.main()