import unittest

from Common.DAL.Connectors.Files import FileExplorer

FILENAME = 'test_s3.csv'

class S3Test(unittest.TestCase):

    def test0_write(self):
        # Create a csv and attempt to write it to an S3 bucket
        sample = [{'foo': 'bar'}]
        fe = FileExplorer()
        con = S3.S3Wrap()
        fe.csv_writer(dlst=sample, name='test_s3')
        res = con.upload(fname=FILENAME)
        self.assertIsNone(res)

    def test1_read(self):
        fe = FileExplorer()
        con = S3.S3Wrap()
        con.download(fname=FILENAME)
        out = fe.csv_reader(name=FILENAME)
        self.assertNotEqual(len(out), 0)

    def test2_write_pos(self):
        # In the case of the POS class, it's important to write the test file to its local address
        sample = [{'foo': 'bar'}]
        con = S3.S3pos()
        fe = FileExplorer(adr=con.loc_addrs)
        fe.csv_writer(dlst=sample, name='test_s3')
        res = con.pos_upload(f_name=FILENAME)
        self.assertIsNone(res)

    def test3_read_pos(self):
        con = S3.S3pos()
        fe = FileExplorer(adr=con.loc_addrs)
        con.pos_download(f_name=FILENAME)
        out = fe.csv_reader(name=FILENAME)
        self.assertNotEqual(len(out), 0)


if __name__ == '__main__':
    unittest.main()