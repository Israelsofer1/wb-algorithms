import unittest
from Common.Infra import fuzzy_matching as fz
import pandas  as pd


class fuzzytest(unittest.TestCase):

    def test0_word_matcher(self):
        res = fz.word_matcher('stella','stella artios')
        self.assertEqual(res,float(100))
        return()

    def test1_word_matcher(self):
        res = fz.word_matcher('ab','20')
        self.assertEqual(res,float(0))
        return()

    def test2_word_matcher(self):
        res = fz.word_matcher('20','20')
        self.assertAlmostEqual(res,float(100))
        return()

    def test0_df_title_matcher(self):
        res = fz.df_title_matcher(ex_product,df_of_brands_ex,'brand_id','title',3)
        self.assertIsInstance(res,list)
        return()

    def test1_df_title_matcher(self):
        res = fz.df_title_matcher(ex_product,df_of_brands_ex,'brand_id','title',0)
        self.assertEqual(res,[])
        return()

    def test0_df_word_matcher(self):
        res = fz.df_word_matcher(ex_product,df_of_brands_ex,'brand_id','title',0)
        self.assertEqual(res,[])
        return()

    def test1_df_word_matcher(self):
        res = fz.df_title_matcher(ex_product, df_of_brands_ex, 'brand_id', 'title', 3)
        self.assertIsInstance(res, list)
        return ()


d1 = {'title': ['stella btl', 'Goose', 'CHEESBURGER DI$SCOUNT'], 'price': [3.54, 4.45,6.3]}
df_of_products_ex = pd.DataFrame(data = d1)

d2 = {'title': ['cheese burger', 'go island', 'stella artios' , 'Goldstar draught'], 'brand_id' :[1,2,3,4]}
df_of_brands_ex = pd.DataFrame(data = d2)

ex_product = 'Stl draft'

if __name__ == '__main__':
    unittest.main()