import unittest
from Common.DAL.Connectors.Mongo import MongoExplorer as mongo

TEST_HOST = 'DEV'

class MongoTest(unittest.TestCase):

    def test0_write(self):
        con = mongo(client=TEST_HOST)
        q = [{'var_c': 'ab', 'var_i': 1, 'var_f': 0.5}]
        con.insert_many(q)

    def test1_read(self):
        con = mongo(client=TEST_HOST)
        q = {'var_c': 'ab'}
        out = con.find(q)
        self.assertEqual(out[0]['var_c'], 'ab')

    def test2_delete(self):
        con = mongo(client=TEST_HOST)
        q = {'var_c': 'ab'}
        con.collection.delete_many(q)
        out = con.find(q)
        self.assertEqual(len(out), 0)


if __name__ == '__main__':
    unittest.main()