from pyspark.sql.types import *


class Columns(object):
    CATEGORY_COLUMNS = ['bar_product_id', 'category_id', 'category_id_prob']
    BEER_COLUMNS = ['bar_product_id', 'brand_id', 'brand_id_prob', 'serving_type_id', 'serving_type_id_prob', 'volume_id', 'volume_id_prob']
    FOOD_COLUMNS = ['bar_product_id', 'type_id', 'super_type_id']


class Tables(object):
    CATEGORY_TABLE = 'pos_category_suggestions'
    BEER_TABLE = 'pos_beer_suggestions_final'
    FOOD_TABLE = 'pos_food_suggestions_final'


class Schemas(object):

    CATEGORY_SCHEMA = StructType([
                    StructField('product_id', StringType(), False),
                    StructField('matching_data',
                        StructType([StructField('tag_groups', ArrayType(
                            StructType([StructField('tag_sets', ArrayType(
                                StructType([StructField('category_id',
                                    StructType([StructField('value', IntegerType(), False),
                                                StructField('prob', FloatType(), False)
                                    ]))
                                ])))
                            ])))
                        ])),
                    StructField('mode', StringType(), False)
                    ])

    BEER_SCHEMA = StructType([
                    StructField('product_id', StringType(), False),
                    StructField('matching_data',
                        StructType([StructField('tag_groups', ArrayType(
                            StructType([StructField('tag_sets', ArrayType(
                                StructType([StructField('beer_brand_id',
                                                StructType([StructField('value', IntegerType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ])),
                                            StructField('beer_serving_type_id',
                                                StructType([StructField('value', IntegerType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ])),
                                            StructField('volume_per_unit',
                                                StructType([StructField('value', FloatType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ])),
                                            StructField('units',
                                                StructType([StructField('value', FloatType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ]))
                                ])))
                            ])))
                        ])),
                    StructField('mode', StringType(), False)
                    ])

    FOOD_SCHEMA = StructType([
                    StructField('product_id', StringType(), False),
                    StructField('matching_data',
                        StructType([StructField('tag_groups', ArrayType(
                            StructType([StructField('tag_sets', ArrayType(
                                StructType([StructField('food_type_ids', ArrayType(
                                                StructType([StructField('value', IntegerType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ]))),
                                            StructField('food_super_type_ids', ArrayType(
                                                StructType([StructField('value', IntegerType(), False),
                                                            StructField('prob', FloatType(), False)
                                    ])))
                                ])))
                            ])))
                        ])),
                    StructField('mode', StringType(), False)
                    ])


class Constants(object):
    MODEL_CATEGORY = 'category'
    MODEL_BEER = 'beer'
    MODEL_FOOD = 'food'
    WRITE_MODE = 'merge'

# [
#     product_id,
#     [
#         [
#             [[x.value, x.probability] for x in tag],
#             [[y.value, y.probability] for y in type]
#         ]
#     ],
#     mode
# ]