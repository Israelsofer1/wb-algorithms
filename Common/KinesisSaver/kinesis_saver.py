from math import ceil
import boto3
import json
from .matching_suggestions_structure_config import Constants as const, Schemas as schemas, Columns as columns, Tables as tables
import os
from pyspark.sql.functions import col, udf, lit
from pyspark.sql.types import StringType
from .. import logging_config


logger = logging_config.configure_logger(__name__, "DEBUG")


def lines_generator(input_string):
    prev_line = -1
    while True:
        next_line = input_string.find('\n', prev_line + 1)
        if next_line < 0:
            break
        yield input_string[prev_line + 1:next_line]
        prev_line = next_line


class KinesisSaver(object):
    def __init__(self, spark, model_name, stream_name, region='eu-west-1', environment='dev'):

        logger.info('KinesisSaver initializing')
        logger.info('Arguments received:')
        logger.info(f'Spark session: {spark}')
        logger.info(f'Model name: {model_name}')
        logger.info(f'Model name: {stream_name}')
        logger.info(f'Region name: {region}')
        logger.info(f'Environment name: {environment}')

        self.spark = spark
        self.model_name = model_name
        self.stream_name = stream_name
        self.region = region
        self.environment = environment
        self.payload = None
        self.kinesis_client = boto3.client('kinesis', region_name=self.region)
        self.kinesis_batch_max_records = 500.
        self.s3 = boto3.resource('s3')
        self.s3_bucket_name = 'wb-ds-' + self.environment + '-models'
        self.s3_bucket = self.s3.Bucket(self.s3_bucket_name)
        self.temp_folder = 'matching_to_kinesis_temp'

    def load_transform(self, run_id):

        logger.info(f'Load Transform started for run id: {run_id}')

        if self.model_name == const.MODEL_CATEGORY:
            df = self.spark.sql("select " + ','.join(columns.CATEGORY_COLUMNS) + " from stg." + tables.CATEGORY_TABLE + " where run_id =  '" + run_id + "'")
            df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
            format_category = udf(lambda product_id, category, probability, mode: [product_id, [[[[[[category, probability]]]]]], mode], schemas.CATEGORY_SCHEMA)
            df = df.withColumn('Data', format_category(col('bar_product_id'), col('category_id'), col('category_id_prob'), lit(const.WRITE_MODE)))
            df = df.select('Data', 'PartitionKey')
            self.payload = df

        elif self.model_name == const.MODEL_BEER:
            df = self.spark.sql("select " + ','.join(columns.BEER_COLUMNS) + " from stg." + tables.BEER_TABLE + " where run_id = '" + run_id + "'")
            df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
            format_beer = udf(lambda product_id, brand, brand_probability, serving_type, serving_type_probability, volume, volume_probability, units, units_probability, mode: [product_id, [[[[[[brand, brand_probability], [serving_type, serving_type_probability], [volume, volume_probability], [units, units_probability]]]]]], mode], schemas.BEER_SCHEMA)
            df = df.withColumn('Data', format_beer(col('bar_product_id'), col('brand_id'), col('brand_id_prob'), col('serving_type_id'), col('serving_type_id_prob'), col('volume_id'), col('volume_id_prob'), lit(1), lit(0.86), lit(const.WRITE_MODE)))  # TODO: change to real probability upon supporting units in the model
            df = df.select('Data', 'PartitionKey')
            self.payload = df

        elif self.model_name == const.MODEL_FOOD:
            df = self.spark.sql("select " + ','.join(columns.FOOD_COLUMNS) + " from stg." + tables.FOOD_TABLE + " where run_id = '" + run_id + "'")
            df = df.withColumnRenamed('id', 'bar_product_id')
            df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
            format_food = udf(lambda product_id, type, super_type, mode: [product_id, [[[[[[[x.type, x.score] for x in type], [[y.super_type, y.score] for y in super_type]]]]]], mode], schemas.FOOD_SCHEMA)
            df = df.withColumn('Data', format_food(col('bar_product_id'), col('type'), col('super_type'), lit(const.WRITE_MODE)))
            df = df.select('Data', 'PartitionKey')
            self.payload = df

    def send_to_kinesis(self):

        logger.info(f'Send to Kinesis started')

        self.payload = self.payload.repartition(1)
        path = os.path.join("s3://", self.s3_bucket_name, self.temp_folder)
        self.payload.write.mode('overwrite').json(path)
        for f in self.s3_bucket.objects.filter(Prefix=self.temp_folder):
            if '.json' in f.key and '.crc' not in f.key:
                file_object = f

        f = file_object.get()['Body'].read().decode('utf-8')
        for line in lines_generator(f):
            put_response = self.kinesis_client.put_record(
                StreamName=self.stream_name,
                Data=json.dumps(json.loads(line)["Data"]),
                PartitionKey=json.loads(line)["PartitionKey"])
            logger.debug(put_response)
        for key in self.s3_bucket.objects.filter(Prefix=self.temp_folder+'/'):
            key.delete()
        return put_response

    def send_to_kinesis_local(self):

        logger.info(f'Send to Kinesis from local started')

        self.payload = self.payload.repartition(1)
        self.payload.write.mode('overwrite').json(self.s3_bucket)
        for r, d, f in os.walk(self.s3_bucket):
            for file in f:
                if '.json' in file and '.crc' not in file:
                    file_path = os.path.join(r, file)

        with open(file_path, 'r') as f:
            for line in f:
                put_response = self.kinesis_client.put_record(
                    StreamName=self.stream_name,
                    Data=json.dumps(json.loads(line)["Data"]),
                    PartitionKey=json.loads(line)["PartitionKey"])
                logger.debug(put_response)
        return put_response

    def batch_send_to_kinesis(self):

        logger.info(f'Batch send to Kinesis started')

        number_of_records = self.payload.count()
        number_of_partitions = int(ceil(number_of_records / self.kinesis_batch_max_records))
        self.payload = self.payload.repartition(number_of_partitions)
        path = os.path.join("s3://", self.s3_bucket_name, self.temp_folder)
        self.payload.write.mode('overwrite').json(path)
        files = []
        for f in self.s3_bucket.objects.filter(Prefix=self.temp_folder):
            if '.json' in f.key and '.crc' not in f.key:
                files.append(f)
        for _file in files:
            f = _file.get()['Body'].read().decode('utf-8')
            put_response = self.kinesis_client.put_records(
                Records=[{"Data": json.dumps(json.loads(line)["Data"]), "PartitionKey": json.loads(line)["PartitionKey"]} for line in lines_generator(f)],
                StreamName=self.stream_name
                )
            logger.debug(put_response)
        for key in self.s3_bucket.objects.filter(Prefix=self.temp_folder+'/'):
            key.delete()
        return put_response

    def batch_send_to_kinesis_local(self):

        logger.info(f'Batch send to Kinesis from local started')

        number_of_records = self.payload.count()
        number_of_partitions = int(ceil(number_of_records / self.kinesis_batch_max_records))
        self.payload = self.payload.repartition(number_of_partitions)
        self.payload.write.mode('overwrite').json(self.s3_bucket)
        path = self.s3_bucket
        files = []
        for r, d, f in os.walk(path):
            for file in f:
                if '.json' in file and '.crc' not in file:
                    files.append(os.path.join(r, file))

        for _file in files:
            with open(_file, 'r') as f:
                put_response = self.kinesis_client.put_records(
                    Records=[{"Data": json.dumps(json.loads(line)["Data"]), "PartitionKey": json.loads(line)["PartitionKey"]} for line in f],
                    StreamName=self.stream_name
                    )
                logger.debug(put_response)
        return put_response

    def load_manual_payload(self, df):
        # CATEGORY

        # df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
        # format_category = udf(
        #     lambda product_id, category, probability, mode: [product_id, [[[[[[category, probability]]]]]], mode],
        #     schemas.CATEGORY_SCHEMA)
        # df = df.withColumn('Data', format_category(col('bar_product_id'), col('category_id'), col('category_id_prob'), lit(const.WRITE_MODE)))
        # df = df.select('Data', 'PartitionKey')
        # self.payload = df

        # BEER

        # df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
        # format_beer = udf(lambda product_id, brand, brand_probability, serving_type, serving_type_probability, volume, volume_probability, units, units_probability, mode: [product_id, [[[[[[brand, brand_probability], [serving_type, serving_type_probability], [volume, volume_probability], [units, units_probability]]]]]], mode], schemas.BEER_SCHEMA)
        # df = df.withColumn('Data', format_beer(col('bar_product_id'), col('brand_id'), col('brand_id_prob'), col('serving_type_id'), col('serving_type_id_prob'), col('volume_id'), col('volume_id_prob'), lit(1), lit(0), lit(const.WRITE_MODE)))
        # df = df.select('Data', 'PartitionKey')
        # self.payload = df

        # FOOD

        df = df.withColumn('PartitionKey', col('bar_product_id').cast(StringType()))
        format_food = udf(lambda product_id, type, super_type, mode: [product_id, [[[[[[[x.type, x.score] for x in type], [[y.super_type, y.score] for y in super_type]]]]]], mode], schemas.FOOD_SCHEMA)
        df = df.withColumn('Data', format_food(col('bar_product_id'), col('type'), col('super_type'), lit(const.WRITE_MODE)))
        df = df.select('Data', 'PartitionKey')
        self.payload = df

    def override_path(self, path):
        self.s3_bucket = str(path)
