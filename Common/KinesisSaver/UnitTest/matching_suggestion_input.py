from pyspark.sql.types import *


class Schemas(object):
    TOY_CATEGORY_SCHEMA = StructType([
                    StructField("bar_product_id", StringType(), False),
                    StructField("category_id", IntegerType(), False),
                    StructField("category_id_prob", FloatType(), False),
                     ])

    TOY_BEER_SCHEMA = StructType([
                    StructField("bar_product_id", StringType(), False),
                    StructField("brand_id", IntegerType(), False),
                    StructField("brand_id_prob", FloatType(), False),
                    StructField("serving_type_id", IntegerType(), False),
                    StructField("serving_type_id_prob", FloatType(), False),
                    StructField("volume_id", FloatType(), False),
                    StructField("volume_id_prob", FloatType(), False),
                     ])

    TOY_FOOD_SCHEMA = StructType([
                    StructField("bar_product_id", StringType(), False),
                    StructField("type", ArrayType(
                        StructType([
                            StructField("type", IntegerType(), False),
                            StructField("score", FloatType(), False)
                        ]))),
                    StructField("super_type", ArrayType(
                        StructType([
                            StructField("super_type", IntegerType(), False),
                            StructField("score", FloatType(), False)
                        ])))
                     ])
