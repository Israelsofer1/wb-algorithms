import unittest
from Common.KinesisSaver.kinesis_saver import KinesisSaver
from pyspark.sql import SparkSession, Row
from Common.KinesisSaver.matching_suggestions_structure_config import Constants as const, Schemas as schemas, Columns as columns
from Common.KinesisSaver.UnitTest.matching_suggestion_input import Schemas as input_schemas
from Common import logging_config

# TODO: setup a stream and delete it each run
# TODO: test all messages received?
# TODO: check s3 compatability when running local


logger = logging_config.configure_logger(__name__, logging_config.DEBUG, False)


class KinesisSaveTests(unittest.TestCase):

    def setUp(self, model_name='food', file_path=None):
        self.spark = SparkSession \
            .builder \
            .appName("Kinesis Saver Test") \
            .getOrCreate()
        self.model_name = model_name
        if self.model_name == const.MODEL_CATEGORY:
            suggestion = Row(*columns.CATEGORY_COLUMNS)
            row_1 = suggestion('f0bfc3e3f4426ad4981da983f6e45792', 5, 0.963720202445984)
            # row_2 = suggestion('2', 4, 0.5003103017807007)
            # row_3 = suggestion('3', 8, 0.9977113008499146)
            rows = [row_1]
            # for i in range(400):
            #     rows.append(row_1)
            #     rows.append(row_2)
            #     rows.append(row_3)
            self.input = self.spark.createDataFrame(rows, input_schemas.TOY_CATEGORY_SCHEMA)
        elif self.model_name == const.MODEL_BEER:
            suggestion = Row(*columns.BEER_COLUMNS)
            row_1 = suggestion('1', 232, 0.6463, 2, 0.3475, 0.591, 0.3252)
            row_2 = suggestion('2', 44, 0.5003, 1, 0.9318, 1.774, 0.6903)
            row_3 = suggestion('3', 15, 0.9977, 4, 0.3452, 0.330, 0.9989)
            rows = []
            for i in range(400):
                rows.append(row_1)
                rows.append(row_2)
                rows.append(row_3)
            self.input = self.spark.createDataFrame(rows, input_schemas.TOY_BEER_SCHEMA)
        elif self.model_name == const.MODEL_FOOD:
            suggestion = Row(*columns.FOOD_COLUMNS)
            row_1 = suggestion('1', [[32, 0.6463720202445984]], [(36, 0.5487222345)])
            row_2 = suggestion('2', [[14, 0.5445520202445984]], [])
            row_3 = suggestion('3', [[9, 0.9977113008499146]], [])
            rows = []
            for i in range(400):
                rows.append(row_1)
                rows.append(row_2)
                rows.append(row_3)
            self.input = self.spark.createDataFrame(rows, input_schemas.TOY_FOOD_SCHEMA)
        elif file_path:
            self.input = self.spark.read.csv(path=file_path, header=True)
            logger.info('Using file input as Toy DF')
        else:
            logger.error('No Input for Tests')
            exit(1)
        logger.debug('Toy DF:')
        if logger.isEnabledFor(logging_config.DEBUG):
            logger.debug(self.input.show())

    def test0_instanciate(self):
        logger.info('test0_instanciate:')
        model_name = 'food'
        stream_name = 'dev-auto-matching-stream'
        region = 'eu-west-1'
        saver = KinesisSaver(spark=self.spark, model_name=model_name, stream_name=stream_name, region=region)
        logger.debug(saver)

    def test2_send_to_kinesis_local(self):
        logger.info('test2_send_to_kinesis_local')
        model_name = 'category'
        stream_name = 'stg-auto-matching-stream'
        region = 'eu-west-1'
        saver = KinesisSaver(spark=self.spark, model_name=model_name, stream_name=stream_name, region=region)
        saver.load_manual_payload(self.input)
        saver.override_path('jsons.csv')
        response = saver.send_to_kinesis_local()
        logger.debug(response)

    def test3_send_to_kinesis(self):
        logger.info('test3_send_to_kinesis')
        model_name = 'food'
        stream_name = 'dev-auto-matching-stream'
        region = 'eu-west-1'
        saver = KinesisSaver(spark=self.spark, model_name=model_name, stream_name=stream_name, region=region)
        saver.load_manual_payload(self.input)
        response = saver.send_to_kinesis()
        logger.debug(response)

    def test4_batch_send_to_kinesis_local(self):
        logger.info('test4_send_to_kinesis_local')
        model_name = 'food'
        stream_name = 'dev-auto-matching-stream'
        region = 'eu-west-1'
        saver = KinesisSaver(spark=self.spark, model_name=model_name, stream_name=stream_name, region=region)
        saver.load_manual_payload(self.input)
        saver.override_path('jsons.csv')
        response = saver.batch_send_to_kinesis_local()
        logger.debug(response)

    def test5_batch_send_to_kinesis(self):
        logger.info('test5_send_to_kinesis')
        model_name = 'food'
        stream_name = 'dev-auto-matching-stream'
        region = 'eu-west-1'
        saver = KinesisSaver(spark=self.spark, model_name=model_name, stream_name=stream_name, region=region)
        saver.load_manual_payload(self.input)
        response = saver.batch_send_to_kinesis()
        logger.debug(response)

    def tearDown(self):
        logger.info('ok')
        # delete stream
