import pandas as pd
import numpy as np
import string
import time

def csv_reading_df(filename):
    data = pd.read_csv(filename, encoding="utf-8")
    return (data)

def data_cleaner(df, column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation + "0123456789"), '')
    df[column_name].filter(regex=r'\D')
    df[column_name] = df[column_name].str.lower()
    df[column_name] = df[column_name].str.strip()
    df = df.dropna(subset=[column_name])
    return (df)

def find_words_product(word,df):
    train = csv_reading_df(df)
    train_cleaned = data_cleaner(train, 'title')
    print('the word is' +str(word))
    temp_train = train_cleaned[train_cleaned.title.str.contains(str(word))]
    return(temp_train)


#activation
words = csv_reading_df('words_in_union.csv')
list_of_words = words.title.values
word = list_of_words[1]
print(find_words_product(word,'train_set.csv'))
