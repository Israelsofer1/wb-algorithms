# This file is mailnly meant to create the config (INI) file for the different connections
from Common.DAL.Connectors.Files import FileExplorer
CONFIG_NAME = 'config.json'


def read_config():
    with FileExplorer() as fe:
        config = fe.jsonread(CONFIG_NAME)
    return config


def create_connfig():
    config = {
        'SQL': {
            'con': {
                'PROD': 'mysql01.prod.alcoholanalytics.com',
                'STAGE': 'mysql01.stg.alcoholanalytics.com',
                'DEV': 'mysql01.dev.alcoholanalytics.com'
            },
            'user': {
                'DEFAULT': 'ds_pos',
                'POS': 'ds_pos'
            },
            'password': {
                'DEFAULT': 'fSQeiOruuAYF6Ufhm',
                'POS': 'fSQeiOruuAYF6Ufh'
            },
        },
        'Mongo': {
            'con': {
                'PROD': 'mongodb://prod_mongo:0rzHLPx8E519fuMW@prod-analytics-shard-00-00-mmvnu.mongodb.net:27017,prod-analytics-shard-00-01-mmvnu.mongodb.net:27017,prod-analytics-shard-00-02-mmvnu.mongodb.net:27017/test?ssl=true&replicaSet=prod-analytics-shard-0&authSource=admin',
                'STAGE': 'mongodb://dev_mongo:Y2UPHQx0Q5eVLjXG@staging-shard-00-00-mmvnu.mongodb.net:27017,staging-shard-00-01-mmvnu.mongodb.net:27017,staging-shard-00-02-mmvnu.mongodb.net:27017/test?ssl=true&replicaSet=staging-shard-0&authSource=admin',
                'DEV': 'mongodb://dev_mongo:Y2UPHQx0Q5eVLjXG@cluster0-shard-00-00-mmvnu.mongodb.net:27017,cluster0-shard-00-01-mmvnu.mongodb.net:27017,cluster0-shard-00-02-mmvnu.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin',
                'REPLICA': 'mongodb://prod_mongo_us:bhfsFxv0otPoCDOi@prod-us-shard-00-00-j6pon.mongodb.net:27017,prod-us-shard-00-01-j6pon.mongodb.net:27017,prod-us-shard-00-02-j6pon.mongodb.net:27017/test?ssl=true&replicaSet=Prod-US-shard-0&authSource=admin'
            }
        },
        'S3': {
            'access_key': 'AKIAID42LK6VX4KGLRCQ',
            'secret_key': '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',
            'region': 'eu-west-1'
        }
    }
    with FileExplorer() as fe:
        fe.jsonwrite(obj=config, name=CONFIG_NAME)


if __name__ == '__main__':
    create_connfig()