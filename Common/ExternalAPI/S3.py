import logging as log
import os

import boto3

import Common.data_access_config as dac
from Common.DAL.Connectors.Files import get_fname, local_address


class S3Wrap(object):
    def __init__(self, bucket='wb-ds-models', path=None, s3_path=None):
        log.info('S3 - Copying connection to bucket %s' % bucket)
        self.bucket = bucket
        config = dac.read_config()
        cred = config['S3']
        self.s3_resource = boto3.resource('s3',
                                          region_name=cred['region'],
                                          aws_access_key_id=cred['access_key'],
                                          aws_secret_access_key=cred['secret_key']
                                          )
        self.client = boto3.client('s3',
                                          region_name=cred['region'],
                                          aws_access_key_id=cred['access_key'],
                                          aws_secret_access_key=cred['secret_key']
                                          )
        self.main_bucket = bucket
        self.path_local = path
        self.s3_path = s3_path

    def full_adr(self, adr, fname):
        return '%s/%s' % (adr, fname)

    def copy_between_buckets(self, from_bucket, from_path, target_bucket, target_path):
        log.info('S3 - Copying from bucket %s to bucket %s to path %s' % (from_bucket, target_bucket, target_path))
        self.s3_resource.Object(target_bucket, target_path).copy_from(
            CopySource=os.path.join(from_bucket, from_path)
        )

    def put(self, target_object, path_s3=''):
        self.s3_resource.Bucket(self.main_bucket).put_object(Body=target_object,
                                                             Bucket=self.main_bucket,
                                                             Key=path_s3)

    def upload(self, fname, path_s3=None, path_local=None):
        """
        Upload a file from an address, if both path_local and path_s3 are None we rely on the filename
        :param fname: The name of the file, assuming it's in the current folder
        :param path_s3: Where to upload to in th S3 bucket
        :param path_local: The full path including the name of the file
        :return:
        """
        if path_local is None:
            path_local = local_address(fname) if self.path_local is None else local_address(fname, adr=self.path_local)
        if path_s3 is None:
            path_s3 = fname if self.s3_path is None else self.s3_path
        path_s3 = self.full_adr(path_s3, fname)
        log.info('S3 - Uploading file %s to bucket path %s' % (path_local, path_s3))
        bucket = self.s3_resource.Bucket(self.main_bucket)
        bucket.upload_file(path_local, path_s3)

    def download(self, fname, path_s3=None, path_local=None):
        if path_local is None:
            path_local = local_address(fname) if self.path_local is None else local_address(fname, adr=self.path_local)
        if path_s3 is None:
            path_s3 = fname if self.s3_path is None else self.s3_path
        path_s3 = self.full_adr(path_s3, fname)
        log.info('S3 - Downloading from bucket path %s to file path %s' % (path_s3, path_local))
        bucket = self.s3_resource.Bucket(self.main_bucket)
        bucket.download_file(path_s3, path_local)

    def list_all_files(self):
        bucket = self.s3_resource.Bucket(self.main_bucket)
        return bucket.objects.all()

    def list_files_in_folder(self, folder_adr, only_names=True, forbidden_names=None, wanted_names=None):
        files = self.list_all_files()
        file_names = [r.key.split('/') for r in files]
        res = [r for r, n in zip(files, file_names) if n[:-1] == folder_adr.split('/')]
        if forbidden_names:
            res = [v for v in res if v.key.split('/')[-1] not in forbidden_names]
        if wanted_names:
            res = [v for v in res if v.key.split('/')[-1] in wanted_names]
        if only_names:
            res = [v.key.split('/')[-1] for v in res]
        return res

    def check_if_exists(self, key):
        bucket = self.s3_resource.Bucket(self.main_bucket)
        return bool(list(bucket.objects.filter(Prefix=key)))

    def wait_for_file(self, key):
        bucket = self.s3_resource.Bucket(self.main_bucket)
        obj = bucket.Object(key)
        obj.wait_until_exists()


class S3pos(S3Wrap):
    def __init__(self,
                 bucket_name='wb-ds-models', s3addrs='pos/', loc_addrs=os.path.dirname(os.path.realpath(__file__))):
        self.bucket_name = bucket_name
        self.s3addrs = s3addrs
        self.loc_addrs = loc_addrs
        S3Wrap.__init__(self, bucket=self.bucket_name)

    def pos_address(self, long_local_address):
        # The local_address is long, we only need the last part
        trimmed_local_address = get_fname(long_local_address)
        return '%s%s' % (self.s3addrs, trimmed_local_address)

    def pos_download(self, f_name):
        ps3 = self.pos_address(f_name)
        local = local_address(f_name, self.loc_addrs)
        self.download(fname=f_name, path_s3=ps3, path_local=local)

    def pos_upload(self, f_name):
        ps3 = self.pos_address(f_name)
        local = local_address(f_name, self.loc_addrs)
        self.upload(fname=f_name, path_s3=ps3, path_local=local)



if __name__ == '__main__':
    s3 = S3Wrap()
    print(s3)