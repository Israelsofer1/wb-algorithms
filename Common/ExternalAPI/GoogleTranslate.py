from googleapiclient import discovery as GoogleAPI
from pyspark.sql import SparkSession
import pyspark.sql.functions as func
from pyspark.sql.window import *
from pyspark.sql.types import StringType, ArrayType
import boto3
import base64
import ast
from botocore.exceptions import ClientError
import datetime
import traceback

class GoogleTranslate(object):
    def __init__(self):
        secret_string = ast.literal_eval(self.get_secret()['SecretString'])
        self.secret_key = secret_string['DataScienceGoogleAPIKey_DevStg']
        self.client = GoogleAPI.build('translate', 'v2', developerKey=self.secret_key)
        self.base_lang = 'en'


    def get_secret(self):

        secret_name = "datascience/devstg/GoogleAPIKey"
        region_name = "eu-west-1"

        # Create a Secrets Manager client
        session = boto3.session.Session()
        client = session.client(
            service_name='secretsmanager',
            region_name=region_name
        )

        # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        # We rethrow the exception by default.

        try:
            get_secret_value_response = client.get_secret_value(
                SecretId=secret_name
            )
            # print(get_secret_value_response)
            return get_secret_value_response
        except ClientError as e:
            if e.response['Error']['Code'] == 'DecryptionFailureException':
                # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InternalServiceErrorException':
                # An error occurred on the server side.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InvalidParameterException':
                # You provided an invalid value for a parameter.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InvalidRequestException':
                # You provided a parameter value that is not valid for the current state of the resource.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'ResourceNotFoundException':
                # We can't find the resource that you asked for.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            else:
                # Decrypts secret using the associated KMS CMK.
                # Depending on whether the secret is a string or binary, one of these fields will be populated.
                if 'SecretString' in get_secret_value_response:
                    secret = get_secret_value_response['SecretString']
                else:
                    decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])


    def trans_many(self, string_list_2_trans, source_lang=None):
        if not isinstance(string_list_2_trans, list):
            raise Exception("trans_many() must receive a list ot strings to translate")
        if source_lang:
            trans = self.client.translations().list(source=source_lang,
                                                    target=self.base_lang,
                                                    q=string_list_2_trans).execute()
        else:
            trans = self.client.translations().list(target=self.base_lang,
                                                    q=string_list_2_trans).execute()
        return trans

    def trans_text(self, string_2_trans, source_lang=None):
        try:
            string_2_trans = str(string_2_trans)
            if not isinstance(string_2_trans, str):
                raise Exception("trans_text() must receive a single string to translate, received type is {:s}".format(str(type(string_2_trans))))
            if source_lang:
                trans = self.client.translations().list(source=source_lang,
                                                        target=self.base_lang,
                                                        q=string_2_trans).execute()
            else:
                trans = self.client.translations().list(target=self.base_lang,
                                                        q=string_2_trans).execute()
            return trans
        except Exception:
            print("The string 2 translate is")
            print(string_2_trans)
            print("error: ")
            traceback.print_exc()
            print('\n')
            pass

class Translator(object):
    def __init__(self, src_lang=None, spark=None, limit=None):
        self.translator = GoogleTranslate()
        self.src_lang = src_lang
        self.limit = limit
        if spark:
            self.spark = spark
        else:
            self.spark = SparkSession \
                        .builder \
                        .appName("Python_Spark_SQL_Google_Translate") \
                        .enableHiveSupport() \
                        .getOrCreate()

    def get_text_2_translate(self, spark=None):
        query = """
        SELECT DISTINCT title from (
            SELECT DISTINCT pbp.title 
            FROM stg.pos_bars_products pbp
            WHERE NOT EXISTS (
                SELECT *
                FROM stg.pos_translations pt
                WHERE pt.src_text = pbp.title 
            )
            UNION
            SELECT DISTINCT pbp.pos_category_name as title 
            FROM stg.pos_bars_products pbp
            WHERE NOT EXISTS (
                SELECT *
                FROM stg.pos_translations pt
                WHERE pt.src_text = pbp.pos_category_name 
            )
        )
        """
        if self.src_lang == "ko":
            query += " where title rlike '[ㄱ-ㅎ가-힣]'"
        if self.limit:
            query += " LIMIT {:.0f}".format(self.limit)
        if not spark:
            spark = self.spark
        items_2_trans = spark.sql(query)
        return items_2_trans

    def translate_df(self, df_items_2_trans, spark = None, src_lang=None):
        if not spark:
            spark = self.spark
        trans_broadcast = spark.sparkContext.broadcast(self.translator)
        def translate_text_udf(text):
            return translate_text(text, src_lang, trans_broadcast.value)
        udf_translate_text = func.udf(translate_text_udf, ArrayType(StringType()))
        df_items_2_trans = df_items_2_trans.withColumn('translated_text', udf_translate_text(func.col('title')))\
            .withColumn("src_lang", func.col("translated_text")[1])\
            .withColumn("translated_text", func.col("translated_text")[0])\
            .withColumn("dest_lang", func.lit(self.translator.base_lang))\
            .withColumnRenamed("title", "src_text")
        return df_items_2_trans


    def write_translated_df_2_hive(self, df_2_write, spark=None):
        if not spark:
            spark = self.spark
        current_max_trans_id = int(spark.sql("SELECT MAX(translation_id) as max_trans_id FROM stg.pos_translations").select("max_trans_id").collect()[0]["max_trans_id"])
        # current_max_trans_id = 2
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        df_2_write = df_2_write.withColumn("run_time", func.lit(current_time)).withColumn("translation_id", func.lit(current_max_trans_id) + func.row_number().over(Window.partitionBy("run_time").orderBy("translated_text")))
        # df_2_write.show()
        num_rows = df_2_write.count()
        num_partitions = int(1 + num_rows/10000)
        df_2_write.repartition(num_partitions).registerTempTable("translations_temp")
        table_name = "pos_translations"
        print('save data to table', table_name, 'with columns', df_2_write.columns)
        print('insert into ' + str(table_name))
        query = """INSERT OVERWRITE TABLE stg.{tn}
                SELECT
                translation_id,
                src_text,
                translated_text ,
                src_lang,
                dest_lang,
                run_time
                FROM translations_temp""".format(tn=table_name)
        print('num of rows to insert', num_rows)
        result = spark.sql(query)
        print(str(table_name) + ' table has been updated with result', result)
        return result


    def get_translate_write(self, spark=None):
        if not spark:
            spark = self.spark
        print("Querying data to translate")
        df2translate = self.get_text_2_translate(spark=spark)
        print("Done querying data to translate")
        # df2translate.show()
        print("Translating the data")
        df2translate = self.translate_df(df_items_2_trans=df2translate, spark=spark, src_lang=self.src_lang)
        print("Done translating the data")
        print("Saving the translated data to hive")
        self.write_translated_df_2_hive(df_2_write=df2translate, spark=spark)
        print("Done saving the translated data to hive")




def translate_text(text, src_lang, translator):
    try:
        # print(src_lang)
        translation = translator.trans_text(string_2_trans=text, source_lang=src_lang)
        # print(text + "->" + translation["translations"][0]["translatedText"])
        translated_text = translation["translations"][0]["translatedText"]
        if not src_lang:
            src_lang = translation["translations"][0]["detectedSourceLanguage"]
        return [translated_text, src_lang]
    except Exception as e:
        print("error translating:")
        print(text)
        print("error msg:")
        traceback.print_exc()
        print('\n')
        return ["", src_lang]


