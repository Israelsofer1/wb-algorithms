# coding=utf-8
import datetime as dt
import numpy as np
import itertools, collections, csv, operator
import math
import numbers
import re

from time import time
from bisect import bisect_left
from itertools import combinations

import os
import socket

from gensim.parsing.preprocessing import stem_text, strip_multiple_whitespaces, strip_punctuation

if os.name != "nt":
    import fcntl
    import struct


def get_interface_ip(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s',
                                                                        ifname[:15]))[20:24])


def get_lan_ip():
    ip = socket.gethostbyname(socket.gethostname())
    if ip.startswith("127.") and os.name != "nt":
        interfaces = [
            "eth0",
            "eth1",
            "eth2",
            "wlan0",
            "wlan1",
            "wifi0",
            "ath0",
            "ath1",
            "ppp0",
        ]
        for ifname in interfaces:
            try:
                ip = get_interface_ip(ifname)
                break
            except IOError:
                pass
    return ip


def encode_lod(lod, keys=None):
    """
        Make sure that all values in the lod (which may be of a mixed datatype) are properly encoded
    :param lod: List of dicts
    """
    if not keys:
        keys = lod[0].keys()
    for row in lod:
        for k in keys:
            try:
                row[k] = row[k].encode('utf-8')
            except AttributeError:
                # NoneType object, no problem
                pass


def decode_lod(lod, keys=None, unicode=True):
    """
        Convert incoming data (usually Unicode) to int or date if necessary
    :param lod: List of dicts
    """
    if not keys:
        keys = lod[0].keys()
    if unicode:
        for d in lod:
            for k in keys:
                d[k] = decode(d[k])
    else:
        for d in lod:
            for k in keys:
                try:
                    d[k] = float(d[k])
                    continue
                except (ValueError, TypeError):
                    pass


def decode(val):
    if not val:
        return None
    elif isinstance(val, numbers.Number):
        return float(val)
    elif isinstance(val, str):
        try:  # If date
            return dt.datetime.strptime(val, '%Y-%m-%d %H:%M:%S')
        except (ValueError, TypeError):  # If str
            try:
                return val.decode('utf-8')
            except UnicodeEncodeError:
                return val
    elif isinstance(val, list):
        return [decode(obj) for obj in val]
    elif isinstance(val, tuple):
        return tuple([decode(obj) for obj in val])
    elif isinstance(val, dict):
        return {decode(k): decode(v) for k, v in val.iteritems()}
    else:
        raise Exception


def group_by(lod, by, on, operation='sum'):
    """
    :return: Dict of dicts (gr_by: {agr_on: value}}
    :param lod: List of dicts
    :param by: Colomns we group by
    :param on: Columns we aggregate on
    :param operation: Agregative operation we perform (sum by default)
    """
    dist_cols = set([tuple(r[k] for k in by) for r in lod])
    if operation == 'sum':
        agr_lod = {r: {c: 0 for c in on} for r in dist_cols}
        for row in lod:
            for col in on:
                agr_lod[tuple(row[k] for k in by)][col] += row[col]
    else:
        return ValueError
    return agr_lod


def density_split(data, col, dist):
    """
        Split based on time difference between rows (split where difference too large)
        Assumes list is already ordered by key and date
    :param data: List of dicts
    :param dist: int, min distance between consecutive values
    :param col: str
    :return: List of lists of dicts
    """
    # Save the column differences between all consecutive observations
    dts = (d1[col] - d0[col] for d0, d1 in zip(data[:-1], data[1:]))
    # Save the positions of the large time differences (the ones that devide the groups)
    split_at = [i + 1 for i, dt in enumerate(dts) if dt >= dist]
    # Store the seperate groups in seperate lists
    # So we have a list of lists (groups) of dicts (rows), all are references to data
    groups = [data[i:j] for i, j in zip([0] + split_at, split_at + [None])]
    return groups


def key_split(data, col, sort=True):
    """
        Split based on column values (split when they change)
        Assumes list is already ordered
    :param col: LIST - Names of the columns we monitor change over
    :param data: List of dicts
    :return: A generator of a List of lists of dicts
    """
    # The key-split function requires the data to be pre-sorted on the columns which are to be split
    if sort:
        for c in col:
            data.sort(key=lambda x: x[c])
    # The keys we'll check
    keys = [[d[column] for column in col] for d in data]
    # Save the positions of key changes
    split_at = [i for i, d in enumerate(keys) if keys[i] != keys[i - 1]]
    if split_at:
        # Store the split groups in separate lists
        groups = (data[i:j] for i, j in zip(split_at, split_at[1:] + [None]))
        for gr in groups:
            yield gr
    else:
        yield data


def mark_outliers(data, per, col, lim_mul):
    """
    Add outlier markings to the list and return the outlier limit
    :param data: A sorted list of dicts
    :param per: list of the column values percentiles
    :param col: Column for which we want to find outliers
    :param lim_mul: Multiplier for the outlier lower limit
    :return: float - Outlier limit for the line
    """
    iqr = per[75] - per[25]
    lim = per[75] + iqr * lim_mul * 0.5
    for row in data:
        if row[col] > lim:
            row['outlier'] = 1
        else:
            row['outlier'] = 0
    return lim


def get_percentiles(lst, jump):
    """
    Get the percentiles of deltas
    :param lst: list of deltas
    """
    perc = {}
    for p in range(0, 100 + jump, jump):
        perc[p] = np.percentile(lst, p)
    return perc


def find_closest(lst, num, sort=True):
    """
    Assumes lst is sorted. Returns closest value to num.

    If two numbers are equally close, return the smallest number.
    """
    if sort:
        lst.sort()
    pos = bisect_left(lst, num)
    if pos == 0:
        return lst[0]
    if pos == len(lst):
        return lst[-1]
    before = lst[pos - 1]
    after = lst[pos]
    if after - num < num - before:
        return after
    else:
        return before


def list_combinations(lst, min_combo=1, max_combo=None):
    if not max_combo:
        max_combo = min_combo
    return [comb
            for n in range(min_combo, max_combo + 1)
            for comb in combinations(lst, n)]


def maxd(dct):
    """
        returns the key with the maximal value
    :param dct: List of dicts
    :return:
    """
    return max(dct.iteritems(), key=operator.itemgetter(1))[0]


def maxlot(lot):
    """

    :param lot: List of tuples [(return val, sort value)]
    :return: return val
    """
    scores = [s[1] for s in lot]
    try:
        maxi = scores.index(max(scores))
        return lot[maxi][0]
    except ValueError:
        # Empty list
        return None


def minlot(lot):
    """

    :param lot: List of tuples [(return val, sort value)]
    :return: return val
    """
    scores = [s[1] for s in lot]
    try:
        mini = scores.index(min(scores))
        return lot[mini][0]
    except ValueError:
        # Empty list
        return None


def remove_from_list(lst, indexes, sort=True):
    if sort:
        indexes.sort()
    for i, j in enumerate(indexes):
        del lst[j - i]


def unravel_dict(raw_d):
    """
    Return all keys of a nested dictionary (recursive)
    :param raw_d: Dict which may contain other dicts within it
    :return out: List of all dict keys
    """
    out = []
    for k, v in raw_d.iteritems():
        if isinstance(raw_d[k], dict):
            out += unravel_dict(raw_d[k])
        else:
            out.append((k, v))
    return out


def complete_dict(larger, smaller):
    """
        Add keys to smaller using larger
    :param larger: dict
    :param smaller: dict
    """
    existing_keys = smaller.keys()
    for k in larger.keys():
        if k not in existing_keys:
            smaller[k] = None


def consume(iterator, n):
    """
    :param iterator: Iterator that needs to be advanced
    :param n: Number of steps we need to advance it
    """
    collections.deque(itertools.islice(iterator, n), maxlen=0)


def yesterday_default(time=True, days_back=1, day_interval=1, from_h=20):
    ystrday = dt.date.today() + dt.timedelta(days=-days_back)
    if time:
        base_date = dt.datetime(ystrday.year, ystrday.month, ystrday.day)
        start_date = base_date + dt.timedelta(days=-day_interval) + dt.timedelta(
            hours=from_h)  # 8PM 2 days ago (by default)
        end_date = base_date + dt.timedelta(hours=from_h)  # 8PM yesterday
        return (str(start_date), str(end_date))
    else:
        return (str(ystrday), str(ystrday))


def this_night_default():
    ystrday = dt.date.today() + dt.timedelta(days=-1)
    start_date = dt.datetime(ystrday.year, ystrday.month, ystrday.day) + dt.timedelta(hours=20)  # 8PM yesterday
    end_date = start_date + dt.timedelta(hours=12)  # 8AM today
    return (str(start_date), str(end_date))


def datediff(sdate, days):
    tdate = dt.datetime.strptime(sdate, '%Y-%m-%d %H:%M:%S') + dt.timedelta(days=days)
    return str(tdate)


def date_range(start_date, n_days, day_jump=1):
    """
    :return: List of dates making jumps of n days, x days forward
    """
    return [(str(dt.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S") + dt.timedelta(days=x))
             , str(dt.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S") + dt.timedelta(days=x + day_jump)))
            for x in range(0, n_days / day_jump)]


def day_iter(start_date, end_date):
    for n in range((end_date - start_date).days + 1):
        yield min(start_date + dt.timedelta(n), end_date)


def timedelta_sec(dt1, dt2):
    """
    dt2 > dt1
    :param dt1: datetime
    :param dt2: datetime
    :return: seconds diff
    """
    return (dt2 - dt1).seconds + ((dt2 - dt1).days * 86400)


def distinct_id():
    return (str(hex(int(time() * 10000000)))[2:] + str(np.random.randint(0, 10 ** 8)))


def round_up(x, base=10):
    return int(math.ceil(x / float(base))) * base


def round_down(x, base=10):
    return int(math.floor(x / float(base))) * base


def clean_text(text):
    # return ' '.join(re.split('(\d+)', strip_multiple_whitespaces(strip_punctuation(stem_text(str(text))))))
    return ' '.join(re.split('(\d+)', strip_multiple_whitespaces(stem_text(str(text))))).replace('.', ' ')


def tokenize_combo(text, padding=0):
    pad_token = "<PAD>"
    if text is not None and text != '':
        token = clean_text(text).split()
        # Pad the sentences to a uniform length
        if padding > 0:
            token += [pad_token for _ in range(padding - len(token))]
            token = token[:padding]
    else:
        token = []
    return token

def merge_two_dicts(dict_one, dict_two):
    dict_combine = dict_one.copy()  # start with x's keys and values
    dict_combine.update(dict_two)  # modifies z with y's keys and values & returns None
    return dict_combine


if __name__ == '__main__':
    pass