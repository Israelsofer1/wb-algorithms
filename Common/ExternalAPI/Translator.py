from pyspark.sql import SparkSession
import pyspark.sql.functions as func
from pyspark.sql.window import *
from pyspark.sql.types import StringType, ArrayType
import datetime
import traceback
from .GoogleTranslate import GoogleTranslate

class Translator(object):
    def __init__(self, src_lang=None, spark=None, limit=None, usd_limit=20): #Oren: limit is a limitation of rows whereas usd_limit is the limitation of money usage per day through the Google translate API
        self.translator = GoogleTranslate()
        self.src_lang = src_lang
        self.limit = limit
        self.usd_limit = usd_limit
        if spark:
            self.spark = spark
        else:
            self.spark = SparkSession \
                        .builder \
                        .appName("Python_Spark_SQL_Google_Translate") \
                        .enableHiveSupport() \
                        .getOrCreate()


    def limit_cost(self, _df):
        cost_per_mil_chars = 20 if self.src_lang else 40
        num_chars = int(_df.withColumn("len_ttl", func.length("title")).groupBy().sum("len_ttl").collect()[0][0])
        actual_cost = cost_per_mil_chars * num_chars / 1000000
        if actual_cost > self.usd_limit:
            reduction_rate = float(self.usd_limit / actual_cost) * 0.85
            new_size = int(_df.count() * reduction_rate)
            _df = _df.limit(new_size)
            print("Num of characters in the dataframe exceeds the daily limit. "
                  "limiting the dataframe's by a rate of {:.3f}, to a size of {:d}".format(reduction_rate, new_size))
        return _df


    def get_text_2_translate(self, spark=None):
        query = """
        SELECT DISTINCT title from (
            SELECT DISTINCT pbp.title 
            FROM stg.pos_bars_products pbp
            WHERE NOT EXISTS (
                SELECT *
                FROM stg.pos_translations pt
                WHERE pt.src_text = pbp.title 
            )
            UNION
            SELECT DISTINCT pbp.pos_category_name as title 
            FROM stg.pos_bars_products pbp
            WHERE NOT EXISTS (
                SELECT *
                FROM stg.pos_translations pt
                WHERE pt.src_text = pbp.pos_category_name 
            )
        )
        """
        if self.src_lang == "ko":
            query += " where title rlike '[ㄱ-ㅎ가-힣]'"
        if self.limit:
            query += " LIMIT {:.0f}".format(self.limit)
        if not spark:
            spark = self.spark
        items_2_trans = spark.sql(query)
        if self.usd_limit and not items_2_trans.rdd.isEmpty():
            items_2_trans = self.limit_cost(items_2_trans)
        return items_2_trans


    def translate_df(self, df_items_2_trans, spark = None, src_lang=None):
        if not spark:
            spark = self.spark
        trans_broadcast = spark.sparkContext.broadcast(self.translator)
        def translate_text_udf(text):
            return translate_text(text, src_lang, trans_broadcast.value)
        udf_translate_text = func.udf(translate_text_udf, ArrayType(StringType()))
        df_items_2_trans = df_items_2_trans.withColumn('translated_text', udf_translate_text(func.col('title')))\
            .withColumn("src_lang", func.col("translated_text")[1])\
            .withColumn("translated_text", func.col("translated_text")[0])\
            .withColumn("dest_lang", func.lit(self.translator.base_lang))\
            .withColumnRenamed("title", "src_text")
        return df_items_2_trans


    def write_translated_df_2_hive(self, df_2_write, spark=None):
        if not spark:
            spark = self.spark
        current_max_trans_id = int(spark.sql("SELECT MAX(translation_id) as max_trans_id FROM stg.pos_translations").select("max_trans_id").collect()[0]["max_trans_id"])
        # current_max_trans_id = 2
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
        df_2_write = df_2_write.withColumn("run_time", func.lit(current_time)).withColumn("translation_id", func.lit(current_max_trans_id) + func.row_number().over(Window.partitionBy("run_time").orderBy("translated_text")))
        # df_2_write.show()
        num_rows = df_2_write.count()
        num_partitions = int(1 + num_rows/10000)
        df_2_write.repartition(num_partitions).registerTempTable("translations_temp")
        table_name = "pos_translations"
        print('save data to table', table_name, 'with columns', df_2_write.columns)
        print('insert into ' + str(table_name))
        query = """INSERT OVERWRITE TABLE stg.{tn}
                SELECT
                translation_id,
                src_text,
                translated_text ,
                src_lang,
                dest_lang,
                run_time
                FROM translations_temp""".format(tn=table_name)
        print('num of rows to insert', num_rows)
        result = spark.sql(query)
        print(str(table_name) + ' table has been updated with result', result)
        return result


    def get_translate_write(self, spark=None):
        if not spark:
            spark = self.spark
        print("Querying data to translate")
        df2translate = self.get_text_2_translate(spark=spark)
        print("Done querying data to translate")
        # df2translate.show()
        if df2translate.rdd.isEmpty():
            print("Nothing to translate")
            exit(code=0)
        print("Translating the data")
        df2translate = self.translate_df(df_items_2_trans=df2translate, spark=spark, src_lang=self.src_lang)
        print("Done translating the data")
        print("Saving the translated data to hive")
        self.write_translated_df_2_hive(df_2_write=df2translate, spark=spark)
        print("Done saving the translated data to hive")


def translate_text(text, src_lang, translator):
    try:
        # print(src_lang)
        translation = translator.trans_text(string_2_trans=text, source_lang=src_lang)
        # print(text + "->" + translation["translations"][0]["translatedText"])
        translated_text = translation["translations"][0]["translatedText"]
        if not src_lang:
            src_lang = translation["translations"][0]["detectedSourceLanguage"]
        return [translated_text, src_lang]
    except Exception as e:
        print("error translating:")
        print(text)
        print("error msg:")
        traceback.print_exc()
        print('\n')
        return ["", src_lang]


