import logging as log
import boto3, time, json, string
import pandas as pd
import numpy as np
import csv
from datetime import datetime

from sagemaker.transformer import Transformer
from sagemaker.mxnet import MXNet
ec2 = boto3.client('ec2')

import Common.data_access_config as dac


class SMWrap(object):
    def __init__(self):
        self.client = boto3.client('sagemaker')
        self.client_run = boto3.client('sagemaker-runtime')
        self.f_type_mime = {
            'npy': 'application/octet-stream',
            'hdf5': 'application/x-hdf5',
            'csv': 'text/csv',
            'pickle': 'application/octet-stream',
            'json': 'application/json'
        }
        self.default_image = "520713654638.dkr.ecr.eu-west-1.amazonaws.com/sagemaker-mxnet:1.2-gpu-py2"
        self.default_role = "arn:aws:iam::287168362162:role/service-role/AmazonSageMaker-ExecutionRole-20180416T110432"

    def name_constraints(self, name):
        restricted = string.punctuation.replace('-', '')
        table = str.maketrans({key: None for key in restricted})
        return name.translate(table)

    def job_name(self, mname, version):
        cur_time = time.strftime("%Y-%m-%d-%H-%M-%S")
        return self.name_constraints('{n}-{v}-{t}'.format(n=mname, v=version, t=cur_time))

    def model_name(self, mname, version):
        cur_time = time.strftime("%Y-%m-%d")
        return self.name_constraints('{n}-{v}-{t}'.format(n=mname, v=version, t=cur_time))

    def trans_tags(self, tags):
        """
        Transform to format excepted by AWS
        :param tags: dict {key: value, ...}
        :return: list [[{'Key': key, 'Value': value}], ...]
        """
        if tags is not None and len(tags) > 0:
            return [{'Key': k, 'Value': v} for k, v in tags.items()]
        else:
            return []

    def train(self, mname, version='1', **kwargs):
        """
        :param mname: Name of the model to train (0, 1, 2, 3...) needs to corespond to training data name
        :param ftype: npy, hdf5 or csv
        :param inp_adrs: Folder in bucket s3://wb-ds-models/
        :param out_adrs: Folder in bucket s3://wb-ds-models/
        :param instance: 'ml.m4.xlarge OR 'ml.p2.xlarge
        :param instance_num: int (1, 2..)
        :param ram: int (8, 16, 32..)
        :param learning_rate: float (under 1)
        :param backend_code: Code address for the code in Sagemaker Jupyter dir
        :return:
        """
        image = kwargs.get("image", self.default_image)
        role = kwargs.get("role", self.default_role)
        jname = self.job_name(mname, version)

        algo = {
            "TrainingImage": image,
            "TrainingInputMode": "File"
        }
        input = [
            {
                'ChannelName': 'data',
                'DataSource': {
                    'S3DataSource': {
                        'S3DataType': 'S3Prefix',
                        'S3Uri': 's3://wb-ds-models/{a}'.format(a=kwargs.get("inp_adrs", 'pos_training')),
                        'S3DataDistributionType': 'FullyReplicated'
                    }
                },
                'ContentType': self.f_type_mime[kwargs.get("ftype", 'hdf5')]
            },
        ]
        output = {'S3OutputPath': 's3://wb-ds-models/{a}'.format(a=kwargs.get("out_adrs", 'artifacts'))}
        rsrc = {
            'InstanceType': kwargs.get("instance", 'ml.m4.xlarge'),
            'InstanceCount': kwargs.get("instance_num", 1),
            'VolumeSizeInGB': kwargs.get("ram", 32)
        }
        stop = {'MaxRuntimeInSeconds': (60 * 60 * 24)}

        # kwargs.update(
        #     {
        #         'mname': mname,
        #         'version': version,
        #         'sagemaker_job_name': jname,
        #         'training_image': image,
        #         'role': role,
        #
        #         'learning_rate': str(kwargs.get("learning_rate", 0.01)),
        #         'limit': str(kwargs.get("limit", 10**7)),
        #         'sagemaker_program': kwargs.get("backend_code", 'mxnet_conv.py'),
        #     }
        # )
        # for k in ['batch', 'fold', 'epochs']:
        #     if k in kwargs:
        #         kwargs[k] = str(kwargs[k])
        # meta = kwargs

        meta = {
            'learning_rate': str(kwargs.get("learning_rate", 0.01)),
            'limit': str(kwargs.get("limit", 10**7)),
            'mname': mname,
            'version': version,
            'sagemaker_job_name': jname,
            'sagemaker_program': kwargs.get("backend_code", 'mxnet_conv.py'),
            'training_image': image,
            'role': role
        }

        response = self.client.create_training_job(TrainingJobName=jname, AlgorithmSpecification=algo,
                                                              RoleArn=role, InputDataConfig=input,
                                                              OutputDataConfig=output, ResourceConfig=rsrc,
                                                              StoppingCondition=stop, HyperParameters=meta)
        return jname

    def get_training_jobs(self, training_job_name):
        """

        :param training_job_name: str
        :param status: 'InProgress' | 'Completed' | 'Failed' | 'Stopping' | 'Stopped'
        :return:
        """
        response = self.client.list_training_jobs(
            MaxResults=100,
            CreationTimeAfter=datetime(2015, 1, 1),
            NameContains=training_job_name
        )
        return response

    def create_model(self, training_job_name, mname, version, image=None, role=None):
        """

        NOTE: At the end of MXNet train there is an automatic model creation process so no need to run this again

        :param training_job_name: The name of the Training job on which we base the model (not the ARN)
        :param mname: The name of the model as specified in training
        :param version: model version
        :param image:
        :param role:
        :return: dict ModelArn
        """
        if image is None:
            image = self.default_image
        if role is None:
            role = self.default_role

        info = self.client.describe_training_job(TrainingJobName=training_job_name)
        model_data = info['ModelArtifacts']['S3ModelArtifacts']
        primary_container = {
            'Image': image,
            'ModelDataUrl': model_data
        }
        responce = self.client.create_model(
            ModelName=self.model_name(mname=mname, version=version),
            ExecutionRoleArn=role,
            PrimaryContainer=primary_container)
        return responce

    def create_endpoint(self, mname, version, instance='ml.m4.4xlarge', tags=None):
        """
        Creates the endpoint config and the endpoint
        :param mname: The name of the model created in create_model (the Amazon name not custom and not ARN)
        :param version: model version
        :param instance:
        :param tags:
        :return: The ARN of the endpoint
        """
        job_name = self.job_name(mname, version)
        response = self.client.create_endpoint_config(EndpointConfigName=job_name + '-config',
                                                        ProductionVariants=[
                                                            {
                                                                'VariantName': job_name,
                                                                'ModelName': mname,
                                                                'InitialInstanceCount': 1,
                                                                'InstanceType': instance
                                                                #, 'AcceleratorType': 'ml.eia1.medium' | 'ml.eia1.large' | 'ml.eia1.xlarge'
                                                            },
                                                        ],
                                                        Tags=[
                                                            {'Key': 'model', 'Value': str(mname)},
                                                            {'Key': 'version', 'Value': str(version)},
                                                            {'Key': 'date', 'Value': time.strftime("%Y-%m-%d-%H-%M-%S")}
                                                        ] + self.trans_tags(tags)
                                                        #, KmsKeyId='string'
                                                      )
        print(response)

        response = self.client.create_endpoint(EndpointName=job_name,
                                                EndpointConfigName=job_name + '-config',
                                                Tags=[
                                                         {'Key': 'model', 'Value': str(mname)},
                                                         {'Key': 'version', 'Value': str(version)},
                                                         {'Key': 'date', 'Value': time.strftime("%Y-%m-%d-%H-%M-%S")}
                                                     ] + self.trans_tags(tags)
                                                )
        return response

    def body_prep(self, data, dformat='csv'):
        """
        Prep the data to be sent for prediction in SageMaker
        :param data: The data
        :param dformat: (str) csv, json, parquet
        :return: body ready to be sent
        """
        if dformat == 'json':
            data = {k: v.tolist() if isinstance(v, np.ndarray) else v for k, v in data.items()}
            return json.dumps(data)
        elif dformat == 'csv':
            data = pd.DataFrame(data)
            return json.dumps({k: data[k] for k in data.columns})
        elif dformat == 'parquet':
            return json.dumps({k: data[k] for k in data.columns})
        else:
            raise BaseException('Unsupported file type. Use: csv, json or parquet')

    def transform_ep(self, endpoint_name, body, accept='json'):
        """
        :return: list of lists of lists
                feature(rows(values())) can also be represented as [f, r, v]
        """
        res = self.client_run.invoke_endpoint(EndpointName=endpoint_name,
                                              Body=body,
                                              Accept=self.f_type_mime[accept]
                                              )
        return json.loads(res['Body'].read().decode())

    def transform_s3(self, mname, inp_adr, out_adr):
        # Initialize the transformer object
        transformer = Transformer(
            base_transform_job_name='Batch-Transform',
            model_name=mname,
            strategy='MultiRecord',
            instance_count=1,
            instance_type='ml.c4.xlarge',
            output_path=out_adr,
            max_payload=100
        )
        # To start a transform job:
        transformer.transform(inp_adr, content_type='application/json', compression_type='Gzip')
        # Then wait until transform job is completed
        transformer.wait()


if __name__ == '__main__':

    s3 = SMWrap()
    # s3.train('should', 'crash')

    s3.transform_s3(
        mname='sagemaker-mxnet-2019-01-01-10-32-41-826',
        inp_adr='s3://dev-matching-steps/beer/0.1/Match/test_pre_process/pre_process_NN.gzip/part-00000-343f58ee-c8f6-4a7b-a245-529259ae3276-c000.json.gz',
        out_adr='s3://wb-ds-models/model_versioning/beer/0.1/results'
    )

    # test_dump = json.dumps({'title': [[52, 17025, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    #                                   [52, 17025, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]],
    #                         'country_id': [[0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    #                                        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]],
    #                         'price': [[0.64888], [0.64888]]})
    #
    # res = s3.transform(endpoint_name='sagemaker-mxnet-2018-12-03-08-34-12-100', body=test_dump)
    # print(res)

    # s3.get_training_jobs('sagemaker-mxnet-2018-12-03-12-40-49-826')

    # s3.create_model(training_job_name='sagemaker-mxnet-2018-12-20-08-06-54-245', mname='beer-test', version='0.1')
    # s3.create_endpoint(mname='beer-test-01-2018-12-20', version='0.1')