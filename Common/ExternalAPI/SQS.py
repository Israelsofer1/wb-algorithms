import boto3
import json
import logging as log

from time import ctime


class SQSWrap(object):
    def __init__(self, queue_name=None):
        """
        Create a resource instance which represents Amazon Simple Queue Service (SQS)

        :param queue_name: Queue name of queue to connect to (Optional), able to set queue later with set_queue
        """
        self.sqs = boto3.resource('sqs', region_name='eu-west-1')
        if queue_name is not None:
            self.set_queue(queue_name)
        else:
            self.queue = None

    def show_all_queues_urls(self):
        """
        :return: Prints out all SQS queues
        """
        for queue in self.sqs.queues.all():
            log.info(queue.url)

    def set_queue(self, queue_name):
        """
        Retrieve SQS Queue using specified queue name and assign it to instance variable queue

        :param queue_name: Queue name of SQS queue
        """
        self.queue = self.sqs.get_queue_by_name(QueueName=queue_name)

    def bpid_to_queue(self, bpid, message_group_id, show_response=False):
        """
        Sends a single bar product id (bpid) to queue

        :param bpid: Bar product id
        :param message_group_id: Tag that specifies that a message belongs to a specific message group;
            MessageGroupId is required for FIFO queues
        :param show_response: Print out queue responses

        :return: Queue responses from message sent to queue
        """
        response = self.queue.send_message(MessageBody=str(bpid), MessageGroupId=message_group_id)
        if show_response:
            log.info(' '.join(['Message sent to SQS Queue -', str(self.queue.url), ':', str(ctime())]))
            log.info(response)
        return response

    def bpids_batch_to_queue(self, bpids_list, message_group_id, show_responses=False, batch_size=10):
        """
        Sends several bar product ids (bpids) to queue (Up to 10 per batch)

        :param bpids_list: List of bar product ids
        :param message_group_id: Tag that specifies that messages belongs to a specific message group;
            MessageGroupId is required for FIFO queues
        :param show_responses: Print out queue responses
        :param batch_size: How many messages (bpids) to send in a batch (Up to 10 per batch)

        :return: Dictionary of successful and failed messages
        """
        counter = 0
        while counter < len(bpids_list):
            if len(bpids_list) < counter + batch_size:
                bpids = bpids_list[counter: len(bpids_list)]
            else:
                bpids = bpids_list[counter: counter + batch_size]
            entries = [{'Id': str(bpid), 'MessageBody': str(bpid), 'MessageGroupId': message_group_id}
                       for bpid in bpids]
            responses = self.queue.send_messages(Entries=entries)
            counter += batch_size
            if show_responses:
                log.info('Messages sent to SQS Queue -', self.queue.url, ':', ctime())
                log.info(responses)
            return responses

    def dict_to_queue(self, dictionary, message_group_id, show_response=False):
        """
        Sends dictionary data to SQS Queue

        :param dictionary: Dictionary to send to SQS Queue
        :param message_group_id: Tag that specifies that a message belongs to a specific message group;
            MessageGroupId is required for FIFO queues
        :param show_response: Print out queue response

        :return: Queue response from message sent to queue
        """
        response = self.queue.send_message(MessageBody=str(json.dumps(dictionary)), MessageGroupId=message_group_id)
        if show_response:
            log.info(' '.join(['Message sent to SQS Queue -', str(self.queue.url), ':', str(ctime())]))
            log.info(response)
        return response

    def dicts_to_queue(self, lod, message_group_id, show_response=False):
        """
        Sends list of dictionaries to SQS Queue

        :param lod: List of dictionaries to send to SQS Queue
        :param message_group_id: Tag that specifies that messages belongs to a specific message group;
            MessageGroupId is required for FIFO queues
        :param show_response: Print out queue responses

        :return: List of queue responses from messages sent to queue
        """
        queue_responses = []
        for dictionary in lod:
            response = self.dict_to_queue(dictionary=dictionary, message_group_id=message_group_id,
                                          show_response=show_response)
            queue_responses.append(response)
        return queue_responses

    def receive_messages(self, num_messages=10, attribute_list=None,
                         show_message_details=False, show_message_content=False):
        """
        Receive messages sent to SQS queue

        :param num_messages: Number of messages to receive from queue
        :param attribute_list: Attributes to show for received message(s)
            http://boto3.readthedocs.io/en/latest/reference/services/sqs.html#SQS.Queue.receive_messages
        :param show_message_details: Print out message details (MessageId and ReceiptHandle)
        :param show_message_content: Print out message(s) content

        :return: List of messages received from SQS queue
        """
        if attribute_list is None:
            attribute_list = []
        messages_list = self.queue.receive_messages(MaxNumberOfMessages=num_messages, AttributeNames=attribute_list)
        if show_message_details:
            for message in messages_list:
                if show_message_content:
                    log.info('MessageId (Id):', message.message_id, '\nReceiptHandle:', message.receipt_handle,
                          '\nMessage Content:', message.body, '\n')
                else:
                    log.info('MessageId (Id):', message.message_id, '\nReceiptHandle:', message.receipt_handle, '\n')
        return messages_list

    def extract_messages_body(self, messages_list):
        """
        Extract message(s) body and store in a list

        :param messages_list: List of messages received from SQS queue [Output of receive_messages]

        :return: List of message(s) body
        """
        return [eval(message.body) for message in messages_list]

    def delete_messages(self, message_list=None, all_messages=False, show_response=True):
        """
        Delete messages from list of messages OR delete all messages from SQS queue
        :param message_list: List of messages received from SQS queue [Output of receive_messages]
        :param all_messages: Purge queue (Remove all messages) if True
        :param show_response: Print out response from delete_messages function
        :return: Message(s) deletion response if message_list is provided
        """
        if all_messages:
            self.queue.purge()
            if show_response:
                log.info('Queue purged', '\n')
        elif message_list:
            receipt_handles_lod = []
            for message in message_list:
                receipt_handles_lod.append({'Id': message.message_id, 'ReceiptHandle': message.receipt_handle})
            response = self.queue.delete_messages(Entries=receipt_handles_lod)
            if show_response:
                log.info('Message Deletion Response:', response, '\n')
            return response
        else:
            if show_response:
                log.warning('Deleted nothing, provided neither messages list to delete nor all_messages is set to True')


