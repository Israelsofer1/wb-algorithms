from Common.Infra import fuzzy_matching


import difflib
import itertools as it
import string
import time
import pandas as pd





def matcher(word_a,word_b):
    seq = difflib.SequenceMatcher(None,word_a,word_b)
    matching_rez = seq.ratio()*100
    return(matching_rez)

#take csv and return df
def csv_reading_df(filename):
    data = pd.read_csv(filename,encoding = "utf-8")
    return(data)

#take filename , df and create csv from this df under filename 'namne'
def writing_to_csv(filename,df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return("Done")

#take dataframe series by column_name and return clean df
def data_cleaner(df,column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation), '')
    df[column_name]= df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return(df)




def michal_df_title_matcher(doshi_name,abi_df, name_column ,top_x_superbrands):
    temp_df = abi_df
    temp_df['matching_res'] = temp_df[name_column].apply(lambda abi_name: matcher(abi_name,doshi_name))
    temp_df = temp_df.sort_values('matching_res',ascending=False)
    temp_df= temp_df.head(n=top_x_superbrands)
    df_to_tuples = [tuple(x) for x in temp_df.values]
    return(df_to_tuples)


abi_df = csv_reading_df('abi.csv')
doshi_df = csv_reading_df('doshi.csv')

abi_df_clean = data_cleaner(abi_df,'street')
doshi_df_clean = data_cleaner(doshi_df,'street')


doshi_df_clean['best_match'] = doshi_df_clean.street.apply(lambda doshi_name: michal_df_title_matcher(doshi_name,abi_df_clean,'street',6))

writing_to_csv('doshi_abi_street',doshi_df_clean)

