import difflib
import itertools as it
import string
import time
import pandas as pd
########################################################################################################################

#takes string and returns clean shape lowercase of him
def string_cleaner(str):
    exclude = set(string.punctuation)
    str_after_clean = ''.join(ch for ch in str if ch not in exclude)
    str_after_clean = str_after_clean.lower()
    return(str_after_clean)


#takes 2 strings and return similarity grade
def matcher(word_a,word_b):
    seq = difflib.SequenceMatcher(None,word_a,word_b)
    matching_rez = seq.ratio()*100
    return(matching_rez)

#take csv and return df
def csv_reading_df(filename):
    data = pd.read_csv(filename,encoding = "utf-8")
    return(data)

#take filename , df and create csv from this df under filename 'namne'
def writing_to_csv(filename,df):
    df.to_csv(filename, sep='\t', encoding='utf-8')
    return("Done")

#take dataframe series by column_name and return clean df
def data_cleaner(df,column_name):
    df[column_name] = df[column_name].str.replace('[{}]'.format(string.punctuation+"0123456789"), '')
    df[column_name]= df[column_name].str.lower()
    df = df.dropna(subset=[column_name])
    return(df)


#this code take 2 titles, comapre words within them and return the highest similarity grade among.
def word_matcher(product,brand):
    grades_list = []
    splitted_product = product.split()
    splitted_brand = brand.split()
    united_list = splitted_brand+splitted_product
    combinations = it.combinations(united_list,2)
    for tuple in combinations:
        grade = matcher(tuple[0],tuple[1])
        grades_list.append(grade)
    grades_list.sort(reverse = True)
    return(grades_list[0])

#main word matcher#
def main_fuzzy_word_matcher(product,brand):
    product_after_clean = string_cleaner(product)
    brand_after_clean = string_cleaner(brand)
    return(word_matcher(product_after_clean,brand_after_clean))


#example#
#ex_brand = 'stella aRtios '
#ex_prod = 'STL 20ozDraught'
#print(main_fuzzy_word_matcher(ex_brand,ex_prod))

##########################################################################################################################


#this code take 2 titles, comapre them as they are and return similarity grade.
def title_matcher(product_title,brand_title):
    grade = matcher(product_title,brand_title)
    return(grade)

#main title matcher#
def main_fuzzy_title_matcher(product,brand):
    product_after_clean = string_cleaner(product)
    brand_after_clean = string_cleaner(brand)
    return(title_matcher(product_after_clean,brand_after_clean))

#example#
#ex_brand = 'stella aRtios '
#ex_prod = 'STL 20ozDraught'
#print(main_fuzzy_title_matcher(ex_brand,ex_prod))

###########################################################################################################################

#this code takes 2 df (df of products and df of brands),for each product check grade similarity with every brand and return the top X brands with the highest grade

def df_title_matcher(product_title,df_of_brands,id_column, name_column ,top_x_superbrands):
    temp_df = df_of_brands
    temp_df['matching_res'] = temp_df[name_column].apply(lambda brand: matcher(brand,product_title))
    temp_df = temp_df.sort_values('matching_res',ascending=False)
    temp_df= temp_df.head(n=top_x_superbrands)
    temp_df = temp_df[[id_column,name_column]]
    df_to_tuples = [tuple(x) for x in temp_df.values]
    return(df_to_tuples)

#main fuzzy_df_matcher#
def main_fuzzy_df_matcher(products_filename,brands_filename,name_of_product_column,name_of_brands_column,id_of_brands_column,top_x_superbrands=3):
    pd.options.mode.chained_assignment = None
    start_time = time.time()
    df_of_products = csv_reading_df(products_filename)
    df_of_brands = csv_reading_df(brands_filename)
    df_of_products_after_clean = data_cleaner(df_of_products,name_of_product_column)
    df_of_brands_after_clean = data_cleaner(df_of_brands,name_of_brands_column)
    df_of_products_after_clean['fuzzy_result'] = df_of_products_after_clean[name_of_product_column].apply(lambda product_title: df_title_matcher(product_title, df_of_brands_after_clean, id_of_brands_column, name_of_brands_column, top_x_superbrands))
    writing_to_csv('main_fuzzy_df_matcher_result', df_of_products_after_clean)
    print("this program executed in %s seconds " % (time.time() - start_time))
    return()

#example#
#print(main_fuzzy_df_matcher('ex_products.csv','ex_brands.csv','title','title','id'))



###########################################################################################################################

#this code takes 2 df (df of products and df of brands),for each product check grade similarity with every brand by single word and return the top X brands with the highest grade

def df_word_matcher(product_title,df_of_brands,id_column, name_column ,top_x_superbrands):
    temp_df = df_of_brands
    temp_df['matching_res'] = temp_df[name_column].apply(lambda brand: word_matcher(brand,product_title))
    temp_df = temp_df.sort_values('matching_res',ascending=False)
    temp_df= temp_df.head(n=top_x_superbrands)
    temp_df = temp_df[[id_column,name_column]]
    df_to_tuples = [tuple(x) for x in temp_df.values]
    return(df_to_tuples)

def main_fuzzy_df_word_matcher(products_filename,brands_filename,name_of_product_column,name_of_brands_column,id_of_brands_column,top_x_superbrands=3):
    pd.options.mode.chained_assignment = None
    start_time = time.time()
    df_of_products = csv_reading_df(products_filename)
    df_of_brands = csv_reading_df(brands_filename)
    df_of_products_after_clean = data_cleaner(df_of_products,name_of_product_column)
    df_of_brands_after_clean = data_cleaner(df_of_brands,name_of_brands_column)
    df_of_products_after_clean['fuzzy_result'] = df_of_products_after_clean[name_of_product_column].apply(lambda product_title: df_word_matcher(product_title, df_of_brands_after_clean, id_of_brands_column, name_of_brands_column, top_x_superbrands))
    writing_to_csv('main_fuzzy_df_matcher_result', df_of_products_after_clean)
    print("this program executed in %s seconds " % (time.time() - start_time))
    return()


#example#
#print(main_fuzzy_df_word_matcher('ex_products.csv','ex_brands.csv','title','title','id'))