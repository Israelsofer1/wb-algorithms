import re


class Map(object):

    def __init__(self):
        # Map table abbreviations
        self.tbl_abr = {
            None: 'on',
            'pos_bars_products': 'bp',
            'pos_products_brands': 'pr',
            'pos_products': 'o',
            'bars': 'a',
            'brands': 'br',
            'pos_products_to_modifiers': 'ppm',
            'pos_bar_modifiers': 'pbm',
            'pos_category_suggestions': 'm',
            'pos_matching_suggestions': 'm2',
            'pos_beer_suggestions': 'bs'
        }
        # Map the column names in output, grouped by table abbreviation
        self.name_col = {
            'bar_product_id': 'bp.id',
            'bar_id': 'a.id',
            'country_id': 'a.country',
            'modifier_id': 'COALESCE(ppm.modifier_id, 0)',
            'title_long': 'concat(COALESCE(bp.pos_category_name,'')," ",COALESCE(bp.title,'')," ",COALESCE(pbm.title,""))',
            'title': 'bp.title',
            'price': 'bp.price',
            'units_in_pack': 'COALESCE(bp.units_in_pack,1)'
        }
        self.abr_tbl = {v: k for k, v in self.tbl_abr.items()}
        self.col_name = {v: k for k, v in self.name_col.items()}
        # Abbrevations must be of 1 <= len <= 3
        abr_len = [len(k) for k in self.abr_tbl.keys()]
        if max(abr_len) > 3:
            raise Exception("There can't be a table with an abreviation longer than 3 chars!")

    # def get_abr(self, st):
    #     return re.findall('[a-z]{1,3}\.', st)
    #
    # def get_req_tables(self, lst):
    #     # Extract all the table prefixes
    #     return set([r[:-1] for s in lst for r in self.get_abr(st=self.name_col[s])])
    #     # return [self.abr_tbl[a] for a in table_abr]


if __name__ == '__main__':
    mpr = Map()
    cols = ['country_id', 'title_simple', 'price']
    mpr.get_req_tables(cols)
