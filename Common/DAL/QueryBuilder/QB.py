from Common.DAL.DataLoader.Mapper import Map
from Common.DAL.Connectors.SQL import DbExplorer
import re


class SQLBuilder(object):
    def __init__(self):
        self.mp = Map()

        self.qs = {
            'pos_matcher': """
            SELECT  {s}
            FROM	pos_bars_products bp
                    LEFT JOIN
                    bars a ON a.id = bp.bar_id
                    LEFT JOIN
                    pos_category_suggestions m on bp.id = m.bar_product_id
                    LEFT JOIN
                    pos_matching_suggestions m2 on bp.id = m2.bar_product_id
                    LEFT JOIN
                    pos_beer_suggestions bs on bp.id = bs.bar_product_id
                    LEFT JOIN
                    pos_products_brands pr ON bp.product_id = pr.product_id
                    LEFT JOIN
                    pos_products o ON o.id = pr.product_id
                    LEFT JOIN
                    pos_products_to_modifiers ppm on ppm.bar_product_id = bp.id
                    LEFT JOIN
                    pos_bar_modifiers pbm on pbm.id = ppm.modifier_id
            {w}
            ORDER BY bp.inserted_at asc
            """
        }
        # The filters are built in such a way that they can be formatted (add more info)
        # There is always only 1 place for formatting and it's marked with an {s}
        self.formats = {'list': form_lst, 'bool': form_boo, 'str': form_str, 'date': form_dt}
        self.filters = {
            'untagged_products': {
                'q':
                """{s} (m.bar_product_id is null
                    and m2.bar_product_id is null
                    and bs.bar_product_id is null
                    and (bp.category_id = 0 or bp.category_id is Null))
                """,
                'format': 'bool'
            },
            'bar_ids': {
                'q':
                """
                    a.bar_id IN({s})
                """,
                'format': 'list'
            }
        }

    def __clean_qstr(self, s):
        """
        Prepare the Q string for processing
        """
        return s.lower().replace('\n', ' ').replace('\r', ' ').replace('\t', ' ').replace('from', ' ').strip()

    def __get_abr(self, st):
        return [s[:-1] for s in re.findall('[a-z]{1,3}\.', st)]

    def __find_tbls_f(self, sfrom):
        """
        Find the table abbreviations in the from string
        :param sfrom: String of the FROM part of the Q
        :return: List of unique table abbreviations
        """
        sfrom = self.__clean_qstr(sfrom)
        # table_gr = sfrom.split('join')
        # table_names = [tg.strip().split(' ')[0] for tg in table_gr]
        table_names = re.findall(' [a-z]{1,3} ', sfrom)
        return list(set([s.strip() for s in table_names]))

    def __find_tbls_w(self, swhere):
        """
        Find the table abrevations in the where string
        :param swhere: String of the WHERE part of the Q
        :return: List of unique table abrevations
        """
        sfrom = self.__clean_qstr(swhere)
        table_gr = self.__get_abr(sfrom)
        # table_names = [tg.split(' ')[-1] for tg in table_gr[:-1]]
        return list(set(table_gr))

    def __find_tbls_s(self, lst):
        # Extract all the table prefixes
        return [r for s in lst for r in self.__get_abr(st=self.mp.name_col[s])]

    def check_col_compatability(self, cols, sfrom, swhere):
        """
        Check for compatability of SELECT and WHERE fields to FROM
        :param cols: list of str
        :param sfrom: str
        :param swhere: str
        :return:
        """
        s_tables = self.__find_tbls_s(lst=cols)
        q_tables = self.__find_tbls_f(sfrom=sfrom)
        w_tables = self.__find_tbls_w(swhere=swhere)
        if not set(s_tables) <= set(q_tables):
            raise Exception("SELECT field doesn't match the tables in the FROM field")
        elif None in s_tables:
            raise Exception("SELECT field refers to unsupported tables (unknown abbreviations)")
        if not set(w_tables) <= set(q_tables):
            raise Exception("WHERE field doesn't match the tables in the FROM field")
        else:
            return True

    def get_s(self, lst):
        """

        :param lst: A list of columns we want to select
        :return:
        """
        return ', '.join(['%s as %s' % (self.mp.name_col[s], s) for s in lst])

    def get_w(self, filters):
        if filters is not None:
            return 'and '.join([self.filters[fn]['q'].format(s=self.formats[self.filters[fn]['format']](inp))
                                for fn, inp in filters.items()])
        else:
            return ''

    def constructor(self, cols, q_name, filters=None, limit=None):
        """
        Query constructor
        :param cols: list of str
        :param filters: dict {filter_name: filter_condition, filter_name: ....}
        :param q_name: str
        :param limit: int
        """
        s = self.get_s(lst=cols)
        q = self.qs[q_name]
        w = self.get_w(filters)
        if self.check_col_compatability(cols, sfrom=q, swhere=w):
            if limit is not None:
                return q.format(s=s, w=w) + ' LIMIT %i' % limit
            return q.format(s=s, w=w)


def form_boo(val):
    if isinstance(val, list):
        if not val:
            return 'Not'
        else:
            return ''
    elif isinstance(val, str):
        return val
    else:
        print(val)
        raise Exception('This filter input should be a bool (TRUE/FALSE) or string of a bool')


def form_lst(val):
    if isinstance(val, list):
        return ', '.join([str(v) for v in val])
    elif isinstance(val, str):
        return val
    else:
        print(val)
        raise Exception('This filter input should be a list or string of a list')


def form_dt(val):
    pass


def form_str(val):
    if isinstance(val, str):
        return val
    else:
        print(val)
        raise Exception('This filter input should be a string')


if __name__ == '__main__':
    bl = SQLBuilder()
    q = bl.constructor(cols=['title', 'price'], filters={'bar_ids': [156, 12]}, q_name='pos_matcher')
    with DbExplorer('PROD') as db:
        db.dbreader(q)
