import logging as log

import pymongo as pm

import Common.data_access_config as dac


class MongoExplorer(object):
    def __init__(self, client, db_name=u"shared", collection_name=u"posts"):
        """
        Connect to the Mongo cluster
        :param client: DEV, STAGE, PROD
        :param db_name: DB-Collection-Documments
        :param collection_name: DB-Collection-Documments
        """
        config = dac.read_config()
        server = config['Mongo']['con'][client]
        self.env = client
        self.client = pm.MongoClient(server)
        self.set_db(db_name)
        self.set_collection(collection_name)
        log.info('Mongo - Opened concetion in %s' % self.env)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.client.close()

    def __drop_db(self, db_name, reset=True):
        self.client.drop_database(db_name)
        log.warning('Mongo - dropped database in %s' % self.env)
        if reset:
            log.info('Mongo - Recreated the database' % self.env)
            self.set_db(db_name)

    def set_db(self, db_name):
        self.dbname = db_name
        log.info('Mongo - Changed DB to %s' % db_name)
        self.db = self.client[db_name]

    def set_collection(self, collection, db_name=None):
        if db_name:
            self.set_db(db_name)
        log.info('Mongo - Changed collection to %s' % db_name)
        self.collection = self.client[self.dbname][collection]

    def index_collections(self, fields):
        # Go over all collections in the DB
        cols = self.client[self.dbname].collection_names()
        # Create a compound index using the provided keys
        log.warning('Mongo - Creating new indexes for %s' % str(cols))
        for col in cols:
            self.client[self.dbname][col].create_index([(f, pm.ASCENDING) for f in fields])
        log.info("Mongo - Indexed All")

    def find(self, q_dct, limit=None):
        """
            Returns a list, limited by "lim_num"
        """
        if limit is None:
            out = list(self.collection.find(q_dct))
        else:
            out = list(self.collection.find(q_dct).limit(limit))
        log.info('Mongo - Returned %i documents' % len(out))
        return out

    def find_generator(self, q_dct):
        """
            Generator
            Returns a dict
        """
        for r in self.collection.find(q_dct):
            yield r

    def find_batcher(self, q_dct, batch_size):
        """
            Generator
            Returns a list with len "batch_size"
        """
        # The total number of documents returned
        pipe = [
            {"$match": q_dct},
            {"$group": {"_id": 0, "count": {"$sum": 1}}}
        ]
        doc_num = list(self.agregate(pipeline=pipe))[0]['count']
        # Iterate over the documents in batches, yield 1 batch at a time
        for bn in range(batch_size, doc_num, batch_size):
            yield list(self.collection.find(q_dct)[(bn - batch_size):bn])

    def insert_one(self, q_dct):
        return self.collection.insert_one(q_dct).inserted_id

    def insert_many(self, lod):
        log.info('Mongo - Inserting %i items to %s' % (len(lod), self.collection))
        return self.collection.insert_many(lod)

    # def increment(self, oid, field, num=1):
    #     return self.collection.update({'_id': oid}, {'$inc': {field: num}})
    #
    # def increment1(self, oid, field):
    #     # For backwards compatability
    #     return self.collection.update({'_id': oid}, {'$inc': {field: 1}})

    def update(self, oid, field, val):
        return self.collection.update({'_id': oid}, {'$set': {field: val}})

    def agregate(self, pipeline):
        return self.collection.aggregate(pipeline=pipeline)

