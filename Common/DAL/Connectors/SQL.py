import datetime as dt
import logging as log
import numbers
import pandas as pd
from time import sleep
import pymysql as mq

#import MySQLdb as mq

import Common.data_access_config as dac

class DbExplorer(object):
    def __init__(self, host, algo_key='POS'):
        config = dac.read_config()
        cred = config['SQL']
        self.iparam = [host, algo_key]
        self.hostkey = host
        self.dbkey = "weissserver"
        self.connection = mq.connect(host=cred['con'][host],  # your host, usually localhost
                                     user=cred['user'][algo_key],  # your username
                                     passwd=cred['password'][algo_key],  # your password
                                     db="weissserver",  # name of the data base
                                     charset="utf8")
        self.cur = self.connection.cursor()
        log.info('SQL - Opened conncetion in %s' % self.hostkey)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.connection.open:
            self.connection.close()

    def __err_handle__(self, id):
        if id == 2013:
            log.warning("SQL - Lost connection to the server, retry in a minute")
        self.connection.close()
        sleep(20)
        self.__init__(host=self.hostkey)

    def dbreader(self, q, decode=True):
        """
            Read from tables
        :return: List of dicts
        """
        try:
            self.cur.execute(q)
        except self.cur.OperationalError as err:
            # In case of a lost connection, wait for a minute and retry
            if err.args[0] == 2013:
                self.__err_handle__(2013)
                self.cur.execute(q)
            else:
                raise
        if self.cur.description is not None:
            # Fetch column names
            headers = [n[0] for n in self.cur.description]
            # Fetch rows
            out = [dict(zip(headers, row)) for row in list(self.cur)]
            log.info("SQL - Found %i rows" % len(out))
            if decode:
                self.decode_lod(out, keys=headers, unicode=False)
            return out
        else:
            log.info("SQL - No results found, returning empry list")
            return []

    def dbalter(self, q):
        # Execute the query
        """
            Alter or create tables in the DB
        """
        try:
            log.info("SQL - Changing the DB in %s" % self.hostkey)
            self.cur.connection.autocommit(True)
            self.cur.execute(q)
        except self.cur.OperationalError as err:
            # In case of a lost connection, wait for a minute and retry
            if err.args[0] == 2013:
                self.__err_handle__(2013)
                self.cur.connection.autocommit(True)
                self.cur.execute(q)
            else:
                raise
                # # Save the changes
                # self.collection.commit()
        p = self.iparam  # [host, user, passwd, db, charset]
        log.debug("SQL - DB altered closing reopening connection to refresh")
        self.connection.close()
        self.__init__(p[0], p[1])

    def insert(self, lst, table, cols=None, update=False):
        """
        Insert a table into the DB in batches
        :param cols: Names of the columns to bow_insert. If None: Insert all
        :param lst: Lit of dicts
        :param table: Name of table we bow_insert into
        """
        if isinstance(lst, pd.DataFrame):
            lst = list(lst.T.to_dict().values())
        length = len(lst)
        log.info("SQL - %i rows to insert_many" % length)
        num = int(length / 500)
        for j in range(0, num + 1):
            batch = lst[j * 500: min([length, 500 * (j + 1)])]
            if len(batch) > 0:
                log.debug("SQL - Inserting %i rows in batch" % len(batch))
                Q = self.insert_query(batch, table, cols, update=update)
                self.dbalter(Q)

    def insert_query(self, lst, table, cols=None, update=None):
        """
            Build the bow_insert querry for each batch
        :return: str
        """
        rows = []
        if not cols:
            cols = lst[0].keys()
        Q = """
            INSERT INTO weissserver.""" + table + '(' + ', '.join([str(k) for k in cols]) + ')' + """ VALUES
            (
        """
        for dct in lst:
            vals = []
            for k in cols:
                if dct[k] is not None:
                    vals.append("'" + str(dct[k]).replace("'", "''") + "'")
                else:
                    vals.append("NULL")
            rows.append(', '.join(vals))
        if update:
            Q += '),\r\n('.join(rows) + ')\r\n'
            Q += 'ON DUPLICATE KEY UPDATE '
            update_lst = []
            for k in update:
                update_lst.append(str(k) + '=VALUES(' + str(k) + ')')
            Q += '\r\n, '.join(update_lst)
        else:
            Q += '),\r\n('.join(rows) + ')'
        Q += ';'
        return Q

    def decode_lod(self, lod, keys=None, unicode=True):
        """
            Convert incoming data (usually Unicode) to int or date if necessary
        :param lod: List of dicts
        """
        if not keys:
            keys = lod[0].keys()
        if unicode:
            for d in lod:
                for k in keys:
                    d[k] = self.decode(d[k])
        else:
            for d in lod:
                for k in keys:
                    try:
                        d[k] = float(d[k])
                        continue
                    except (ValueError, TypeError):
                        pass

    def decode(self, val):
        if not val:
            return None
        elif isinstance(val, numbers.Number):
            return float(val)
        elif isinstance(val, str):
            try:  # If date
                return dt.datetime.strptime(val, '%Y-%m-%d %H:%M:%S')
            except (ValueError, TypeError):  # If str
                try:
                    return val.decode('utf-8')
                except UnicodeEncodeError:
                    log.warning(val)
                    log.warning('SQL - Value decode error')
                    return val
        elif isinstance(val, list):
            return [self.decode(obj) for obj in val]
        elif isinstance(val, tuple):
            return tuple([self.decode(obj) for obj in val])
        elif isinstance(val, dict):
            return {self.decode(k): self.decode(v) for k, v in val.iteritems()}
        else:
            log.warning("SQL - The value type of %s is unidentified" % str(val))
            raise Exception


if __name__ == '__main__':
    log.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=log.INFO,
                    filename='sql.log')
    with DbExplorer('DEV') as db:
        db.dbreader(q="select   *   from    pos_bars_products   limit 1")