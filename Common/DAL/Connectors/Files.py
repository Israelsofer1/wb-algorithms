# coding=utf-8
import pickle as pickle
import tarfile as tar
import json
import os
import logging as log
import pandas as pd
import h5py


class FileExplorer():
    def __init__(self, adr=None):
        if adr:
            self.source = adr
        else:
            self.source = os.path.dirname(os.path.realpath(__file__))
        log.info('File system - working with address %s' % self.source)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def check_filetype(self, name, type):
        try:
            if '.' in str(name):
                return name
            else:
                return name + '.' + type
        except IndexError:
            return name + '.' + type

    def get_dir(self, name, adr=None):
        """
        :param adr: Directory of the object, address can be omitted if the file is in the auxiliary folder
        :param name: Name of the pickled object
        :return: True Address
        """
        if adr:
            d = os.path.join(adr, name)
        else:
            d = os.path.join(self.source, name)
        return d

    def hdf5write(self, lod, name, adr=None):
        if adr is not None:
            name = self.get_dir(name, adr)
        hf = h5py.File(name, 'w')
        for k in lod:
            hf.create_dataset(k, data=lod[k], compression_opts=9, compression='gzip')
        hf.close()

    def hdf5read(self, name, adr=None):
        if adr is not None:
            name = self.get_dir(name, adr)
        hf = h5py.File(name, 'r')
        out = {}
        for k in hf.keys():
            out[k] = hf[k].value
        hf.close()
        return out

    def tarreader(self, name, adr=None):
        """
        :return: object
        """
        name = self.check_filetype(name, 'tar.gz')
        d = self.get_dir(name, adr)
        tf = tar.open(d, "r:gz")
        log.info('File system - opened .tar object %s' % name)
        for member in tf.getmembers():
            f = tf.extractfile(member)
            if f is not None:
                yield f.read()

    def pklreader(self, name, adr=None):
        """
        :return: object
        """
        name = self.check_filetype(name, 'p')
        d = self.get_dir(name, adr)
        obj = pickle.load(open(d, "rb"))
        log.info('File system - Opened .p file in %s' % d)
        return obj

    def pklwriter(self, obj, name, adr=None):
        """
        :param obj: object to be stored for later
        """
        name = self.check_filetype(name, 'p')
        d = self.get_dir(name, adr)
        log.info('File system - Writting to .p file in %s' % d)
        pickle.dump(obj, open(d, "wb"), protocol=pickle.HIGHEST_PROTOCOL)

    def jsonread(self, name, adr=None):
        """
        :return: object
        """
        name = self.check_filetype(name, 'json')
        d = self.get_dir(name, adr)
        with open(d, "r") as f:
            obj = json.load(f)
            log.info('File system - Opened .json file in %s' % d)
            return obj

    def jsonwrite(self, obj, name, adr=None):
        """
        :param obj: object to be stored for later
        """
        name = self.check_filetype(name, 'json')
        d = self.get_dir(name, adr)
        with open(d, "w") as f:
            log.info('File system - Writting to .json file in %s' % d)
            json.dump(obj, f)

    def csv_reader(self, name, adr=None, lod=True):
        """
        CSV file should preferably encoded into UTF-8
        :rtype: A list of dicts (if lod=True) or list of lists
        """
        name = self.check_filetype(name, 'csv')
        d = self.get_dir(name, adr)
        out = pd.read_csv(filepath_or_buffer=d)
        if lod:
            out = out.to_dict('records')
            log.info('File system - Opened .csv file in %s . number of records: %i' % (d, len(out)))
        return out

    def csv_writer(self, dlst, name, adr=None):
        """
        :param dlst: List of dicts (if lod=True) or list of lists
        """
        name = self.check_filetype(name, 'csv')
        d = self.get_dir(name, adr)
        if not isinstance(dlst, pd.DataFrame):
            dlst = pd.DataFrame(dlst)
        log.info('File system - Writing to .csv file in %s . number of records: %i' % (d, len(dlst.index)))
        dlst.to_csv(path_or_buf=d, index=False)


def get_fname(file_path):
    return file_path.split('\\')[-1]


def local_address(f_name, adr=None):
    if adr is None:
        adr = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(adr, f_name)