import logging
import logging.config

NOTSET = logging.NOTSET
DEBUG = logging.DEBUG
INFO = logging.INFO
WARN = logging.WARN
WARNING = logging.WARNING
ERROR = logging.ERROR
CRITICAL = logging.CRITICAL


def configure_logger(name, level, disable_existing=False):
    logging.config.dictConfig({
        "version": 1,
        "disable_existing_loggers": disable_existing,
        "formatters": {
            "simple": {
                "format": "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)d - %(message)s"
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": "DEBUG",
                "formatter": "simple",
                "stream": "ext://sys.stdout"
            },
            "null_handler": {
                "class": "logging.NullHandler",
            }
        },
        "loggers": {
            "": {
                "handlers": ["console"],
            },
            name: {
                "handlers": ["null_handler"],
                "level": level,
                "propagate": True
            }
            }
            }
        )
    return logging.getLogger(name)
