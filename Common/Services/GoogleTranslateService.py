import time
import sys
import pandas as pd
import logging as log
from Common.ExternalAPI import GoogleTranslate as GT
from Common.DAL.Connectors import SQL
from Common.Services import GoogleTranslateService_config as config


class GoogleTranslateService(object):

    def __init__(self, host):

        self.gt_connector = GT.GoogleTranslate()
        self.host = host
        self.LANGS = config.LANGS
        self.char_size_limit = config.char_size_limit
        self.list_size_limit = config.list_size_limit
        self.cols_to_insert = config.cols_to_insert
        self.cols_to_update = config.cols_to_update


    def __pulling_untranslated_items(self, country):
        """

        :param country: country pull products from
        :return:
        """

        pull_products_query = """select pb.id as bar_product_id,pb.title,ts.machine_translation,b.country
                   from bars b join pos_bars_products pb on b.id = pb.bar_id
                   left join pos_translation_suggestions ts on ts.bar_product_id = pb.id
                   where pb.machine_translation is null and ts.machine_translation is null
                   and b.country in ('"""+country+"""')"""


        with SQL.DbExplorer(host=self.host) as Stream:
            Stream.dbalter("SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED")
            items = Stream.dbreader(pull_products_query)
        return items

    def __insert_db(self, translated_products):
        """

        :param translated_products: lod to insert_many the db
        :return:
        """

        with SQL.DbExplorer(host=self.host) as Stream:
            query = Stream.insert_query(lst=translated_products, table="pos_translation_suggestions",
                                        cols=self.cols_to_insert, update=self.cols_to_update)
            Stream.dbalter(query)

        log.info('inset %s products to DB', len(translated_products))
        return

    def __resplit_answer(self, translated_list, id_list):
        """

        :param translated_list: output from google translate
        :param id_list: list of bar_product_if that were translated
        :return: lod of items to insert_many the db
        """
        translated_list = [x['translatedText'] for x in translated_list]
        items_to_insert_df = pd.DataFrame(list(zip(id_list, translated_list)),
                                          columns=['bar_product_id', 'machine_translation'])
        items_to_insert_lod = items_to_insert_df.to_dict('records')
        return items_to_insert_lod

    def __send_api(self, titles_list, source_lang):
        """
        :param titles_list: titles_list of titles to send the api
        :param source_lang: the source language
        :return:
        """
        try:
            trans = self.gt_connector.trans_many(chrlist=titles_list, source_lang=source_lang)
        except Exception as e:
            log.error('Error occurred in google translate API, quit job- ' + str(e))
            exit
        return trans


    def __process_products(self, bar_products, lang):
        """

        :param bar_products: products to translate
        :param lang: google api language code
        :return:
        """
        # char_counter - counts the total char sent to API
        # trans_list - lis of title to send to the API
        # id_list - list of bar product ids to later insert_many the DB with the translation
        char_counter = 0
        trans_list = []
        id_list = []
        for bar_product in bar_products:
            if char_counter+len(str(bar_product['title'])) > self.char_size_limit or len(trans_list) > self.list_size_limit:
                # if 1 more item will exceed some google API limit, send what you have and init all parameters
                translated_obj = self.__send_api(titles_list=trans_list, source_lang=lang)
                translated_list = translated_obj['translations']
                time.sleep(1)
                translated_lod = self.__resplit_answer(translated_list=translated_list, id_list=id_list)
                self.__insert_db(translated_products=translated_lod)

                # initial new counter and lists
                trans_list = []
                char_counter = 0
                id_list = []
            else:
                # add another item to the list
                char_counter += len(str(bar_product['title']))
                trans_list.append(str(bar_product['title']))
                id_list.append(bar_product['bar_product_id'])
        if len(trans_list) > 0:
            # finish the leftover items
            translated_obj = self.__send_api(titles_list=trans_list, source_lang=lang)
            translated_list = translated_obj['translations']
            translated_lod = self.__resplit_answer(translated_list=translated_list, id_list=id_list)
            self.__insert_db(translated_products=translated_lod)
        return

    def run(self, countries=['Korea']):

        # config logging
        log.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=log.INFO)

        log.info('GoogleTranslateService - started')
        for country in countries:
            log.info('stating products from %s', country)
            products_to_translate = self.__pulling_untranslated_items(country)
            log.info('found %s untranslated items', len(products_to_translate))
            self.__process_products(bar_products=products_to_translate, lang=self.LANGS[country])
        log.info('GoogleTranslateService - ended')


    # def __join_products(self, big_string, current_item):
    #     """
    #
    #     :param big_string: string with words to translate
    #     :param current_item: the current item to add to the big string
    #     :return: big string with current item
    #     """
    #
    #     return big_string+current_item+self.SEPARATOR









if __name__ == '__main__':

    # goog_test = GoogleTranslateService(host='DEV')
    # goog_test.run(countries=['Brazil'])

