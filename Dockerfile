FROM ubuntu:16.04
RUN apt-get update
RUN apt-get -qq -y install curl
# Anaconda install

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git mercurial subversion

RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh && \
    wget --quiet https://repo.continuum.io/archive/Anaconda2-5.0.1-Linux-x86_64.sh -O ~/anaconda.sh && \
    /bin/bash ~/anaconda.sh -b -p /opt/conda && \
     rm ~/anaconda.sh

RUN apt-get install -y curl grep sed dpkg && \
TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'` && \
curl -L "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}.deb" > tini.deb && \
dpkg -i tini.deb && \
rm tini.deb && \
apt-get clean

ENV PATH /opt/conda/bin:$PATH

RUN conda create -n my_env2_7_12 python=2.7.12

RUN /bin/bash -c "source activate my_env2_7_12"

RUN conda install -c conda-forge spacy
RUN conda install libgcc


# Python installation and related
RUN apt-get install -y python python-dev libmysqlclient-dev python-pip
RUN pip install --upgrade pip
RUN python -m spacy download en
RUN pip install MySQL-python
RUN pip install scipy pandas elasticsearch requests_aws4auth korean python-dateutil
RUN pip install konlpy
RUN pip install pymongo
RUN pip install pyflux
RUN pip install datadog
RUN pip install logzio-python-handler
RUN pip install pyspark
RUN pip install flask
RUN pip install flask_restful
RUN pip install boto3
RUN pip install xlsxwriter
RUN pip install mlxtend

RUN apt-get install -y gcc


RUN pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.4.0-cp27-none-linux_x86_64.whl
# RUN pip install xgboost
RUN pip install --upgrade google-api-python-client
RUN conda uninstall scikit-learn
RUN conda uninstall blaze
RUN conda install blaze==0.10.1
RUN conda install scikit-learn==0.17.1
WORKDIR /tmp
RUN wget https://pypi.python.org/packages/34/85/456a1a8c762f646671043e446a59efbce02b5f408f522c4ef8793e860c5e/xgboost-0.6a2.tar.gz#md5=c486211efa29b95771c74f5d8701ca4a
RUN tar -xvf xgboost-0.6a2.tar.gz
WORKDIR /tmp/xgboost-0.6a2/xgboost
RUN ./build-python.sh
WORKDIR /tmp/xgboost-0.6a2
RUN python setup.py install
RUN conda install libgcc

RUN conda install numpy=1.11.1

ENTRYPOINT [ "/usr/bin/tini", "--" ]
#CMD [ "/bin/bash" ]


# Define environment variable

COPY analytics /home/ubuntu/p_scripts/analytics

# RUN ls -lha /home/ubuntu/p_scripts/

# RUN /bin/bash -c "cd /home/ubuntu/p_scripts/analytics/; ls -lha"

RUN chmod 777 /home/ubuntu/p_scripts/analytics/exec_python.sh

ENV NAME analytics

CMD [ "sh","-c","/home/ubuntu/p_scripts/analytics/./exec_python.sh"]