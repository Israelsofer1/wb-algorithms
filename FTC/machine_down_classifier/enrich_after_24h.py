from pyspark.sql.window import Window
import pyspark.sql.functions as func
from pyspark.sql.functions import col, when







def voltage_enrichment(spark, volage_df):
    # test ok
    # the query looks back 30 days from now() and bring back all voltage info per machine
    # using window func for creating voltage events - for each machine in each day,sort voltage by time
    w = Window.partitionBy("machine_id", "nd").orderBy('time_local')
    # active window func - 2 adjacent events will show in the same row
    iot_adc_after = volage_df.withColumn('time_local2', func.lag(volage_df['time_local']).over(w)).withColumn('value2',func.lag(volage_df['value']) \
    .over(w)).withColumn('timediff', func.unix_timestamp('time_local') - func.unix_timestamp('time_local2'))
    # algo rule base - voltage event is an event which voltage is a declining series and the adjacent voltage (by time) hold voltage lower than 6000
    iot_adc_after = iot_adc_after.withColumn('status', when((iot_adc_after.value2 > iot_adc_after.value) & (iot_adc_after.value2 < 6000), 'voltage_event').otherwise('other'))
    voltage_events = iot_adc_after.filter(iot_adc_after.status.isin('voltage_event'))
    # filter only voltage events -a voltage event define to occur with max interval of 120 sec and adjacent events will occur with max interval of 60 sec
    voltage_events = voltage_events.filter(voltage_events.timediff < 120)
    return (voltage_events)


def daily_weekly_enrichment(voltage_events):
    # group events per machine and nd , count them in one column and count all events that have less than 60 sec timediff (those events per day and machine is an ongoing event)
    daily_grouping = voltage_events.groupBy("machine_id", 'nd').agg((func.count(when(col("timediff") > 60, True)).alias('count_above_60')))
    daily_grouping = daily_grouping.withColumn('voltage_events_per_day', daily_grouping.count_above_60 + 1)
    # add week,month and year labels to the data
    month_grouping = daily_grouping.withColumn('week_num', func.weekofyear(daily_grouping.nd)).withColumn('month_num', func.month(daily_grouping.nd)).withColumn('year_num', func.year(daily_grouping.nd))
    # group daily voltage events - how many voltage events happend only once a day, for each machine?
    daily_events = month_grouping.groupBy("machine_id").agg((func.count(when(col("voltage_events_per_day") == 1, True)).alias('voltage_events_per_month'))) \
    .orderBy('voltage_events_per_month', ascending=False)
    daily_events = daily_events.selectExpr('machine_id as daily_machine_id','voltage_events_per_month as voltage_events_per_month')
    # pre group for weekly calc - count how many voltage events per day happend per week,month and machine
    weekly_grouping = month_grouping.groupBy('machine_id', 'month_num', 'week_num').agg((func.count(when(col("voltage_events_per_day") == 1, True)) \
    .alias('voltage_events_per_week'))).orderBy('month_num', 'week_num', ascending=False)
    # group weekly voltage events - how many single daily voltage events happend each week during the month, for each machine?
    weekly_events = weekly_grouping.groupBy('machine_id').agg((func.count(when(col("voltage_events_per_week") == 1, True)).alias('voltage_events_foreach_week'))).orderBy('voltage_events_foreach_week', ascending=False)
    weekly_events = weekly_events.selectExpr('machine_id as weekly_machine_id','voltage_events_foreach_week as voltage_events_foreach_week')
    return (daily_events, weekly_events)

