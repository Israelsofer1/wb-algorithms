import pyspark.sql.functions as func


############################   part 1 , db data that feeds the algo    #######################################################

# first query returns relevant machines that went down in the last 2 hours
#the first query get startdate and hours back.
# startdate is the point on time where the algo think we are
# hours back determine how many hours the algo will go back and ask for machines that have no connection after this point

def bad_machines_enr(spark, startdate, hours_back):
    query_res = spark.sql \
        ("""SELECT  
      b.venue_id
     , b.id as bar_id
      , b.name as bar_name
      , last_conn_table.machine_id
      , last_conn_table.last_connection
      , last_conn_table.first_connection
      , m.mac as imei
      , b.country
      , b.state ,
      case when ((datediff(last_conn_table.last_connection,m.created_at) <= 1) or(last_conn_table.last_connection is null)) then '1' else '0' end 24_h_from_installation 

       FROM

      (
      SELECT
      machine_id,
      max(time_local) as last_connection,
      min(time_local) as first_connection
      FROM
      stg.iot_connections
      where time_local  < ('""" + startdate + """' )
      GROUP BY machine_id having last_connection < ('""" + startdate + """' -  INTERVAL '""" + hours_back + """' hour)  
      )

      as last_conn_table

      JOIN

      (
      select * from 	
      stg.machines
      where is_active = 1
      )

      as m      

      on m.id = last_conn_table.machine_id

	 JOIN 

      stg.bars as b

      ON

      b.id = m.bar_id

     JOIN

     stg.bars_service_levels as bsl

     ON

     bsl.bar_id = b.id

     WHERE
     bsl.service_level_id in (1,5)
     AND
      b.status not in (0)
      AND
      b.country in
      (
      'Argentina',
      'Australia',
      'Canada',
      'China',
      'Israel',
      'France',
      'Korea',
      'United Kingdom',
      'United states'
      )
      """)
    return(query_res)


# query returns max voltage for each machine and the time of this max_voltage. we use those 2 columns for the never plugged condition

def never_plugged_enr(spark,startdate):
    query_res = spark.sql \
        ("""select machine_id as machine_id_1 , max(value) as max_voltage from stg.iot_adc where nd < '""" + startdate + """' group by machine_id """)
    return (query_res)


def latest_voltage_enr(spark,startdate):
    query_res = spark.sql \
        ("""select machine_id as latest_voltage_machine_id , value as latest_voltage , t1.latest_voltage_time from stg.iot_adc
    join
    (select machine_id as latest_voltage_machine_id , max(time_local) as latest_voltage_time from stg.iot_adc where nd < '""" + startdate + """' group by machine_id ) as t1 
    on stg.iot_adc.machine_id = t1.latest_voltage_machine_id and stg.iot_adc.time_local = t1.latest_voltage_time where sensor_num =1 """)
    return (query_res)


def poor_reception_enr(spark, startdate):
    query_res = spark.sql \
        (
            """ select c.machine_id as machine_id_2 ,avg(rssi) as avg_rssi ,min(rssi) as min_rssi, operator from stg.iot_connections c where time_local between ('""" + startdate + """' -  interval 1 day ) and '""" + startdate + """' group by machine_id,operator""")
    return (query_res)


# query returns resets details for each machine since ever per machine.  we use those columns for the incorrect_imei condition

def incorrect_imei_enr(spark,startdate):
    query_res = spark.sql \
        ("""select mac , count(*) as num_of_mac_resets from stg.machine_reset where nd < '""" + startdate + """' group by mac""")
    return (query_res)


# query returns voltage details from the last 30 days.  we use those columns for the daily/weekly/intentionally shutdown conditions
def voltage_enr(spark, startdate, voltage_days_interval):
    query_res = spark.sql \
        (
            """select * from stg.iot_adc where sensor_num =1 and nd > ('""" + startdate + """' - interval '""" + voltage_days_interval + """' day) order by machine_id , nd asc ,`value` desc""")
    return (query_res)


# query returns servings (pourings) table. we use those columns for the intentionall unplugged condition
def lines(spark,startdate):
    query_res = spark.sql \
        ("""select * from  stg.lines where created_at <'""" + startdate + """' """)
    return (query_res)


# query returns lines table. we use those columns for the intentionall unplugged condition
def servings(spark,startdate):
    query_res = spark.sql \
        ("""select * from  stg.servings where timestamp <'""" + startdate + """'""")
    return (query_res)


# query returns resets table. we use those columns for the connectivity resets condition
def machine_reset(spark,startdate):
    query_res = spark.sql \
        ("""select * from stg.machine_reset where timestamp < '""" + startdate + """'  """)
    return (query_res)


# query returns resets table. we use those columns for the connectivity resets condition
def suspected_removal(spark):
    query_res = spark.sql(
        """select bar_id as bars_id, count(id) as machines_per_bar from stg.machines m where bar_id > 0 group by bar_id """)
    return (query_res)


# query returns holidays table. we use those columns for the seasonality resets condition
def holidays(spark):
    query_res = spark.sql("""select * from stg.holidays_calendar""")
    return (query_res)


def split_tables(spark,final_data_enrichment):
    under_24h = final_data_enrichment.filter(func.col('24_h_from_installation').isin(1))   #left side of the tree
    after_24h = final_data_enrichment.filter(func.col('24_h_from_installation').isin(0))   #right side of the tree
    return(under_24h,after_24h)


def conn_table(spark):
    query_res = spark.sql(""" select machine_id as conn_machine_id , time_local , rssi , operator from stg.iot_connections """)
    return(query_res)