from pyspark.sql import SparkSession
from pyspark.sql.functions import broadcast , lit , col , when
import pyspark.sql.functions as func
from pyspark.sql.functions import monotonically_increasing_id

def under_24h_data_enrichment(spark,startdate,bad_machines_v,never_plugged_v,poor_reception_v,incorrect_imei_v,relevant_columns):
    q1 = bad_machines_v.join(broadcast(never_plugged_v), never_plugged_v.machine_id_1 == bad_machines_v.machine_id , how = 'left')\
    .join(broadcast(poor_reception_v),poor_reception_v.machine_id_2 == bad_machines_v.machine_id , how = 'left')\
    .join(broadcast(incorrect_imei_v), incorrect_imei_v.mac == bad_machines_v.imei , how ='left')
    ##add time fields
    q1 = q1.withColumn('created_at',lit(startdate))
    q1 = q1.withColumn('nd',lit(str(startdate[:10])))
    q1 = q1.withColumn('resolved',lit(0))
    q1 = q1.withColumn('modified_at',lit(startdate))
    q1 = q1.withColumn('classification_status',lit('other'))
    q1 = q1.dropDuplicates(['machine_id'])
    final_data_enrichment = q1.select(relevant_columns)
    return(final_data_enrichment)



def under_24h_data_enrichment_new(spark,startdate,bad_machines_v,never_plugged_v,incorrect_imei_v,conn_df_v,relevant_columns):
    q1 = bad_machines_v.join(broadcast(never_plugged_v), never_plugged_v.machine_id_1 == bad_machines_v.machine_id , how = 'left')\
    .join(broadcast(incorrect_imei_v), incorrect_imei_v.mac == bad_machines_v.imei , how ='left')
    ##add time fields
    final_data_enrichment = q1.withColumn('created_at',lit(startdate)).withColumn('nd',lit(str(startdate[:10]))).withColumn('resolved',lit(0)).withColumn('modified_at',lit(startdate))\
    .withColumn('classification_status',lit('other')).dropDuplicates(['machine_id'])
    reception_enrichment = conn_df_v.join(final_data_enrichment,final_data_enrichment.machine_id == conn_df_v.conn_machine_id,'left')
    temp_table = reception_enrichment.withColumn('24h_from_last_conn',when(func.datediff(reception_enrichment.last_connection,reception_enrichment.time_local)<2,'yes')\
    .otherwise('no'))
    temp_table=temp_table.filter(col('24h_from_last_conn')=='yes')
    final = temp_table.groupBy('machine_id','operator').agg(func.mean('rssi').alias('avg_rssi'), func.min('rssi').alias('min_rssi')).withColumnRenamed('machine_id', 'temp_machine_id')
    ##get for each machine_id and operator, get max_time connection
    latest_operator_prep = conn_df_v.groupBy('conn_machine_id','operator').agg(func.max('time_local').alias('max_time')).sort(col('max_time').desc()).drop('max_time')
    ##add increasing index
    operator_with_index = latest_operator_prep.withColumn("id", monotonically_increasing_id())
    ##for each machine, return max index, thats where the operator with the latest connection
    max_index = operator_with_index.groupBy('conn_machine_id').agg(func.min('id').alias('min_id')).withColumnRenamed('conn_machine_id', 'conn_machine_id2')
    ##join the tables will bring back only machine_id with real operator and max_connection time
    latest_operator = operator_with_index.join(max_index,max_index.min_id==operator_with_index.id).drop('conn_machine_id2','min_id','id').withColumnRenamed('operator', 'operator2')
    #latest_operator.filter(latest_operator.conn_machine_id == 7138).show()
    cond =[latest_operator.conn_machine_id ==final.temp_machine_id,latest_operator.operator2 ==final.operator]
    pre_final_table = final.join(latest_operator,cond).drop('conn_machine_id','operator2')
    final_df = pre_final_table.join(final_data_enrichment,pre_final_table.temp_machine_id == final_data_enrichment.machine_id).select(relevant_columns)
    return(final_df)





def unplugged_intentionally_enrichment(spark,lines_df,servings_df,voltage_events):
    lines_df = lines_df.select(lines_df.columns[:2])
    stats_with_machines = servings_df.join(lines_df,lines_df.id == servings_df.line_id)
    stats_with_machines = stats_with_machines.select('machine_id','timestamp','status','nd','consumption')
    #for each machine, when was her last pouring event?
    stats_with_machines = stats_with_machines.groupBy('machine_id').agg((func.max(col("timestamp")).alias('last_pouring_time')))
    stats_with_machines = stats_with_machines.selectExpr('machine_id as stats_machine_id','last_pouring_time as last_pouring_time')
    #join between voltage events and recent table will bring us the last pouring time infront of any voltage event time
    stats_with_machines_and_events = voltage_events.join(broadcast(stats_with_machines),voltage_events.machine_id == stats_with_machines.stats_machine_id)
    unplugged_intentionall_status_perp = stats_with_machines_and_events.withColumn('status',when((func.datediff(stats_with_machines_and_events\
    .last_pouring_time,stats_with_machines_and_events.time_local)<1),'unplugged_intentional_nominate')\
    .otherwise('other')).drop('id','stats_machine_id','sensor_num','time_local','time_utc','value','nd','time_local2','value2','timediff')
    #now we have for each machine weather if she unplugged intentionl or not
    unplugged_intentionall_status_perp =unplugged_intentionall_status_perp.select('machine_id','status','last_pouring_time').distinct()
    #filtering only relevant machines
    unplugged_intentionall_status_perp = unplugged_intentionall_status_perp.selectExpr('last_pouring_time as last_pouring_time','machine_id as up_machine_id','status as temp_status')
    #join after 24h to the unplugged_intentionall_status_perp and create consistent flow
    return(unplugged_intentionall_status_perp)


