from .get_data import split_tables , latest_voltage_enr , bad_machines_enr , never_plugged_enr , poor_reception_enr , incorrect_imei_enr , servings , lines, voltage_enr , machine_reset , conn_table
from .enrich_under_24h import  unplugged_intentionally_enrichment , under_24h_data_enrichment , under_24h_data_enrichment_new
from .enrich_after_24h import  daily_weekly_enrichment , voltage_enrichment
from .classifie_after_24h import  connectivity_reset_cond , connectivity_rssi_cond , unplugged_intentionally_cond , daily_weekly_union , unplugged_intentionally_cond_new
from .classifie_under_24h import never_transmitted_cond , incorrect_imei_cond , poor_reception_cond , never_plugged_cond
from .save_results import insert_results  , map_data , reshaping_final_table
from ..machine_down_configuration.configuration import *
from datetime import datetime



############################################################   ACTIVATION   ###################################################

def classification_process(spark,startdate,hours_back):
    print("%s - import tables -  started" % datetime.now())
    bad_machines = bad_machines_enr(spark,startdate,hours_back)
    print('bad machine count is ' + str(bad_machines.count()))
    never_plugged = never_plugged_enr(spark,startdate)
    print('never plugged count is ' + str(never_plugged.count()))
    #poor_reception= poor_reception_enr(spark,startdate)
    incorrect_imei= incorrect_imei_enr(spark,startdate)
    print('incorrect_imei count is ' + str(incorrect_imei.count()))
    #servings_df = servings(spark,startdate)
    #lines_df = lines(spark,startdate)
    volatge_df = voltage_enr(spark,startdate,voltage_days_interval)
    print('volatge_df count is ' + str(volatge_df.count()))
    machine_resets = machine_reset(spark,startdate)
    print('machine_resets count is ' + str(machine_resets.count()))
    latest_voltage = latest_voltage_enr(spark,startdate)
    print('latest_voltage count is ' + str(latest_voltage.count()))
    conn_df = conn_table(spark)
    print('conn_df count is ' + str(conn_df.count()))
    print("%s - import tables -  ended" % datetime.now())

    print("%s - under24h data enrichment -  started" % datetime.now())
    #final_data_enrichment = under_24h_data_enrichment(spark,startdate,bad_machines,never_plugged,poor_reception,incorrect_imei,under_24h_enr_relevant_columns)
    final_data_enrichment = under_24h_data_enrichment_new(spark,startdate, bad_machines, never_plugged, incorrect_imei, conn_df,under_24h_enr_relevant_columns)
    print(str(final_data_enrichment.count())+' machines at the entry point')
    print("%s - under24h data enrichment -  ended" % datetime.now())

    print("%s - split tables -  started" % datetime.now())
    under_after_24h_splited = split_tables(spark,final_data_enrichment)
    print("%s - split tables -  ended" % datetime.now())

    print("%s - under 24h conditions  -  started" % datetime.now())
    never_plugged = never_plugged_cond(spark,under_after_24h_splited[0])
    poor_reception = poor_reception_cond(spark,under_after_24h_splited[0])
    incorrect_imei = incorrect_imei_cond(spark,under_after_24h_splited[0])
    never_transmitted = never_transmitted_cond(spark,under_after_24h_splited[0])
    print("%s - under 24h conditions  -  ended" % datetime.now())

    print("%s - after 24h data enrichment -  started" % datetime.now())
    voltage_events = voltage_enrichment(spark,volatge_df)
    daily_weekly_events = daily_weekly_enrichment(voltage_events)
    daily_weekly = daily_weekly_union(spark,under_after_24h_splited[1],daily_weekly_events[0],daily_weekly_events[1])
    #unplugged_intentionally_enr = unplugged_intentionally_enrichment(spark,lines_df,servings_df,voltage_events)
    print("%s - after 24h data enrichment -  ended" % datetime.now())

    print("%s - after 24h conditions  -  started" % datetime.now())
    #unplugged_intentionally = unplugged_intentionally_cond(spark,under_after_24h_splited[1],unplugged_intentionally_enr,latest_voltage)
    unplugged_intentionally = unplugged_intentionally_cond_new(spark,under_after_24h_splited[1], latest_voltage)
    connectivity_rssi = connectivity_rssi_cond(spark,under_after_24h_splited[1])
    connectivity_resets = connectivity_reset_cond(spark,under_after_24h_splited[1],machine_resets)
    ############################classification df's that go into the save results process ###########################
    classification_dfs = [never_plugged, poor_reception, incorrect_imei, never_transmitted, daily_weekly,unplugged_intentionally, connectivity_rssi, connectivity_resets]
    ############################classification df's that go into the save results process ###########################
    print("%s - after 24h conditions  -  ended" % datetime.now())

    print("%s - reshaping and mapping by order classification part  -  started" % datetime.now())
    classification_res = reshaping_final_table(spark,classification_dfs,final_classification_cols)
    mapper = spark.createDataFrame(mapping_vals, mapping_columns)
    classification = map_data(classification_res,mapper,final_classification_cols)
    print("%s - reshaping and mapping by order classification part  -  ended" % datetime.now())

    print("%s - classification - insert results -  started" % datetime.now())
    insert_results(classification, spark)
    print("%s - classification - insert results -  ended" % datetime.now())

    print(str(classification.count()) + ' machines at the end point')

    return('classification done')




