from pyspark.sql.functions import broadcast , lit , col , when
from ..machine_down_configuration.configuration import *


def never_plugged_cond(spark,df):
    never_plugged = df.withColumn('classification_status',when(((df.max_voltage < never_plugged_voltage) | (df.max_voltage.isNull()))&(df.num_of_mac_resets > 0) ,'never_plugged').otherwise('unknown_installation_issue'))
    return(never_plugged)



def poor_reception_cond(spark,df):
    poor_reception_status = df.withColumn('classification_status',when((df.avg_rssi < avg_rssi_arg)&\
    (df. min_rssi< min_rssi_arg),'poor_reception').otherwise('unknown_installation_issue'))
    return(poor_reception_status)



def incorrect_imei_cond(spark,df):
    incorrect_imei_status = df.withColumn('classification_status',when((df.last_connection.isNull())&(df.num_of_mac_resets.isNull())\
    ,'incorrect_imei').otherwise('unknown_installation_issue'))
    return(incorrect_imei_status)




def never_transmitted_cond(spark,df):
    never_transmitted_status = df.withColumn('classification_status',when((df.last_connection.isNull())\
    &(df.num_of_mac_resets > 0),'never_transmitted').otherwise('unknown_installation_issue'))
    return(never_transmitted_status)
