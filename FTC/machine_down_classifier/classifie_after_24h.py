from pyspark.sql.functions import  when
import pyspark.sql.functions as func
from ..machine_down_configuration.configuration import *


def unplugged_intentionally_cond(spark,df,enrichment,voltage_table):
    ui_joined_table = df.join(enrichment,enrichment.up_machine_id ==df.machine_id).join(voltage_table,df.machine_id == voltage_table.latest_voltage_machine_id)
    unplugged_intentionall_status = ui_joined_table.withColumn("classification_status",when((ui_joined_table.temp_status == 'unplugged_intentional_nominate') & (ui_joined_table.latest_voltage < latest_voltage), 'unplugged_intentionally')\
    .otherwise('unknown_issue'))\
    .drop('up_machine_id','temp_status','latest_voltage_machine_id','max_voltage','latest_voltage','last_pouring_time')
    return(unplugged_intentionall_status)

def unplugged_intentionally_cond_new(spark,df,voltage_table):
    ui_joined_table = df.join(voltage_table,df.machine_id == voltage_table.latest_voltage_machine_id)
    unplugged_intentionall_status = ui_joined_table.withColumn("classification_status",when((ui_joined_table.latest_voltage < min_latest_voltage)&(func.datediff(ui_joined_table\
    .last_connection,ui_joined_table.latest_voltage_time)<unplugged_num_of_days), 'unplugged_intentionally')\
    .otherwise('unknown_issue'))\
    .drop('up_machine_id','temp_status','latest_voltage_machine_id','max_voltage','latest_voltage','last_pouring_time')
    return(unplugged_intentionall_status)

def connectivity_rssi_cond(spark,df):
    connectivity_rssi_status = df.withColumn('classification_status',when((df.min_rssi < min_rssi_arg)|(df.avg_rssi < avg_rssi_arg),'connectivity_rssi').otherwise('unknown_issue'))
    return(connectivity_rssi_status)



def connectivity_reset_cond(spark, after_24h, machine_resets):
    # test OK
    # import table and clean irrelevant fields
    machine_resets = machine_resets.drop('id', 'reboot_cause', 'version')
    machine_resets = machine_resets.selectExpr('mac as mac', "timestamp as reset_timestamp")
    # join table with nominate down machines
    joined_df = after_24h.join(machine_resets, after_24h.imei == machine_resets.mac)
    # grouping num of resets in the pass week since the last connection
    last_week_resets = joined_df.groupBy('mac').agg((func.count(when(func.datediff(joined_df.last_connection,joined_df.reset_timestamp) < conn_reset_daydiff,True)).alias('num_of_resets_last_week')))
    connectivity_reset_status = last_week_resets.join(after_24h, after_24h.imei == last_week_resets.mac)
    connectivity_reset_status = connectivity_reset_status.withColumn('classification_status', when(connectivity_reset_status.num_of_resets_last_week > num_of_resets, 'connectivity_reset').otherwise('unknown_issue'))
    return (connectivity_reset_status)



def daily_weekly_union(spark, df, daily_events, weekly_events):
    # test ok
    # for each machine - add daily weekly info - weather she flagged as daily or weekly?
    daily_weekly_nominates = df.join(daily_events, daily_events.daily_machine_id == df.machine_id, how='left').join(weekly_events, weekly_events.weekly_machine_id == df.machine_id, 'left')
    daily_shutdown_status = daily_weekly_nominates.withColumn('daily_shutdown',when(daily_weekly_nominates.voltage_events_per_month > events_per_month,'yes').otherwise('no'))
    daily_weekly_status = daily_shutdown_status.withColumn('weekly_shutdown',when(daily_shutdown_status.voltage_events_foreach_week > events_per_week,'yes').otherwise('no'))
    daily_weekly_status = daily_weekly_status.withColumn('classification_status',when(daily_weekly_status.daily_shutdown == 'yes','daily_shutdown').otherwise('unknown_issue'))
    daily_weekly_status = daily_weekly_status.withColumn('classification_status',when((daily_weekly_status.weekly_shutdown == 'yes') & \
    (daily_weekly_status.classification_status.isin('unknown_issue')), 'weekly_shutdown').otherwise(daily_weekly_status.classification_status))
    return (daily_weekly_status)