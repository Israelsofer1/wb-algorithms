from pyspark.sql.functions import  when ,lit
import pyspark.sql.functions as func



def reshaping_final_table(spark, lst_of_dfs, lst_of_columns):
    first = lst_of_dfs[0].select(lst_of_columns)
    for cond in range(len(lst_of_dfs)):
        temp = lst_of_dfs[cond].select(lst_of_columns)
        first = first.union(temp)
    first = first.dropDuplicates()
    return (first)



def map_data(df,mapper,classification_cols):
    agg_cols = classification_cols[:-1]
    df = df.join(mapper,df.classification_status == mapper.issue_name)
    aggregate = df.groupBy(agg_cols).agg(func.min(df.issue_id).alias('prior_classification'))
    aggregate = aggregate.selectExpr('machine_id as temp_machine_id' , 'prior_classification as prior_classification')
    cond = [aggregate.prior_classification==df.issue_id,aggregate.temp_machine_id == df.machine_id]
    final_table = aggregate.join(df,cond).select(classification_cols)
    return(final_table)





def insert_results(output_df, spark):
    output_df = output_df.repartition('nd')
    output_df.registerTempTable("dis_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.class_machine_down_events PARTITION(nd) SELECT
            venue_id,
            bar_id,
            bar_name,
            imei,
            machine_id,
            country,
            state,
            operator,
            last_connection,
            classification_status,
            created_at,
            modified_at,
            resolved,
            nd
            FROM dis_df""")