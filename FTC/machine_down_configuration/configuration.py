from pyspark.sql import SparkSession


################################################### classification configuration ###############################################

##columns we choose to the classification process entry

under_24h_enr_relevant_columns = ['venue_id','bar_id','bar_name','imei','machine_id','country','state','operator','last_connection','first_connection'\
    ,'24_h_from_installation','max_voltage','avg_rssi','min_rssi','num_of_mac_resets','created_at','resolved','modified_at','nd','classification_status']

#columns we choose to present at the final output

final_classification_cols = ['venue_id', 'bar_id', 'bar_name', 'imei', 'machine_id', 'country', 'state', 'created_at',
                       'operator', 'last_connection', 'modified_at', 'resolved', 'nd', 'classification_status']


#mapper function is the relation between each classifiction function and the classify order between them:
#issue_id => corelative with the issues order. means that any machine suspected as never plugged and reach all other condition before get to be unknown issue

voltage_days_interval = "120"

mapping_columns = ["issue_id", "issue_name"]


mapping_vals = [(1, "never_plugged"), (2, "poor_reception"), (3, "incorrect_imei"), (4, "never_transmitted"),(5, "daily_shutdown"), (6, "weekly_shutdown"), (7, "unplugged_intentionally"),(8, "connectivity_rssi"), (9, "connectivity_reset"), (10, "unknown_issue"), (10, "unknown_installation_issue")]


#never_plugged_condition

never_plugged_voltage = 9000

#poor_reception_condition & connectivity_rssi_condition


avg_rssi_arg = -105

min_rssi_arg = -100

#unplugged_intentionally_condition

latest_voltage = 6000
min_latest_voltage = 6000
unplugged_num_of_days = 4

#connectivity_reset_condition

conn_reset_daydiff =7

num_of_resets = 20

##daily/weekly condition
events_per_month = 18
events_per_week = 4



################################################### Diagnostic configuration ##########################################################

#####suspected removal ccondition
##if the bar had this num of machines and they all down this is a suspected removal.

num_of_machines_arg = 4



#####operator blacout ccondition

ob_num_of_events = 8

ob_num_of_bars_participating = 2

ob_timediff_between_events = 1



#####country blacout ccondition

cb_num_of_events = 10

cb_num_of_bars_participating = 20

cb_timediff_between_events = 1



#####country down ccondition

cd_num_of_events = 15

cd_num_of_bars_participating = 30

cd_num_of_operator_participating= 1

cd_timediff_between_events = 1

#production issue
#treshold between machines that are down relate to the whole batch count

bad_batch_tr = 0.5


######seasonality state ccondition


ss_num_of_events = 1

ss_num_of_bars_participating = 1

######seasonality country ccondition

sc_num_of_events= 1

sc_num_of_bars_participating=1

sc_num_of_states_participating = 1

######installation issue ccondition

inst_lst_of_issues=['never_transmitted','never_plugged','poor_reception','incorrect_imei']

######reception issue ccondition

recept_lst_of_issues =['connectivity_rssi','connectivity_reset']

#columns we choose to present at the final output

diagnostic_final_cols_list = ['venue_id', 'bar_id', 'bar_name', 'imei','machine_id','country','state','operator','last_connection','classification_status','diagnostic_status','created_at','modified_at','resolved','nd']

######irrelevant machjne ccondition

lst_of_conventiones = ['[PHR]', 'PHR', 'removed', 'REMOVED', 'Removed', 'QA', 'qa', 'TEST', 'test', 'Test']

####report result path
path = 's3://wb-ds-models/FTC-Machine_down/res'

