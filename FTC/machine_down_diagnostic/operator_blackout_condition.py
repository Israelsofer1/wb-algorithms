from pyspark.sql.functions import broadcast , lit , col , when
import pyspark.sql.functions as func



def operator_blackout_cond(spark, df_a, df_b, num_of_events, num_of_bars_participating, timediff_between_events):
    # test OK
    operator_blackout_condition_entry_point = df_a.filter(df_a.classification_status.isin('unknown_installation_issue'))
    unknown_issue_nominates = df_b.filter(df_b.diagnostic_status.isin('unknown_issue')).drop('diagnostic_status')
    operator_blackout_condition_entry_point = operator_blackout_condition_entry_point.union(unknown_issue_nominates)
    operator_state_blackout_condition_grouping = operator_blackout_condition_entry_point.groupBy("state",'operator').agg(func.count(col("machine_id")).alias('events'), func.countDistinct(col("bar_id")).alias('bars_participating'))
    operator_state_blackout_condition_grouping = operator_state_blackout_condition_grouping.filter(func.col('events') >= num_of_events).filter(func.col('bars_participating') >= num_of_bars_participating).filter(func.col('operator').isNull() == False)
    operator_state_blackout_condition_grouping = operator_state_blackout_condition_grouping.selectExpr('state as distinct_state', 'operator as distinct_operator')
    cond = [operator_blackout_condition_entry_point.state == operator_state_blackout_condition_grouping.distinct_state,operator_blackout_condition_entry_point.operator == operator_state_blackout_condition_grouping.distinct_operator]
    joined = operator_blackout_condition_entry_point.join(operator_state_blackout_condition_grouping, cond, 'left')
    operator_state_blackout_status = joined.withColumn("diagnostic_status", when((func.datediff(joined.created_at, joined.last_connection) <= timediff_between_events) & (joined.distinct_state.isNull() == False), 'operator_in_specific_state').otherwise('other'))
    operator_state_blackout_status = operator_state_blackout_status.drop('distinct_state', 'distinct_operator')
    return (operator_state_blackout_status)


# df => operator blackout result
def country_blackout_cond(spark, df, num_of_events, num_of_bars_participating, timediff_between_events):
    df = df.filter(df.diagnostic_status.isin('operator_in_specific_state'))
    operator_country_blackout_condition_grouping = df.groupBy("country", 'operator').agg(func.count(col("machine_id")).alias('events'),func.countDistinct(col("bar_id")).alias('bars_participating'))
    operator_country_blackout_condition_grouping = operator_country_blackout_condition_grouping.filter(func.col('events') >= num_of_events).filter(func.col('bars_participating') >= num_of_bars_participating)
    operator_country_blackout_condition_grouping = operator_country_blackout_condition_grouping.selectExpr('country as distinct_country', 'operator as distinct_operator')
    cond = [df.country == operator_country_blackout_condition_grouping.distinct_country,df.operator == operator_country_blackout_condition_grouping.distinct_operator]
    joined = df.join(operator_country_blackout_condition_grouping, cond, 'left')
    operator_country_blackout_status = joined.withColumn("diagnostic_status", when((func.datediff(joined.created_at, joined.last_connection) <= timediff_between_events) & (joined.distinct_country.isNull() == False), 'operator_in_specific_country').otherwise(joined.diagnostic_status))
    operator_country_blackout_status = operator_country_blackout_status.drop('distinct_country', 'distinct_operator')
    return (operator_country_blackout_status)




def country_down_blackout_cond(spark, df, num_of_events, num_of_bars_participating, num_of_operator_participating,timediff_between_events):
    country_down_condition_grouping = df.groupBy("country", 'operator').agg(func.count(col("machine_id")).alias('events'),func.countDistinct(col("bar_id")).alias('bars_participating'),func.countDistinct(col("operator")).alias('operator_participating'))
    country_down_condition_grouping = country_down_condition_grouping.filter(func.col('events') >= num_of_events).filter(func.col('bars_participating') >= num_of_bars_participating).filter(func.col('operator_participating') >= num_of_operator_participating)
    country_down_condition_grouping = country_down_condition_grouping.selectExpr('country as distinct_country','operator as distinct_operator')
    cond = [df.country == country_down_condition_grouping.distinct_country,df.operator == country_down_condition_grouping.distinct_operator]
    country_down_joined = df.join(country_down_condition_grouping, cond, 'left')
    country_down_blackout_status = country_down_joined.withColumn("diagnostic_status", when((func.datediff(country_down_joined.created_at,country_down_joined.last_connection) <= timediff_between_events) & \
    (country_down_joined.distinct_country.isNull() == False), 'country_down').otherwise(country_down_joined.diagnostic_status))
    country_down_blackout_status = country_down_blackout_status.drop('distinct_country', 'distinct_operator')
    return (country_down_blackout_status)