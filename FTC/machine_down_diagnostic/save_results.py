from pyspark.sql.functions import broadcast , lit , col , when
import pyspark.sql.functions as func



def insert_results(output_df,spark):
    output_df =output_df.repartition('nd')
    output_df.registerTempTable("dis_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.new_machine_down_events PARTITION(nd) SELECT
            venue_id,
            bar_id,
            bar_name,
            imei,
            machine_id,
            country,
            state,
            operator,
            last_connection,
            classification_status,
            diagnostic_status,
            created_at,
            modified_at,
            resolved,
            nd
            FROM dis_df""")


def reshaping_final_table(spark,lst_of_dfs,lst_of_columns):
    first = lst_of_dfs[0].select(lst_of_columns)
    for cond in range(len(lst_of_dfs)):
        lst_of_dfs[cond].groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
        temp = lst_of_dfs[cond].select(lst_of_columns)
        first = first.union(temp)
    first = first.dropDuplicates(lst_of_columns)
    return(first)



def into_csv(df,path):
    df.repartition(1).write.csv(path=path, mode='append', header=True)
    return('into csv completed')


def table_update(previous_output,curr_output,startdate,report_path):
    lst = previous_output.select('created_at').collect()
    if len(lst) > 0:
        maxval = str(max(lst)[0])
    else:
        maxval=str(startdate)
    print(str(previous_output.count())+' machines before updating table')
    print('maxval is '+str(maxval))
    prev_output = previous_output.withColumnRenamed('machine_id', 'prev_machine_id').filter(previous_output.created_at==maxval)
    curr_output = curr_output.withColumnRenamed('machine_id', 'curr_machine_id')
    cond =[prev_output.prev_machine_id==curr_output.curr_machine_id]
    joined_table = prev_output.join(curr_output,cond,'full').select('prev_machine_id','curr_machine_id')
    state_table = joined_table.withColumn('machine_state',when((joined_table.prev_machine_id==joined_table.curr_machine_id) ,'still_down')\
    .when(((joined_table.prev_machine_id.isNull()==False)&(joined_table.curr_machine_id.isNull()==True)),'solved_event')\
    .when(((joined_table.prev_machine_id.isNull()==True)&(joined_table.curr_machine_id.isNull()==False)),'new_event'))
    into_csv(state_table,report_path)
    print("report created and uploaded into "+ str(report_path))
    return('table_updated')