from .get_data import yarka_v5 , holidays , suspected_removal, classification_res
from .other_diagnostic_conditions import suspected_removal_cond , production_issue_cond, installation_issue_cond ,irrelevant_machine_cond ,reception_issue_cond ,daily_weekly_cond
from .operator_blackout_condition import operator_blackout_cond , country_blackout_cond , country_down_blackout_cond
from .seasonality_condition import  state_seasonality_cond ,country_seasonality_cond
from .save_results import insert_results , table_update ,  reshaping_final_table , into_csv
from ..machine_down_configuration.configuration import *
from datetime import datetime




############################################################   ACTIVATION   ###################################################


def diagnostic_process(spark,startdate):
    curr_nd = str(startdate[:10])
    print("%s - get data -  started" % datetime.now())
    machines_per_bar = suspected_removal(spark,startdate)
    holidays_df = holidays(spark,)
    production_batch_df = yarka_v5(spark)
    final_classification_res = classification_res(spark,startdate)
    print(str(final_classification_res.count())+' machines at the diagnostic part entry point')
    print("%s - get data -  ended" % datetime.now())

    print("%s - suspected removal condition -  started" % datetime.now())
    suspected_removal_res = suspected_removal_cond(final_classification_res, machines_per_bar, num_of_machines_arg)
    print("%s - suspected removal condition -  ended" % datetime.now())

    print("%s - operator blackout conditions -  started" % datetime.now())
    operator_blackout = operator_blackout_cond(spark, final_classification_res, suspected_removal_res[1], ob_num_of_events,ob_num_of_bars_participating, ob_timediff_between_events)
    #operator_blackout.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    country_blackout = country_blackout_cond(spark, operator_blackout, cb_num_of_events, cb_num_of_bars_participating,cb_timediff_between_events)
    #country_blackout.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    country_down = country_down_blackout_cond(spark, country_blackout, cd_num_of_events, cd_num_of_bars_participating,cd_num_of_operator_participating, cd_timediff_between_events)
    #country_down.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    print("%s - operator blackout conditions -  ended" % datetime.now())

    print("%s - production issue condition -  started" % datetime.now())
    production_issue = production_issue_cond(spark, operator_blackout, production_batch_df,bad_batch_tr)
    #production_issue.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    print("%s - production issue condition -  ended" % datetime.now())

    print("%s -seasonality conditions -  started" % datetime.now())
    state_seasonality = state_seasonality_cond(spark, final_classification_res, holidays_df, ss_num_of_events,ss_num_of_bars_participating)
    #state_seasonality[0].groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    country_seasonality = country_seasonality_cond(spark,state_seasonality[0],holidays_df,sc_num_of_events,sc_num_of_bars_participating,sc_num_of_states_participating)
    #country_seasonality.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    print("%s -seasonality conditions -  ended" % datetime.now())

    print("%s -other diagnostic conditions -  started" % datetime.now())
    installation_issue = installation_issue_cond(final_classification_res, inst_lst_of_issues)
    #installation_issue.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    reception_issue = reception_issue_cond(final_classification_res, recept_lst_of_issues)
    #reception_issue.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    final_daily_weekly = daily_weekly_cond(final_classification_res)
    #final_daily_weekly.groupBy("diagnostic_status").agg(func.count(col("machine_id"))).show()
    print("%s -other diagnostic conditions -  ended" % datetime.now())

    print("%s -reshape,update and save final results -  started" % datetime.now())
    ############################diagnostic df's that go into the save results process ###########################
    diagnostic_dfs = [reception_issue, installation_issue, final_daily_weekly, country_down, country_seasonality,suspected_removal_res[0], production_issue,state_seasonality[1]]
    ############################diagnostic df's that go into the save results process ###########################
    diganostic_combined = reshaping_final_table(spark, diagnostic_dfs, diagnostic_final_cols_list)
    diganostic_final = irrelevant_machine_cond(diganostic_combined, lst_of_conventiones)
    print(str(diganostic_final.count()) + ' machines at the diagnostic part end point')
    print("%s -insert final results -  started" % datetime.now())
    insert_results(diganostic_final, spark)
    print("%s -insert final results -  ended" % datetime.now())

    #prev_output = spark.sql("""select * from stg.new_machine_down_events """)
    #table_update(prev_output, diganostic_final, startdate,path)

    print("%s -reshape,update and save final results -  ended" % datetime.now())

    return(diganostic_final)





