from pyspark.sql.functions import broadcast , lit , col , when
import pyspark.sql.functions as func



def state_seasonality_cond(spark,df,holiday_df,num_of_events,num_of_bars_participating):
    seasonality_entry_point = df.filter(df.classification_status.isin('unplugged_intentionally'))
    seasonality_condition_grouping = seasonality_entry_point.groupBy("state").agg(func.count(col("machine_id"))\
    .alias('events'),func.countDistinct(col("bar_id")).alias('bars_participating'))
    seasonality_condition_grouping = seasonality_condition_grouping.selectExpr('state as distinct_state','events as events','bars_participating as bars_participating')
    cond = [seasonality_entry_point.state==seasonality_condition_grouping.distinct_state]
    joined = seasonality_entry_point.join(seasonality_condition_grouping,cond,'left')
    seasonality_joined = joined.drop('distinct_state')
    country_id_df = spark.sql("""select * from stg.countries""")
    #print('before join country id '+str(seasonality_joined.count()))
    seasonality_joined = seasonality_joined.join(country_id_df,country_id_df.name ==seasonality_joined.country,'left').withColumnRenamed('id','joined_country_id').drop('name').where(seasonality_joined.country.isNotNull())
    seasonality_joined = seasonality_joined.join(broadcast(holiday_df),(seasonality_joined.joined_country_id==holiday_df.country_id),'left')
    seasonality_joined = seasonality_joined.filter(seasonality_joined.country_id.isNull()==False).dropDuplicates(['machine_id'])
    #print('after join holidays df '+str(seasonality_joined.count()))
    relevant_seasonality_joined = seasonality_joined.withColumn("diagnostic_status",when((seasonality_joined.last_connection >= holiday_df.event_start)\
    & (joined.last_connection <= holiday_df.event_end)&(seasonality_joined.events >= num_of_events)\
    & (seasonality_joined.bars_participating >= num_of_bars_participating),'state_seasonality').otherwise(seasonality_joined.classification_status))
    region_seasonality_status = relevant_seasonality_joined.drop('joined_country_id','country_id','state_id','event_title','event_start','event_end','bars_participating','events')
    unplugged_intentionally = region_seasonality_status.filter(region_seasonality_status.diagnostic_status.isin('unplugged_intentionally'))
    return(region_seasonality_status,unplugged_intentionally)


def country_seasonality_cond(spark,df,holiday_df,num_of_events,num_of_bars_participating,num_of_states_participating):
    seasonality_condition_grouping = df.groupBy("country").agg(func.count(col("machine_id"))\
    .alias('events'),func.countDistinct(col("bar_id")).alias('bars_participating'),func.countDistinct(col("state")).alias('states_participating'))
    seasonality_condition_grouping = seasonality_condition_grouping.filter(func.col('events') >= num_of_events)\
    .filter(func.col('bars_participating') >= num_of_bars_participating).filter(func.col('states_participating') >= num_of_states_participating)
    seasonality_condition_grouping = seasonality_condition_grouping.selectExpr('country as distinct_country')
    cond = [df.country==seasonality_condition_grouping.distinct_country]
    joined = df.join(seasonality_condition_grouping,cond,'left')
    seasonality_joined = joined.drop('distinct_country')
    country_id_df = spark.sql("""select * from stg.countries""")
    seasonality_joined = seasonality_joined.join(country_id_df,country_id_df.name ==seasonality_joined.country,'left').withColumnRenamed('id','joined_country_id').drop('name')
    seasonality_joined = seasonality_joined.join(broadcast(holiday_df),(seasonality_joined.joined_country_id==holiday_df.country_id),'left').dropDuplicates(['machine_id'])
    #seasonality_joined.show()
    seasonality_full_status = seasonality_joined.withColumn("diagnostic_status",when((seasonality_joined.last_connection >= holiday_df.event_start)\
    & (seasonality_joined.last_connection <= holiday_df.event_end) & (seasonality_joined.event_start.isNull()==False),'country_seasonality').otherwise(seasonality_joined.diagnostic_status))
    return(seasonality_full_status)