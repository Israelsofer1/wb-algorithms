from pyspark.sql.functions import broadcast , lit , col , when
from operator import or_
import pyspark.sql.functions as func
from ..machine_down_configuration.configuration import *
from functools import *

def suspected_removal_cond(df,enrichment,num_of_machines):
    filtered_df = df.filter(df.classification_status.isin('unknown_issue'))
    agg_events_per_bar_1 = filtered_df.groupBy("bar_id").agg(func.count(col("machine_id")).alias('down_events'))
    agg_events_per_bar_2 = agg_events_per_bar_1.withColumnRenamed('bar_id', 'temp_bar_id')
    agg_events_per_bar_3 = agg_events_per_bar_2.join(enrichment,enrichment.bars_id == agg_events_per_bar_2.temp_bar_id).drop('bars_id')
    bars_with_status = agg_events_per_bar_3.withColumn('diagnostic_status',when((agg_events_per_bar_3.machines_per_bar == agg_events_per_bar_3.down_events)&(agg_events_per_bar_3.machines_per_bar >num_of_machines),'suspected_removal')\
    .otherwise('unknown_issue')).drop('machines_per_bar','down_events')
    suspected_removal_status = filtered_df.join(bars_with_status,df.bar_id ==bars_with_status.temp_bar_id).drop('temp_bar_id')
    suspected_removal_res = suspected_removal_status.filter(suspected_removal_status.diagnostic_status.isin('suspected_removal'))
    return(suspected_removal_res , suspected_removal_status)


def production_issue_cond(spark,df,enrichment,tr):
    filtered_df = df.filter(df.diagnostic_status.isin('other'))
    enrichment = enrichment.selectExpr('imei as enr_imei','batch_id as batch_id')
    cond = [filtered_df.imei == enrichment.enr_imei]
    df_with_batch = filtered_df.join(enrichment,cond,'left')
    bad_machine_per_batch = df_with_batch.groupBy('batch_id').agg(func.count(col("machine_id")).alias('down_machines_per_batch'))
    machine_per_batch = enrichment.groupBy('batch_id').agg(func.count(col("enr_imei")).alias('machines_per_batch'))\
    .selectExpr('machines_per_batch as machines_per_batch','batch_id as temp_batch_id')
    cond1 = [bad_machine_per_batch.batch_id == machine_per_batch.temp_batch_id]
    batches = bad_machine_per_batch.join(machine_per_batch,cond1)
    cond2 = [df_with_batch.batch_id == batches.temp_batch_id]
    final_join = df_with_batch.join(batches,cond2,'left')
    production_issue_status = final_join.withColumn("diagnostic_status",when((final_join.temp_batch_id.isNull()==False)&(final_join.down_machines_per_batch/final_join.machines_per_batch > tr),'production_issue')\
    .otherwise(final_join.classification_status)).drop('down_machines_per_batch','machines_per_batch','temp_batch_id','batch_id')
    return(production_issue_status)




def installation_issue_cond(df,lst_of_issue_id):
    installation_issue_status = df.withColumn('diagnostic_status',when(reduce(or_, (df.classification_status.isin(issue) for issue in lst_of_issue_id)) =='True','installation_issue')\
    .otherwise('other'))
    installation_issue_res = installation_issue_status.filter(installation_issue_status.diagnostic_status.isin('installation_issue'))
    return(installation_issue_res)



def reception_issue_cond(df,lst_of_reception_issues):
    reception_full_status = df.withColumn('diagnostic_status',when(reduce(or_, (df.classification_status.isin(issue) for issue in lst_of_reception_issues)) =='True','reception_issue')\
    .otherwise('not_relevant'))
    reception_issue_res = reception_full_status.filter(reception_full_status.diagnostic_status.isin('reception_issue'))
    return(reception_issue_res)



def daily_weekly_cond(df):
    daily = df.withColumn('diagnostic_status',when(df.classification_status =='daily_shutdown','daily_shutdown').otherwise('other'))
    daily_weekly = daily.withColumn('diagnostic_status',when(daily.classification_status =='weekly_shutdown','weekly_shutdown').otherwise(daily.classification_status))
    daily_weekly = daily_weekly.filter((daily_weekly.diagnostic_status.isin('daily_shutdown'))|(daily_weekly.diagnostic_status.isin('weekly_shutdown')))
    return(daily_weekly)


def irrelevant_machine_cond(df,lt_of_conventiones):
    irrelevant_machine_status = df.withColumn('diagnostic_status',when(reduce(or_, (df.bar_name.contains(conv) for conv in lst_of_conventiones)) =='True','irrelevant_machine')\
    .otherwise(df.diagnostic_status))
    return(irrelevant_machine_status)


