import pyspark.sql.functions as func


def suspected_removal(spark,startdate):
    query_res = spark.sql("""select bar_id as bars_id, count(id) as machines_per_bar from stg.machines m where bar_id > 0 and created_at <'""" + startdate + """'  group by bar_id """)
    return (query_res)


def yarka_v5(spark):
    query_res = spark.sql("""select * from stg.yarka_v5""")
    return(query_res)



def holidays(spark):
    query_res = spark.sql("""select * from stg.holidays_calendar""")
    return (query_res)


def split_tables(spark,final_data_enrichment):
    under_24h = final_data_enrichment.filter(func.col('24_h_from_installation').isin(1))   #left side of the tree
    after_24h = final_data_enrichment.filter(func.col('24_h_from_installation').isin(0))   #right side of the tree
    return(under_24h,after_24h)


def classification_res(spark,startdate):
    query_res = spark.sql("""select * from stg.class_machine_down_events where created_at ='""" + startdate + """'""")
    return(query_res)
