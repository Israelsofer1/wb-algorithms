from pyspark.sql import SparkSession
from datetime import datetime, timedelta
from .L1 import blu_merge_classification_job
from .L2 import blu_l2_classification_job
from .L3 import blu_l3
import pandas as pd
from pyspark.sql import functions as func
import itertools
from pyspark.sql.types import StringType


def get_entities(spark, bars):

    bars_ids = spark.sql(bars)
    bars_ids_lst = [str(x.bar_id) for x in bars_ids.select('bar_id').collect()]

    bars_str = ','.join(bars_ids_lst)

    entities_df = spark.sql("""select 
                (case when b.venue_id>0 then 1 else 0 end) as is_venue,
                 b.id as bar_id,
                 b.venue_id 
                 from stg.bars b where b.id in ("""+bars_str+""")""")

    entities_df = entities_df.withColumn("entity_id",
                                         func.when(func.col("is_venue") == 1,
                                                   func.concat(entities_df['venue_id'], func.lit(", -1")))
                                         .otherwise(func.concat(func.lit("-1, "), entities_df['bar_id'])))

    entities_lst = ["\"" + str(x.entity_id) + "\"" for x in entities_df.select('entity_id').collect()]
    return entities_lst


def create_new_run_id(spark,start_date, end_date, job_id):
    # run_id = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # data = [{'run_id': run_id, 'job_id': job_id, 'start_date': start_date, 'end_date': end_date, 'created_at': datetime.now()}]
    # new_id_df = spark.createDataFrame(data)
    metadata_df = spark.sql("""select max(run_id) as max_id from stg.blu_metadata_dates""")
    if metadata_df.first()[0] is None:
        new_id = 1
    else:
        new_id = metadata_df.first()[0] + 1
    print('Your run_id is ' + str(new_id))
    new_id_df = metadata_df
    new_id_df = new_id_df.withColumn("run_id",func.lit(new_id))
    new_id_df = new_id_df.withColumn("job_id", func.lit(job_id))
    new_id_df = new_id_df.withColumn("start_date", func.lit(start_date))
    new_id_df = new_id_df.withColumn("end_date", func.lit(end_date))
    new_id_df = new_id_df.withColumn("created_at", func.lit(datetime.now()))
    new_id_df = new_id_df.drop("max_id")
    new_id_df.createOrReplaceTempView("new_run_df")

    spark.sql("""INSERT INTO TABLE stg.blu_metadata_dates SELECT
                        run_id,
                        job_id,
                        start_date,
                        end_date,
                        created_at
                        from new_run_df where run_id not in (select run_id from stg.blu_metadata_dates)""")

    return new_id


def remove_par(entity_id_str):

    return entity_id_str.replace('"', '')


def pull_l1_groups(spark, run_id):
    entities_df = spark.sql(
        """select group, concat('"',entity_id,'"') as entity_id from stg.blu_merge_classification where run_id=""" +
        str(run_id))
    entities_df = entities_df.groupby('group').agg(func.collect_list('entity_id').alias("entities"))
    entities_df = entities_df.toPandas()
    return entities_df

def write_results_l1(spark, run_id):
    # if only l1 ran
    entites_df = spark.sql("""select group, concat('"',entity_id,'"') as entity_id from stg.blu_merge_classification""")
    entites_df = entites_df.groupby('group').agg(func.collect_list('entity_id').alias("entities"))

    output_pd = pd.DataFrame(columns=['entity_id', 'entity_id_like_you'])
    entites_pd = entites_df.toPandas()

    for index, row in entites_pd.iterrows():

        for perm in itertools.permutations(row['entities'], 2):
            output_pd = output_pd.append({'entity_id': perm[0], 'entity_id_like_you': perm[1]}, ignore_index=True)

    output_df = spark.createDataFrame(output_pd)
    entity_bars = spark.sql("""select concat('"',entity_id,'"') as entity_id, min(venue_id) as venue_id, min(bar_id) as bar_id
                                   from stg.bars_distances group by entity_id""")

    output_df = output_df.join(entity_bars, on='entity_id', how='inner')


    entity_bars = spark.sql("""select concat('"',entity_id,'"') as entity_id_like_you, min(venue_id) as venue_id_like_you, min(bar_id) as bar_id_like_you
                                   from stg.bars_distances group by entity_id""")
    output_df = output_df.join(entity_bars, on='entity_id_like_you', how='inner')
    output_df = output_df.withColumn("created_at", func.lit(datetime.now()))
    output_df = output_df.withColumn("run_id", func.lit(run_id))
    output_df = output_df.withColumn('entity_id_fixed', udf_remove_par(output_df['entity_id']))
    output_df = output_df.withColumn('entity_id_like_you_fixed', udf_remove_par(output_df['entity_id_like_you']))
    output_df = output_df.drop('entity_id')
    output_df = output_df.drop('entity_id_like_you')
    output_df = output_df.withColumnRenamed('entity_id_fixed', 'entity_id')
    output_df = output_df.withColumnRenamed('entity_id_like_you_fixed', "entity_id_like_you")
    output_df = output_df.filter((output_df.entity_id != output_df.entity_id_like_you))
    output_df.repartition(1).registerTempTable("l1_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_bars_like_you SELECT
                     entity_id,
                     venue_id,
                     bar_id,
                     entity_id_like_you,
                     venue_id_like_you,
                     bar_id_like_you,
                     winning,
                     created_at,
                     run_id
                     FROM l1_df where run_id not in (select run_id from stg.blu_bars_like_you)""")

    return

def write_results_l2(spark, run_id):
    # if only l1,l2 / l2 ran
    # TODO
    return

udf_remove_par = func.udf(remove_par, StringType())


def run(path, profile_name):

    spark = SparkSession \
        .builder \
        .appName("BLU_job") \
        .enableHiveSupport() \
        .getOrCreate()

    print("Read Config from s3")

    config_df = spark.read.format("json").option("multiline", "true").load(path)
    JobConfig = config_df.first().asDict()
    JobConfig['l1'] = JobConfig['l1'].asDict()
    JobConfig['l2'] = JobConfig['l2'].asDict()
    JobConfig['l3'] = JobConfig['l3'].asDict()
    JobConfig['l1']['weights'] = JobConfig['l1']['weights'].asDict()
    JobConfig['l2']['features'] = JobConfig['l2']['features'].asDict()
    feats_list = list(JobConfig['l2']['features'].keys())
    start_date = JobConfig['from_date']
    end_date = JobConfig['to_date']
    etl_days_back = int(JobConfig['etl_days_back'])
    if etl_days_back > 0:
        now = datetime.now()
        start_date = (now + timedelta(days=-etl_days_back)).strftime("%Y-%m-%d")
        end_date = (now + timedelta(days=-1)).strftime("%Y-%m-%d")

    bars = JobConfig['bars']
    entities = get_entities(spark=spark, bars=bars)
    run_id = create_new_run_id(spark=spark, start_date=start_date, end_date=end_date, job_id=JobConfig['job_id'])

    if JobConfig['l1']['active'] == 1:
        blu_merge_classification_job.run_process(entities=entities, run_id=run_id, spark=spark, JobConfig=JobConfig)
        if JobConfig['l2']['active'] == 1:
            l1_groups = pull_l1_groups(spark=spark, run_id=run_id)
            for index, group in l1_groups.iterrows():
                entity_list = group['entities']
                blu_l2_classification_job.run_process(spark=spark, run_id=run_id, entities=entity_list,
                                                      JobConfig=JobConfig, feat_list=feats_list, group=group[0])
        if JobConfig['l3']['active'] == 1:
            criteria = JobConfig['l3']['criteria']
            min_entities = JobConfig['l3']['min_entities']
            if JobConfig['l2']['active'] == 1:
                prev_layer = 'l2'
                l2_threshold = JobConfig['l3']['threshold']
            else:
                prev_layer = 'l1'
                l2_threshold = -1
            print("%s - BLU_L3 - wining poc job - started" % datetime.now())
            blu_l3.run_l3(spark=spark, run_id=run_id, criteria=criteria, prev_layer=prev_layer, threshold=min_entities,
                          l2_threshold=l2_threshold, job_id=JobConfig['job_id'], profile_name=profile_name)
            print("%s - BLU_L3 - wining poc job - ended" % datetime.now())

        if JobConfig['l3']['active'] == 0:
            if JobConfig['l2']['active'] == 1:
                write_results_l2(spark=spark, run_id=run_id)
            else:
                write_results_l1(spark=spark, run_id=run_id)
    else:
        # l1 not active
        blu_l2_classification_job.run_process(spark=spark, run_id=run_id, entities=entities, JobConfig=JobConfig,
                                              feat_list=feats_list)
        if JobConfig['l3']['active'] == 1:
            criteria = JobConfig['l3']['criteria']
            min_entities = JobConfig['1']['l3']['min_entities']
            prev_layer = 'l2'
            l2_threshold = JobConfig['l3']['threshold']
            print("%s - BLU_L3 - wining poc job - started" % datetime.now())
            blu_l3.run_l3(spark=spark, run_id=run_id, criteria=criteria, prev_layer=prev_layer, threshold=min_entities,
                          l2_threshold=l2_threshold, job_id=JobConfig['job_id'], profile_name=profile_name)
            print("%s - BLU_L3 - wining poc job - ended" % datetime.now())
        else:
            write_results_l2(spark=spark, run_id=run_id)

if __name__ == "__main__":
    pass