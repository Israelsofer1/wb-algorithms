from pyspark.sql import SparkSession
from datetime import datetime
import sys
from .import blu_daily_raw_l1_etl_process
from . import blu_daily_matrix_classification_etl


def main(spark, etl_days_back):

    print("%s - BLU daily etl - started - arguments length: %d" % (datetime.now(), len(sys.argv)))

    if int(etl_days_back) > 0:
        # print("Argument etl_days_back to run is : %s" % etl_days_back)
        # print("start matrix feats")
        blu_daily_matrix_classification_etl.process_etl_days_back(days_back=etl_days_back, spark=spark)
        # print("finish matrix feats, start l1 feats")
        blu_daily_raw_l1_etl_process.process_etl_days_back(days_back=etl_days_back, spark=spark)
    else:
        print("No argument etl_days_back to run, run 1 day")
        etl_days_back = 1
        # print('run ' + etl_days_back + " days")
        # print("start matrix feats")
        blu_daily_matrix_classification_etl.process_etl_days_back(days_back=etl_days_back, spark=spark)
        # print("finish matrix feats, start l1 feats")
        blu_daily_raw_l1_etl_process.process_etl_days_back(days_back=etl_days_back, spark=spark)

    print("%s - BLU raw_l1 etl process - ended" % datetime.now())


if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .appName("blu_daily_etl_test") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()
    main(spark)
