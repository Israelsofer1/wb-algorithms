from datetime import datetime
from pyspark.sql import functions as func
from pyspark.sql import SparkSession


def pull_bars_data(spark, country_id):
    bars_data_df = spark.sql("""select (case when b.venue_id is not null and b.venue_id<>0 then 1 else 0 end) as is_venue,
                  case when b.venue_id is NULL then b.id else NULL end as bar_id,
                  b.venue_id,
                  b.lat,
                  b.lng
                  from stg.bars as b where country_id = """ + str(country_id))
    bars_data_df = bars_data_df.withColumn("entity_id",
                                            func.when(func.col("is_venue") == 1,
                                            func.concat(bars_data_df['venue_id'], func.lit(", -1")))
                                            .otherwise(func.concat(func.lit("-1, "), bars_data_df['bar_id'])))

    bars_data_df = bars_data_df.groupby('entity_id').agg(
                                                    func.first(bars_data_df['bar_id']).alias('bar_id'),
                                                    func.first(bars_data_df['venue_id']).alias('venue_id'),
                                                    func.first(bars_data_df['lat']).alias('lat'),
                                                    func.first(bars_data_df['lng']).alias('lng'))
    return bars_data_df


def process_distances(entity_df):
    entity_df = entity_df.join(entity_df.withColumnRenamed('entity_id', '2nd_entity_id')
                                        .withColumnRenamed('lat', 'lat_2')
                                        .withColumnRenamed('bar_id', '2nd_bar_id')
                                        .withColumnRenamed('venue_id', '2nd_venue_id')
                                        .withColumnRenamed('lng', 'lng_2'))
    # entity_df = entity_df.filter((func.col('entity_id') != func.col('2nd_entity_id')))
    entity_df = entity_df.dropna(subset=['entity_id', '2nd_entity_id'])
    entity_df = entity_df.withColumn("distance", func.acos(func.sin(func.toRadians(func.col('lat'))) *
                                                           func.sin(func.toRadians(func.col('lat_2'))) +
                                                           func.cos(func.toRadians(func.col('lat'))) *
                                                           func.cos(func.toRadians(func.col('lat_2'))) *
                                                           func.cos(func.toRadians(func.col('lng')) -
                                                           func.toRadians(func.col('lng_2')))) *
                                                           func.lit(6371.0))
    entity_df = entity_df.drop("lat")
    entity_df = entity_df.drop("lng")
    entity_df = entity_df.drop("lat_2")
    entity_df = entity_df.drop("lng_2")
    entity_df = entity_df.withColumn("created_at", func.lit(datetime.now()))
    return entity_df


def insert_results(output_df, spark, country_id):
    output_df = output_df.withColumn("country_id", func.lit(country_id))
    output_df.repartition(1).registerTempTable("dis_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.bars_distances SELECT 
                    entity_id,
                    venue_id,
                    bar_id,
                    2nd_entity_id,
                    2nd_venue_id,
                    2nd_bar_id,
                    distance,
                    created_at,
                    country_id
                    FROM dis_df""")


def run(spark, country_id):
    distance_df = pull_bars_data(spark, country_id)
    distance_df = process_distances(distance_df)
    insert_results(distance_df, spark, country_id)


if __name__ == '__main__':
    spark = SparkSession \
        .builder \
        .appName("bars_distances") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()
    run(spark, country_id=195)
