from pyspark.sql import SparkSession
from datetime import datetime, timedelta
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, IntegerType
from ..Config import blu_matrix_features_config as f_config

# # define paths for redshift, hive
# redshift_url = jobs_config.redshift_url
# redshift_temp_dir = jobs_config.redshift_temp_dir
# redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
# hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name

# spark = SparkSession \
#     .builder \
#     .appName("blu_matrix_classification_daily_etl") \
#     .config("hive.exec.dynamic.partition", "true") \
#     .config("hive.exec.dynamic.partition.mode", "nonstrict") \
#     .enableHiveSupport() \
#     .getOrCreate()


# pos
def pull_pos_data(spark, from_nd, until_nd):
    pos_data_df = spark.sql \
        ("""select (case when b.venue_id is not null and b.venue_id<>0 then 1 else 0 end) as is_venue,
                     b.id as bar_id,
                     b.venue_id,
                     o.order_id,
                     o.order_time,
                     o.order_time_closed,
                     o.order_item_time,
                     o.order_item_id,
                     o.title,
                     o.category_id,
                     o.beer_brand_id,
                     o.beer_serving_type_id,
                     o.beer_volume,
                     o.item_qty as qty,
                     o.sales_before_tax,
                     o.sales_inc_tax,
                     o.status,
                     o.nd,
                     hour(coalesce(o.order_item_time, o.order_time)) as item_hour,
                     bb.brewery_id,coalesce(bbb.is_competitor,0) as iscompetitor
                                from stg.pos_l1 o
                                join stg.bars b on b.id = o.bar_id
                                left join stg.pos_types_of_orders as pt on pt.order_id = o.order_id
                                left join stg.breweries_bars bb on bb.bar_id = o.bar_id
                                left join stg.breweries br on br.id = bb.brewery_id
                                left join stg.breweries_brands bbb on bbb.brand_id = o.beer_brand_id and bbb.brewery_id = bb.brewery_id
                                where o.nd between '""" + from_nd + """' and '""" + until_nd + """' and
                                o.category_id in (1,2,3,4,5,6) and
                                (pt.type_id  not in (1,4,13,14,15,16,17,18,19) or pt.type_id is NULL) and
                                br.status = 1""")

    pos_data_df = pos_data_df.withColumn("entity_id",
                                         func.when(func.col("is_venue") == 1,
                                                   func.concat(pos_data_df['venue_id'], func.lit(", -1")))
                                         .otherwise(func.concat(func.lit("-1, "), pos_data_df['bar_id'])))
    pos_data_df.cache()
    return pos_data_df


# bar_data
def create_duration(time_open, time_close):
    timeFmt = "yyyy-MM-dd' 'HH:mm:ss"
    timeDiff = (func.unix_timestamp('order_end', format=timeFmt)
                - func.unix_timestamp('order_start', format=timeFmt))
    timeDiff = 0

    return timeDiff


def pull_bar_metadata(spark):
    bar_data_df = spark.sql \
        ("""select
        (case when b.venue_id is not null and b.venue_id<>0 then 1 else 0 end) as is_venue,
        b.venue_id,
        b.id as bar_id,
        bar_type_id,bar_type,wh.weekday,wh.hour_open, wh.hour_close,ln.num_lines as cnt_taps
        from stg.bars b join 
        (select w.bar_id,w.weekday,hour(time_open) as hour_open,hour(time_close) as hour_close
        from stg.bars_working_hours w) wh on
        b.id = wh.bar_id left join (select m.bar_id,count(distinct l.id) as num_lines from stg.machines m
        join stg.lines l on l.machine_id = m.id
        where l.status=1 and line_num<17 group by m.bar_id) ln on ln.bar_id = b.id""")

    bar_data_df = bar_data_df.withColumn("entity_id", func.when(func.col("is_venue") == 1,
                                                                func.concat(bar_data_df['venue_id'],
                                                                            func.lit(", -1")))
                                         .otherwise(
        func.concat(func.lit("-1, "), bar_data_df['bar_id'])))
    bar_data_df.cache()
    return bar_data_df


def create_avg_check_duration(pos_data):
    timefmt = "yyyy-MM-dd' 'HH:mm:ss"
    timediff = ((func.unix_timestamp('order_time_closed', format=timefmt)
                 - func.unix_timestamp('order_time', format=timefmt)) / 60)

    agg_orders_df = pos_data.groupby('nd', 'entity_id', 'order_id').agg(
        func.first(pos_data['order_time']).alias('order_time'), func.first(pos_data['order_time_closed'])
            .alias('order_time_closed'))
    duration_df = agg_orders_df.withColumn("Duration", func.round(timediff, 3))
    agg_duration_df = duration_df.groupby('entity_id', 'nd').agg(func.sum('duration').alias('total_check_duration'))
    agg_num_of_orders_df = pos_data.groupby('nd', 'entity_id').agg(
        func.countDistinct(pos_data['order_id']).alias('number_of_orders'))

    joined_df = agg_num_of_orders_df.join(agg_duration_df, on=['entity_id', 'nd'], how='inner')

    return joined_df


def create_count_beer_orders(pos_data):
    agg_orders_df = pos_data.groupby('nd', 'entity_id', 'order_id').agg(
        func.first(pos_data['beer_volume']).alias('beer_volume')).filter(func.col('beer_volume') > 0)
    agg_num_of_beer_orders_df = agg_orders_df.groupby('nd', 'entity_id').agg(
        func.countDistinct(agg_orders_df['order_id']).alias('number_of_beer_orders'))
    return agg_num_of_beer_orders_df


def create_sum_features(pos_data):
    entity_agg = pos_data.groupBy(pos_data['entity_id'], pos_data['nd'], pos_data['category_id'],
                                  pos_data['iscompetitor'], pos_data['beer_serving_type_id']).agg(
        func.sum(func.col('sales_before_tax')).alias('total_revenue'),
        func.when(func.col("category_id") == 1, func.sum(func.col('sales_before_tax'))).alias('total_1_revenue'),
        func.when(func.col("category_id") == 2, func.sum(func.col('sales_before_tax'))).alias('total_2_revenue'),
        func.when(func.col("category_id") == 3, func.sum(func.col('sales_before_tax'))).alias('total_3_revenue'),
        func.when(func.col("category_id") == 4, func.sum(func.col('sales_before_tax'))).alias('total_4_revenue'),
        func.when(func.col("category_id") == 5, func.sum(func.col('sales_before_tax'))).alias('total_5_revenue'),
        func.when(func.col("category_id") == 6, func.sum(func.col('sales_before_tax'))).alias('total_6_revenue'),
        func.when(((func.col('category_id') == 1) & (func.col('iscompetitor') == 0)),
                  func.sum(func.col('sales_before_tax'))).alias('total_abi_revenue'),
        func.sum(func.col('qty')).alias('total_qty'),
        func.when(func.col("category_id") == 1, func.sum(func.col('qty'))).alias('total_1_qty'),
        func.when(func.col("category_id") == 2, func.sum(func.col('qty'))).alias('total_2_qty'),
        func.when(func.col("category_id") == 3, func.sum(func.col('qty'))).alias('total_3_qty'),
        func.when(func.col("category_id") == 4, func.sum(func.col('qty'))).alias('total_4_qty'),
        func.when(func.col("category_id") == 5, func.sum(func.col('qty'))).alias('total_5_qty'),
        func.when(func.col("category_id") == 6, func.sum(func.col('qty'))).alias('total_6_qty'),
        func.when(((func.col('category_id') == 1) & (func.col('iscompetitor') == 0)), func.sum(func.col('qty'))).alias(
            'total_abi_qty'),
        func.when(func.col("iscompetitor") == 0, func.sum(func.col('beer_volume'))).alias('total_abi_volume'),
        func.sum(func.col('beer_volume')).alias('total_volume'),
        func.when(func.col("beer_serving_type_id") == 2, func.sum(func.col('beer_volume'))).alias('draught_volume'),
        func.when(func.col("beer_serving_type_id").isin(1, 3), func.sum(func.col('beer_volume'))).alias(
            'package_volume'))

    entity_feats_agg = entity_agg.groupBy(entity_agg['entity_id'], entity_agg['nd']).agg(
        func.round(func.sum(func.col('total_revenue')), 3).alias('total_revenue'),
        func.round(func.sum(func.col('total_1_revenue')), 3).alias('total_1_revenue'),
        func.round(func.sum(func.col('total_2_revenue')), 3).alias('total_2_revenue'),
        func.round(func.sum(func.col('total_3_revenue')), 3).alias('total_3_revenue'),
        func.round(func.sum(func.col('total_4_revenue')), 3).alias('total_4_revenue'),
        func.round(func.sum(func.col('total_5_revenue')), 3).alias('total_5_revenue'),
        func.round(func.sum(func.col('total_6_revenue')), 3).alias('total_6_revenue'),
        func.round(func.sum(func.col('total_abi_revenue')), 3).alias('total_abi_revenue'),
        func.round(func.sum(func.col('total_qty')), 3).alias('total_qty'),
        func.round(func.sum(func.col('total_1_qty')), 3).alias('total_1_qty'),
        func.round(func.sum(func.col('total_2_qty')), 3).alias('total_2_qty'),
        func.round(func.sum(func.col('total_3_qty')), 3).alias('total_3_qty'),
        func.round(func.sum(func.col('total_4_qty')), 3).alias('total_4_qty'),
        func.round(func.sum(func.col('total_5_qty')), 3).alias('total_5_qty'),
        func.round(func.sum(func.col('total_6_qty')), 3).alias('total_6_qty'),
        func.round(func.sum(func.col('total_abi_qty')), 3).alias('total_abi_qty'),
        func.round(func.sum(func.col('total_abi_volume')), 3).alias('total_abi_vol'),
        func.round(func.sum(func.col('total_volume')), 3).alias('total_vol'),
        func.round(func.sum(func.col('draught_volume')), 3).alias('total_draught_vol'),
        func.round(func.sum(func.col('package_volume')), 3).alias('total_package_vol'))

    return entity_feats_agg


def create_peak_hour(pos_data, only_beer):
    if only_beer == 1:
        entity_vol_hour_agg = pos_data.groupBy(pos_data['entity_id'], pos_data['nd'], pos_data['category_id'],
                                               pos_data['item_hour'].alias('beer_vol_peak_hour')).agg(
            func.round(func.sum(pos_data['beer_volume']), 3).alias('consumption'))
        max_hour = entity_vol_hour_agg.groupBy(entity_vol_hour_agg['entity_id'], pos_data['nd'],
                                               entity_vol_hour_agg['category_id']).agg(
            func.max('consumption').alias('consumption')).filter(func.col('category_id') == 1)
        entity_vol_hour_agg = entity_vol_hour_agg.join(max_hour, on=['entity_id', 'nd', 'consumption'], how='inner')
        entity_vol_hour_agg = entity_vol_hour_agg.drop('category_id')
        entity_vol_hour_agg = entity_vol_hour_agg.drop('consumption')
        entity_vol_hour_agg = entity_vol_hour_agg.groupBy(entity_vol_hour_agg['entity_id'],
                                                          entity_vol_hour_agg['nd']).agg(
            func.max(func.col('beer_vol_peak_hour')).alias('beer_vol_peak_hour'))

        return entity_vol_hour_agg

    else:
        entity_sales_hour_agg = pos_data.groupBy(pos_data['entity_id'], pos_data['nd'],
                                                 pos_data['item_hour'].alias('sales_peak_hour')).agg(
            func.round(func.sum(pos_data['sales_before_tax']), 3).alias('consumption'))
        max_hour = entity_sales_hour_agg.groupBy(entity_sales_hour_agg['entity_id'], pos_data['nd']).agg(
            func.max('consumption').alias('consumption'))
        entity_sales_hour_agg = entity_sales_hour_agg.join(max_hour, on=['entity_id', 'nd', 'consumption'], how='inner')
        entity_sales_hour_agg = entity_sales_hour_agg.drop('consumption')

        entity_sales_hour_agg = entity_sales_hour_agg.groupBy(entity_sales_hour_agg['entity_id'],
                                                              entity_sales_hour_agg['nd']).agg(
            func.max(func.col('sales_peak_hour')).alias('sales_peak_hour'))

        return entity_sales_hour_agg


def group_bar_metadata(bar_metadata):
    entity_metadata = bar_metadata.groupBy(bar_metadata['entity_id'], bar_metadata['weekday']).agg(
        func.first('venue_id').alias('venue_id'),
        func.first('bar_id').alias('bar_id'),
        func.first('bar_type_id').alias('bar_type_id'),
        func.first('hour_open').alias('bar_open_hour'),
        func.first('hour_close').alias('bar_close_hour'),
        func.sum('cnt_taps').alias('cnt_taps'))
    return entity_metadata


def create_pos_open_time(pos_data):
    entity_pos_open_time_agg = pos_data.groupBy(pos_data['entity_id'], pos_data['nd']).agg(
        func.min(pos_data['order_time']).alias('pos_open_time'))

    return entity_pos_open_time_agg


def create_pos_close_time(pos_data):
    entity_pos_close_time_agg = pos_data.groupBy(pos_data['entity_id'], pos_data['nd']).agg(
        func.max(pos_data['order_time_closed']).alias('max_close_time_order'),
        func.max(pos_data['order_time']).alias('max_time_order'))
    entity_pos_close_time_agg = entity_pos_close_time_agg.withColumn('pos_close_time',
                                                                     func.greatest(entity_pos_close_time_agg[
                                                                                       'max_close_time_order'],
                                                                                   entity_pos_close_time_agg[
                                                                                       'max_time_order']))
    entity_pos_close_time_agg = entity_pos_close_time_agg.drop('max_close_time_order')
    entity_pos_close_time_agg = entity_pos_close_time_agg.drop('max_time_order')
    return entity_pos_close_time_agg


def create_abi_bar_type(bar_type_id, bar_type_id_arr):
    if bar_type_id in bar_type_id_arr:
        return 1
    else:
        return 0


def create_features_daily_data_table(spark):
    spark.sql("""create external table if not exists stg.blu_daily_matrix_features (
                        entity_id string,
                        venue_id bigint,
                        bar_id bigint,
                        total_revenue float,
                        total_1_revenue float,
                        total_2_revenue float,
                        total_3_revenue float,
                        total_4_revenue float,
                        total_6_revenue float,
                        total_abi_revenue float,
                        total_qty float,
                        total_1_qty float,
                        total_2_qty float,
                        total_3_qty float,
                        total_4_qty float,
                        total_6_qty float,
                        total_abi_qty float,
                        total_vol float,
                        total_draught_vol float,
                        total_package_vol float,
                        total_abi_vol float,
                        total_check_duration float,
                        number_of_orders bigint,
                        number_of_beer_orders bigint,
                        beer_vol_peak_hour integer,
                        sales_peak_hour integer,
                        pos_open_time timestamp,
                        pos_close_time timestamp,
                        bar_open_hour integer,
                        bar_close_hour integer,
                        cnt_taps integer,
                        abi_not_highend integer,
                        abi_highend integer,
                        abi_sports_active integer,
                        abi_nightlife integer,
                        created_at timestamp)
                        PARTITIONED BY(nd DATE) STORED AS ORC LOCATION 's3://wb-emr-hive-stg-dev/blu_daily_matrix_features' tblproperties ('orc.compress'='ZLIB')""")


def process_features(spark, from_nd, until_nd):
    abi_not_highend = f_config.features['abi_not_highend']['parameters']['bar_type_id']
    abi_highend = f_config.features['abi_highend']['parameters']['bar_type_id']
    abi_sports_active = f_config.features['abi_sports_active']['parameters']['bar_type_id']
    abi_nightlife = f_config.features['abi_nightlife']['parameters']['bar_type_id']

    # abi_not_highend = [21, 1, 5, 8, 10, 6, 22, 25, 4, 2]
    # abi_highend = [9, 16, 18, 20]
    # abi_sports_active = [15, 19]
    # abi_nightlife = [11, 24, 3, 17]

    # create_features_daily_data_table()
    pos_data = pull_pos_data(spark, from_nd, until_nd)
    bar_metadata = pull_bar_metadata(spark)
    print('pull')
    sum_total_df = create_sum_features(pos_data)
    sum_total_df.cache()
    duration_df = create_avg_check_duration(pos_data)
    duration_df.cache()

    count_beer_orders_df = create_count_beer_orders(pos_data)
    count_beer_orders_df.cache()

    peak_hour_beer_df = create_peak_hour(pos_data, 1)
    peak_hour_beer_df.cache()

    peak_hour_df = create_peak_hour(pos_data, 0)
    peak_hour_df.cache()

    pos_open_time_df = create_pos_open_time(pos_data)
    pos_open_time_df.cache()

    pos_close_time_df = create_pos_close_time(pos_data)
    pos_close_time_df.cache()

    entity_metadata = group_bar_metadata(bar_metadata)

    # entity_metadata = entity_metadata.withColumn('abi_not_highend',
    #                                              udf_create_abi_bar_type(entity_metadata['bar_type_id'], func.array(
    #                                                  [func.lit(x) for x in abi_not_highend])))
    # entity_metadata = entity_metadata.withColumn('abi_highend',
    #                                              udf_create_abi_bar_type(entity_metadata['bar_type_id'], func.array(
    #                                                  [func.lit(x) for x in abi_highend])))
    # entity_metadata = entity_metadata.withColumn('abi_sports_active',
    #                                              udf_create_abi_bar_type(entity_metadata['bar_type_id'], func.array(
    #                                                  [func.lit(x) for x in abi_sports_active])))
    # entity_metadata = entity_metadata.withColumn('abi_nightlife',
    #                                              udf_create_abi_bar_type(entity_metadata['bar_type_id'], func.array(
    #                                                  [func.lit(x) for x in abi_nightlife])))
    # entity_metadata.cache()

    all_bars_df = entity_metadata.groupby('entity_id').agg(func.sum('bar_id')).select('entity_id')
    dates_df = spark.sql(
        """select * from (select distinct nd, case when date_format(nd, 'u') = 7 then 0 else date_format(nd, 'u') 
          end as weekday from stg.pos_l1 where nd between '""" + from_nd + """' and '""" + until_nd + """')""")
    all_bars_df = all_bars_df.crossJoin(dates_df)
    # print('before joins')
    df_list = [sum_total_df, duration_df, peak_hour_beer_df, peak_hour_df, pos_open_time_df, pos_close_time_df, count_beer_orders_df]
    df_final = all_bars_df
    for df_next in df_list:
        df_final = df_final.join(df_next, on=['entity_id', 'nd'], how='left')
    df_final.cache()
    # print('after joins')
    # df_final.show()
    df_final = df_final.join(entity_metadata, on=['entity_id', 'weekday'], how='left')
    df_final = df_final.na.fill(0)

    df_final = df_final.drop('bar_type_id')
    df_final = df_final.withColumn("abi_not_highend", func.lit(-1))
    df_final = df_final.withColumn("abi_highend", func.lit(-1))
    df_final = df_final.withColumn("abi_sports_active", func.lit(-1))
    df_final = df_final.withColumn("abi_nightlife", func.lit(-1))
    df_final = df_final.withColumn("created_at", func.lit(datetime.now()))

    # df_final.printSchema()

    df_final.repartition('nd').registerTempTable("l2_feat_df")
    # print('insert')
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_daily_matrix_features PARTITION(nd) SELECT 
                entity_id,
                venue_id,
                bar_id,
                total_revenue,
                total_1_revenue,
                total_2_revenue,
                total_3_revenue,
                total_4_revenue,
                total_5_revenue,
                total_6_revenue,
                total_abi_revenue,
                total_qty,
                total_1_qty,
                total_2_qty,
                total_3_qty,
                total_4_qty,
                total_5_qty,
                total_6_qty,
                total_abi_qty,
                total_vol ,
                total_draught_vol,
                total_package_vol,
                total_abi_vol,
                total_check_duration,
                number_of_orders,
                number_of_beer_orders,
                beer_vol_peak_hour,
                sales_peak_hour,
                pos_open_time,
                pos_close_time,
                bar_open_hour,
                bar_close_hour,
                cnt_taps,
                abi_not_highend,
                abi_highend,
                abi_sports_active,
                abi_nightlife,
                created_at,
                nd FROM l2_feat_df""")


udf_create_duration = func.udf(create_duration, FloatType())
udf_create_abi_bar_type = func.udf(create_abi_bar_type, IntegerType())


def process_etl_days_back(days_back, spark):
    print("%s - BLU_matrix_classification_daily_etl start" % datetime.now())
    days_back = int(days_back)
    end_nd = (datetime.today() - timedelta(1)).strftime('%Y-%m-%d')
    start_nd = (datetime.today() - timedelta(days_back)).strftime('%Y-%m-%d')
    process_features(spark=spark, from_nd=start_nd, until_nd=end_nd)
    print("%s - BLU_matrix_classification_daily_etl end" % datetime.now())


