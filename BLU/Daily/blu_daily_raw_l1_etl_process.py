from datetime import datetime, timedelta
import jobs_config
from pyspark.sql import functions as func

redshift_url = jobs_config.redshift_url
redshift_temp_dir = jobs_config.redshift_temp_dir
redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name


def process_etl_days_back(days_back, spark):
    print("%s - BLU_raw_l1_etl_process start" % datetime.now())
    days_back = int(days_back)
    end_nd = (datetime.today() - timedelta(1)).strftime('%Y-%m-%d')
    start_nd = (datetime.today() - timedelta(days_back)).strftime('%Y-%m-%d')
    calculate_l1_data(from_nd=start_nd, until_nd=end_nd, spark=spark)

    print("%s - BLU_raw_l1_etl_process end" % datetime.now())


def calculate_l1_data(from_nd, until_nd,spark):
    """
    calculate raw data for l1 layer save result in blu_l1_raw_data table
    :param nd: tha day to run (string)
    :param spark: spark session
    :return:
    """
    l1_daily_df = spark.sql("""select entity_id, 
                               min(venue_id) as venue_id,
                               min(bar_id) as bar_id,
                               category_id,
                               count(distinct num_orders) as num_orders,
                               sum(qty) as qty,
                               sum(revenue) as revenue,
                               o.nd as nd
                               from 
                               (select concat(coalesce(b.venue_id,'-1'),', ',
                                coalesce((case when b.venue_id is NULL then o.bar_id else NULL end),'-1')) as entity_id,
                                b.venue_id as venue_id,
                                case when b.venue_id is NULL then o.bar_id else NULL end as bar_id,
                                o.nd, 
                                o.category_id,
                                o.order_id as num_orders,
                                o.item_qty as qty,
                                o.sales_before_tax as revenue
                                from stg.pos_l1 o left join stg.pos_types_of_orders as pt on pt.order_id = o.order_id
                                join stg.bars b on b.id = o.bar_id
                                where o.category_id in (1,2,3,6) and
                                (pt.type_id  not in (1,4,13,14,15,16,17,18,19) or pt.type_id is NULL) and
                                o.nd  between '""" + from_nd + """' and '"""+until_nd+"""') o 
                                group by o.entity_id,o.nd,o.category_id """)

    l1_daily_df = l1_daily_df.withColumn("created_at", func.lit(datetime.now()))
    l1_daily_df.cache()
    l1_daily_df.repartition("nd").registerTempTable("l1_daily_dff")

    spark.sql("""INSERT OVERWRITE TABLE stg.blu_l1_raw_data PARTITION(nd)
                 SELECT 
                 entity_id,
                 venue_id,
                 bar_id,
                 category_id,
                 num_orders,
                 qty,
                 revenue,
                 created_at,
                 nd FROM l1_daily_dff""")

    spark.catalog.dropTempView("l1_daily_dff")

    # l1_daily_df.write \
    #     .format("com.databricks.spark.redshift") \
    #     .option("url", redshift_url) \
    #     .option("dbtable", "stg.blu_l1_raw_data") \
    #     .option("tempdir", redshift_temp_dir) \
    #     .option("aws_iam_role", redshift_aws_iam_role) \
    #     .mode("overwrite") \
    #     .save()
    #



# def preprocess_etl(nd, spark):
#     print("creating raw data for " + str(nd))
#     create_l1_table(spark)
#     pull_order_ids(spark, nd=nd)
#     calculate_l1_data(spark)
#     print("%s - BLU_raw_l1_etl - process completed" % datetime.now())

# def pull_order_ids(spark, nd):
#     """
#     import the order ids to process in certain days (after di)
#     :param nd('YYYY-MM-DD'): the nd to pull order ids from
#     :return:df of clean order_ids to process
#     """
#     nd = str(nd)
#     clean_order_id_df = spark.sql("""select id from stg.pos_orders o
#                                       left join stg.pos_types_of_orders a
#                                       on o.id = a.order_id
#                                       where (a.type_id  not in (1,4,13,14,15,16,17,18,19) or a.type_id is NULL) and
#                                        o.nd = '""" + nd + """'""")
#
#     clean_order_id_df.registerTempTable("clean_order_id")


# def create_l1_table(spark):
#     # create l1 raw data table
#     print("trying to create blu_l1_raw_data table")
#     spark.sql("""create external table if not exists stg.blu_l1_raw_data (
#                         bar_id bigint,
#                         category integer,
#                         num_orders bigint,
#                         qty double,
#                         revenue double)
#                         PARTITIONED BY(nd DATE) STORED AS ORC LOCATION 's3://wb-emr-hive-stg-dev/blu_l1_raw_data' tblproperties ('orc.compress'='ZLIB')""")
#     print("stg.blu_l1_raw_data was created in S3")


