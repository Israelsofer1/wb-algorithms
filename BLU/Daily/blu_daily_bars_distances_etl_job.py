from pyspark.sql import SparkSession
from datetime import datetime
import sys
from . import blu_daily_bars_distances_etl


def main(spark, country_ids):
    for country_id in country_ids:
        print("%s - BLU daily bars distances etl - %s - started - arguments length: %d" % (datetime.now(), country_id, len(sys.argv)))
        blu_daily_bars_distances_etl.run(spark, country_id)
        print("%s - BLU daily bars distances etl - %s - ended" % (datetime.now(), country_id))


if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("blu_daily_bars_distances_etl_test") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()

    main(spark)
