from pyspark.sql import SparkSession
from datetime import datetime
from . import blu_matrix_classification_shares
from . import blu_matrix_classification
import sys


def run_process(entities, run_id, spark, JobConfig, feat_list, group):

    print("%s - BLU - matrix classification - job  started - arguments length: %d" % (datetime.now(), len(sys.argv)))
    feats = JobConfig['l2']['features']

    print("%s - BLU_L2 matrix classification shares  started" % (datetime.now()))
    blu_matrix_classification_shares.process_features(spark=spark, run_id=run_id, entity_list=entities, feat_list=feat_list, group=group)
    print("%s - BLU_L2 matrix classification shares  ended" % (datetime.now()))

    print("%s - BLU_L2  matrix classification  started" % (datetime.now()))
    blu_matrix_classification.run_process(spark=spark, run_id=run_id, feats=feats, group=group)
    print("%s - BLU_L2  matrix classification   ended" % (datetime.now()))

    print("%s - BLU - matrix classification - job  ended" % datetime.now())

    print("%s - BLU_L2  - ended" % datetime.now())




# if __name__ == "__main__":
#     print("%s - BLU_l1 l2 classification -  started - arguments length: %d" % (datetime.now(), len(sys.argv)))
#     spark = SparkSession \
#         .builder \
#         .appName("BLU_l2 classification - merge") \
#         .enableHiveSupport() \
#         .getOrCreate()
#     job = 1








