from pyspark.sql import SparkSession
from datetime import datetime
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, StructType, StructField, StringType, ArrayType
import numpy as np

from ..Config import blu_matrix_features_config as f_config

mtrxschema = ArrayType(
    StructType([StructField("score", FloatType(), False), StructField("entity", StringType(), False)]))


def norm_df(df_agg, feat_dict):
    col_names = df_agg.schema.names
    for col_name in col_names:
        if col_name in feat_dict.keys():
            if feat_dict[col_name]['norm_type'] == 'max_min':
                maxval = df_agg.agg({col_name: "max"}).collect()[0]["max(" + col_name + ")"]
                minval = df_agg.agg({col_name: "min"}).collect()[0]["min(" + col_name + ")"]
                df_agg = df_agg.withColumn(str(col_name), (df_agg[col_name] - minval) / (maxval - minval))
            elif feat_dict[col_name]['norm_type'] == 'robust':
                q1, q3 = df_agg.approxQuantile(col_name, [0.25, 0.75], 0)
                df_agg = df_agg.withColumn(col_name, (func.col(col_name) - q1) / (q3 - q1))
                # TODO: check if querying hive for max/min/quantiles etc. is faster than collect
    return df_agg


def pull_data(spark, run_id, features, is_distance, group, dist_scaler):
    feats_str = ",".join(features)
    entities_features_df = spark.sql("""select entity_id,venue_id,bar_id,run_id,""" + feats_str + """ 
                                        from stg.blu_matrix_features_shares
                                        where run_id =""" + str(run_id) + """ 
                                        and group = '""" + str(group) + """'""")
    entities_df = entities_features_df.select('entity_id').dropDuplicates()
    distance_df = spark.sql("""select * from stg.bars_distances""")

    # TODO: limit to country_id?

    distance_df = distance_df.join(entities_df, on='entity_id', how='inner')
    entities_df = entities_df.withColumnRenamed('entity_id', '2nd_entity_id')
    distance_df = distance_df.join(entities_df, on='2nd_entity_id', how='inner')
    distance_df = distance_df.orderBy(['entity_id', '2nd_entity_id'])

    if is_distance:
        distance_df = norm_distances(distance_df, dist_scaler)
    entities_df = entities_df.orderBy(['entity_id'])

    return entities_features_df, distance_df, entities_df.count()


def insert_output(spark, output_df):
    output_df.repartition(1).registerTempTable("l2_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_matrix_classification SELECT 
                 entity_id,
                 venue_id,
                 bar_id,
                 entities_like_you,
                 created_at,
                 run_id,
                 group
                 FROM l2_df where concat(run_id," ",entity_id) not in
                  (select concat(run_id," ",entity_id) from stg.blu_matrix_classification)""")


def encode_bars(bars, venues):
    output = [str((-1, x)) for x in bars]
    for v in venues:
        output.append(str((v, -1)))
    return output


def get_entities_distances(spark, entities):
    entities_str = ",".join([str(entity) for entity in entities])
    # pull latest date for all bars
    entities_features_df = spark.sql("""select entity_id,distance from stg.bars_distances where
                                        entity_id in (""" + entities_str + """)
                                         and 2nd_entity_id in (""" + entities_str + """)""")
    return entities_features_df


def split_feats_weights(feats):
    feats_lst = []
    weights_lst = []
    for x, y in feats.items():
        if x != 'distance':
            feats_lst.append(x)
            weights_lst.append(y)
    weights_array = np.array(weights_lst)
    return weights_array, feats_lst


def matrix_calculation(main_df_broad, weights_matrix_broad):
    def matrix_calculation_(f_array, distances):
        f_array = np.array(f_array)
        minus_matrix = main_df_broad - f_array
        distances = np.array(distances)
        distances = np.transpose(distances)
        distances = distances.reshape(-1, 1)
        minus_matrix = np.hstack((minus_matrix, distances))
        # return str(minus_matrix.shape)
        square_matrix = np.apply_along_axis(np.square, -1, minus_matrix)
        weighted_matrix = np.multiply(weights_matrix_broad, square_matrix)
        sum_weighted_matrix = weighted_matrix.sum(axis=1)
        root_sum_weighted_matrix = np.apply_along_axis(np.sqrt, -1, sum_weighted_matrix)
        root_sum_weighted_matrix[np.isnan(root_sum_weighted_matrix)] = 0
        root_sum_weighted_matrix.tolist()
        # output = sorted(zip(root_sum_weighted_matrix.tolist(), bars_matrix_broad.value))
        output = root_sum_weighted_matrix.tolist()

        return str(output)

    return func.udf(matrix_calculation_, StringType())


def add_entities(entities):
    def add_entities_(scores):
        scores_lst = scores.split(",")
        scores_lst = [s.replace('[', '') for s in scores_lst]
        scores_lst = [s.replace(']', '') for s in scores_lst]
        x = str(sorted(zip(scores_lst, entities)))
        return x

    return func.udf(add_entities_, StringType())


def create_distance_matrix(entities_distances):
    entities_distances = entities_distances.na.fill(0)
    entities_distances = entities_distances.orderBy("entity_id", "2nd_entity_id")
    entities_distances = entities_distances.withColumn("id", func.monotonically_increasing_id()).groupBy("entity_id").\
        agg(func.collect_list(func.struct('id', 'distance')).alias("distances"))\
        .select("entity_id", func.sort_array("distances").getItem("distance").alias("distances"))
    entities_distances = entities_distances.orderBy("entity_id")
    return entities_distances


def norm_distances(dis_df, dist_scaler):
    if dist_scaler == 'max_min':
        dis_df = dis_df.na.fill(0)
        agg_dis_df = dis_df.agg({"distance": "max"}).collect()[0]
        max_val = agg_dis_df["max(distance)"]
        agg_dis_df = dis_df.agg({"distance": "min"}).collect()[0]
        min_val = agg_dis_df["min(distance)"]
        dis_df = dis_df.withColumn("distance", (func.col('distance') - min_val) / (max_val - min_val))
    elif dist_scaler == 'robust':
        q1, q3 = dis_df.approxQuantile('distance', [0.25, 0.75], 0)
        dis_df = dis_df.withColumn('distance', func.when(func.col('entity_id') == func.col('2nd_entity_id'), 0).otherwise((func.col('distance') - q1) / (q3 - q1)))
    return dis_df


def add_distance(distance_df, matrix_df):
    matrix_df = matrix_df.orderBy(['entity_id'])
    matrix_df = matrix_df.join(distance_df, on='entity_id', how='inner')
    return matrix_df


def add_empty_distance(matrix_df, num_of_entities):
    matrix_df = matrix_df.withColumn('distances', func.array([func.lit(0) for x in range(0, num_of_entities)]))
    return matrix_df


def run_process(spark, run_id, feats, group):
    group = str(group)
    if 'distance' in feats.keys():
        is_distance = True
        distance_weights = feats['distance']
        temp_distance = feats['distance']
        del feats['distance']
    else:
        is_distance = False

    if not feats:
        # fake for creating the matrix, weight is 0
        feats = {'cnt_taps': 0}

    feat_dict = f_config.features
    dist_scaler = f_config.distance_scaler
    weights_matrix, features = split_feats_weights(feats)
    agg_features, distance_df, num_of_entities = pull_data(spark=spark, run_id=run_id, features=features,
                                                           is_distance=is_distance, group=group, dist_scaler=dist_scaler)

    df_norm = norm_df(agg_features, feat_dict)
    matrix_df = df_norm

    for i in features:
        matrix_df = matrix_df.withColumn(i, func.col(i).cast(FloatType()))
    matrix_df = matrix_df.drop('bar_id')
    matrix_df = matrix_df.drop('venue_id')
    matrix_df = matrix_df.drop('run_id')
    if is_distance:  # add varible distance
        weights_matrix = np.append(weights_matrix, distance_weights)  # this is a vector
        distance_matrix = create_distance_matrix(distance_df)
        matrix_df = add_distance(distance_matrix, matrix_df)
    else:
        weights_matrix = np.append(weights_matrix, -1)
        matrix_df = add_empty_distance(matrix_df, num_of_entities)

    matrix_df_copy = matrix_df
    matrix_df_copy = matrix_df_copy.drop('entity_id')
    matrix_df_copy = matrix_df_copy.na.fill(0)
    matrix_df_copy = matrix_df_copy.drop('distances')
    matrix_df_copy = matrix_df_copy.orderBy(['entity_id'])
    main_matrix = np.array(matrix_df_copy.collect())
    matrix_df = matrix_df.na.fill(0)
    matrix_df = matrix_df.orderBy(['entity_id'])
    entities = [str(i.entity_id) for i in matrix_df.select('entity_id').collect()]
    matrix_df = matrix_df.withColumn('feat_array', func.array(matrix_df.columns[1:-1]))
    matrix_df = matrix_df.na.fill(0)

    output_df = matrix_df.withColumn('output', matrix_calculation(main_matrix, weights_matrix)(matrix_df['feat_array'],
                                                                                               matrix_df['distances']))

    output_df = output_df.withColumn('entities_like_you', add_entities(entities)(output_df['output']))
    output_df = output_df.join(agg_features.select('entity_id', 'venue_id', 'bar_id'), on='entity_id', how='inner')
    output_df = output_df.withColumn("created_at", func.lit(datetime.now()))
    output_df = output_df.withColumn("run_id", func.lit(run_id))
    output_df = output_df.withColumn("group", func.lit(group))
    insert_output(spark, output_df)
    if is_distance:
        feats['distance'] = temp_distance


udf_norm_distances = func.udf(norm_distances, ArrayType(FloatType()))
# TODO: scale distance and features together


# if __name__ == '__main__':
    # spark = SparkSession \
    #     .builder \
    #     .appName("blu matrix classification") \
    #     .enableHiveSupport() \
    #     .config("hive.exec.dynamic.partition", "true") \
    #     .config("hive.exec.dynamic.partition.mode", "nonstrict") \
    #     .getOrCreate()

    # feats = {'avg_check_duration': 0.15, 'pct_food_revenue': 0.25,
    #          'pct_unique_wine_items': 0.25, 'pct_beer_revenue': 0.35, 'distance': 0.1}

    #feats = {'distance': 1}
    #run_id = '22'

    #run_process(run_id, feats)

    # agg_features, distance_df = pull_data(spark=spark, run_id=run_id, features=features)
    # distance_df.show(100)
    # distance_matrix = create_distance_matrix(distance_df)
    # distance_matrix_norm = norm_distance_matrix(distance_matrix)
    # print(distance_matrix_norm)

