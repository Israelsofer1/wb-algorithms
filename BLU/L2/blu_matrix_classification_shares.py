from pyspark import SparkContext
from pyspark.sql import SQLContext
from datetime import datetime
from pyspark.sql import functions as func
from pyspark.sql.window import Window


# define paths for redshift, hive
# redshift_url = jobs_config.redshift_url
# redshift_temp_dir = jobs_config.redshift_temp_dir
# redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
# hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name

# spark = SparkSession \
# # #     .builder \
# # #     .appName("blu_matrix_classification_shares") \
# # #     .config("hive.exec.dynamic.partition", "true") \
# # #     .config("hive.exec.dynamic.partition.mode", "nonstrict") \
# # #     .enableHiveSupport() \
# # #     .getOrCreate()


# pos
def pull_entity_data(spark, start, end, entities):
    entities_str = ','.join(entities)
    feat_data_df = spark.sql \
        ("""select 
                entity_id,
                venue_id,
                bar_id,
                total_revenue,
                total_1_revenue,
                total_2_revenue,
                total_3_revenue,
                total_4_revenue,
                total_6_revenue,
                total_abi_revenue,
                total_qty,
                total_1_qty,
                total_2_qty,
                total_3_qty,
                total_4_qty,
                total_6_qty,
                total_abi_qty,
                total_check_duration,
                number_of_orders,
                beer_vol_peak_hour,
                sales_peak_hour,
                hour(pos_open_time) as pos_open_time,
                hour(pos_close_time) as pos_close_time,
                bar_open_hour,
                bar_close_hour,
                cnt_taps,
                abi_not_highend,
                abi_highend,
                abi_sports_active,
                abi_nightlife,
                total_abi_vol,
                total_vol,
                number_of_beer_orders,
                total_draught_vol,
                total_package_vol,
                nd
            from stg.blu_daily_matrix_features o
            where nd between '""" + start + """' and '""" + end + """'
            and entity_id in (""" + entities_str + """)""")

    return feat_data_df


def add_period_total_revenue(df):
    window_spec = Window.partitionBy(df['entity_id'])
    df = df.withColumn('period_total_revenue', func.sum(func.col('total_revenue')).over(window_spec))
    return df


# product_data
def pull_product_data(spark):
    products_data_df = spark.sql("""
        select (case when b.venue_id is not null and b.venue_id<>0 then 1 else 0 end) as is_venue,
        b.id as bar_id,
        b.venue_id as venue_id,
        pb.title,
        pb.category_id,
        p.id as product_id,
        p.brand_id as brand_id,
        br.beer_type_id,
        p.serving_type_id as serving_type_id,
        pb.category_id,
        brb.is_competitor
        from stg.bars b
        join stg.breweries_bars br on br.bar_id = b.id
        left join stg.pos_bars_products pb on pb.bar_id = b.id
        left join stg.pos_products p on p.id = pb.product_id
        left join stg.pos_products_brands pbp on p.id = pbp.product_id 
        left join stg.brands br on br.id = p.brand_id
        left join stg.breweries_brands brb on brb.brewery_id = br.brewery_id and brb.brand_id = pbp.brand_id""")

    products_data_df = products_data_df.withColumn("entity_id", func.when(func.col("is_venue") == 1,
                                                                          func.concat(func.lit("("),
                                                                                      products_data_df['venue_id'],
                                                                                      func.lit(",-1)")))
                                                   .otherwise(
        func.concat(func.lit("(-1,"), products_data_df['bar_id'], func.lit(")"))))
    # pull_bar_data_df.show()

    return products_data_df


def create_has_draught(products_data_df):
    entity_has_draught_agg = products_data_df.groupBy(
        products_data_df['entity_id'], products_data_df['serving_type_id']). \
        agg(func.count(products_data_df['title']).alias('items')) \
        .filter(func.col('serving_type_id').isin(2, 4, 5))

    entity_has_draught_agg = entity_has_draught_agg.withColumn('has_draught',
                                                               func.when(func.col("items") > 0, 1).otherwise(0))

    entity_has_draught_agg = entity_has_draught_agg.drop('items')
    return entity_has_draught_agg


def create_division_feature(entity_data_df, numerator, denominator, col_name):
    entity_share_agg_df = entity_data_df.groupBy(entity_data_df['entity_id']).agg(
        func.sum(entity_data_df[numerator]).alias(numerator),
        func.sum(entity_data_df[denominator]).alias(denominator))

    entity_share_agg_df = entity_share_agg_df.withColumn(col_name,
                                                         func.round((func.col(numerator) / func.col(denominator)), 3))
    entity_share_agg_df = entity_share_agg_df.drop(numerator)
    entity_share_agg_df = entity_share_agg_df.drop(denominator)

    return entity_share_agg_df


def create_division_features(entity_data_df):
    entity_share_agg_df = entity_data_df.groupBy(entity_data_df['entity_id']).agg(
        (func.round(func.sum(entity_data_df['total_1_revenue']) / func.sum(func.col('total_1_qty')), 3)).alias(
            'avg_beer_price'),
        (func.round(func.sum(entity_data_df['total_check_duration']) / func.sum(func.col('number_of_orders')), 3)).alias(
            'avg_check_duration'),
        (func.round(func.sum(entity_data_df['total_2_revenue']) / func.sum(func.col('total_2_qty')), 3)).alias(
            'avg_food_price'),
        (func.round(func.sum(entity_data_df['total_3_revenue']) / func.sum(func.col('total_3_qty')), 3)).alias(
            'avg_spirit_price'),
        (func.round(func.sum(entity_data_df['total_4_revenue']) / func.sum(func.col('total_4_qty')), 3)).alias(
            'avg_nab_price'),
        (func.round(func.sum(entity_data_df['total_6_revenue']) / func.sum(func.col('total_6_qty')), 3)).alias(
            'avg_wine_price'),
        (func.round(func.sum(entity_data_df['total_1_revenue']) / func.sum(func.col('total_revenue')), 3)).alias(
            'pct_beer_revenue'),
        (func.round(func.sum(entity_data_df['total_2_revenue']) / func.sum(func.col('total_revenue')), 3)).alias(
            'pct_food_revenue'),
        (func.round(func.sum(entity_data_df['total_3_revenue']) / func.sum(func.col('total_revenue')), 3)).alias(
            'pct_spirit_revenue'),
        (func.round(func.sum(entity_data_df['total_4_revenue']) / func.sum(func.col('total_revenue')), 3)).alias(
            'pct_nab_revenue'),
        (func.round(func.sum(entity_data_df['total_6_revenue']) / func.sum(func.col('total_revenue')), 3)).alias(
            'pct_wine_revenue'),
        (func.round(func.sum(entity_data_df['total_abi_revenue']) / func.sum(func.col('total_1_revenue')), 3)).alias(
            'pct_abi_revenue'),
        (func.round(func.sum(entity_data_df['total_abi_vol']) / func.sum(func.col('total_vol')), 3)).alias(
            'pct_abi_volume'),
        (func.round(func.sum(entity_data_df['total_revenue']) / func.sum(func.col('number_of_orders')), 3)).alias(
            'avg_check_size'),
        (func.round(func.mean(entity_data_df['number_of_orders']), 3)).alias('avg_num_checks'),
        (func.round(func.sum(entity_data_df['total_draught_vol']) / func.sum(func.col('total_vol')), 3)).alias(
            'pct_vol_draught'),
        (func.round(func.sum(entity_data_df['total_package_vol']) / func.sum(func.col('total_vol')), 3)).alias(
            'pct_vol_package'),
        (func.round(func.sum(entity_data_df['number_of_orders']), 3)).alias('total_num_checks'),
        (func.round(func.sum(entity_data_df['total_vol']) / func.sum(func.col('number_of_beer_orders')), 3)).alias(
            'avg_beer_vol_per_check'))
    return entity_share_agg_df


def create_product_division_feature(product_data_df, col_name, category_id):
    entity_product_total_agg_df = product_data_df.groupBy(product_data_df['entity_id']).agg(
        func.countDistinct(product_data_df['title']).alias('total_products'))

    entity_category_total_agg_df = product_data_df.groupBy(product_data_df['entity_id'],
                                                           product_data_df['category_id']).agg(
        func.countDistinct(product_data_df['title']).alias(str('total_' + str(category_id)))).filter(
        func.col('category_id') == category_id)

    joined_df = entity_product_total_agg_df.join(entity_category_total_agg_df, on='entity_id', how='left')
    joined_df = joined_df.withColumn(col_name,
                                     func.round(func.col(str('total_' + str(category_id))) / func.col('total_products'),
                                                3))
    joined_df = joined_df.drop(str('total_' + str(category_id)))
    joined_df = joined_df.drop(str(category_id))
    joined_df = joined_df.drop('total_products')
    joined_df = joined_df.select('entity_id', col_name)
    return joined_df


def create_max_date_features(entity_data, end_date):
    entity_share_agg_df = entity_data.groupBy(entity_data['entity_id'], entity_data['nd']).agg(

        func.sum(entity_data['cnt_taps']).alias('cnt_taps'),
        func.sum(entity_data['abi_not_highend']).alias('abi_not_highend'),
        func.sum(entity_data['abi_highend']).alias('abi_highend'),
        func.sum(entity_data['abi_sports_active']).alias('abi_sports_active'),
        func.sum(entity_data['abi_nightlife']).alias('abi_nightlife')).filter(func.col('nd') == end_date)

    entity_share_agg_df = entity_share_agg_df.drop('nd')

    return entity_share_agg_df


def create_max_date_feature(entity_data, column_to_agg, col_name, end_date):
    entity_share_agg_df = entity_data.groupBy(entity_data['entity_id'], entity_data['nd']).agg(
        func.sum(entity_data[column_to_agg]).alias(col_name)).filter(func.col('nd') == end_date)
    entity_share_agg_df = entity_share_agg_df.drop('nd')

    return entity_share_agg_df


def create_hour_feature(entity_data, column_to_agg, col_name, revenue_weights=False):
    entity_date_hour_df = entity_data.withColumn(column_to_agg + 'hour', func.when(
        func.col(column_to_agg) < 8, func.col(column_to_agg) + 16).otherwise(func.col(column_to_agg) - 8))
    if not revenue_weights:
            entity_date_hour_agg_df = entity_date_hour_df.groupBy(entity_data['entity_id']).agg(
                func.round(func.avg(entity_date_hour_df[column_to_agg + 'hour']), 3).alias(col_name))
    else:
        entity_date_hour_df = entity_date_hour_df.withColumn('weighted', func.col(column_to_agg + 'hour') * func.col('total_revenue')/ func.col('period_total_revenue'))
        entity_date_hour_agg_df = entity_date_hour_df.groupBy(entity_data['entity_id']).agg(
                func.round(func.avg(func.col('weighted')), 3).alias(col_name))
    return entity_date_hour_agg_df


def create_unique_beer_brands(product_data, col_name):
    unique_beer_brands_agg_df = product_data.groupBy(product_data['entity_id'], product_data['category_id']).agg(
        func.countDistinct(product_data['brand_id']).alias('total')).filter(func.col('category_id') == 1)

    unique_abi_beer_brands_agg_df = product_data.groupBy(product_data['entity_id'], product_data['category_id'],
                                                         product_data['is_competitor']).agg(
        func.countDistinct(product_data['brand_id']).alias('abi')).filter(
        (func.col('category_id') == 1) & (func.col('is_competitor') == 0))

    joined_df = unique_beer_brands_agg_df.join(unique_abi_beer_brands_agg_df, on='entity_id', how='left')
    joined_df = joined_df.withColumn(col_name, func.round(func.col('abi') / func.col('total'), 3))
    joined_df = joined_df.select('entity_id', col_name)

    return joined_df


def create_count_features(product_data):
    unique_draught_agg_df = product_data.groupBy(product_data['entity_id'], product_data['serving_type_id']).agg(
        func.countDistinct(product_data['product_id']).alias('cnt_unique_beer_brands_draught')).filter(
        func.col('serving_type_id') == 2)
    unique_draught_agg_df = unique_draught_agg_df.drop('serving_type_id')
    unique_package_agg_df = product_data.groupBy(product_data['entity_id'], product_data['serving_type_id']).agg(
        func.countDistinct(product_data['product_id']).alias('cnt_unique_beer_brands_package')).filter(
        func.col('serving_type_id').isin(1, 3))
    unique_package_agg_df = unique_package_agg_df.drop('serving_type_id')
    unique_styles_agg_df = product_data.groupBy(product_data['entity_id']).agg(
        (func.countDistinct(product_data['serving_type_id']) - 1).alias('num_of_styles'))

    joined_df = unique_draught_agg_df.join(unique_package_agg_df, on='entity_id', how='left')
    joined_df = joined_df.join(unique_styles_agg_df, on='entity_id', how='left')

    return joined_df


def create_abi_brand_feature(product_data, col_name):
    entity_unique_beer_brands_agg_df = product_data.groupBy(product_data['entity_id'], product_data['category_id']).agg(
        func.countDistinct(product_data['product_id']).alias(col_name)).filter(func.col('category_id') == 1)
    entity_unique_beer_brands_agg_df = entity_unique_beer_brands_agg_df.drop('category_id')

    return entity_unique_beer_brands_agg_df


def create_features_daily_data_table(spark):
    spark.sql("""create external table if not exists stg.blu_matrix_features_shares (
                        run_id bigint,
                        entity_id string,
                        venue_id bigint,
                        bar_id bigint,
                        start_date date,
                        end_date date,
                        avg_check_duration float,
                        cnt_taps bigint,
                        entity_open_hour float,
                        entity_close_hour float,
                        entity_open_hour_pos float,
                        entity_close_hour_pos float,
                        abi_not_highend integer,
                        abi_highend integer,
                        abi_sports_active integer,
                        abi_nightlife integer,
                        sales_peak_hour integer,
                        beer_peak_hour integer,
                        avg_beer_price float,
                        avg_food_price float,
                        avg_spirit_price float,
                        avg_nab_price float,
                        avg_wine_price float,
                        pct_beer_revenue float,
                        pct_food_revenue float,
                        pct_spirit_revenue float,
                        pct_nab_revenue float,
                        pct_wine_revenue float,
                        pct_abi_revenue float,
                        cnt_unique_beer_brands bigint,
                        pct_unique_beer_items float,
                        pct_unique_food_items float,
                        pct_unique_spirit_items float,
                        pct_unique_nab_items float,
                        pct_unique_wine_items float,
                        pct_abi_volume float,
                        pct_abi_number_of_brands float,
                        has_draught integer,
                        avg_num_checks float,
                        avg_check_size float,
                        num_of_styles integer,
                        pct_vol_draught float,
                        total_num_checks bigint,
                        avg_beer_vol_per_check float,
                        cnt_unique_beer_brands_draught integer,
                        cnt_unique_beer_brands_package integer,
                        created_at timestamp)
                        STORED AS ORC LOCATION 's3://wb-emr-hive-stg-dev/blu_matrix_features_shares' tblproperties ('orc.compress'='ZLIB')""")


def pull_run_id_info(run_id, spark):
    metadata_df = spark.sql("""select * from stg.blu_metadata_dates where run_id = """ + str(run_id))
    start_date = metadata_df.select('start_date').collect()[0][0]
    end_date = metadata_df.select('end_date').collect()[0][0]
    return str(start_date), str(end_date)


def process_features(spark, run_id, entity_list, feat_list, group='default'):
    group = str(group)
    sparkContext = SparkContext.getOrCreate()
    add_unused_feat = []
    join_dfs_lst = []

    start, end = pull_run_id_info(run_id=run_id, spark=spark)
    entity_data_df = pull_entity_data(spark, start=start, end=end, entities=entity_list)
    entity_data_df = add_period_total_revenue(entity_data_df)
    product_data = pull_product_data(spark)

    all_bars_df = entity_data_df.groupby('entity_id'). \
        agg(func.first('bar_id').alias('bar_id'), func.first('venue_id').alias('venue_id'))

    features_division_df = create_division_features(entity_data_df=entity_data_df)
    features_division_df.cache()
    join_dfs_lst.append(features_division_df)
    if 'entity_open_hour' in feat_list:
        entity_open_hour_df = create_hour_feature(entity_data_df, column_to_agg='bar_open_hour',
                                                  col_name='entity_open_hour')
        join_dfs_lst.append(entity_open_hour_df)
    else:
        add_unused_feat.append('entity_open_hour')
    # entity_open_hour_df.cache()

    if 'entity_close_hour' in feat_list:
        entity_close_hour_df = create_hour_feature(entity_data_df, column_to_agg='bar_close_hour',
                                                   col_name='entity_close_hour')
        join_dfs_lst.append(entity_close_hour_df)
    else:
        add_unused_feat.append('entity_close_hour')
    # entity_open_hour_df.cache()
    if 'entity_open_hour_pos' in feat_list:
        entity_open_hour_pos_df = create_hour_feature(entity_data_df, column_to_agg='pos_open_time',
                                                      col_name='entity_open_hour_pos')
        join_dfs_lst.append(entity_open_hour_pos_df)
    else:
        add_unused_feat.append('entity_open_hour_pos')

    if 'entity_close_hour_pos' in feat_list:
        entity_close_hour_pos_df = create_hour_feature(entity_data_df, column_to_agg='pos_close_time',
                                                       col_name='entity_close_hour_pos')
        join_dfs_lst.append(entity_close_hour_pos_df)
    else:
        add_unused_feat.append('entity_close_hour_pos')

    if 'sales_peak_hour' in feat_list:
        sales_peak_hour_df = create_hour_feature(entity_data_df, column_to_agg='sales_peak_hour',
                                                 col_name='sales_peak_hour', revenue_weights=True)
        join_dfs_lst.append(sales_peak_hour_df)
    else:
        add_unused_feat.append('sales_peak_hour')

    if 'beer_peak_hour' in feat_list:
        beer_vol_peak_hour_df = create_hour_feature(entity_data_df, column_to_agg='beer_vol_peak_hour',
                                                    col_name='beer_peak_hour', revenue_weights=True)
        join_dfs_lst.append(beer_vol_peak_hour_df)
    else:
        add_unused_feat.append('beer_peak_hour')

    if 'pct_unique_beer_items' in feat_list:
        pct_unique_beer_items_df = create_product_division_feature(product_data, category_id=1,
                                                                   col_name='pct_unique_beer_items')
        join_dfs_lst.append(pct_unique_beer_items_df)
    else:
        add_unused_feat.append('pct_unique_beer_items')

    if 'pct_unique_food_items' in feat_list:
        pct_unique_food_items_df = create_product_division_feature(product_data, category_id=2,
                                                                   col_name='pct_unique_food_items')
        join_dfs_lst.append(pct_unique_food_items_df)
    else:
        add_unused_feat.append('pct_unique_food_items')

    if 'pct_unique_spirit_items' in feat_list:
        pct_unique_spirit_items_df = create_product_division_feature(product_data, category_id=3,
                                                                     col_name='pct_unique_spirit_items')
        join_dfs_lst.append(pct_unique_spirit_items_df)
    else:
        add_unused_feat.append('pct_unique_spirit_items')

    if 'pct_unique_nab_items' in feat_list:
        pct_unique_nab_items_df = create_product_division_feature(product_data, category_id=4,
                                                                  col_name='pct_unique_nab_items')
        join_dfs_lst.append(pct_unique_nab_items_df)
    else:
        add_unused_feat.append('pct_unique_nab_items')

    if 'pct_unique_wine_items' in feat_list:
        pct_unique_wine_items_df = create_product_division_feature(product_data, category_id=6,
                                                                   col_name='pct_unique_wine_items')
        join_dfs_lst.append(pct_unique_wine_items_df)
    else:
        add_unused_feat.append('pct_unique_wine_items')

    if 'has_draught' in feat_list:
        has_draught_df = create_has_draught(products_data_df=product_data)

        join_dfs_lst.append(has_draught_df)
    else:
        add_unused_feat.append('has_draught')

    if 'cnt_unique_beer_brands' in feat_list:
        cnt_unique_beer_brands_df = create_unique_beer_brands(product_data, col_name='cnt_unique_beer_brands')
        join_dfs_lst.append(cnt_unique_beer_brands_df)
    else:
        add_unused_feat.append('cnt_unique_beer_brands')

    if 'pct_abi_number_of_brands' in feat_list:
        pct_abi_number_of_brands_df = create_abi_brand_feature(product_data, col_name='pct_abi_number_of_brands')
        join_dfs_lst.append(pct_abi_number_of_brands_df)
    else:
        add_unused_feat.append('pct_abi_number_of_brands')

    max_dates_features_df = create_max_date_features(entity_data_df, end_date=end)
    count_features_df = create_count_features(product_data)

    max_dates_features_df.cache()
    join_dfs_lst.append(max_dates_features_df)
    join_dfs_lst.append(count_features_df)

    # pct_abi_number_of_brands_df.cache()
    # df_list = [max_dates_features_df, features_division_df, entity_open_hour_df, entity_close_hour_df,
    #            entity_open_hour_pos_df,
    #            entity_close_hour_pos_df, sales_peak_hour_df, beer_vol_peak_hour_df, cnt_unique_beer_brands_df,
    #            pct_unique_beer_items_df,
    #            pct_unique_food_items_df, pct_unique_spirit_items_df, pct_unique_nab_items_df, pct_unique_wine_items_df,
    #            pct_abi_number_of_brands_df, has_draught_df]

    df_final = all_bars_df
    for df_next in join_dfs_lst:
        df_final = df_final.join(df_next, on='entity_id', how='left')
        df_final.cache()
        df_next.unpersist()

    df_final = df_final.withColumn('run_id', func.lit(run_id))
    df_final = df_final.withColumn('start_date', func.lit(start))
    df_final = df_final.withColumn('end_date', func.lit(end))
    df_final = df_final.withColumn("created_at", func.lit(datetime.now()))
    df_final = df_final.withColumn("group", func.lit(group))
    # df_final.show()
    for col in add_unused_feat:
        df_final = df_final.withColumn(col, func.lit(-1))
    # df_final.show()
    df_final = df_final.na.fill(0)
    # spark.catalog.clearCache()
    df_final.cache()
    sqlContext = SQLContext(spark)
    df_final.repartition(1).createOrReplaceTempView("l2_feat_shares_df")
    # df_final.show()
    sqlContext.cacheTable("l2_feat_shares_df")

    spark.sql("""INSERT OVERWRITE TABLE stg.blu_matrix_features_shares SELECT
                    entity_id,
                    venue_id,
                    bar_id,
                    start_date,
                    end_date,
                    avg_check_duration,
                    cnt_taps,
                    entity_open_hour,
                    entity_close_hour,
                    entity_open_hour_pos,
                    entity_close_hour_pos,
                    abi_not_highend,
                    abi_highend,
                    abi_sports_active,
                    abi_nightlife,
                    sales_peak_hour,
                    beer_peak_hour,
                    avg_beer_price,
                    avg_food_price,
                    avg_spirit_price,
                    avg_nab_price,
                    avg_wine_price,
                    pct_beer_revenue,
                    pct_food_revenue,
                    pct_spirit_revenue,
                    pct_nab_revenue,
                    pct_wine_revenue,
                    pct_abi_revenue,
                    cnt_unique_beer_brands,
                    pct_unique_beer_items,
                    pct_unique_food_items,
                    pct_unique_spirit_items,
                    pct_unique_nab_items,
                    pct_unique_wine_items,
                    pct_abi_volume,
                    pct_abi_number_of_brands,
                    has_draught,
                    avg_num_checks,
                    avg_check_size,
                    num_of_styles,
                    pct_vol_draught,
                    total_num_checks,
                    avg_beer_vol_per_check,
                    cnt_unique_beer_brands_draught,
                    cnt_unique_beer_brands_package,
                    created_at,
                    run_id,
                    group
                    FROM l2_feat_shares_df where (concat(run_id," ",entity_id) not in
                     (select concat(run_id," ",entity_id)
                      from stg.blu_matrix_features_shares)
                    )""")


# listt = ['cnt_unique_beer_brands_package', 'cnt_unique_beer_brands_package']
# process_features(spark, run_id=100, entity_list=["'-1, 11276'", "'-1, 11391'", "'-1, 2031'"], feat_list=listt)

