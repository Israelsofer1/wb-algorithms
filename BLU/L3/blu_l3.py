from pyspark.sql import SparkSession
from datetime import datetime, timedelta
import datetime as dt
from pyspark import SparkContext
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, StructType, StructField, IntegerType, StringType, ArrayType
import sys
import pyspark.sql as sql
from ast import literal_eval
import struct
import re
from itertools import chain, cycle
import pandas as pd
import jobs_config

# define paths for redshift, hive
redshift_url = jobs_config.redshift_url
redshift_temp_dir = jobs_config.redshift_temp_dir
redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name

# spark = SparkSession \
#     .builder \
#     .appName("Python Spark SQL activation daily measurement") \
#     .enableHiveSupport() \
#     .getOrCreate()


def pull_from_list(elem, lst):
    #udf
    lst.remove(elem)
    return lst


def create_output_table_l2(spark):
    spark.sql("""create external table if not exists stg.blu_matrix_classification(
                        run_id bigint,
                        entity_id bigint,
                        venue_id bigint,
                        bar_id bigint,
                        entities_like_you string,
                        created_at timestamp)
                        STORED AS ORC LOCATION 's3://wb-emr-hive-stg-dev/blu_matrix_classification' tblproperties ('orc.compress'='ZLIB')""")


def create_output_table_l3(spark):
    spark.sql("""create external table if not exists stg.blu_bars_like_you(
                        run_id bigint,
                        entity_id bigint,
                        venue_id bigint,
                        bar_id bigint,
                        entities_like_you string,
                        winning_like_you string,
                        created_at timestamp)
                        STORED AS ORC LOCATION 's3://wb-emr-hive-stg-dev/blu_bars_like_you' tblproperties ('orc.compress'='ZLIB')""")


def rev_change(entities, from_date, to_date, spark):
    entities_str = ",".join(entities)
    beer_rev_df = spark.sql(
        """select entity_id,sum(revenue) as beer_revenue FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                         '""" + to_date + """' and category = 1 and entity_id in (""" + entities_str + """) group by entity_id""")

    to_date_frmt = dt.datetime.strptime(to_date, '%Y-%m-%d')
    from_date_frmt = dt.datetime.strptime(from_date, '%Y-%m-%d')
    days_diff = (to_date_frmt - from_date_frmt).days + 1
    from_date_ftmt_new = from_date_frmt + timedelta(days=-days_diff)
    from_date_new = from_date_ftmt_new.strftime('%Y-%m-%d')
    to_date_ftmt_new = from_date_frmt + timedelta(days=-1)
    to_date_new = to_date_ftmt_new.strftime('%Y-%m-%d')

    beer_rev_df_prev = spark.sql(
        """select entity_id,sum(revenue) as beer_revenue_prev FROM stg.blu_l1_raw_data where nd between '""" + from_date_new + """' and
                         '""" + to_date_new + """' and category = 1 and entity_id in (""" + entities_str + """) group by entity_id""")

    beer_rev_change_df = beer_rev_df.join(beer_rev_df_prev, on='entity_id', how='left')
    beer_rev_change_df = beer_rev_change_df.withColumn('criteria', (
                (beer_rev_change_df['beer_revenue'] - beer_rev_change_df['beer_revenue_prev']) / beer_rev_change_df[
            'beer_revenue_prev']))
    # criteria_lst = [float(i.criteria) for i in beer_rev_change_df.select('criteria').collect()]
    # avgr = sum(criteria_lst)/len(criteria_lst)

    return beer_rev_change_df


def beer_rev(entities, from_date, to_date, spark):
    entities_str = ",".join(entities)
    beer_rev_df = spark.sql(
        """select entity_id,sum(revenue) as criteria FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                         '""" + to_date + """' and category = 1 and entity_id in (""" + entities_str + """) group by entity_id""")
    # criteria_lst = [float(i.criteria) for i in beer_rev_df.select('criteria').collect()]
    # avgr = sum(criteria_lst)/len(criteria_lst)

    return beer_rev_df


def vol_per_check(entities, from_date, to_date, spark):
    entities_str = ",".join(entities)
    vol_per_check_df = spark.sql(
        """select entity_id,sum(total_vol)/sum(number_of_orders) as criteria from stg.blu_daily_matrix_features where nd between '""" + from_date + """' and
                     '""" + to_date + """' and entity_id in (""" + entities_str + """) group by entity_id""")
    # criteria_lst = [float(i.criteria) for i in vol_per_check_df.select('criteria').collect()]
    # avgr = sum(criteria_lst)/len(criteria_lst)

    return vol_per_check_df


def beer_penetration(entities, from_date, to_date, spark):
    entities_str = ",".join(entities)
    beer_pen_df = spark.sql(
        """select entity_id,sum(num_orders) as beer_num_orders FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                         '""" + to_date + """' and category = 1 and entity_id in (""" + entities_str + """) group by entity_id""")
    # consider change the name of the df because its confusing
    bar_pen_df = spark.sql(
        """select entity_id,sum(num_orders) as total_num_orders FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                     '""" + to_date + """' and entity_id in (""" + entities_str + """) group by entity_id""")

    beer_pen_df = bar_pen_df.join(beer_pen_df, on='entity_id', how='left')
    beer_pen_df = beer_pen_df.withColumn("criteria", (func.col("beer_num_orders") / func.col("total_num_orders")))
    # criteria_lst = [float(i.criteria) for i in beer_pen_df.select('criteria').collect()]
    # avgr = sum(criteria_lst)/len(criteria_lst)

    return beer_pen_df


def spirit_penetration(entities, from_date, to_date, spark):  # fix query
    entities_str = ",".join(entities)
    spirit_pen_df = spark.sql(
        """select entity_id,sum(num_orders) as spirit_num_orders FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                         '""" + to_date + """' and category = 3 and entity_id in (""" + entities_str + """) group by entity_id""")
    # consider change the name of the df because its confusing
    bar_pen_df = spark.sql(
        """select entity_id ,sum(num_orders) as total_num_orders FROM stg.blu_l1_raw_data where nd between '""" + from_date + """' and
                 '""" + to_date + """' and entity_id in (""" + entities_str + """) group by entity_id""")

    spirit_pen_df = bar_pen_df.join(spirit_pen_df, on='entity_id', how='left')
    spirit_pen_df = spirit_pen_df.withColumn("criteria", udf_calc_criteria(spirit_pen_df["spirit_num_orders"],
                                                                           spirit_pen_df["total_num_orders"]))
    # criteria_lst = [float(i.criteria) for i in spirit_pen_df.select('criteria').collect()]
    # avgr = sum(criteria_lst)/len(criteria_lst)
    return spirit_pen_df


def calculate_winnning_pocs(entities, from_date, to_date, criteria, spark):
    if criteria == 'vol_per_check':
        criteria_df = vol_per_check(entities, from_date, to_date, spark)
    elif criteria == 'rev_change':
        criteria_df = rev_change(entities, from_date, to_date, spark)
    elif criteria == 'beer_rev':
        criteria_df = beer_rev(entities, from_date, to_date, spark)
    elif criteria == 'beer_penetration':
        criteria_df = beer_penetration(entities, from_date, to_date, spark)
    elif criteria == 'spirit_penetration':
        criteria_df = spirit_penetration(entities, from_date, to_date, spark)

    return criteria_df


def pull_run_id_info(run_id, spark):
    metadata_df = spark.sql("""select * from  stg.blu_metadata_dates where run_id = """ + str(run_id))
    start = metadata_df.select('start_date').collect()[0][0]
    end = metadata_df.select('end_date').collect()[0][0]
    return str(start), str(end)


def arrage_l1_output(l1_ouput):
    new_output = l1_ouput.groupby('group').agg(func.collect_list('entity_id').alias("entity_like_you_inc"))
    new_output = l1_ouput.join(new_output, on='group', how='inner')
    new_output = new_output.withColumn('entity_like_you',
                                       udf_pull_from_list(new_output['entity_id'], new_output["entity_like_you_inc"]))
    new_output = new_output.drop('entity_like_you_inc')
    return new_output


def pull_group_output(run_id, prev_layer, spark):
    output_df = None

    if prev_layer == 'l1':
        output_df = spark.sql("""select concat('"',entity_id,'"') 
            as entity_id, group from stg.blu_merge_classification where run_id = """+str(run_id))
        output_df = arrage_l1_output(output_df)
    elif prev_layer == 'l2':
        output_df = spark.sql(
            """select concat('"',entity_id,'"') as entity_id, entities_like_you as entity_like_you 
                from stg.blu_matrix_classification where run_id = """ + str(run_id))

    return output_df


def calculate_winning_for_bar(scores, entity_like_you, my_entity_id):

    scores_lst = [y for x, y in scores if x in entity_like_you or x == my_entity_id]
    try:
        scores_avg = sum(scores_lst) / len(scores_lst)
        winning_bars_lst = [x for x, y in scores if (y > scores_avg and x in entity_like_you)]
        return winning_bars_lst
    except ZeroDivisionError:
        print(f'entity id {my_entity_id} has no entities like it')
        return []


def calculate_winning_pocs(entity_id, scores, entity_like_you, min_bars, l2_treshold):
    if l2_treshold == -1:
        entity_to_check = [x for x in entity_like_you]
        # only l1, use all bars
        winning_pocs = calculate_winning_for_bar(scores, entity_to_check, entity_id)
    else:
        # based l2
        number_of_entity_to_check = len([bar for score, bar in entity_like_you if float(score) <= l2_treshold])
        while number_of_entity_to_check <= len(entity_like_you):
            number_of_entity_to_check = min(number_of_entity_to_check, len(entity_like_you))
            entity_to_check = [y for x, y in entity_like_you[0:number_of_entity_to_check]]
            winning_pocs = calculate_winning_for_bar(scores, entity_to_check, entity_id)
            if len(winning_pocs) < min_bars and number_of_entity_to_check < len(entity_like_you):
                number_of_entity_to_check = number_of_entity_to_check + 1
                if number_of_entity_to_check > len(entity_like_you):
                    number_of_entity_to_check = len(entity_like_you)
            else:
                break
    output = []
    for entity in entity_to_check:
        if entity in winning_pocs:
            output.append((entity, 1))
        else:
            output.append((entity, 0))

    return output


def post_process_columns(d_frame):
    # separate output into 2 columns - blu and winning blu
    d_frame = d_frame.astype('object')
    d_frame['entity_like_you'] = 0
    d_frame['winning_like_you'] = 0

    for index, row in d_frame.iterrows():
        d_frame['entity_like_you'] = d_frame['entity_like_you'].astype(object)
        d_frame['winning_like_you'] = d_frame['winning_like_you'].astype(object)
        d_frame.at[index, 'entity_like_you'] = [x for x, y in row['entity_winning_like_you'] if x != d_frame['entity_id'][index].replace('"', '')]
        d_frame.at[index, 'winning_like_you'] = [x for x, y in row['entity_winning_like_you'] if (y == 1) & (x != d_frame['entity_id'][index].replace('"', ''))]

    return d_frame

def remove_par(entity_id_str):

    return entity_id_str.replace('"', '')

def add_columns(output_df,spark, run_id, job_id, profile_name):
    entity_bars = spark.sql("""select concat('"',entity_id,'"') as entity_id, min(venue_id) as venue_id,
                               (case when min(venue_id)>0 then -1 else min(bar_id) end) as bar_id
                               from stg.bars_distances group by entity_id""")

    output_df = output_df.join(entity_bars, on='entity_id', how='inner')
    output_df = output_df.withColumn("created_at", func.lit(datetime.now()))
    output_df = output_df.withColumn("run_id", func.lit(run_id))
    output_df = output_df.withColumn("job_id", func.lit(job_id))
    output_df = output_df.withColumn("profile_name", func.lit(profile_name))

    entity_bars = spark.sql("""select entity_id as entity_id_like_you, min(venue_id) as venue_id_like_you, 
                                   (case when min(venue_id)>0 then -1 else min(bar_id) end) as bar_id_like_you
                                   from stg.bars_distances group by entity_id""")
    output_df = output_df.join(entity_bars, on='entity_id_like_you', how='inner')

    return output_df

def insert_results(output_df, spark,run_id, job_id, profile_name):

    output_df = output_df.withColumn('entity_like_you', func.explode(output_df.entity_like_you))
    output_df = output_df.withColumn('winning_like_you_2',
                       func.when(contains_udf(output_df.entity_like_you, output_df.winning_like_you) == True, func.lit(1)).otherwise(
                           func.lit(0)))
    output_df = output_df.drop('winning_like_you')
    output_df = output_df.withColumnRenamed("winning_like_you_2", "winning")
    output_df = output_df.withColumnRenamed("entity_like_you", "entity_id_like_you")
    output_df = add_columns(output_df, spark=spark,run_id=run_id, job_id=job_id, profile_name=profile_name)
    output_df = output_df.withColumn('entity_id_fixed', udf_remove_par(output_df['entity_id']))
    output_df = output_df.withColumn('entity_id_like_you_fixed', udf_remove_par(output_df['entity_id_like_you']))
    output_df = output_df.drop('entity_id')
    output_df = output_df.drop('entity_id_like_you')
    output_df = output_df.withColumnRenamed('entity_id_fixed', 'entity_id')
    output_df = output_df.withColumnRenamed('entity_id_like_you_fixed', "entity_id_like_you")
    output_df.repartition(1).registerTempTable("l3_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_bars_like_you SELECT
                 job_id,
                 profile_name,
                 entity_id,
                 venue_id,
                 bar_id,
                 entity_id_like_you,
                 venue_id_like_you,
                 bar_id_like_you,
                 winning,
                 created_at,
                 run_id
                 FROM l3_df  where run_id not in (select run_id from stg.blu_bars_like_you)""")

    #insert redshift
    output_df.write \
        .format("com.databricks.spark.redshift") \
        .option("url", redshift_url) \
        .option("dbtable", "public.blu_bars_like_you") \
        .option("tempdir", redshift_temp_dir) \
        .option("aws_iam_role", redshift_aws_iam_role) \
        .mode("append") \
        .save()

def calc_criteria(numerator, denominator):
    return round(numerator / denominator, 3)

def contains(element, array):
    if element in array:
        return True
    else:
        return False


def run_l3(criteria, run_id, prev_layer, threshold, l2_threshold, job_id, profile_name, spark):

    from_date, to_date = pull_run_id_info(run_id, spark)
    prev_layer_output = pull_group_output(run_id, prev_layer, spark)
    prev_layer_output.cache()
    entities = list(set([str(i.entity_id) for i in prev_layer_output.select("entity_id").collect()]))
    pocs_scores = calculate_winnning_pocs(entities, from_date, to_date, criteria, spark)

    criterias = list([round(float(i.criteria or 0), 3) for i in pocs_scores.select("criteria").collect()])
    entities_2 = list([i.entity_id for i in pocs_scores.select("entity_id").collect()])
    score_tuple = list(zip(entities_2, criterias))

    # pandas
    pd_data_to_process = prev_layer_output.toPandas()


    for index, row in pd_data_to_process.iterrows():
        listt = literal_eval(str(row['entity_like_you']))
        row['entity_like_you'] = listt

    pd_data_to_process['entity_winning_like_you'] = pd_data_to_process.apply(lambda row: calculate_winning_pocs(
        row['entity_id'], score_tuple, row['entity_like_you'], threshold, l2_threshold), axis=1)

    pd_output = post_process_columns(pd_data_to_process)
    pd_output = pd_output.drop('entity_winning_like_you', 1)


    df_output = spark.createDataFrame(pd_output)

    insert_results(output_df=df_output, spark=spark, run_id=run_id, job_id=job_id, profile_name=profile_name)


udf_pull_from_list = func.udf(pull_from_list, ArrayType(StringType()))
udf_calc_criteria = func.udf(calc_criteria, FloatType())
udf_remove_par = func.udf(remove_par, StringType())

contains_udf = func.udf(contains)

