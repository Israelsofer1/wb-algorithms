from pyspark.sql import SparkSession
from datetime import datetime, timedelta
import blu_l1_etl_bar_shares


def process(id):

    print("%s - BLU_l1_etl_bar_shares - started" % (datetime.now()))

    spark = SparkSession \
        .builder \
        .appName("Python Spark SQL to execute activation_tracking_job to redshift") \
        .enableHiveSupport() \
        .getOrCreate()

    now = datetime.now()

    from_date, to_date, typee = get_parameters
    to_date = (now + timedelta(days=-1)).strftime("%Y-%m-%d")
    blu_l1_etl_bar_shares.process_etl(start=from_date, end=to_date, type='M', spark=spark, id = id)
    print("%s - BLU_l1_etl_job - ended" % datetime.now())


