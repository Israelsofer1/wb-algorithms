from pyspark.sql import SparkSession
from datetime import datetime, timedelta
import jobs_config
import blu_config as conf
import blu_merge_classification
import blu_individual_classification
import blu_l1_etl_bar_shares
import sys

# define paths for redshift, hive
redshift_url = jobs_config.redshift_url
redshift_temp_dir = jobs_config.redshift_temp_dir
redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name

hive_stg_s3_bucket_name = "C:/Users/user/Desktop/git/hive/stg"


# define run length types
types_days_back = {'W': 7, 'M': 30, 'Q': 90, 'Y': 365}


def create_metadata_dates(spark, from_date, to_date, typee, job_id):
    # create and return id
    id_df = spark.sql("""select max(id) from blu_metadata_dates """)
    new_id = int(id_df.first()[0]+1)

    new_id_df = spark.createDataFrame([new_id, (int(job_id)), from_date, to_date, typee],
                                      ["run_id", "job_id", "start", "end", "type"])
    new_id_df.write \
        .format("com.databricks.spark.redshift") \
        .option("url", redshift_url) \
        .option("dbtable", "stg.blu_metadata_dates") \
        .option("tempdir", redshift_temp_dir) \
        .option("aws_iam_role", redshift_aws_iam_role) \
        .mode("overwrite") \
        .save()

    return new_id


def check_exists_dates(spark, from_date, to_date, typee,job_id):
    # run_id (positive integer) - the metadata
    id_df = spark.sql("""select run_id from blu_metadata_dates 
                            where `start` = '"""+to_date+"""' and
                            `end` = '"""+from_date+"""' and
                            `typee` = """+typee+""" and
                            `job_id` = """+str(job_id))
                            
    if len(id_df.head(1)) == 0:
        return create_metadata_dates(spark=spark, from_date=from_date, to_date=to_date, typee=typee, job_id=job_id)
    else:
        return id_df.first()[0]


def run(job_config, spark):
    # run a blu_category_classification_run job

    # define run variables
    typee = job_config['l1']['type']
    job_id = job_config['job_id']
    now = datetime.now()
    from_date = (now + timedelta(days=-types_days_back[typee])).strftime("%Y-%m-%d")
    to_date = (now + timedelta(days=-1)).strftime("%Y-%m-%d")

    # check if dates metadata exists
    idd = check_exists_dates(spark=spark, from_date=from_date, to_date=to_date, typee=typee, job_id=job_id)
    blu_l1_etl_bar_shares.etl_process(spark=spark, start=from_date, end=to_date, typee=typee, run_id=idd)
    blu_individual_classification.process(spark=spark, id=idd, share_weights=job_config['l1']['weights'],run_id=idd)
    blu_merge_classification.process(spark=spark, id=idd, bars=job_config['bars'], minn=job_config['l1']['min_bars'], run_id=idd)

if __name__ == "__main__":

    print("%s - blu_category_classification_run -  started - arguments length: %d" % (datetime.now(), len(sys.argv)))

    spark = SparkSession \
        .builder \
        .appName("bars like you") \
        .enableHiveSupport() \
        .getOrCreate()
    print("%s - job_id: %d" % (datetime.now(), sys.argv[1]))
    print("%s - job_id: %d" % (datetime.now(), 1))
    job_config = conf.job_dict['1']
    job_config = conf.job_dict[str(sys.argv[1])]
    run(job_config, spark)
    print("%s - blu_category_classification_run - ended" % datetime.now())
