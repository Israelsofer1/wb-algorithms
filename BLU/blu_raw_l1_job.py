from pyspark.sql import SparkSession
from datetime import datetime
import sys
import blu_raw_l1_etl_process


if __name__ == "__main__":
    print("%s - BLU_raw_l1_etl_process_job - started - arguments length: %d" % (datetime.now(), len(sys.argv)))

    spark = SparkSession \
        .builder \
        .appName("Python Spark SQL to execute BLU_raw_l1_etl_process") \
        .enableHiveSupport() \
        .getOrCreate()

    if len(sys.argv) == 2:
        print("Argument etl_days_back to run is : %s" % sys.argv[1])
        etl_days_back = int(sys.argv[1])
    else:
        print("No argument etl_days_back to run, run 1 day")
        etl_days_back = 1
    blu_raw_l1_etl_process.process_etl_days_back(days_back=etl_days_back, spark=spark)


    print("%s - BLU_raw_l1_etl_process - ended" % datetime.now())

