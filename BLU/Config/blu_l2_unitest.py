from pyspark.sql import SparkSession
from datetime import datetime, timedelta
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, StructType, StructField, IntegerType, StringType, ArrayType
import sys
import pyspark.sql as sql
import numpy as np

# script - blu_individual_classification

spark = SparkSession \
    .builder \
    .appName("blu l2 unitest") \
    .enableHiveSupport() \
    .config("hive.exec.dynamic.partition", "true") \
    .config("hive.exec.dynamic.partition.mode", "nonstrict") \
    .getOrCreate()


mtrxschema = ArrayType(StructType([StructField("score", FloatType(), False), StructField("bar", StringType(), False)]))
feat_dict = {'test_feat':
                 {'name': 'test_feat',
                  'data_source': 'pos',
                  'agg_type': 'avg',
                  'norm_type': 'max_min',
                  'parameters': {}},
             'test_feat_2':
                 {'name': 'test_feat_2',
                  'data_source': 'pos',
                  'agg_type': 'max',
                  'norm_type': '',
                  'parameters': {}}}
features = ['test_feat', 'test_feat_2']

main_df = spark.createDataFrame(
    [("440", float(0), float(1)), ("444", float(1), float(0.5)), ("442", float(0.5), float(0))], ["bar_id", "f1", "f2"])
weights_df = spark.createDataFrame([(0.33, 0.66)], ["f1", "f2"])

main_matrix = np.array([[0.0, 1.0], [1.0, 0.5], [0.5, 0.0]])
weights_matrix = np.array([0.33, 0.66])
bars_matrix = ["440", "444", "442"]

main_df_broad = spark.sparkContext.broadcast(main_matrix)
weights_df_broad = spark.sparkContext.broadcast(weights_matrix)
bars_matrix_broad = spark.sparkContext.broadcast(bars_matrix)


def agg_feature(feat_df, feat_name, agg_type, max_date):
    if agg_type == 'avg':
        agg_df = feat_df.groupby('bar_id').agg(func.avg(feat_name).alias(feat_name))
    elif agg_type == 'latest':
        agg_df = feat_df.filter(func.col('nd') == max_date).select('bar_id', feat_name)
    elif agg_type == 'max':
        agg_df = feat_df.groupby('bar_id').agg(func.max(feat_name).alias(feat_name))

    return agg_df


def test_agg_feature():
    feat_df = spark.sparkContext.parallelize([[1, 100, '2018-10-01'],
                                              [1, 100, '2018-10-02'],
                                              [1, 100, '2018-10-03'],
                                              [1, 90, '2018-10-04'],
                                              [1, 110, '2018-10-05']]).toDF(["bar_id", "test_feat", "nd"])

    output = agg_feature(feat_df, "test_feat", 'max', '2018-10-05')
    ans = int(output.select('test_feat').collect()[0][0])

    return agg_feature.__name__ + '  ' + str(ans == 110)


def test2_agg_feature():
    feat_df = spark.sparkContext.parallelize([[1, 100, '2018-10-01'],
                                              [1, 100, '2018-10-02'],
                                              [1, 100, '2018-10-03'],
                                              [1, 90, '2018-10-04'],
                                              [1, 110, '2018-10-05']]).toDF(["bar_id", "test_feat", "nd"])

    output = agg_feature(feat_df, "test_feat", 'latest', '2018-10-05')
    ans = int(output.select('test_feat').collect()[0][0])

    return agg_feature.__name__ + ' 2 ' + str(ans == 110)


def test3_agg_feature():
    feat_df = spark.sparkContext.parallelize([[1, 100, '2018-10-01'],
                                              [1, 100, '2018-10-02'],
                                              [1, 100, '2018-10-03'],
                                              [1, 90, '2018-10-04'],
                                              [1, 110, '2018-10-05']]).toDF(["bar_id", "test_feat", "nd"])

    output = agg_feature(feat_df, "test_feat", 'avg', '2018-10-05')
    ans = int(output.select('test_feat').collect()[0][0])

    return agg_feature.__name__ + ' 3 ' + str(ans == 100)


def norm_df(df_agg, feat_dict):
    col_names = df_agg.schema.names
    # col_names = [c for c in col_names if c<>'bar_id']

    for col_name in col_names:
        if col_name in feat_dict.keys():
            if feat_dict[col_name]['norm_type'] == 'max_min':
                maxval = df_agg.agg({col_name: "max"}).collect()[0]["max(" + col_name + ")"]
                minval = df_agg.agg({col_name: "min"}).collect()[0]["min(" + col_name + ")"]
                df_agg = df_agg.withColumn("scaled_" + str(col_name), (df_agg[col_name] - minval) / (maxval - minval))
                df_agg = df_agg.drop(col_name)
                df_agg = df_agg.withColumnRenamed("scaled_" + str(col_name), col_name)

    return df_agg


def test_norm_df():
    agg_df = spark.sparkContext.parallelize([[1, 50, '2018-10-01'],
                                             [2, 100, '2018-10-02'],
                                             [3, 150, '2018-10-03']]).toDF(["bar_id", "test_feat", "nd"])

    output = norm_df(agg_df, feat_dict)
    normlst = output.select("test_feat").collect()
    normlst = [float(i.test_feat) for i in normlst]
    return norm_df.__name__ + '  ' + str(normlst == [0.0, 0.5, 1.0])


def agg_df(df, to_date):
    df_list = []
    for feat in features:
        feat_df = agg_feature(df, feat_dict[feat]['name'], feat_dict[feat]['agg_type'], to_date)
        df_list.append(feat_df)
    df_agg = df_list[0]
    for df_next in df_list[1:]:
        df_agg = df_agg.join(df_next, on='bar_id', how='inner')
    return df_agg


def test_agg_df():
    feat_df2 = spark.sparkContext.parallelize([[1, 100, 100, '2018-10-01'],
                                               [1, 100, 100, '2018-10-02'],
                                               [1, 100, 100, '2018-10-03'],
                                               [1, 90, 90, '2018-10-04'],
                                               [1, 110, 110, '2018-10-05']]).toDF(
        ["bar_id", "test_feat", "test_feat_2", "nd"])

    output = agg_df(feat_df2, '2018-10-05')
    ans = int(output.select('test_feat').collect()[0][0])
    ans2 = int(output.select('test_feat_2').collect()[0][0])

    return test_agg_df.__name__ + '  ' + str(ans == 100.0 and ans2 == 110)


def matrix_calculation(f_array):
    minus_matrix = main_df_broad.value - f_array
    square_matrix = np.apply_along_axis(np.square, -1, minus_matrix)
    weighted_matrix = np.multiply(weights_matrix, square_matrix)
    sum_weighted_matrix = weighted_matrix.sum(axis=1)
    root_sum_weighted_matrix = np.apply_along_axis(np.sqrt, -1, sum_weighted_matrix)
    root_sum_weighted_matrix.tolist()
    output = sorted(zip(root_sum_weighted_matrix.tolist(), bars_matrix_broad.value))

    return str(output)


def test_matrix_calculation():
    main_df = spark.createDataFrame(
        [("440", float(0), float(1)), ("444", float(1), float(0.5)), ("442", float(0.5), float(0))],
        ["bar_id", "f1", "f2"])
    main_df = main_df.withColumn('feat_array', func.array(main_df.columns[1:]))
    output = main_df.withColumn('output', matrix_calculation_udf(main_df['feat_array']))
    ans = output.select("output").collect()[0][0]
    return test_matrix_calculation.__name__ + '  ' + str(
        ans == "[(0.0, '440'), (0.7035623639735145, '444'), (0.8616843969807043, '442')]")


matrix_calculation_udf = func.udf(matrix_calculation, StringType())
# matrix_calculation_udf = func.udf(matrix_calculation, mtrxschema)




print(test_agg_feature())
print(test2_agg_feature())
print(test3_agg_feature())
print(test_norm_df())
print(test_agg_df())
print(test_matrix_calculation())








