from pyspark.sql import SparkSession
from datetime import datetime, timedelta
from pyspark.sql import functions as func
from pyspark.sql.types import FloatType, StructType, StructField, IntegerType, StringType, ArrayType
import sys
import pyspark.sql as sql
import jobs_config
import numpy as np
import blu_matrix_features_config as f_config
import blu_config as conf
import blu_merge_classification
import blu_individual_classification
import blu_l1_etl_bar_shares
import blu_matrix_classification_daily_etl
import blu_raw_l1_etl_process
import unittest

#TEST_HOST = 'DEV'


class BLUL1Test(unittest.TestCase):

    def test0_blu_l1_daily_etl(self):
        spark = SparkSession \
            .builder \
            .appName("Python Spark SQL to execute BLU_raw_l1_etl_process") \
            .enableHiveSupport() \
            .getOrCreate()
        BLU_raw_l1_etl_process.calculate_l1_data('2018-10-10', spark)


if __name__ == '__main__':
    unittest.main()