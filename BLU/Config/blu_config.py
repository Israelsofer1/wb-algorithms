
job_dict = {'1':
                {'job_id': 1,
                 'bars':"""select id as bar_id from stg.bars where
                  country = 'Canada' and status between 10 and 19 limit 100""",
                 'from_date': "",
                 'to_date': "",
                 'l1':{'active':1, 'weights': {"penetration_share": 0.33, "qty_share": 0.33, "revenue_share": 0.33},
                        'min_bars': 2},
                 'l2':{'active':1, 'features':{'cnt_taps':0.25,'avg_check_duration':0.15,'abi_highend':0.35,'pct_beer_revenue':0.25}},
                 'l3':{'active':1,'criteria':'beer_penetration','min_entities':5,'threshold':0.4}},
            '2':
                {'job_id': 2,
                 'bars':"""select id as bar_id from stg.bars where venue_id>0
                  and country = 'Australia' and status between 10 and 19 limit 100""",
                 'from_date': '',
                 'to_date': '',
                 'l1':{'active':1, 'weights': {"penetration_share": 0.33, "qty_share": 0.33, "revenue_share": 0.33},
                        'min_bars': 3},
                 'l2':{'active':1, 'features':
                     {'avg_check_duration':0.15,'pct_food_revenue':0.25,'pct_unique_wine_items':0.25,'pct_beer_revenue':0.35}},
                 'l3': {'active': 1, 'criteria': 'spirit_penetration', 'min_entities': 2, 'threshold': 0.2}},
            '3':
                {'job_id': 3,
                 'bars':"""select id as bar_id from stg.bars where
                  country = 'Argentina' and status between 10 and 19 limit 100""",
                 'from_date': '',
                 'to_date': '',
                 'l1':{'active':1, 'weights': {"penetration_share": 0.33, "qty_share": 0.33, "revenue_share": 0.33},
                        'min_bars': 2},
                 'l2':{'active':1, 'features':{'cnt_taps':0.25,'avg_check_duration':0.15,'abi_highend':0.35,'pct_beer_revenue':0.25}},
                 'l3':{'active':1,'criteria':'beer_penetration','min_entities':5,'threshold':0.4}}}



