from datetime import datetime, timedelta
from pyspark.sql import SparkSession
from pyspark.sql.types import FloatType, StructType, StructField, IntegerType, StringType, ArrayType
from pyspark.sql import functions as func
from pyspark.sql import SQLContext as sc

spark = SparkSession \
    .builder \
    .appName("Python Spark SQL activation daily measurement") \
    .enableHiveSupport() \
    .getOrCreate()


# script - blu_individual_classification
def calculate_partial_category_score(arr):
    # arr[0] - cumulative score
    # arr[1] - share
    # arr[2]  - weight
    return arr[0] + arr[1] * arr[2]


def test_calculate_partial_category_score():
    test_arr = [0, 0.5, 0.5]
    ans = calculate_partial_category_score(test_arr)

    return calculate_partial_category_score.__name__ + '  ' + str(ans == 0.25)


def add_weights_cols(weights, df):
    # add weights columns to send to df
    # add category_score column with 0
    for k, v in weights.items():
        df = df.withColumn(k + "_weight", func.lit(v))

    df = df.withColumn("category_score", func.lit(0))
    return df


def test_add_weights_cols():
    weights = {'test': 0.5}
    test_df = spark.createDataFrame(["1", "2", "3"], StringType()).toDF("test")
    ans = add_weights_cols(weights=weights, df=test_df)
    return add_weights_cols.__name__ + '  ' + str(ans.schema.names == ['test', 'test_weight', 'category_score'])


def calc_mse1(category_score, category):
    # calculate mse1
    joined_list = zip(category_score, category)
    joined_list.sort(reverse=True)
    if len(joined_list) == 1:
        # only 1 category
        return 0.0
    avg = sum([tup[0] for tup in joined_list[1:]]) / len(joined_list[1:])
    mse1 = 0.0
    for elem in joined_list[1:]:
        mse1 += (elem[0] - avg) ** 2
    return mse1


def test_calc_mse1():
    # MSE1 = Sum for ranks 2 to 4 {(rank i score – average ranks score)^2]
    category = [1, 2, 3, 6]
    category_score = [0.1, 0.2, 0.3, 0.4]
    ans = calc_mse1(category_score, category)
    return calc_mse1.__name__ + '  ' + str(round(ans, 3) == 0.02)


def test2_calc_mse1():
    # MSE1 = Sum for ranks 2 to 4 {(rank i score – average ranks score)^2]
    category = [1]
    category_score = [0.1]
    ans = calc_mse1(category_score, category)
    return calc_mse1.__name__ + ' 2 ' + str(round(ans, 2) == 0.0)


def calc_mse2(category_score, category):
    # calculate mse 2
    joined_list = zip(category_score, category)
    joined_list.sort(reverse=True)
    if len(joined_list) < 2:
        # only 1 category
        return 1.0
    else:
        avg12 = sum([tup[0] for tup in joined_list[0:2]]) / len(joined_list[0:2])
    if len(joined_list) > 2:
        avg34 = sum([tup[0] for tup in joined_list[2:]]) / len(joined_list[2:])
    else:
        avg34 = 0
    mse2 = 0.0
    for category_rank in range(len(joined_list)):
        if category_rank in (0, 1):  ## change to specipic conditions
            mse2 += (joined_list[category_rank][0] - avg12) ** 2
        else:
            mse2 += (joined_list[category_rank][0] - avg34) ** 2

    return mse2


def test_calc_mse2():
    category = [1, 2, 3, 6]
    category_score = [0.1, 0.2, 0.3, 0.4]
    ans = calc_mse2(category_score, category)
    return calc_mse2.__name__ + '  ' + str(ans == 0.01)


def test2_calc_mse2():
    category = [1]
    category_score = [0.1]
    ans = calc_mse2(category_score, category)
    return calc_mse2.__name__ + ' 2 ' + str(round(ans, 2) == 1.0)


def test3_calc_mse2():
    category = [1, 2]
    category_score = [0.1, 0.2]
    ans = calc_mse2(category_score, category)
    return calc_mse2.__name__ + ' 3 ' + str(round(ans, 3) == 0.005)


def calc_rank(category_score, category, rank_number):
    # calculate category ranked 2
    joined_list = zip(category_score, category)
    joined_list.sort(reverse=True)
    # rank_num = rank_number.collect()[0]
    # for test only
    rank_num = rank_number[0]
    try:
        rank = joined_list[rank_num][1]
    except IndexError:
        rank = -1

    return rank


def test_calc_rank():
    category = [1, 2, 3, 6]
    category_score = [0.1, 0.2, 0.3, 0.4]
    rank_number = [2, 2, 2, 2]
    ans = calc_rank(category_score, category, rank_number)
    return calc_rank.__name__ + '  ' + str(ans == 2)


def assign_group(mse1, mse2, rank1, rank2):
    # assign bar into group (hybrid and non hybrid)
    if mse2 >= mse1:
        return [rank1]
    else:
        group = [rank1, rank2]
        group.sort()
        return group


def test_assign_group():
    mse1 = 1
    mse2 = 0.5
    rank1 = 1
    rank2 = 2
    ans = assign_group(mse1, mse2, rank1, rank2)
    return assign_group.__name__ + '  ' + str(ans == [1, 2])


def test2_assign_group():
    mse1 = 0.5
    mse2 = 1
    rank1 = 2
    rank2 = 1
    ans = assign_group(mse1, mse2, rank1, rank2)
    return assign_group.__name__ + ' 2 ' + str(ans == [2])


# script - blu_merge_classification

def arr_to_string(my_arr):
    return ','.join(map(str, my_arr))


def string_to_arr(my_string):
    print(my_string)
    return [my_string.split(",")]


def hybrid_merge_switch(rank):
    # udf
    return [rank]


def hybrid_merge(bars_df, min_bars):
    # preform hybrid merge

    hybrid_bars_df = bars_df.filter((func.size(func.col("group")) == 2))
    non_hybrid_bars_df = bars_df.filter((func.size(func.col("group")) == 1))

    # creating arr df that count number of bars
    agg_df = hybrid_bars_df.groupby('group').agg({'bar_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(bar_id)", "number_of_bars")
    # print('agg df')
    # agg_df.show()
    filtered_agg_df = agg_df.filter((func.col('number_of_bars') < min_bars.value) & (func.size(func.col("group")) == 2))
    # print('filtered')
    # filtered_agg_df.show()
    filtered_agg_df = filtered_agg_df.withColumn('groupstr', udf_arr_to_string((filtered_agg_df['group'])))
    group_list = filtered_agg_df.select('groupstr').collect()
    group_array = [str(i.groupstr) for i in group_list]
    hybrid_bars_df = hybrid_bars_df.withColumn('groupstr', udf_arr_to_string((hybrid_bars_df['group'])))
    non_hybrid_bars_df = non_hybrid_bars_df.withColumn('groupstr', udf_arr_to_string((non_hybrid_bars_df['group'])))
    hybrid_bars_ok = hybrid_bars_df.filter(~hybrid_bars_df.groupstr.isin(group_array))
    hybrid_bars_not_ok = hybrid_bars_df.filter(hybrid_bars_df.groupstr.isin(group_array))

    # print('ok bars')
    # print(group_array)
    # hybrid_bars_ok.show()
    hybrid_bars_not_ok = hybrid_bars_not_ok.withColumn('group', udf_hybrid_merge_switch(hybrid_bars_not_ok['rank1']))
    non_hybrid_bars_df = non_hybrid_bars_df.union(hybrid_bars_not_ok)
    non_hybrid_bars_df = non_hybrid_bars_df.withColumn('groupstr', udf_arr_to_string((non_hybrid_bars_df['group'])))
    hybrid_bars_ok = hybrid_bars_ok.withColumn('groupstr', udf_arr_to_string((hybrid_bars_ok['group'])))
    # print('print tables')
    # hybrid_bars_ok.show()
    # non_hybrid_bars_df.show()
    return non_hybrid_bars_df, hybrid_bars_ok


def test_hybrid_merge():
    min_bars = sc.broadcast(2)
    bars_df = spark.sparkContext.parallelize([[1, 111, 0.5, 0.1, 1, 2, 3, 6, [1, 2]],
                                              [1, 222, 0.1, 0.5, 1, 2, 3, 6, [1]],
                                              [1, 333, 0.5, 0.1, 2, 1, 3, 6, [1, 2]],
                                              [1, 444, 0.1, 0.5, 2, 1, 3, 6, [2]],
                                              [1, 555, 0.1, 0.5, 6, 1, 3, 2, [6]]]).toDF(
        ["run_id", "bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group'])
    output1, output2 = hybrid_merge(bars_df, min_bars)
    test1_df = spark.sparkContext.parallelize([
        [1, 222, 0.1, 0.5, 1, 2, 3, 6, [1], 1],
        [1, 444, 0.1, 0.5, 2, 1, 3, 6, [2], 2],
        [1, 555, 0.1, 0.5, 6, 1, 3, 2, [6], 6]]).toDF(
        ["run_id", "bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'groupstr'])
    test2_df = spark.sparkContext.parallelize([
        [1, 111, 0.5, 0.1, 1, 2, 3, 6, [1, 2], "1,2"],
        [1, 333, 0.5, 0.1, 2, 1, 3, 6, [1, 2], "1,2"]]).toDF(
        ["run_id", "bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'groupstr'])

    return hybrid_merge.__name__ + '  ' + str(
        output1.subtract(test1_df).count() == test1_df.subtract(output1).count() == 0 == output2.subtract(
            test2_df).count() == test2_df.subtract(output2).count())


def test2_hybrid_merge():
    min_bars = sc.broadcast(3)
    bars_df = spark.sparkContext.parallelize([[1, 111, 0.5, 0.1, 1, 2, 3, 6, [1, 2]],
                                              [1, 222, 0.1, 0.5, 1, 2, 3, 6, [1]],
                                              [1, 333, 0.5, 0.1, 2, 1, 3, 6, [1, 2]],
                                              [1, 444, 0.1, 0.5, 2, 1, 3, 6, [2]],
                                              [1, 555, 0.1, 0.5, 6, 1, 3, 2, [6]]]).toDF(
        ["run_id", "bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group'])
    output1, output2 = hybrid_merge(bars_df, min_bars)
    test1_df = spark.sparkContext.parallelize([
        [1, 222, 0.1, 0.5, 1, 2, 3, 6, [1], 1],
        [1, 444, 0.1, 0.5, 2, 1, 3, 6, [2], 2],
        [1, 555, 0.1, 0.5, 6, 1, 3, 2, [6], 6],
        [1, 111, 0.5, 0.1, 1, 2, 3, 6, [1], 1],
        [1, 333, 0.5, 0.1, 2, 1, 3, 6, [2], 2]]).toDF(
        ["run_id", "bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'groupstr'])
    return hybrid_merge.__name__ + ' 2 ' + str(
        0 == output1.subtract(test1_df).count() == test1_df.subtract(output1).count())


def non_hybrid_merge_with_order(new_group, group):
    if new_group == -1:
        return [group]
    else:
        return [new_group]


def non_hybrid_merge_perm(non_hybrid_bars_df, filtered_agg_df, agg_df, perm, min_bars):
    non_hybrid_bars_df_perm = non_hybrid_bars_df
    filtered_agg_df_perm = filtered_agg_df
    agg_df_perm = agg_df
    # print("starting order: "+str(perm))
    for rank in range(2, 5, 1):
        for cat in perm:
            # print('category to run '+str(cat))
            # print('perm 2')
            # filtered_agg_df_perm.show()
            if filtered_agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
                # no need to run on group
                # print("no need to run on group")
                continue
            # non_hybrid_bars_df_perm.show()
            if agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
                continue;
            if int(agg_df_perm.filter(func.col("groupstr") == str(cat)).collect()[0][
                       'number_of_bars']) >= min_bars.value:
                # the group has enough bars
                # print('have enough')
                continue
            else:
                # the group doesnt has enough bars
                bars_to_change = non_hybrid_bars_df_perm.filter(func.col("groupstr") == str(cat))
                bars_to_keep = non_hybrid_bars_df_perm.filter(func.col("groupstr") != str(cat))
                bars_to_change = bars_to_change.withColumn('group', udf_non_hybrid_merge_with_order(
                    bars_to_change['rank' + str(rank)], bars_to_change['groupstr']))
                # print('after change group')
                # bars_to_change.show()
                bars_to_change = bars_to_change.withColumn('groupstr', udf_arr_to_string((bars_to_change['group'])))
                # bars_to_change.show()
                non_hybrid_bars_df_perm = bars_to_keep.union(bars_to_change)
                # update agg dataframe
                agg_df_perm = non_hybrid_bars_df_perm.groupby('group').agg({'bar_id': 'count'})
                agg_df_perm = agg_df_perm.withColumnRenamed("count(bar_id)", "number_of_bars")
                agg_df_perm = agg_df_perm.withColumn('groupstr', udf_arr_to_string((agg_df_perm['group'])))
                filtered_agg_df_perm = agg_df_perm.filter((func.col('number_of_bars') < min_bars.value))
                # print('perm')
                # filtered_agg_df_perm.show()
                # print('merge')
                # non_hybrid_bars_df_perm.show()
                non_hybrid_bars_df_perm.cache()
    non_hybrid_bars_df_perm = non_hybrid_bars_df_perm.withColumn("group", func.split(func.col("groupstr"), ",\s*").cast(
        ArrayType(IntegerType())))
    non_hybrid_bars_df_perm = check_na(non_hybrid_bars_df_perm, min_bars=min_bars)
    return non_hybrid_bars_df_perm
    # exit after 1 permutation at the moment


def test_non_hybrid_merge_perm():
    min_bars = sc.broadcast(3)
    bars_df = spark.sparkContext.parallelize([[111, 0.5, 0.1, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [222, 0.1, 0.5, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [333, 0.5, 0.1, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [444, 0.1, 0.5, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [555, 0.1, 0.5, 6, 1, 3, 2, [6], '2018-10-10', "6"]]).toDF(
        ["bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'created_date', 'groupstr'])
    agg_df = bars_df.groupby('group').agg({'bar_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(bar_id)", "number_of_bars")
    agg_df = agg_df.withColumn('groupstr', udf_arr_to_string((agg_df['group'])))
    filtered_agg_df = agg_df.filter((func.col('number_of_bars') < min_bars.value))
    perm = [1, 2, 3, 6]
    output1 = non_hybrid_merge_perm(bars_df, filtered_agg_df, agg_df, perm, min_bars)
    groupstrlst = output1.select("groupstr").collect()
    groupstrlst = [int(i.groupstr) for i in groupstrlst]
    return non_hybrid_merge_perm.__name__ + '  ' + str(groupstrlst == [2, 2, 2, 2, 2])


def test2_non_hybrid_merge_perm():
    min_bars = sc.broadcast(3)
    bars_df = spark.sparkContext.parallelize([[111, 0.5, 0.1, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [222, 0.1, 0.5, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [333, 0.5, 0.1, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [444, 0.1, 0.5, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [555, 0.1, 0.5, 6, 1, 3, -1, [6], '2018-10-10', "6"]]).toDF(
        ["bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'created_date', 'groupstr'])
    agg_df = bars_df.groupby('group').agg({'bar_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(bar_id)", "number_of_bars")
    agg_df = agg_df.withColumn('groupstr', udf_arr_to_string((agg_df['group'])))
    filtered_agg_df = agg_df.filter((func.col('number_of_bars') < min_bars.value))
    perm = [1, 2, 3, 6]
    output1 = non_hybrid_merge_perm(bars_df, filtered_agg_df, agg_df, perm, min_bars)
    groupstrlst = output1.select("groupstr").collect()
    groupstrlst = [int(i.groupstr) for i in groupstrlst]
    return non_hybrid_merge_perm.__name__ + ' 2 ' + str(groupstrlst == [2, 2, 2, 2, -1])


def check_na(bars_df, min_bars):
    # mark bars that don't have groups
    agg_df = bars_df.groupby('groupstr').agg({'bar_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(bar_id)", "number_of_bars")
    filtered_agg_df = agg_df.filter((func.col('number_of_bars') < min_bars.value))
    if len(filtered_agg_df.head(1)) == 0:
        return bars_df
    else:
        mvv_list = filtered_agg_df.selectExpr("groupstr as groupstr")
        na_group_array = [int(i.groupstr) for i in mvv_list.collect()]
        bars_to_na = bars_df.filter(func.col('groupstr').isin(na_group_array))
        bars_to_na = bars_to_na.withColumn('groupstr', func.lit(-1))
        rest_of_bars = bars_df.filter(~func.col('groupstr').isin(na_group_array))
        return rest_of_bars.union(bars_to_na)


def test_check_na():
    # mark bars that don't have groups
    min_bars = sc.broadcast(2)
    bars_df = spark.sparkContext.parallelize([[111, 0.5, 0.1, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [222, 0.1, 0.5, 1, 2, 3, 6, [1], '2018-10-10', "1"],
                                              [333, 0.5, 0.1, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [444, 0.1, 0.5, 2, 1, 3, 6, [2], '2018-10-10', "2"],
                                              [555, 0.1, 0.5, 6, 1, 3, -1, [6], '2018-10-10', "6"]]).toDF(
        ["bar_id", "mse1_score", "mse2_score", "rank1", "rank2", "rank3", "rank4", 'group', 'created_date', 'groupstr'])
    output1 = check_na(bars_df, min_bars)
    groupstrlst = output1.select("groupstr").collect()
    groupstrlst = [int(i.groupstr) for i in groupstrlst]
    return check_na.__name__ + '  ' + str(groupstrlst == [1, 1, 2, 2, -1])


udf_arr_to_string = func.udf(arr_to_string, StringType())
udf_string_to_arr = func.udf(string_to_arr, ArrayType(IntegerType()))
udf_hybrid_merge_switch = func.udf(hybrid_merge_switch, ArrayType(IntegerType()))
udf_non_hybrid_merge_with_order = func.udf(non_hybrid_merge_with_order, ArrayType(IntegerType()))

print("script name - blu_individual_classification")
print(test_calculate_partial_category_score())
print(test_add_weights_cols())
print(test_calc_mse1())
print(test2_calc_mse1())
print(test_calc_mse2())
print(test2_calc_mse2())
print(test3_calc_mse2())
print(test_calc_rank())
print(test_assign_group())
print(test2_assign_group())
print("-------------------------------------------------------------")
print("script name - blu_merge_classification")
print(test_hybrid_merge())
print(test2_hybrid_merge())
print(test_non_hybrid_merge_perm())
print(test2_non_hybrid_merge_perm())
print(test_check_na())