from pyspark.sql import SparkSession
from datetime import datetime
from BLU import blu_job
from .Daily import blu_daily_bars_distances_etl_job, blu_daily_etl_job
import argparse

if __name__ == "__main__":

    spark = SparkSession \
        .builder \
        .appName("blu") \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .enableHiveSupport() \
        .getOrCreate()

    print("%s - BLU job - started" % datetime.now())

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', required=True)
    parser.add_argument('--daily', required=True)
    parser.add_argument('--etl_days_back', required=True)
    args = parser.parse_args()

    print("arguments received: ")
    print("path: %s" % args.path)
    print("daily: %s" % args.daily)
    print("etl days back: %s" % args.etl_days_back)

    if args.daily == 'True':
        print("%s - BLU daily - started" % datetime.now())
        print("%s - BLU daily bars distances job - started" % datetime.now())
        blu_daily_bars_distances_etl_job.main(spark)
        print("%s - BLU daily bars distances job - ended" % datetime.now())
        print("%s - BLU daily etl job - started" % datetime.now())
        blu_daily_etl_job.main(spark, args.etl_days_back)
        print("%s - BLU daily etl job - ended" % datetime.now())
        print("%s - BLU daily - ended" % datetime.now())
    else:
        print("%s - BLU job - started" % datetime.now())
        blu_job.run(path=args.path)
        print("%s - BLU job - ended" % datetime.now())
