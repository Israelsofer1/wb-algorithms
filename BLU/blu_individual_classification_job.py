from pyspark.sql import SparkSession
from datetime import datetime
import blu_config as conf
import blu_l1
import sys


if __name__ == "__main__":
    print("%s - BLU_l1 - started - arguments length: %d" % (datetime.now(), len(sys.argv)))

    spark = SparkSession \
        .builder \
        .appName("Python Spark SQL to execute activation_tracking_job to redshift") \
        .enableHiveSupport() \
        .getOrCreate()
    job = 'job_1'
    settings = conf.job_dict[job]
    blu_l1.process(spark=spark, share_weights=settings['l1_weights'])
    print("%s - BLU_l1_job - ended" % datetime.now())


