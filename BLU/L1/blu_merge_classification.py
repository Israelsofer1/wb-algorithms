import itertools
from pyspark.sql.types import IntegerType, StringType, ArrayType
from pyspark.sql import functions as func
# import jobs_config
from datetime import datetime


# define paths for redshift, hive
# redshift_url = jobs_config.redshift_url
# redshift_temp_dir = jobs_config.redshift_temp_dir
# redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
# hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name


def arr_to_string(my_arr):
    return ','.join(map(str, my_arr))


def string_to_arr(my_string):
    print(my_string)
    return [my_string.split(",")]


def pull_l1_groups(spark, run_id):
    # need to add more config with the dates
    # pull entities group for latest date
    l1_df = spark.sql(
        """select * from stg.blu_individual_classification 
           where run_id = """+str(run_id))

    return l1_df

def hybrid_merge_switch(rank):
    # udf
    return [rank]


def hybrid_merge(entities_df, min_bars):
    # preform hybrid merge

    hybrid_entities_df = entities_df.filter((func.size(func.col("group")) == 2))
    non_hybrid_entities_df = entities_df.filter((func.size(func.col("group")) == 1))

    # creating arr df that count number of entities_df
    agg_df = hybrid_entities_df.groupby('group').agg({'entity_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(entity_id)", "number_of_entities")
    filtered_agg_df = agg_df.filter((func.col('number_of_entities') < min_bars.value) & (func.size(func.col("group")) == 2))
    filtered_agg_df = filtered_agg_df.withColumn('groupstr', udf_arr_to_string((filtered_agg_df['group'])))
    group_list = filtered_agg_df.select('groupstr').collect()
    group_array = [str(i.groupstr) for i in group_list]
    hybrid_entities_df = hybrid_entities_df.withColumn('groupstr', udf_arr_to_string((hybrid_entities_df['group'])))
    non_hybrid_entities_df = non_hybrid_entities_df.withColumn('groupstr', udf_arr_to_string((non_hybrid_entities_df['group'])))
    hybrid_entities_ok = hybrid_entities_df.filter(~hybrid_entities_df.groupstr.isin(group_array))
    hybrid_entities_not_ok = hybrid_entities_df.filter(hybrid_entities_df.groupstr.isin(group_array))

    hybrid_entities_not_ok = hybrid_entities_not_ok.withColumn('group', udf_hybrid_merge_switch(hybrid_entities_not_ok['rank1']))
    non_hybrid_entities_df = non_hybrid_entities_df.union(hybrid_entities_not_ok)
    non_hybrid_entities_df = non_hybrid_entities_df.withColumn('groupstr', udf_arr_to_string((non_hybrid_entities_df['group'])))
    hybrid_entities_ok = hybrid_entities_ok.withColumn('groupstr', udf_arr_to_string((hybrid_entities_ok['group'])))
    return non_hybrid_entities_df, hybrid_entities_ok


def non_hybrid_merge_with_order(new_group, group):
    if new_group == -1:
        return [group]
    else:
        return [new_group]

def non_hybrid_merge_perm(non_hybrid_entities_df, filtered_agg_df, agg_df, perm, min_bars):
    non_hybrid_entities_df_perm = non_hybrid_entities_df
    filtered_agg_df_perm = filtered_agg_df
    agg_df_perm = agg_df
    # print("starting order: "+str(perm))
    for rank in range(2, 5, 1):
        # print("starting rank: "+str(rank))
        for cat in perm:
            non_hybrid_entities_df_perm.persist()
            if filtered_agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
                # no need to run on group
                continue
            # print('category to run '+str(cat))
            # non_hybrid_bars_df_perm.show()
            if agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
                # no need to run on group
                continue
            # agg_df_perm.show()
            if int(agg_df_perm.filter(func.col("groupstr") == str(cat)).collect()[0][
                       'number_of_entities']) >= min_bars.value:
                # the group has enough bars
                # print('have enough')
                continue
            else:
                # the group doesnt has enough bars
                entities_to_change = non_hybrid_entities_df_perm.filter(func.col("groupstr") == str(cat))
                entities_to_keep = non_hybrid_entities_df_perm.filter(func.col("groupstr") != str(cat))
                entities_to_change = entities_to_change.withColumn('group', udf_non_hybrid_merge_with_order(
                    entities_to_change['rank' + str(rank)], entities_to_change['groupstr']))
                # print('after change group')
                # bars_to_change.show()
                entities_to_change = entities_to_change.withColumn('groupstr', udf_arr_to_string((entities_to_change['group'])))
                # bars_to_change.show()
                non_hybrid_entities_df_perm = entities_to_keep.union(entities_to_change)
                # update agg dataframe
                agg_df_perm = non_hybrid_entities_df_perm.groupby('group').agg({'entity_id': 'count'})
                agg_df_perm = agg_df_perm.withColumnRenamed("count(entity_id)", "number_of_entities")
                agg_df_perm = agg_df_perm.withColumn('groupstr', udf_arr_to_string((agg_df_perm['group'])))
                filtered_agg_df_perm = agg_df_perm.filter((func.col('number_of_entities') < min_bars.value))
                # print('merge')
                # non_hybrid_bars_df_perm.show()
    non_hybrid_entities_df_perm = non_hybrid_entities_df_perm.withColumn("group", func.split(func.col("groupstr"), ",\s*").cast(
        ArrayType(IntegerType())))
    non_hybrid_entities_df_perm = check_na(non_hybrid_entities_df_perm, min_bars=min_bars)
    return non_hybrid_entities_df_perm
    # exit after 1 permutation at the moment


def non_hybrid_merge(entities_df, min_bars):

    # hybrid_bars_df = bars_df.filter((func.size(func.col("group")) == 2))
    non_hybrid_entities_df = entities_df.filter((func.size(func.col("group")) == 1))
    agg_df = non_hybrid_entities_df.groupby('group').agg({'entity_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(entity_id)", "number_of_entities")
    agg_df = agg_df.withColumn('groupstr', udf_arr_to_string((agg_df['group'])))
    # agg_df.show()
    filtered_agg_df = agg_df.filter((func.col('number_of_entities') < min_bars.value))
    for perm in itertools.permutations([1, 2, 3, 6]):
        non_hybrid_entities_df_perm = non_hybrid_merge_perm(non_hybrid_entities_df=non_hybrid_entities_df,
                                                        filtered_agg_df=filtered_agg_df, agg_df=agg_df,
                                                        perm=perm, min_bars=min_bars)
        break

    # non_hybrid_bars_df_perm.drop('groupstr')
    return non_hybrid_entities_df_perm


def check_na(entities_df, min_bars):
    # mark bars that don't have groups
    agg_df = entities_df.groupby('groupstr').agg({'entity_id': 'count'})
    agg_df = agg_df.withColumnRenamed("count(entity_id)", "number_of_entities")
    filtered_agg_df = agg_df.filter((func.col('number_of_entities') < min_bars.value))
    if len(filtered_agg_df.head(1)) == 0:
        return entities_df
    else:
        mvv_list = filtered_agg_df.selectExpr("groupstr as groupstr")
        na_group_array = [int(i.groupstr) for i in mvv_list.collect()]
        entities_to_na = entities_df.filter(func.col('groupstr').isin(na_group_array))
        # print('na bars')
        # bars_to_na.show()
        entities_to_na = entities_to_na.withColumn('groupstr', func.lit(-1))
        rest_of_entities = entities_df.filter(~func.col('groupstr').isin(na_group_array))
        return rest_of_entities.union(entities_to_na)

def insert_results(df_to_insert, spark):
    df_to_insert.repartition(1).registerTempTable("l1_merge_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_merge_classification SELECT 
                 entity_id,
                 venue_id,
                 bar_id,
                 group,
                 created_at,
                 run_id
                 FROM l1_merge_df where run_id not in (select run_id from stg.blu_merge_classification)""")

# def pull_run_id_info(run_id, spark):
#     metadata_df = spark.sql("""select * from  stg.blu_metadata_dates where run_id = """ + str(run_id))
#     start = metadata_df.select('start').collect()[0][0]
#     end = metadata_df.select('end').collect()[0][0]
#     return str(start), str(end)


def process(spark, minn, run_id):

    min_bars = spark.sparkContext.broadcast(minn)
    orig_groups = pull_l1_groups(spark=spark, run_id=run_id)
    df_after_hybrid, hybrid_entities_ok = hybrid_merge(orig_groups, min_bars)
    df_after_non_hybrid = non_hybrid_merge(df_after_hybrid, min_bars=min_bars)
    df_final = df_after_non_hybrid.union(hybrid_entities_ok)
    df_final = df_final.withColumn("run_id", func.lit(run_id))
    df_final = df_final.withColumn("created_at", func.lit(datetime.now()))
    #df_final.show()
    #df_venues_bars = orig_groups.select('entity_id', 'venue_id','bar_id')
    #df_venues_bars.show()
    #df_final = df_final.join(df_venues_bars, on='entity_id', how='inner')

    insert_results(df_final, spark)

# define udf

udf_arr_to_string = func.udf(arr_to_string, StringType())
udf_string_to_arr = func.udf(string_to_arr, ArrayType(IntegerType()))
udf_hybrid_merge_switch = func.udf(hybrid_merge_switch, ArrayType(IntegerType()))
udf_non_hybrid_merge_with_order = func.udf(non_hybrid_merge_with_order, ArrayType(IntegerType()))

# def check_valid(agg_df):
#     # check if a final soulotion is valid
#     filtered_agg_df = agg_df.filter((col('number_of_bars') < min_bars.value))
#     if len(filtered_agg_df.head(1)) == 0:
#         return True
#     else:
#         return False


# def calculate_avg_category_score(bars_df):
#     bars_categories_scores = pull_categories_scores()

    # return

# non_hybrid_bars_df_perm = non_hybrid_bars_df
# filtered_agg_df_perm = filtered_agg_df
# agg_df_perm = agg_df
# # print("starting order: "+str(perm))
# for rank in range(2, 5, 1):
#     # print("starting rank: "+str(rank))
#     for cat in perm:
#         if filtered_agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
#             # no need to run on group
#             continue
#         # print('category to run '+str(cat))
#         # non_hybrid_bars_df_perm.show()
#         if agg_df_perm.filter(func.col("groupstr") == str(cat)).count() == 0:
#             continue;
#         if int(agg_df_perm.filter(func.col("groupstr") == str(cat)).collect()[0][
#                    'number_of_bars']) >= min_bars.value:
#             # the group has enough bars
#             # print('have enough')
#             continue
#         else:
#             # the group doesnt has enough bars
#             bars_to_change = non_hybrid_bars_df_perm.filter(func.col("groupstr") == str(cat))
#             bars_to_keep = non_hybrid_bars_df_perm.filter(func.col("groupstr") <> str(cat))
#             bars_to_change = bars_to_change.withColumn('group', udf_non_hybrid_merge_with_order(
#                 bars_to_change['rank' + str(rank)], bars_to_change['groupstr']))
#             # print('after change group')
#             # bars_to_change.show()
#             bars_to_change = bars_to_change.withColumn('groupstr', udf_arr_to_string((bars_to_change['group'])))
#             # bars_to_change.show()
#             non_hybrid_bars_df_perm = bars_to_keep.union(bars_to_change)
#             # update agg dataframe
#             agg_df_perm = non_hybrid_bars_df_perm.groupby('group').agg({'bar_id': 'count'})
#             agg_df_perm = agg_df_perm.withColumnRenamed("count(bar_id)", "number_of_bars")
#             agg_df_perm = agg_df_perm.withColumn('groupstr', udf_arr_to_string((agg_df_perm['group'])))
#             # print('merge')
#             # non_hybrid_bars_df_perm.show()
# non_hybrid_bars_df_perm.show()
# non_hybrid_bars_df_perm = non_hybrid_bars_df_perm.withColumn("group", func.split(func.col("groupstr"), ",\s*").cast(
#     ArrayType(IntegerType())))
# non_hybrid_bars_df_perm.show()
# non_hybrid_bars_df_perm = check_na(non_hybrid_bars_df_perm, min_bars=min_bars)
# exit after 1 permutation at the moment