from datetime import datetime
from pyspark.sql.types import FloatType, IntegerType, ArrayType
# import jobs_config
from pyspark.sql import functions as func

# define paths for redshift, hive
# redshift_url = jobs_config.redshift_url
# redshift_temp_dir = jobs_config.redshift_temp_dir
# redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
# hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name

# define broadcast variable

def pull_l1_preprocess(spark, run_id):

    # pull latest date for all bars in this run_id
    l1_df = spark.sql(
        """select * from stg.blu_entity_shares where run_id ="""+str(run_id))
    return l1_df


# category_scores

def calculate_partial_category_score(arr):
    # arr[0] - cumulative score
    # arr[1] - share
    # arr[2]  - weight

    for i in range(len(arr)):
        if not arr[i]:
            arr[i] = 0

    return arr[0] + arr[1] * arr[2]


def add_weights_cols(weights, df):
    # add weights columns to send to df
    # add category_score column with 0
    for k, v in weights.items():
        df = df.withColumn(k + "_weight", func.lit(v))

    df = df.withColumn("category_score", func.lit(0)) #remove
    return df


def calculate_category_score(df, input_weights):
    """
    explim why input weights
    :param df:
    :param input_weights:
    :return:
    """
    # calculate category_score #
    for k, v in input_weights.items():
        df = df.withColumn("category_score", udf_category_score(func.array('category_score', k, k + "_weight")))

    return df

# mse 1 & 2 scores

def calc_mse1(category_score, category):
    # calculate mse1
    joined_list = list(zip(category_score, category))
    joined_list.sort(reverse=True)
    if len(joined_list) == 1:
        # only 1 category
        return 0.0
    avg = sum([tup[0] for tup in joined_list[1:]]) / len(joined_list[1:])
    mse1 = 0.0
    for elem in joined_list[1:]:
        mse1 += (elem[0] - avg) ** 2
    return mse1


def calc_mse2(category_score, category):
    # calculate mse 2
    joined_list = list(zip(category_score, category))
    joined_list.sort(reverse=True)
    if len(joined_list) < 2:
        # only 1 category
        return 1.0
    else:
        avg12 = sum([tup[0] for tup in joined_list[0:2]]) / len(joined_list[0:2])
    if len(joined_list) > 2:
        avg34 = sum([tup[0] for tup in joined_list[2:]]) / len(joined_list[2:])
    else:
        avg34 = 0
    mse2 = 0.0
    for category_rank in range(len(joined_list)):
        if category_rank in (0, 1):
            mse2 += (joined_list[category_rank][0] - avg12) ** 2
        else:
            mse2 += (joined_list[category_rank][0] - avg34) ** 2

    return mse2

def calc_rank(category_score, category, rank_number):
    # calculate category ranked 2
    joined_list = list(zip(category_score, category))
    joined_list.sort(reverse=True)
    rank_num = rank_number
    try:
        rank = joined_list[rank_num][1]
    except IndexError:
        rank = -1
    return rank

def assign_group(mse1, mse2, rank1, rank2):
    # assign bar into group (hybrid and non hybrid)
    if mse2 >= mse1:
        return [rank1]
    else:
        group = [rank1, rank2]
        group.sort()
        return group


def join_red(left, right, key):
    # join 2 df by certain key
    return left.join(right, on=key, how='inner')


# definition of udf_category_score udf

udf_category_score = func.udf(calculate_partial_category_score, FloatType())

def pull_run_id_info(run_id, spark):
    metadata_df = spark.sql("""select * from  stg.blu_metadata_dates where run_id = """ + str(run_id))
    start = metadata_df.select('start').collect()[0][0]
    end = metadata_df.select('end').collect()[0][0]
    return str(start), str(end)

def process(spark, JobConfig, run_id):
    # run blu 1 category and mse score and save results on blu_individual_classification table
    df = pull_l1_preprocess(spark=spark, run_id=run_id)
    share_weights = JobConfig['l1']['weights']
    df_with_weights = add_weights_cols(share_weights, df)
    # calculate and round category score
    df_with_category_score = calculate_category_score(df_with_weights, share_weights)
    df_with_category_score = df_with_category_score.withColumn("category_score",
                                                               func.round(df_with_category_score["category_score"], 2))
    # create agg dfs by entity id where categories and scores are lists
    df_grouped_entity_id = df_with_category_score.groupBy(df_with_category_score['entity_id']).agg(
        func.collect_list(df_with_category_score['category_score']).alias('category_score'),
        func.collect_list(df_with_category_score['category_id']).alias('category_id'),
        func.max(df_with_category_score['venue_id']).alias('venue_id'),
        func.max(df_with_category_score['bar_id']).alias('bar_id'))


    # calculate mse1,2 & ranks 1,2,3,4 with udfs
    df_mse1 = df_grouped_entity_id.select(df_grouped_entity_id['entity_id'],
                                       func.udf(calc_mse1, FloatType())(df_grouped_entity_id['category_score'],
                                                                   df_grouped_entity_id['category_id']).alias('mse1_score'))
    df_mse2 = df_grouped_entity_id.select(df_grouped_entity_id['entity_id'],
                                       func.udf(calc_mse2, FloatType())(df_grouped_entity_id['category_score'],
                                                                   df_grouped_entity_id['category_id']).alias('mse2_score'))

    for i in range(0, 4, 1):
        df_grouped_entity_id = df_grouped_entity_id.withColumn("rank"+str(i+1), func.udf(calc_rank, IntegerType())(df_grouped_entity_id['category_score'],
                                                                       df_grouped_entity_id['category_id'], func.lit(i)).alias("rank"+str(i+1)))


    # joining all the dfs into 1 df
    #df_list = [df_grouped_entity_id, df_mse1, df_mse2, df_rank1, df_rank2, df_rank3, df_rank4]
    df_list = [df_grouped_entity_id, df_mse1, df_mse2]
    df_final = df_list[0]
    for df_next in df_list[1:]:
        df_final = df_final.join(df_next, on='entity_id', how='inner')

    # assign groups to bars (before merge)
    df_final = df_final.withColumn('group', func.udf(assign_group, ArrayType(IntegerType()))(df_final['mse1_score'],
                                                                                        df_final['mse2_score'],
                                                                                        df_final['rank1'],
                                                                                        df_final['rank2']))
    df_final = df_final.withColumn("run_id", func.lit(run_id))
    df_final = df_final.withColumn("created_at", func.lit(datetime.now()))
    # write results into sql
    df_final.repartition(1).registerTempTable("individual_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_individual_classification SELECT 
                 entity_id,
                 venue_id,
                 bar_id,
                 mse1_score,
                 mse2_score,
                 rank1,
                 rank2,
                 rank3,
                 rank4,
                 group,
                 created_at,
                 run_id
                 FROM individual_df  where run_id not in (select run_id from stg.blu_individual_classification)""")

    # df_final.write \
    #     .format("com.databricks.spark.redshift") \
    #     .option("url", redshift_url) \
    #     .option("dbtable", "stg.blu_individual_classification") \
    #     .option("tempdir", redshift_temp_dir) \
    #     .option("aws_iam_role", redshift_aws_iam_role) \
    #     .save()

    # df_final.show()
    # df_final.repartition(1).registerTempTable("l1_score_df")

# share_weights = {"penetration_share": 0.33, "qty_share": 0.34, "revenue_share": 0.33}  # BLU_config.l1_weights

# df_rank1 = df_grouped_entity_id.select(df_grouped_entity_id['bar_id'],
#                                     func.udf(calc_rank1, IntegerType())(df_grouped_entity_id['category_score'],
#                                                                    df_grouped_entity_id['category']).alias('rank1'))
# df_rank2 = df_grouped_entity_id.select(df_grouped_entity_id['bar_id'],
#                                     func.udf(calc_rank2, IntegerType())(df_grouped_entity_id['category_score'],
#                                                                    df_grouped_entity_id['category']).alias('rank2'))
# df_rank3 = df_grouped_entity_id.select(df_grouped_entity_id['bar_id'],
#                                     func.udf(calc_rank3, IntegerType())(df_grouped_entity_id['category_score'],
#                                                                    df_grouped_entity_id['category']).alias('rank3'))
# df_rank4 = df_grouped_entity_id.select(df_grouped_entity_id['bar_id'],
#                                     func.udf(calc_rank4, IntegerType())(df_grouped_entity_id['category_score'],
#                                                                    df_grouped_entity_id['category']).alias('rank4'))

# def calc_rank3(category_score, category):
#     # calculate category ranked 3
#     joined_list = zip(category_score, category)
#     joined_list.sort(reverse=True)
#     try:
#         rank3 = joined_list[2][1]
#     except IndexError:
#         rank3 = -1
#     return rank3
#
#
# def calc_rank4(category_score, category):
#     # calculate category ranked 4
#     joined_list = zip(category_score, category)
#     joined_list.sort(reverse=True)
#     try:
#         rank4 = joined_list[3][1]
#     except IndexError:
#         rank4 = -1
#     return rank4

# def calc_rank1(category_score, category):
#     # calculate category ranked 1
#     joined_list = zip(category_score, category)
#     joined_list.sort(reverse=True)
#     return joined_list[0][1]
#
#
# def calc_rank2(category_score, category):
#     # calculate category ranked 2
#     joined_list = zip(category_score, category)
#     joined_list.sort(reverse=True)
#     try:
#         rank2 = joined_list[1][1]
#     except IndexError:
#         rank2 = -1
#     return rank2