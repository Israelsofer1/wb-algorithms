from datetime import datetime
# import jobs_config
from pyspark.sql import functions as func

# redshift_url = jobs_config.redshift_url
# redshift_temp_dir = jobs_config.redshift_temp_dir
# redshift_aws_iam_role = jobs_config.redshift_aws_iam_role
# hive_stg_s3_bucket_name = jobs_config.hive_stg_s3_bucket_name


def pull_blu_raw_data(start, end, spark, entities):
    # pull blu raw data from hive in order to aggregate it
    entities_str = ','.join(entities)
    l1_raw_df = spark.sql(
        """select * from stg.blu_l1_raw_data where entity_id in ("""+entities_str+""") and
        nd between '""" + str(start) + """' and '""" + str(end) + """'""")
    # TODO: move ND to be first

    return l1_raw_df


def process_data_to_write(run_id, l1_joined_df):

    to_add = [('run_id', int(run_id))]
    for tup in to_add:
        l1_joined_df = l1_joined_df.withColumn(tup[0], func.lit(tup[1]))
    l1_joined_df = l1_joined_df.withColumn("created_at", func.lit(datetime.now()))

    # remove unnecessary columns
    to_remove = ['qty', 'num_orders', 'revenue', 'qty_total', 'num_orders_total', 'revenue_total']
    for col in to_remove:
        l1_joined_df = l1_joined_df.drop(col)

    return l1_joined_df

def pull_run_id_info(run_id, spark):
    metadata_df = spark.sql("""select * from  stg.blu_metadata_dates where run_id = """ + str(run_id))
    start_date = metadata_df.select('start_date').collect()[0][0]
    end_date = metadata_df.select('end_date').collect()[0][0]
    return str(start_date), str(end_date)




def etl_process(spark, run_id, entities):

    start_date, end_date = pull_run_id_info(run_id = run_id, spark=spark)


    l1_raw_df = pull_blu_raw_data(start=start_date, end=end_date, spark=spark, entities=entities)
    l1_totals = l1_raw_df.groupBy("entity_id").agg(func.sum("qty").alias('qty_total'),
                                                func.max("venue_id").alias('venue_id'),
                                                func.max("bar_id").alias('bar_id'),
                                                func.sum("num_orders").alias('num_orders_total'),
                                                func.sum("revenue").alias('revenue_total'))

    l1_categories = l1_raw_df.groupBy(["entity_id", "category"]).agg(func.sum("qty").alias('qty'),
                                                                  func.sum("num_orders").alias('num_orders'),
                                                                  func.sum("revenue").alias('revenue'))

    l1_joined_df = l1_categories.join(l1_totals, l1_categories.entity_id == l1_totals.entity_id).drop(l1_categories.entity_id)
    df_with_qty_share = l1_joined_df.withColumn("qty_share", l1_joined_df['qty'] / l1_joined_df['qty_total'])
    df_with_revenue_share = df_with_qty_share.withColumn("revenue_share", df_with_qty_share['revenue'] /
                                                         df_with_qty_share['revenue_total'])
    df_with_penetration_share = df_with_revenue_share.withColumn("penetration_share",
                                                                 df_with_revenue_share['num_orders'] /
                                                                 df_with_revenue_share['num_orders_total'])


    l1_joined_df = df_with_penetration_share

    l1_joined_df = process_data_to_write(run_id=run_id, l1_joined_df=l1_joined_df)
    l1_joined_df = l1_joined_df.withColumnRenamed("category", "category_id")

    # write results into sql
    l1_joined_df.repartition(1).registerTempTable("l1_share_df")
    spark.sql("""INSERT OVERWRITE TABLE stg.blu_entity_shares SELECT 
                 entity_id,
                 venue_id,
                 bar_id,
                 category_id,
                 qty_share,
                 revenue_share,
                 penetration_share,
                 created_at,
                 run_id
                 FROM l1_share_df""")

    print ("%s - BLU l1 etl process completed" % datetime.now())

