from datetime import datetime
from . import blu_individual_classification as individual_l1
from . import blu_l1_etl_bar_shares as bar_shares_l1, blu_merge_classification as merge_l1
import sys


def run_process(entities, run_id, spark, JobConfig):

    print("%s - BLU -  individual classification - job  started - arguments length: %d" % (datetime.now(), len(sys.argv)))

    mini = JobConfig['l1']['min_bars']

    print("%s - BLU_l1  bar_shares  started" % (datetime.now()))
    bar_shares_l1.etl_process(spark=spark, run_id=run_id, entities=entities)
    print("%s - BLU_l1  bar_shares  ended" % (datetime.now()))

    print("%s - BLU_l1  individual classification  started" % (datetime.now()))
    individual_l1.process(run_id=run_id, spark=spark, JobConfig=JobConfig)
    print("%s - BLU_l1  individual classification   ended" % (datetime.now()))

    print("%s - BLU_l1  merge classification  started" % (datetime.now()))
    merge_l1.process(run_id=run_id, spark=spark, minn=mini)
    print("%s - BLU_ll  merge classification  ended" % (datetime.now()))

    print("%s - BLU -  individual classification - job ended" % datetime.now())
